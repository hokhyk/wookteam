webpackJsonp([6],{

/***/ 295:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(747)
  __webpack_require__(749)
}
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(753)
/* template */
var __vue_template__ = __webpack_require__(754)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-368018bb"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/main/pages/plans.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-368018bb", Component.options)
  } else {
    hotAPI.reload("data-v-368018bb", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 325:
/***/ (function(module, exports) {

module.exports = "/images/statics/images/logo-white.png?069beff1";

/***/ }),

/***/ 747:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(748);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(1)("c79c2438", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-368018bb\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./plans.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-368018bb\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./plans.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 748:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)(false);
// imports


// module
exports.push([module.i, "\n.contact-modal p[data-v-368018bb] {\n  padding: 0;\n  margin: 0;\n  font-size: 16px;\n  text-align: center;\n}\n.contact-modal p img[data-v-368018bb] {\n    display: inline-block;\n    width: 248px;\n}\n", ""]);

// exports


/***/ }),

/***/ 749:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(750);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(1)("bb948d7a", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-368018bb\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=1!./plans.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-368018bb\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=1!./plans.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 750:
/***/ (function(module, exports, __webpack_require__) {

var escape = __webpack_require__(286);
exports = module.exports = __webpack_require__(0)(false);
// imports


// module
exports.push([module.i, "\n@charset \"UTF-8\";\n.page-plans .top-bg[data-v-368018bb] {\n  position: absolute;\n  top: 0;\n  left: 0;\n  width: 100%;\n  height: 640px;\n  padding-top: 192px;\n  z-index: 0;\n  background: url(" + escape(__webpack_require__(751)) + ") center top no-repeat;\n  background-size: 100% 100%;\n}\n.page-plans .top-menu[data-v-368018bb] {\n  position: absolute;\n  top: 0;\n  left: 0;\n  right: 0;\n  z-index: 2;\n}\n.page-plans .top-menu .header[data-v-368018bb] {\n    height: 50px;\n    padding-top: 12px;\n    max-width: 1280px;\n    margin: 0 auto;\n}\n.page-plans .top-menu .header .z-row[data-v-368018bb] {\n      color: #fff;\n      height: 50px;\n      position: relative;\n      z-index: 2;\n      max-width: 1680px;\n      margin: 0 auto;\n}\n.page-plans .top-menu .header .z-row .header-col-sub[data-v-368018bb] {\n        width: 500px;\n}\n.page-plans .top-menu .header .z-row .header-col-sub h2[data-v-368018bb] {\n          position: relative;\n          padding: 1rem 0 0 1rem;\n          display: -webkit-box;\n          display: -ms-flexbox;\n          display: flex;\n          -webkit-box-align: end;\n              -ms-flex-align: end;\n                  align-items: flex-end;\n          cursor: pointer;\n}\n.page-plans .top-menu .header .z-row .header-col-sub h2 img[data-v-368018bb] {\n            width: 150px;\n            margin-right: 6px;\n}\n.page-plans .top-menu .header .z-row .header-col-sub h2 span[data-v-368018bb] {\n            font-size: 12px;\n            font-weight: normal;\n            color: #ffffff;\n            line-height: 14px;\n}\n.page-plans .top-menu .header .z-row .z-1 dl[data-v-368018bb] {\n        position: absolute;\n        right: 20px;\n        top: 0;\n        font-size: 14px;\n}\n.page-plans .top-menu .header .z-row .z-1 dl dd[data-v-368018bb] {\n          line-height: 50px;\n          color: #fff;\n          cursor: pointer;\n          margin-right: 1px;\n}\n.page-plans .top-menu .header .z-row .z-1 dl dd .right-info[data-v-368018bb] {\n            display: inline-block;\n            cursor: pointer;\n            margin-left: 12px;\n            color: #ffffff;\n}\n.page-plans .top-menu .header .z-row .z-1 dl dd .right-info .right-icon[data-v-368018bb] {\n              font-size: 26px;\n              vertical-align: middle;\n}\n.page-plans .banner[data-v-368018bb] {\n  position: relative;\n  z-index: 1;\n  padding-top: 192px;\n}\n.page-plans .banner .banner-title[data-v-368018bb] {\n    font-size: 50px;\n    text-align: center;\n    padding: 0 10px;\n    color: #fff;\n}\n.page-plans .banner .banner-desc[data-v-368018bb] {\n    font-size: 14px;\n    color: #fff;\n    text-align: center;\n    padding: 0 25px;\n    max-width: 940px;\n    margin-left: auto;\n    margin-right: auto;\n    margin-top: 40px;\n    line-height: 30px;\n}\n.page-plans .banner .plans-table[data-v-368018bb] {\n    max-width: 1120px;\n    margin: 110px auto 100px;\n    -webkit-box-shadow: 0 10px 30px rgba(172, 184, 207, 0.3);\n            box-shadow: 0 10px 30px rgba(172, 184, 207, 0.3);\n}\n.page-plans .banner .plans-table em[data-v-368018bb] {\n      font-style: normal;\n      font-size: 14px;\n      color: #666666;\n}\n.page-plans .banner .plans-table .plans-table-bd[data-v-368018bb] {\n      background-color: #fff;\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n}\n.page-plans .banner .plans-table .plans-table-bd .plans-table-item[data-v-368018bb] {\n        -webkit-box-flex: 1;\n            -ms-flex: 1;\n                flex: 1;\n        border-left: 1px solid #eee;\n        position: relative;\n        z-index: 1;\n}\n.page-plans .banner .plans-table .plans-table-bd .plans-table-item > div[data-v-368018bb] {\n          -webkit-transition: background 0.3s;\n          transition: background 0.3s;\n          border-bottom: 1px solid #eee;\n}\n.page-plans .banner .plans-table .plans-table-bd .plans-table-item > div[data-v-368018bb]:first-child, .page-plans .banner .plans-table .plans-table-bd .plans-table-item > div[data-v-368018bb]:last-child {\n            border-bottom: none;\n}\n.page-plans .banner .plans-table .plans-table-bd .plans-table-item[data-v-368018bb]:first-child {\n          -webkit-box-flex: 0;\n              -ms-flex: none;\n                  flex: none;\n          width: 27.7%;\n          border-left: none;\n}\n.page-plans .banner .plans-table .plans-table-bd .plans-table-item[data-v-368018bb]::before {\n          content: \"\";\n          position: absolute;\n          width: 100%;\n          height: 100%;\n          left: 0;\n          top: 0;\n          background: transparent;\n          border-radius: 0;\n          z-index: -2;\n          -webkit-transform: scaleY(1);\n                  transform: scaleY(1);\n          -webkit-transition: all 0.3s;\n          transition: all 0.3s;\n}\n.page-plans .banner .plans-table .plans-table-bd .plans-table-item.active[data-v-368018bb] {\n          position: relative;\n          border-left-color: transparent;\n}\n.page-plans .banner .plans-table .plans-table-bd .plans-table-item.active > div[data-v-368018bb] {\n            border-color: transparent !important;\n            background: transparent;\n}\n.page-plans .banner .plans-table .plans-table-bd .plans-table-item.active[data-v-368018bb]::before {\n            z-index: -1;\n            border-radius: 2px;\n            background: #fff;\n            -webkit-transform: scaleY(1.05);\n                    transform: scaleY(1.05);\n            -webkit-box-shadow: 0 10px 30px rgba(172, 184, 207, 0.3);\n                    box-shadow: 0 10px 30px rgba(172, 184, 207, 0.3);\n}\n.page-plans .banner .plans-table .plans-table-bd .plans-table-item.active + .plans-table-item[data-v-368018bb] {\n            border-left-color: transparent;\n}\n.page-plans .banner .plans-table .plans-table-app .plans-table-td[data-v-368018bb] {\n      height: 60px;\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-pack: center;\n          -ms-flex-pack: center;\n              justify-content: center;\n      -webkit-box-align: center;\n          -ms-flex-align: center;\n              align-items: center;\n}\n.page-plans .banner .plans-table .plans-table-app .plans-table-td[data-v-368018bb]:first-child {\n        border-bottom: 1px solid #eee !important;\n}\n.page-plans .banner .plans-table .plans-table-app .plans-table-td > span[data-v-368018bb] {\n        font-family: -apple-system, Arial, sans-serif;\n}\n.page-plans .banner .plans-table .plans-table-app .plans-table-item .plans-table-td[data-v-368018bb] {\n      position: relative;\n}\n.page-plans .banner .plans-table .plans-table-app .plans-table-item .plans-table-td i[data-v-368018bb] {\n        color: #22d7bb;\n        font-size: 20px;\n}\n.page-plans .banner .plans-table .plans-table-app .plans-table-item .plans-table-td > .info[data-v-368018bb] {\n        position: absolute;\n        font-size: 12px;\n        color: #888;\n        top: 50%;\n        left: 50%;\n        -webkit-transform: translate(30%, -50%);\n                transform: translate(30%, -50%);\n}\n.page-plans .banner .plans-table .plans-table-app .plans-table-item .plans-table-info-btn[data-v-368018bb] {\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-orient: vertical;\n      -webkit-box-direction: normal;\n          -ms-flex-direction: column;\n              flex-direction: column;\n      -webkit-box-pack: center;\n          -ms-flex-pack: center;\n              justify-content: center;\n      -webkit-box-align: center;\n          -ms-flex-align: center;\n              align-items: center;\n      height: 100px;\n}\n.page-plans .banner .plans-table .plans-table-app .plans-table-item.first .plans-table-td[data-v-368018bb] {\n      font-size: 14px;\n      color: #666;\n}\n.page-plans .banner .plans-table .plans-table-app .plans-table-item.first .plans-table-td i[data-v-368018bb] {\n        width: 34px;\n        font-size: 20px;\n        text-align: center;\n        -webkit-transform: translateX(-5px);\n                transform: translateX(-5px);\n}\n.page-plans .banner .plans-table .plans-table-app .plans-table-item.first .plans-table-td:nth-child(1) i[data-v-368018bb] {\n        color: #ff7747;\n}\n.page-plans .banner .plans-table .plans-table-app .plans-table-item.first .plans-table-td:nth-child(2) i[data-v-368018bb] {\n        color: #f669a7;\n}\n.page-plans .banner .plans-table .plans-table-app .plans-table-item.first .plans-table-td:nth-child(3) i[data-v-368018bb] {\n        color: #ffa415;\n}\n.page-plans .banner .plans-table .plans-table-app .plans-table-item.first .plans-table-td:nth-child(4) i[data-v-368018bb] {\n        color: #2dbcff;\n}\n.page-plans .banner .plans-table .plans-table-app .plans-table-item.first .plans-table-td:nth-child(5) i[data-v-368018bb] {\n        color: #66c060;\n}\n.page-plans .banner .plans-table .plans-table-app .plans-table-item.first .plans-table-td:nth-child(6) i[data-v-368018bb] {\n        color: #99d75a;\n}\n.page-plans .banner .plans-table .plans-table-app .plans-table-item.first .plans-table-td:nth-child(7) i[data-v-368018bb] {\n        color: #4e8af9;\n}\n.page-plans .banner .plans-table .plans-table-app .plans-table-item.first .plans-table-td:nth-child(8) i[data-v-368018bb] {\n        color: #ff5b57;\n}\n.page-plans .banner .plans-table .plans-table-app .plans-table-item.first .plans-table-td.plans-table-app-okr[data-v-368018bb] {\n        position: relative;\n}\n.page-plans .banner .plans-table .plans-table-app .plans-table-item.first .plans-table-td.plans-table-app-okr[data-v-368018bb]::after {\n          content: \"(OKR)\";\n          position: absolute;\n          top: 50%;\n          left: 50%;\n          -webkit-transform: translate(90%, -50%);\n                  transform: translate(90%, -50%);\n}\n.page-plans .banner .plans-table .plans-table-info-flex[data-v-368018bb] {\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-orient: vertical;\n      -webkit-box-direction: normal;\n          -ms-flex-direction: column;\n              flex-direction: column;\n      -webkit-box-pack: center;\n          -ms-flex-pack: center;\n              justify-content: center;\n      -webkit-box-align: center;\n          -ms-flex-align: center;\n              align-items: center;\n}\n.page-plans .banner .plans-table .plans-table-info .plans-table-info-th[data-v-368018bb] {\n      height: 70px;\n      background-color: #eef2f8;\n      font-size: 16px;\n      color: #485778;\n      line-height: 70px;\n      text-align: center;\n      font-weight: 600;\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-pack: center;\n          -ms-flex-pack: center;\n              justify-content: center;\n      -webkit-box-align: center;\n          -ms-flex-align: center;\n              align-items: center;\n}\n.page-plans .banner .plans-table .plans-table-info .plans-table-info-th span[data-v-368018bb] {\n        height: 18px;\n        line-height: 18px;\n        font-size: 14px;\n        padding: 0 8px;\n        background-color: #fa3d3f;\n        border-radius: 2px;\n        color: #fff;\n        font-weight: normal;\n        margin-left: 7px;\n}\n.page-plans .banner .plans-table .plans-table-info .plans-table-info-price[data-v-368018bb] {\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-orient: vertical;\n      -webkit-box-direction: normal;\n          -ms-flex-direction: column;\n              flex-direction: column;\n      -webkit-box-pack: center;\n          -ms-flex-pack: center;\n              justify-content: center;\n      -webkit-box-align: center;\n          -ms-flex-align: center;\n              align-items: center;\n      height: 265px;\n}\n.page-plans .banner .plans-table .plans-table-info .plans-table-info-price .plans-version[data-v-368018bb] {\n        margin-bottom: 30px;\n}\n.page-plans .banner .plans-table .plans-table-info .plans-table-info-price .currency[data-v-368018bb] {\n        height: 35px;\n        position: relative;\n        margin-bottom: 18px;\n}\n.page-plans .banner .plans-table .plans-table-info .plans-table-info-price .currency[data-v-368018bb]::before {\n          content: \"\\FFE5\";\n          color: #485778;\n          position: absolute;\n          font-size: 18px;\n          left: 0;\n          top: 0;\n          -webkit-transform: translate(-110%, -20%);\n                  transform: translate(-110%, -20%);\n}\n.page-plans .banner .plans-table .plans-table-info .plans-table-info-price .currency > em[data-v-368018bb] {\n          font-size: 36px;\n          font-weight: 900;\n          display: inline-block;\n          margin-top: -10px;\n          height: 56px;\n          line-height: 56px;\n}\n.page-plans .banner .plans-table .plans-table-info .plans-table-info-price .currency > em.custom[data-v-368018bb] {\n            font-size: 24px;\n            font-weight: 500;\n}\n.page-plans .banner .plans-table .plans-table-info .plans-table-info-desc[data-v-368018bb] {\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-orient: vertical;\n      -webkit-box-direction: normal;\n          -ms-flex-direction: column;\n              flex-direction: column;\n      -webkit-box-pack: center;\n          -ms-flex-pack: center;\n              justify-content: center;\n      -webkit-box-align: center;\n          -ms-flex-align: center;\n              align-items: center;\n      height: 70px;\n      font-size: 14px;\n      color: #aaaaaa;\n}\n.page-plans .banner .plans-table .plans-table-info-btn[data-v-368018bb] {\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-orient: vertical;\n      -webkit-box-direction: normal;\n          -ms-flex-direction: column;\n              flex-direction: column;\n      -webkit-box-pack: center;\n          -ms-flex-pack: center;\n              justify-content: center;\n      -webkit-box-align: center;\n          -ms-flex-align: center;\n              align-items: center;\n      height: 115px;\n}\n.page-plans .banner .plans-table .plans-table-info-btn .plans-info-btns[data-v-368018bb] {\n        display: -webkit-box;\n        display: -ms-flexbox;\n        display: flex;\n        -webkit-box-orient: horizontal;\n        -webkit-box-direction: normal;\n            -ms-flex-direction: row;\n                flex-direction: row;\n        -webkit-box-align: center;\n            -ms-flex-align: center;\n                align-items: center;\n}\n.page-plans .banner .plans-table .plans-table-info-btn .plans-info-btns .btn[data-v-368018bb] {\n          padding: 14px 36px;\n}\n.page-plans .banner .plans-table .plans-table-info-btn .plans-info-btns .github[data-v-368018bb] {\n          margin-left: 10px;\n}\n.page-plans .banner .plans-table .plans-table-info-btn .plans-info-btns .github > i[data-v-368018bb] {\n            font-size: 32px;\n}\n.page-plans .banner .plans-table .plans-table-info-btn .btn[data-v-368018bb] {\n        display: inline-block;\n        color: #fff;\n        background-color: #348FE4;\n        border-color: #348FE4;\n        padding: 14px 54px;\n        font-size: 14px;\n        line-height: 14px;\n        border-radius: 30px;\n        outline: none;\n}\n.page-plans .banner .plans-table .plans-table-info-btn .btn.btn-contact[data-v-368018bb] {\n          background-color: #6BC853;\n          border-color: #6BC853;\n}\n.page-plans .banner .plans-table .plans-accordion-head[data-v-368018bb] {\n      height: 60px;\n      line-height: 60px;\n      background-color: #eef2f8;\n      position: relative;\n      z-index: 2;\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      cursor: pointer;\n}\n.page-plans .banner .plans-table .plans-accordion-head > div[data-v-368018bb] {\n        width: 27.7%;\n        -webkit-box-flex: 1;\n            -ms-flex: 1;\n                flex: 1;\n}\n.page-plans .banner .plans-table .plans-accordion-head > div.first[data-v-368018bb] {\n          width: 27.7%;\n          -webkit-box-flex: 0;\n              -ms-flex: none;\n                  flex: none;\n}\n.page-plans .banner .plans-table .plans-accordion-head > div.first > span[data-v-368018bb] {\n            font-weight: 600;\n            color: #333333;\n            font-size: 14px;\n            padding-left: 30px;\n}\n.page-plans .banner .plans-table .plans-accordion-head > span[data-v-368018bb] {\n        position: absolute;\n        top: 0;\n        right: 30px;\n        line-height: 60px;\n        height: 60px;\n        -webkit-transition: -webkit-transform 0.3s;\n        transition: -webkit-transform 0.3s;\n        transition: transform 0.3s;\n        transition: transform 0.3s, -webkit-transform 0.3s;\n}\n.page-plans .banner .plans-table .plans-accordion-head > span i[data-v-368018bb] {\n          font-size: 20px;\n          color: #aaa;\n}\n.page-plans .banner .plans-table .plans-accordion-head.plans-accordion-close > span[data-v-368018bb] {\n        -webkit-transform: rotate(90deg);\n                transform: rotate(90deg);\n}\n.page-plans .container-fluid[data-v-368018bb] {\n  margin-left: auto;\n  margin-right: auto;\n}\n.page-plans .container-fluid .fluid-info.fluid-info-1[data-v-368018bb] {\n    border-bottom: 1px solid #dddddd;\n}\n.page-plans .container-fluid .fluid-info.fluid-info-3[data-v-368018bb] {\n    background: url(" + escape(__webpack_require__(752)) + ");\n    background-size: 100% 100%;\n}\n.page-plans .container-fluid .fluid-info .fluid-info-item[data-v-368018bb] {\n    max-width: 1120px;\n    margin: 0 auto;\n    height: 780px;\n    padding: 130px 0;\n}\n.page-plans .container-fluid .fluid-info .fluid-info-item .info-title[data-v-368018bb] {\n      text-align: center;\n      font-size: 42px;\n      color: #333333;\n      margin-bottom: 110px;\n}\n.page-plans .container-fluid .fluid-info .fluid-info-item .info-function .func-item[data-v-368018bb] {\n      float: left;\n      width: 33%;\n      text-align: center;\n      padding: 0 40px;\n}\n.page-plans .container-fluid .fluid-info .fluid-info-item .info-function .func-item .image[data-v-368018bb] {\n        height: 215px;\n        margin: 0 auto 40px;\n}\n.page-plans .container-fluid .fluid-info .fluid-info-item .info-function .func-item .image img[data-v-368018bb] {\n          width: 63%;\n}\n.page-plans .container-fluid .fluid-info .fluid-info-item .info-function .func-item .image.image-80 img[data-v-368018bb] {\n          width: 78%;\n}\n.page-plans .container-fluid .fluid-info .fluid-info-item .info-function .func-item .image.image-50 img[data-v-368018bb] {\n          width: 50%;\n}\n.page-plans .container-fluid .fluid-info .fluid-info-item .info-function .func-item .func-desc .desc-title[data-v-368018bb] {\n        font-size: 16px;\n        color: #333333;\n        margin-bottom: 27px;\n        font-weight: 600;\n}\n.page-plans .container-fluid .fluid-info .fluid-info-item .info-function .func-item .func-desc .desc-text[data-v-368018bb] {\n        color: #888888;\n        line-height: 24px;\n}\n.page-plans .contact-footer[data-v-368018bb] {\n  margin: 20px 0;\n  text-align: center;\n  color: #333;\n}\n.page-plans .contact-footer a[data-v-368018bb], .page-plans .contact-footer span[data-v-368018bb] {\n    color: #333;\n    margin-left: 10px;\n}\n", ""]);

// exports


/***/ }),

/***/ 751:
/***/ (function(module, exports) {

module.exports = "/images/statics/images/plans/banner-bg.png?ab992bf9";

/***/ }),

/***/ 752:
/***/ (function(module, exports) {

module.exports = "/images/statics/images/plans/bg_04.jpg?5ca879e8";

/***/ }),

/***/ 753:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    data: function data() {
        return {
            active: 2,

            body1: true,
            body2: true,

            contactShow: false,
            systemConfig: $A.jsonParse($A.storage("systemSetting"))
        };
    },
    created: function created() {
        this.addLanguageData({
            "en": {
                "选择合适你的 WookTeam": "You choose the right WookTeam",
                "选择适合您的团队、企业的版本": "Choose your team, the enterprise version",
                "WookTeam 是新一代企业协作平台，您可以根据您企业的业务需求，选择合适的产品功能。": "WookTeam is a new generation of enterprise collaboration platform, you can according to your business needs, choose the right product features.",
                "从现在开始，WookTeam 为世界各地的团队提供支持，探索适合您的选项。": "From now on, WookTeam to support teams around the world, to explore the option for you.",
                "价格": "Price",
                "概述": "Outline",
                "人数": "Number of people",
                "社区版": "Community Edition",
                "团队版": "Team Edition",
                "适用于轻团队的任务协作": "Suitable for light task team collaboration",
                "无限制": "Unlimited",
                "账号：%、密码：%": "Account password:%",
                "体验DEMO": "DEMO",
                "企业版": "Enterprise Edition",
                "推荐": "Recommend",
                "适用于群组共享和高级权限": "Suitable for group sharing and advanced permissions",
                "定制版": "Custom Edition",
                "自定义": "Customize",
                "根据您的需求量身定制": "Tailored to your needs",
                "联系我们": "Contact us",
                "应用支持": "Application Support",
                "文档/知识库": "Document / Knowledge",
                "团队管理": "Team Management",
                "聊天": "To chat with",
                "国际化": "Globalization",
                "任务动态": "Dynamic task",
                "日程": "Agenda",
                "IM群聊": "IM Group Chat",
                "项目群聊": "Project group chat",
                "项目权限": "Project Permissions",
                "项目搜索": "Project Search",
                "任务类型": "Task Type",
                "知识库搜索": "Knowledge Base Search",
                "团队分组": "Team group",
                "分组权限": "Rights group",
                "成员统计": "Member Statistics",
                "签到功能": "Check-ins",
                "服务支持": "Service support",
                "自助支持": "Self-Support",
                "（Issues/文档/社群）": "(Issues / Document / Community)",
                "支持私有化部署": "Support the deployment of privatization",
                "绑定自有域名": "Binding own domain name",
                "二次开发": "Secondary development",
                "在线咨询支持": "Online consulting support",
                "电话咨询支持": "Telephone support",
                "中英文邮件支持": "Mail support in English",
                "一对一客户顾问": "One on one customer service.",
                "产品培训": "Product Training",
                "上门支持": "On-site support",
                "专属客户成功经理": "Dedicated customer success manager",
                "免费提供一次企业内训": "Corporate Training offers a free",
                "明星客户案例": "Star Customer Case",
                "多种部署方式随心选择": "A variety of deployment options to chose freely",
                "公有云": "Public cloud",
                "无需本地环境准备，按需购买帐户，专业团队提供运维保障服务，两周一次的版本迭代": "No local environment preparation, purchase account demand, professional team to provide operation and maintenance support services, bi-weekly version of the iteration",
                "私有云": "Private Cloud",
                "企业隔离的云服务器环境，高可用性，网络及应用层完整隔离，数据高度自主可控": "Enterprise cloud isolated server environments, high availability, network isolation and complete application layer, data is highly self-control",
                "本地服务器": "Local Server",
                "基于 Docker 的容器化部署，支持高可用集群，快速弹性扩展，数据高度自主可控": "Docker container-based deployment, support for high-availability clustering, rapid elasticity expanded data is highly self-control",
                "完善的服务支持体系": "Perfect service support system",
                "1:1客户成功顾问": "1: 1 Customer Success Consultant",
                "资深客户成功顾问对企业进行调研、沟通需求、制定个性化的解决方案，帮助企业落地": "Senior adviser to the success of enterprise customers to conduct research, communicate needs, develop customized solutions that help companies landing",
                "完善的培训体系": "A comprehensive training system",
                "根据需求定制培训内容，为不同角色给出专属培训方案，线上线下培训渠道全覆盖": "According to customized training needs, given exclusive training programs for different roles, training full coverage online and offline channels",
                "全面的支持服务": "Comprehensive support services",
                "多种支持服务让企业无后顾之忧，7*24 线上支持、在线工单、中英文邮件支持、上门支持": "A variety of support services allow enterprises worry-free, 7 * 24 online support, online ticket, in English and e-mail support, on-site support",
                "多重安全策略保护数据": "Multiple security policies to protect data",
                "高可用性保证": "High availability guarantee",
                "多重方式保证数据不丢失，高可用故障转移，异地容灾备份，99.99%可用性保证": "Multiple ways to ensure that data is not lost, high availability failover, offsite disaster recovery, 99.99% availability guarantee",
                "数据加密": "Data encryption",
                "多重方式保证数据不泄漏，基于 TLS 的数据加密传输，DDOS 防御和入侵检测": "Multiple manner to ensure data does not leak, based on TLS encrypted transmission data, intrusion detection and prevention DDOS",
                "帐户安全": "Account Security",
                "多重方式保证帐户安全，远程会话控制，设备绑定，安全日志以及手势密码": "Multiple ways to ensure account security, remote control session, binding equipment, security logs and gesture password",
                "如有任何问题，欢迎使用微信与我们联系。": "If you have any questions, please contact us using the micro-channel."
            }
        });
    },
    mounted: function mounted() {
        this.getSetting();
    },


    methods: {
        getSetting: function getSetting() {
            var _this = this;

            $A.apiAjax({
                url: 'system/setting',
                error: function error() {
                    $A.storage("systemSetting", {});
                },
                success: function success(res) {
                    if (res.ret === 1) {
                        _this.systemConfig = res.data;
                        $A.storage("systemSetting", _this.systemConfig);
                    } else {
                        $A.storage("systemSetting", {});
                    }
                }
            });
        }
    }
});

/***/ }),

/***/ 754:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "page-plans" },
    [
      _c("v-title", [_vm._v(_vm._s(_vm.$L("选择合适你的 WookTeam")))]),
      _vm._v(" "),
      _c("div", { staticClass: "top-bg" }),
      _vm._v(" "),
      _c("div", { staticClass: "top-menu" }, [
        _c("div", { staticClass: "header" }, [
          _c("div", { staticClass: "z-row" }, [
            _c("div", { staticClass: "header-col-sub" }, [
              _c(
                "h2",
                {
                  on: {
                    click: function($event) {
                      return _vm.goForward({ path: "/" })
                    }
                  }
                },
                [
                  _vm.systemConfig.logo
                    ? _c("img", { attrs: { src: _vm.systemConfig.logo } })
                    : _c("img", {
                        attrs: {
                          src: __webpack_require__(325)
                        }
                      }),
                  _vm._v(" "),
                  _c("span", [_vm._v(_vm._s(_vm.$L("轻量级的团队在线协作")))])
                ]
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "z-1" }, [
              _c("dl", [
                _c(
                  "dd",
                  [
                    _vm.systemConfig.github == "show"
                      ? _c(
                          "a",
                          {
                            staticClass: "right-info",
                            attrs: {
                              target: "_blank",
                              href: "https://github.com/kuaifan/wookteam"
                            }
                          },
                          [
                            _c("Icon", {
                              staticClass: "right-icon",
                              attrs: { type: "logo-github" }
                            })
                          ],
                          1
                        )
                      : _vm._e(),
                    _vm._v(" "),
                    _c(
                      "Dropdown",
                      {
                        staticClass: "right-info",
                        attrs: { trigger: "hover", transfer: "" },
                        on: { "on-click": _vm.setLanguage }
                      },
                      [
                        _c(
                          "div",
                          [
                            _c("Icon", {
                              staticClass: "right-icon",
                              attrs: { type: "md-globe" }
                            }),
                            _vm._v(" "),
                            _c("Icon", { attrs: { type: "md-arrow-dropdown" } })
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "Dropdown-menu",
                          { attrs: { slot: "list" }, slot: "list" },
                          [
                            _c(
                              "Dropdown-item",
                              {
                                attrs: {
                                  name: "zh",
                                  selected: _vm.getLanguage() === "zh"
                                }
                              },
                              [_vm._v("中文")]
                            ),
                            _vm._v(" "),
                            _c(
                              "Dropdown-item",
                              {
                                attrs: {
                                  name: "en",
                                  selected: _vm.getLanguage() === "en"
                                }
                              },
                              [_vm._v("English")]
                            )
                          ],
                          1
                        )
                      ],
                      1
                    )
                  ],
                  1
                )
              ])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "banner" }, [
        _c("div", { staticClass: "banner-title" }, [
          _vm._v(
            "\n            " +
              _vm._s(_vm.$L("选择适合您的团队、企业的版本")) +
              "\n        "
          )
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "banner-desc" }, [
          _vm._v(
            "\n            " +
              _vm._s(
                _vm.$L(
                  "WookTeam 是新一代企业协作平台，您可以根据您企业的业务需求，选择合适的产品功能。"
                )
              ) +
              " "
          ),
          _c("br"),
          _vm._v(
            "\n            " +
              _vm._s(
                _vm.$L(
                  "从现在开始，WookTeam 为世界各地的团队提供支持，探索适合您的选项。"
                )
              ) +
              "\n        "
          )
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "plans-table" }, [
          _c("div", { staticClass: "plans-table-bd plans-table-info" }, [
            _c("div", { staticClass: "plans-table-item first" }, [
              _c("div", { staticClass: "plans-table-info-th" }),
              _vm._v(" "),
              _c("div", { staticClass: "plans-table-info-price" }, [
                _c("em", [_vm._v(_vm._s(_vm.$L("价格")))])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "plans-table-info-desc" }, [
                _c("em", [_vm._v(_vm._s(_vm.$L("概述")))])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "plans-table-info-desc" }, [
                _c("em", [_vm._v(_vm._s(_vm.$L("人数")))])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "plans-table-info-btn" })
            ]),
            _vm._v(" "),
            _c(
              "div",
              {
                staticClass: "plans-table-item",
                class: { active: _vm.active == 1 },
                on: {
                  mouseenter: function($event) {
                    _vm.active = 1
                  }
                }
              },
              [
                _c("div", { staticClass: "plans-table-info-th" }, [
                  _vm._v(_vm._s(_vm.$L("团队版")))
                ]),
                _vm._v(" "),
                _vm._m(0),
                _vm._v(" "),
                _c("div", { staticClass: "plans-table-info-desc" }, [
                  _vm._v(_vm._s(_vm.$L("适用于轻团队的任务协作")))
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "plans-table-info-desc" }, [
                  _vm._v(_vm._s(_vm.$L("无限制")))
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "plans-table-info-btn" }, [
                  _c(
                    "div",
                    { staticClass: "plans-info-btns" },
                    [
                      _c(
                        "Tooltip",
                        {
                          attrs: {
                            content: _vm.$L(
                              "账号：%、密码：%",
                              "admin",
                              "123456"
                            ),
                            transfer: ""
                          }
                        },
                        [
                          _c(
                            "a",
                            {
                              staticClass: "btn",
                              attrs: {
                                href: "https://demo.wookteam.com",
                                target: "_blank"
                              }
                            },
                            [_vm._v(_vm._s(_vm.$L("体验DEMO")))]
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "a",
                        {
                          staticClass: "github",
                          attrs: {
                            href: "https://github.com/kuaifan/wookteam",
                            target: "_blank"
                          }
                        },
                        [_c("Icon", { attrs: { type: "logo-github" } })],
                        1
                      )
                    ],
                    1
                  )
                ])
              ]
            ),
            _vm._v(" "),
            _c(
              "div",
              {
                staticClass: "plans-table-item",
                class: { active: _vm.active == 2 },
                on: {
                  mouseenter: function($event) {
                    _vm.active = 2
                  }
                }
              },
              [
                _c("div", { staticClass: "plans-table-info-th" }, [
                  _vm._v(_vm._s(_vm.$L("企业版")) + " "),
                  _c("span", [_vm._v(_vm._s(_vm.$L("推荐")))])
                ]),
                _vm._v(" "),
                _vm._m(1),
                _vm._v(" "),
                _c("div", { staticClass: "plans-table-info-desc" }, [
                  _vm._v(_vm._s(_vm.$L("适用于群组共享和高级权限")))
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "plans-table-info-desc" }, [
                  _vm._v(_vm._s(_vm.$L("无限制")))
                ]),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "plans-table-info-btn" },
                  [
                    _c(
                      "Tooltip",
                      {
                        attrs: {
                          content: _vm.$L(
                            "账号：%、密码：%",
                            "admin",
                            "123456"
                          ),
                          transfer: ""
                        }
                      },
                      [
                        _c(
                          "a",
                          {
                            staticClass: "btn",
                            attrs: {
                              href: "https://pro.wookteam.com",
                              target: "_blank"
                            }
                          },
                          [_vm._v(_vm._s(_vm.$L("体验DEMO")))]
                        )
                      ]
                    )
                  ],
                  1
                )
              ]
            ),
            _vm._v(" "),
            _c(
              "div",
              {
                staticClass: "plans-table-item",
                class: { active: _vm.active == 3 },
                on: {
                  mouseenter: function($event) {
                    _vm.active = 3
                  }
                }
              },
              [
                _c("div", { staticClass: "plans-table-info-th" }, [
                  _vm._v(_vm._s(_vm.$L("定制版")))
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "plans-table-info-price" }, [
                  _c("img", {
                    staticClass: "plans-version",
                    attrs: {
                      src: __webpack_require__(755)
                    }
                  }),
                  _vm._v(" "),
                  _c("div", { staticClass: "currency" }, [
                    _c("em", { staticClass: "custom" }, [
                      _vm._v(_vm._s(_vm.$L("自定义")))
                    ])
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "plans-table-info-desc" }, [
                  _vm._v(_vm._s(_vm.$L("根据您的需求量身定制")))
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "plans-table-info-desc" }, [
                  _vm._v(_vm._s(_vm.$L("无限制")))
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "plans-table-info-btn" }, [
                  _c(
                    "a",
                    {
                      staticClass: "btn btn-contact",
                      attrs: { href: "javascript:void(0)" },
                      on: {
                        click: function($event) {
                          _vm.contactShow = true
                        }
                      }
                    },
                    [_vm._v(_vm._s(_vm.$L("联系我们")))]
                  )
                ])
              ]
            )
          ]),
          _vm._v(" "),
          _c(
            "div",
            {
              staticClass: "plans-accordion-head",
              class: { "plans-accordion-close": !_vm.body1 },
              on: {
                click: function($event) {
                  _vm.body1 = !_vm.body1
                }
              }
            },
            [
              _c("div", { staticClass: "first" }, [
                _c("span", [_vm._v(_vm._s(_vm.$L("应用支持")))])
              ]),
              _vm._v(" "),
              _c("div", {
                staticClass: "plans-table-item",
                class: { active: _vm.active == 1 },
                on: {
                  mouseenter: function($event) {
                    _vm.active = 1
                  }
                }
              }),
              _vm._v(" "),
              _c("div", {
                staticClass: "plans-table-item",
                class: { active: _vm.active == 2 },
                on: {
                  mouseenter: function($event) {
                    _vm.active = 2
                  }
                }
              }),
              _vm._v(" "),
              _c("div", {
                staticClass: "plans-table-item",
                class: { active: _vm.active == 3 },
                on: {
                  mouseenter: function($event) {
                    _vm.active = 3
                  }
                }
              }),
              _vm._v(" "),
              _c("span", [_c("Icon", { attrs: { type: "ios-arrow-down" } })], 1)
            ]
          ),
          _vm._v(" "),
          _vm.body1
            ? _c("div", { staticClass: "plans-accordion-body" }, [
                _c("div", { staticClass: "plans-table-bd plans-table-app" }, [
                  _c("div", { staticClass: "plans-table-item first" }, [
                    _c("div", { staticClass: "plans-table-td" }, [
                      _vm._v(_vm._s(_vm.$L("项目管理")))
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "plans-table-td" }, [
                      _vm._v(_vm._s(_vm.$L("文档/知识库")))
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "plans-table-td" }, [
                      _vm._v(_vm._s(_vm.$L("团队管理")))
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "plans-table-td" }, [
                      _vm._v("IM" + _vm._s(_vm.$L("聊天")))
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "plans-table-td" }, [
                      _vm._v(_vm._s(_vm.$L("子任务")))
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "plans-table-td" }, [
                      _vm._v(_vm._s(_vm.$L("国际化")))
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "plans-table-td" }, [
                      _vm._v(_vm._s(_vm.$L("甘特图")))
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "plans-table-td" }, [
                      _vm._v(_vm._s(_vm.$L("任务动态")))
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "plans-table-td" }, [
                      _vm._v(_vm._s(_vm.$L("导出任务")))
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "plans-table-td" }, [
                      _vm._v(_vm._s(_vm.$L("日程")))
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "plans-table-td" }, [
                      _vm._v(_vm._s(_vm.$L("周报/日报")))
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "plans-table-td" }, [
                      _vm._v(_vm._s(_vm.$L("IM群聊")))
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "plans-table-td" }, [
                      _vm._v(_vm._s(_vm.$L("项目群聊")))
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "plans-table-td" }, [
                      _vm._v(_vm._s(_vm.$L("项目权限")))
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "plans-table-td" }, [
                      _vm._v(_vm._s(_vm.$L("项目搜索")))
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "plans-table-td" }, [
                      _vm._v(_vm._s(_vm.$L("任务类型")))
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "plans-table-td" }, [
                      _vm._v(_vm._s(_vm.$L("知识库搜索")))
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "plans-table-td" }, [
                      _vm._v(_vm._s(_vm.$L("团队分组")))
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "plans-table-td" }, [
                      _vm._v(_vm._s(_vm.$L("分组权限")))
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "plans-table-td" }, [
                      _vm._v(_vm._s(_vm.$L("成员统计")))
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "plans-table-td" }, [
                      _vm._v(_vm._s(_vm.$L("签到功能")))
                    ])
                  ]),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      staticClass: "plans-table-item",
                      class: { active: _vm.active == 1 },
                      on: {
                        mouseenter: function($event) {
                          _vm.active = 1
                        }
                      }
                    },
                    [
                      _c(
                        "div",
                        { staticClass: "plans-table-td" },
                        [_c("Icon", { attrs: { type: "md-checkmark" } })],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "plans-table-td" },
                        [_c("Icon", { attrs: { type: "md-checkmark" } })],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "plans-table-td" },
                        [_c("Icon", { attrs: { type: "md-checkmark" } })],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "plans-table-td" },
                        [_c("Icon", { attrs: { type: "md-checkmark" } })],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "plans-table-td" },
                        [_c("Icon", { attrs: { type: "md-checkmark" } })],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "plans-table-td" },
                        [_c("Icon", { attrs: { type: "md-checkmark" } })],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "plans-table-td" },
                        [_c("Icon", { attrs: { type: "md-checkmark" } })],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "plans-table-td" },
                        [_c("Icon", { attrs: { type: "md-checkmark" } })],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "plans-table-td" },
                        [_c("Icon", { attrs: { type: "md-checkmark" } })],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "plans-table-td" },
                        [_c("Icon", { attrs: { type: "md-checkmark" } })],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "plans-table-td" },
                        [_c("Icon", { attrs: { type: "md-checkmark" } })],
                        1
                      ),
                      _vm._v(" "),
                      _c("div", { staticClass: "plans-table-td" }, [
                        _vm._v(" - ")
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "plans-table-td" }, [
                        _vm._v(" - ")
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "plans-table-td" }, [
                        _vm._v(" - ")
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "plans-table-td" }, [
                        _vm._v(" - ")
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "plans-table-td" }, [
                        _vm._v(" - ")
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "plans-table-td" }, [
                        _vm._v(" - ")
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "plans-table-td" }, [
                        _vm._v(" - ")
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "plans-table-td" }, [
                        _vm._v(" - ")
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "plans-table-td" }, [
                        _vm._v(" - ")
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "plans-table-td" }, [
                        _vm._v(" - ")
                      ])
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      staticClass: "plans-table-item",
                      class: { active: _vm.active == 2 },
                      on: {
                        mouseenter: function($event) {
                          _vm.active = 2
                        }
                      }
                    },
                    [
                      _c(
                        "div",
                        { staticClass: "plans-table-td" },
                        [_c("Icon", { attrs: { type: "md-checkmark" } })],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "plans-table-td" },
                        [_c("Icon", { attrs: { type: "md-checkmark" } })],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "plans-table-td" },
                        [_c("Icon", { attrs: { type: "md-checkmark" } })],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "plans-table-td" },
                        [_c("Icon", { attrs: { type: "md-checkmark" } })],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "plans-table-td" },
                        [_c("Icon", { attrs: { type: "md-checkmark" } })],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "plans-table-td" },
                        [_c("Icon", { attrs: { type: "md-checkmark" } })],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "plans-table-td" },
                        [_c("Icon", { attrs: { type: "md-checkmark" } })],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "plans-table-td" },
                        [_c("Icon", { attrs: { type: "md-checkmark" } })],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "plans-table-td" },
                        [_c("Icon", { attrs: { type: "md-checkmark" } })],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "plans-table-td" },
                        [_c("Icon", { attrs: { type: "md-checkmark" } })],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "plans-table-td" },
                        [_c("Icon", { attrs: { type: "md-checkmark" } })],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "plans-table-td" },
                        [_c("Icon", { attrs: { type: "md-checkmark" } })],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "plans-table-td" },
                        [_c("Icon", { attrs: { type: "md-checkmark" } })],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "plans-table-td" },
                        [_c("Icon", { attrs: { type: "md-checkmark" } })],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "plans-table-td" },
                        [_c("Icon", { attrs: { type: "md-checkmark" } })],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "plans-table-td" },
                        [_c("Icon", { attrs: { type: "md-checkmark" } })],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "plans-table-td" },
                        [_c("Icon", { attrs: { type: "md-checkmark" } })],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "plans-table-td" },
                        [_c("Icon", { attrs: { type: "md-checkmark" } })],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "plans-table-td" },
                        [_c("Icon", { attrs: { type: "md-checkmark" } })],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "plans-table-td" },
                        [_c("Icon", { attrs: { type: "md-checkmark" } })],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "plans-table-td" },
                        [_c("Icon", { attrs: { type: "md-checkmark" } })],
                        1
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      staticClass: "plans-table-item",
                      class: { active: _vm.active == 3 },
                      on: {
                        mouseenter: function($event) {
                          _vm.active = 3
                        }
                      }
                    },
                    [
                      _c(
                        "div",
                        { staticClass: "plans-table-td" },
                        [_c("Icon", { attrs: { type: "md-checkmark" } })],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "plans-table-td" },
                        [_c("Icon", { attrs: { type: "md-checkmark" } })],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "plans-table-td" },
                        [_c("Icon", { attrs: { type: "md-checkmark" } })],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "plans-table-td" },
                        [_c("Icon", { attrs: { type: "md-checkmark" } })],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "plans-table-td" },
                        [_c("Icon", { attrs: { type: "md-checkmark" } })],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "plans-table-td" },
                        [_c("Icon", { attrs: { type: "md-checkmark" } })],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "plans-table-td" },
                        [_c("Icon", { attrs: { type: "md-checkmark" } })],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "plans-table-td" },
                        [_c("Icon", { attrs: { type: "md-checkmark" } })],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "plans-table-td" },
                        [_c("Icon", { attrs: { type: "md-checkmark" } })],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "plans-table-td" },
                        [_c("Icon", { attrs: { type: "md-checkmark" } })],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "plans-table-td" },
                        [_c("Icon", { attrs: { type: "md-checkmark" } })],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "plans-table-td" },
                        [_c("Icon", { attrs: { type: "md-checkmark" } })],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "plans-table-td" },
                        [_c("Icon", { attrs: { type: "md-checkmark" } })],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "plans-table-td" },
                        [_c("Icon", { attrs: { type: "md-checkmark" } })],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "plans-table-td" },
                        [_c("Icon", { attrs: { type: "md-checkmark" } })],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "plans-table-td" },
                        [_c("Icon", { attrs: { type: "md-checkmark" } })],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "plans-table-td" },
                        [_c("Icon", { attrs: { type: "md-checkmark" } })],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "plans-table-td" },
                        [_c("Icon", { attrs: { type: "md-checkmark" } })],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "plans-table-td" },
                        [_c("Icon", { attrs: { type: "md-checkmark" } })],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "plans-table-td" },
                        [_c("Icon", { attrs: { type: "md-checkmark" } })],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "plans-table-td" },
                        [_c("Icon", { attrs: { type: "md-checkmark" } })],
                        1
                      )
                    ]
                  )
                ])
              ])
            : _vm._e(),
          _vm._v(" "),
          _c(
            "div",
            {
              staticClass: "plans-accordion-head",
              class: { "plans-accordion-close": !_vm.body2 },
              on: {
                click: function($event) {
                  _vm.body2 = !_vm.body2
                }
              }
            },
            [
              _c("div", { staticClass: "first" }, [
                _c("span", [_vm._v(_vm._s(_vm.$L("服务支持")))])
              ]),
              _vm._v(" "),
              _c("div", {
                staticClass: "plans-table-item",
                class: { active: _vm.active == 1 },
                on: {
                  mouseenter: function($event) {
                    _vm.active = 1
                  }
                }
              }),
              _vm._v(" "),
              _c("div", {
                staticClass: "plans-table-item",
                class: { active: _vm.active == 2 },
                on: {
                  mouseenter: function($event) {
                    _vm.active = 2
                  }
                }
              }),
              _vm._v(" "),
              _c("div", {
                staticClass: "plans-table-item",
                class: { active: _vm.active == 3 },
                on: {
                  mouseenter: function($event) {
                    _vm.active = 3
                  }
                }
              }),
              _vm._v(" "),
              _c("span", [_c("Icon", { attrs: { type: "ios-arrow-down" } })], 1)
            ]
          ),
          _vm._v(" "),
          _vm.body2
            ? _c("div", { staticClass: "plans-accordion-body" }, [
                _c(
                  "div",
                  {
                    staticClass:
                      "plans-table-bd plans-table-app plans-table-service"
                  },
                  [
                    _c("div", { staticClass: "plans-table-item first" }, [
                      _c("div", { staticClass: "plans-table-td" }, [
                        _vm._v(_vm._s(_vm.$L("自助支持")) + " "),
                        _c("span", [
                          _vm._v(_vm._s(_vm.$L("（Issues/文档/社群）")))
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "plans-table-td" }, [
                        _vm._v(_vm._s(_vm.$L("支持私有化部署")))
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "plans-table-td" }, [
                        _vm._v(_vm._s(_vm.$L("绑定自有域名")))
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "plans-table-td" }, [
                        _vm._v(_vm._s(_vm.$L("二次开发")))
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "plans-table-td" }, [
                        _vm._v(_vm._s(_vm.$L("在线咨询支持")))
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "plans-table-td" }, [
                        _vm._v(_vm._s(_vm.$L("电话咨询支持")))
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "plans-table-td" }, [
                        _vm._v(_vm._s(_vm.$L("中英文邮件支持")))
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "plans-table-td" }, [
                        _vm._v(_vm._s(_vm.$L("一对一客户顾问")))
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "plans-table-td" }, [
                        _vm._v(_vm._s(_vm.$L("产品培训")))
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "plans-table-td" }, [
                        _vm._v(_vm._s(_vm.$L("上门支持")))
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "plans-table-td" }, [
                        _vm._v(_vm._s(_vm.$L("专属客户成功经理")))
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "plans-table-td" }, [
                        _vm._v(_vm._s(_vm.$L("免费提供一次企业内训")))
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "plans-table-td" }, [
                        _vm._v(_vm._s(_vm.$L("明星客户案例")))
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "plans-table-info-btn" })
                    ]),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "plans-table-item",
                        class: { active: _vm.active == 1 },
                        on: {
                          mouseenter: function($event) {
                            _vm.active = 1
                          }
                        }
                      },
                      [
                        _c(
                          "div",
                          { staticClass: "plans-table-td" },
                          [_c("Icon", { attrs: { type: "md-checkmark" } })],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "plans-table-td" },
                          [_c("Icon", { attrs: { type: "md-checkmark" } })],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "plans-table-td" },
                          [_c("Icon", { attrs: { type: "md-checkmark" } })],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "plans-table-td" },
                          [_c("Icon", { attrs: { type: "md-checkmark" } })],
                          1
                        ),
                        _vm._v(" "),
                        _vm._m(2),
                        _vm._v(" "),
                        _vm._m(3),
                        _vm._v(" "),
                        _vm._m(4),
                        _vm._v(" "),
                        _vm._m(5),
                        _vm._v(" "),
                        _vm._m(6),
                        _vm._v(" "),
                        _vm._m(7),
                        _vm._v(" "),
                        _vm._m(8),
                        _vm._v(" "),
                        _vm._m(9),
                        _vm._v(" "),
                        _vm._m(10),
                        _vm._v(" "),
                        _c("div", { staticClass: "plans-table-info-btn" }, [
                          _c(
                            "div",
                            { staticClass: "plans-info-btns" },
                            [
                              _c(
                                "Tooltip",
                                {
                                  attrs: {
                                    content: _vm.$L(
                                      "账号：%、密码：%",
                                      "admin",
                                      "123456"
                                    ),
                                    transfer: ""
                                  }
                                },
                                [
                                  _c(
                                    "a",
                                    {
                                      staticClass: "btn",
                                      attrs: {
                                        href: "https://demo.wookteam.com",
                                        target: "_blank"
                                      }
                                    },
                                    [_vm._v(_vm._s(_vm.$L("体验DEMO")))]
                                  )
                                ]
                              ),
                              _vm._v(" "),
                              _c(
                                "a",
                                {
                                  staticClass: "github",
                                  attrs: {
                                    href: "https://github.com/kuaifan/wookteam",
                                    target: "_blank"
                                  }
                                },
                                [
                                  _c("Icon", { attrs: { type: "logo-github" } })
                                ],
                                1
                              )
                            ],
                            1
                          )
                        ])
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "plans-table-item",
                        class: { active: _vm.active == 2 },
                        on: {
                          mouseenter: function($event) {
                            _vm.active = 2
                          }
                        }
                      },
                      [
                        _c(
                          "div",
                          { staticClass: "plans-table-td" },
                          [_c("Icon", { attrs: { type: "md-checkmark" } })],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "plans-table-td" },
                          [_c("Icon", { attrs: { type: "md-checkmark" } })],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "plans-table-td" },
                          [_c("Icon", { attrs: { type: "md-checkmark" } })],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "plans-table-td" },
                          [_c("Icon", { attrs: { type: "md-checkmark" } })],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "plans-table-td" },
                          [_c("Icon", { attrs: { type: "md-checkmark" } })],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "plans-table-td" },
                          [_c("Icon", { attrs: { type: "md-checkmark" } })],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "plans-table-td" },
                          [_c("Icon", { attrs: { type: "md-checkmark" } })],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "plans-table-td" },
                          [_c("Icon", { attrs: { type: "md-checkmark" } })],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "plans-table-td" },
                          [_c("Icon", { attrs: { type: "md-checkmark" } })],
                          1
                        ),
                        _vm._v(" "),
                        _vm._m(11),
                        _vm._v(" "),
                        _vm._m(12),
                        _vm._v(" "),
                        _vm._m(13),
                        _vm._v(" "),
                        _vm._m(14),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "plans-table-info-btn" },
                          [
                            _c(
                              "Tooltip",
                              {
                                attrs: {
                                  content: _vm.$L(
                                    "账号：%、密码：%",
                                    "admin",
                                    "123456"
                                  ),
                                  transfer: ""
                                }
                              },
                              [
                                _c(
                                  "a",
                                  {
                                    staticClass: "btn",
                                    attrs: {
                                      href: "https://pro.wookteam.com",
                                      target: "_blank"
                                    }
                                  },
                                  [_vm._v(_vm._s(_vm.$L("体验DEMO")))]
                                )
                              ]
                            )
                          ],
                          1
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "plans-table-item",
                        class: { active: _vm.active == 3 },
                        on: {
                          mouseenter: function($event) {
                            _vm.active = 3
                          }
                        }
                      },
                      [
                        _c(
                          "div",
                          { staticClass: "plans-table-td" },
                          [_c("Icon", { attrs: { type: "md-checkmark" } })],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "plans-table-td" },
                          [_c("Icon", { attrs: { type: "md-checkmark" } })],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "plans-table-td" },
                          [_c("Icon", { attrs: { type: "md-checkmark" } })],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "plans-table-td" },
                          [_c("Icon", { attrs: { type: "md-checkmark" } })],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "plans-table-td" },
                          [_c("Icon", { attrs: { type: "md-checkmark" } })],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "plans-table-td" },
                          [_c("Icon", { attrs: { type: "md-checkmark" } })],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "plans-table-td" },
                          [_c("Icon", { attrs: { type: "md-checkmark" } })],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "plans-table-td" },
                          [_c("Icon", { attrs: { type: "md-checkmark" } })],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "plans-table-td" },
                          [_c("Icon", { attrs: { type: "md-checkmark" } })],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "plans-table-td" },
                          [_c("Icon", { attrs: { type: "md-checkmark" } })],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "plans-table-td" },
                          [_c("Icon", { attrs: { type: "md-checkmark" } })],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "plans-table-td" },
                          [_c("Icon", { attrs: { type: "md-checkmark" } })],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "plans-table-td" },
                          [_c("Icon", { attrs: { type: "md-checkmark" } })],
                          1
                        ),
                        _vm._v(" "),
                        _c("div", { staticClass: "plans-table-info-btn" }, [
                          _c(
                            "a",
                            {
                              staticClass: "btn btn-contact",
                              attrs: { href: "javascript:void(0)" },
                              on: {
                                click: function($event) {
                                  _vm.contactShow = true
                                }
                              }
                            },
                            [_vm._v(_vm._s(_vm.$L("联系我们")))]
                          )
                        ])
                      ]
                    )
                  ]
                )
              ])
            : _vm._e()
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "container-fluid" }, [
        _c("div", { staticClass: "fluid-info fluid-info-1" }, [
          _c("div", { staticClass: "fluid-info-item" }, [
            _c("div", { staticClass: "info-title" }, [
              _vm._v(
                "\n                    " +
                  _vm._s(_vm.$L("多种部署方式随心选择")) +
                  "\n                "
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "info-function" }, [
              _c("div", { staticClass: "func-item" }, [
                _vm._m(15),
                _vm._v(" "),
                _c("div", { staticClass: "func-desc" }, [
                  _c("div", { staticClass: "desc-title" }, [
                    _vm._v(
                      "\n                                " +
                        _vm._s(_vm.$L("公有云")) +
                        "\n                            "
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "desc-text" }, [
                    _vm._v(
                      "\n                                " +
                        _vm._s(
                          _vm.$L(
                            "无需本地环境准备，按需购买帐户，专业团队提供运维保障服务，两周一次的版本迭代"
                          )
                        ) +
                        "\n                            "
                    )
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "func-item" }, [
                _vm._m(16),
                _vm._v(" "),
                _c("div", { staticClass: "func-desc" }, [
                  _c("div", { staticClass: "desc-title" }, [
                    _vm._v(
                      "\n                                " +
                        _vm._s(_vm.$L("私有云")) +
                        "\n                            "
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "desc-text" }, [
                    _vm._v(
                      "\n                                " +
                        _vm._s(
                          _vm.$L(
                            "企业隔离的云服务器环境，高可用性，网络及应用层完整隔离，数据高度自主可控"
                          )
                        ) +
                        "\n                            "
                    )
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "func-item" }, [
                _vm._m(17),
                _vm._v(" "),
                _c("div", { staticClass: "func-desc" }, [
                  _c("div", { staticClass: "desc-title" }, [
                    _vm._v(
                      "\n                                " +
                        _vm._s(_vm.$L("本地服务器")) +
                        "\n                            "
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "desc-text" }, [
                    _vm._v(
                      "\n                                " +
                        _vm._s(
                          _vm.$L(
                            "基于 Docker 的容器化部署，支持高可用集群，快速弹性扩展，数据高度自主可控"
                          )
                        ) +
                        "\n                            "
                    )
                  ])
                ])
              ])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "fluid-info" }, [
          _c("div", { staticClass: "fluid-info-item" }, [
            _c("div", { staticClass: "info-title" }, [
              _vm._v(
                "\n                    " +
                  _vm._s(_vm.$L("完善的服务支持体系")) +
                  "\n                "
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "info-function" }, [
              _c("div", { staticClass: "func-item" }, [
                _vm._m(18),
                _vm._v(" "),
                _c("div", { staticClass: "func-desc" }, [
                  _c("div", { staticClass: "desc-title" }, [
                    _vm._v(
                      "\n                                " +
                        _vm._s(_vm.$L("1:1客户成功顾问")) +
                        "\n                            "
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "desc-text" }, [
                    _vm._v(
                      "\n                                " +
                        _vm._s(
                          _vm.$L(
                            "资深客户成功顾问对企业进行调研、沟通需求、制定个性化的解决方案，帮助企业落地"
                          )
                        ) +
                        "\n                            "
                    )
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "func-item" }, [
                _vm._m(19),
                _vm._v(" "),
                _c("div", { staticClass: "func-desc" }, [
                  _c("div", { staticClass: "desc-title" }, [
                    _vm._v(
                      "\n                                " +
                        _vm._s(_vm.$L("完善的培训体系")) +
                        "\n                            "
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "desc-text" }, [
                    _vm._v(
                      "\n                                " +
                        _vm._s(
                          _vm.$L(
                            "根据需求定制培训内容，为不同角色给出专属培训方案，线上线下培训渠道全覆盖"
                          )
                        ) +
                        "\n                            "
                    )
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "func-item" }, [
                _vm._m(20),
                _vm._v(" "),
                _c("div", { staticClass: "func-desc" }, [
                  _c("div", { staticClass: "desc-title" }, [
                    _vm._v(
                      "\n                                " +
                        _vm._s(_vm.$L("全面的支持服务")) +
                        "\n                            "
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "desc-text" }, [
                    _vm._v(
                      "\n                                " +
                        _vm._s(
                          _vm.$L(
                            "多种支持服务让企业无后顾之忧，7*24 线上支持、在线工单、中英文邮件支持、上门支持"
                          )
                        ) +
                        "\n                            "
                    )
                  ])
                ])
              ])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "fluid-info fluid-info-3" }, [
          _c("div", { staticClass: "fluid-info-item" }, [
            _c("div", { staticClass: "info-title" }, [
              _vm._v(
                "\n                    " +
                  _vm._s(_vm.$L("多重安全策略保护数据")) +
                  "\n                "
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "info-function" }, [
              _c("div", { staticClass: "func-item" }, [
                _vm._m(21),
                _vm._v(" "),
                _c("div", { staticClass: "func-desc" }, [
                  _c("div", { staticClass: "desc-title" }, [
                    _vm._v(
                      "\n                                " +
                        _vm._s(_vm.$L("高可用性保证")) +
                        "\n                            "
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "desc-text" }, [
                    _vm._v(
                      "\n                                " +
                        _vm._s(
                          _vm.$L(
                            "多重方式保证数据不丢失，高可用故障转移，异地容灾备份，99.99%可用性保证"
                          )
                        ) +
                        "\n                            "
                    )
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "func-item" }, [
                _vm._m(22),
                _vm._v(" "),
                _c("div", { staticClass: "func-desc" }, [
                  _c("div", { staticClass: "desc-title" }, [
                    _vm._v(
                      "\n                                " +
                        _vm._s(_vm.$L("数据加密")) +
                        "\n                            "
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "desc-text" }, [
                    _vm._v(
                      "\n                                " +
                        _vm._s(
                          _vm.$L(
                            "多重方式保证数据不泄漏，基于 TLS 的数据加密传输，DDOS 防御和入侵检测"
                          )
                        ) +
                        "\n                            "
                    )
                  ])
                ])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "func-item" }, [
                _vm._m(23),
                _vm._v(" "),
                _c("div", { staticClass: "func-desc" }, [
                  _c("div", { staticClass: "desc-title" }, [
                    _vm._v(
                      "\n                                " +
                        _vm._s(_vm.$L("帐户安全")) +
                        "\n                            "
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "desc-text" }, [
                    _vm._v(
                      "\n                                " +
                        _vm._s(
                          _vm.$L(
                            "多重方式保证帐户安全，远程会话控制，设备绑定，安全日志以及手势密码"
                          )
                        ) +
                        "\n                            "
                    )
                  ])
                ])
              ])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _vm._m(24),
      _vm._v(" "),
      _c(
        "Modal",
        {
          attrs: {
            title: _vm.$L("联系我们"),
            "class-name": "simple-modal",
            width: "430",
            "footer-hide": ""
          },
          model: {
            value: _vm.contactShow,
            callback: function($$v) {
              _vm.contactShow = $$v
            },
            expression: "contactShow"
          }
        },
        [
          _c("div", { staticClass: "contact-modal" }, [
            _c("p", [
              _vm._v(_vm._s(_vm.$L("如有任何问题，欢迎使用微信与我们联系。")))
            ]),
            _vm._v(" "),
            _c("p", [
              _c("img", {
                attrs: {
                  src: __webpack_require__(756)
                }
              })
            ])
          ])
        ]
      )
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "plans-table-info-price" }, [
      _c("img", {
        staticClass: "plans-version",
        attrs: { src: __webpack_require__(757) }
      }),
      _vm._v(" "),
      _c("div", { staticClass: "currency" }, [_c("em", [_vm._v("0")])])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "plans-table-info-price" }, [
      _c("img", {
        staticClass: "plans-version",
        attrs: { src: __webpack_require__(758) }
      }),
      _vm._v(" "),
      _c("div", { staticClass: "currency" }, [_c("em", [_vm._v("18800")])])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "plans-table-td" }, [
      _c("span", [_vm._v(" - ")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "plans-table-td" }, [
      _c("span", [_vm._v(" - ")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "plans-table-td" }, [
      _c("span", [_vm._v(" - ")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "plans-table-td" }, [
      _c("span", [_vm._v(" - ")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "plans-table-td" }, [
      _c("span", [_vm._v(" - ")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "plans-table-td" }, [
      _c("span", [_vm._v(" - ")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "plans-table-td" }, [
      _c("span", [_vm._v(" - ")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "plans-table-td" }, [
      _c("span", [_vm._v(" - ")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "plans-table-td" }, [
      _c("span", [_vm._v(" - ")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "plans-table-td" }, [
      _c("span", [_vm._v(" - ")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "plans-table-td" }, [
      _c("span", [_vm._v(" - ")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "plans-table-td" }, [
      _c("span", [_vm._v(" - ")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "plans-table-td" }, [
      _c("span", [_vm._v(" - ")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "image" }, [
      _c("img", {
        attrs: { src: __webpack_require__(759) }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "image" }, [
      _c("img", {
        attrs: { src: __webpack_require__(760) }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "image image-80" }, [
      _c("img", {
        attrs: { src: __webpack_require__(761) }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "image" }, [
      _c("img", {
        attrs: { src: __webpack_require__(762) }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "image image-80" }, [
      _c("img", {
        attrs: { src: __webpack_require__(763) }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "image" }, [
      _c("img", {
        attrs: { src: __webpack_require__(764) }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "image" }, [
      _c("img", {
        attrs: { src: __webpack_require__(765) }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "image image-80" }, [
      _c("img", {
        attrs: { src: __webpack_require__(766) }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "image image-50" }, [
      _c("img", {
        attrs: { src: __webpack_require__(767) }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "contact-footer" }, [
      _c("span", [_vm._v("WookTeam © 2018-2020")])
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-368018bb", module.exports)
  }
}

/***/ }),

/***/ 755:
/***/ (function(module, exports) {

module.exports = "/images/statics/images/plans/Ultimate.png?f9922c09";

/***/ }),

/***/ 756:
/***/ (function(module, exports) {

module.exports = "/images/statics/images/plans/wechat.png?dd2755b5";

/***/ }),

/***/ 757:
/***/ (function(module, exports) {

module.exports = "/images/statics/images/plans/free.png?aff5a93c";

/***/ }),

/***/ 758:
/***/ (function(module, exports) {

module.exports = "/images/statics/images/plans/pro.png?ce8b5b66";

/***/ }),

/***/ 759:
/***/ (function(module, exports) {

module.exports = "/images/statics/images/plans/1.svg?30ec9e3e";

/***/ }),

/***/ 760:
/***/ (function(module, exports) {

module.exports = "/images/statics/images/plans/2.svg?3f165dd6";

/***/ }),

/***/ 761:
/***/ (function(module, exports) {

module.exports = "/images/statics/images/plans/3.svg?d114254d";

/***/ }),

/***/ 762:
/***/ (function(module, exports) {

module.exports = "/images/statics/images/plans/4.svg?a638d9d0";

/***/ }),

/***/ 763:
/***/ (function(module, exports) {

module.exports = "/images/statics/images/plans/5.svg?b3b0a07c";

/***/ }),

/***/ 764:
/***/ (function(module, exports) {

module.exports = "/images/statics/images/plans/6.svg?ff11ad91";

/***/ }),

/***/ 765:
/***/ (function(module, exports) {

module.exports = "/images/statics/images/plans/7.svg?478a4940";

/***/ }),

/***/ 766:
/***/ (function(module, exports) {

module.exports = "/images/statics/images/plans/8.svg?77b1586e";

/***/ }),

/***/ 767:
/***/ (function(module, exports) {

module.exports = "/images/statics/images/plans/9.svg?a3d8acd9";

/***/ })

});