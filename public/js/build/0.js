webpackJsonp([0],{

/***/ 290:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(531)
  __webpack_require__(533)
}
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(535)
/* template */
var __vue_template__ = __webpack_require__(601)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-4bac3242"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/main/pages/project/panel.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-4bac3242", Component.options)
  } else {
    hotAPI.reload("data-v-4bac3242", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 305:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(306)
}
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(308)
/* template */
var __vue_template__ = __webpack_require__(309)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-35be3d57"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/main/components/WContent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-35be3d57", Component.options)
  } else {
    hotAPI.reload("data-v-35be3d57", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 306:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(307);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(1)("5ee96958", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-35be3d57\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./WContent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-35be3d57\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./WContent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 307:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)(false);
// imports


// module
exports.push([module.i, "\n.w-content[data-v-35be3d57] {\n  position: absolute;\n  top: 72px;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  overflow: auto;\n  background-repeat: no-repeat;\n  background-position: center;\n  background-color: #EEEEEE;\n  background-size: cover;\n}\n.w-content .w-container[data-v-35be3d57] {\n    min-height: 500px;\n}\n", ""]);

// exports


/***/ }),

/***/ 308:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: 'WContent',
    data: function data() {
        return {
            bgid: -1
        };
    },
    mounted: function mounted() {
        this.bgid = $A.runNum(this.usrInfo.bgid);
    },

    watch: {
        usrInfo: {
            handler: function handler(info) {
                this.bgid = $A.runNum(info.bgid);
            },

            deep: true
        }
    },
    methods: {
        getBgUrl: function getBgUrl(id, thumb) {
            if (id < 0) {
                return 'none';
            }
            id = Math.max(1, parseInt(id));
            return 'url(' + window.location.origin + '/images/bg/' + (thumb ? 'thumb/' : '') + id + '.jpg' + ')';
        }
    }
});

/***/ }),

/***/ 309:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {
      staticClass: "w-content",
      style: "background-image:" + _vm.getBgUrl(_vm.bgid)
    },
    [_vm._t("default")],
    2
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-35be3d57", module.exports)
  }
}

/***/ }),

/***/ 320:
/***/ (function(module, exports, __webpack_require__) {

var baseGetTag = __webpack_require__(10),
    isObjectLike = __webpack_require__(8);

/** `Object#toString` result references. */
var symbolTag = '[object Symbol]';

/**
 * Checks if `value` is classified as a `Symbol` primitive or object.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a symbol, else `false`.
 * @example
 *
 * _.isSymbol(Symbol.iterator);
 * // => true
 *
 * _.isSymbol('abc');
 * // => false
 */
function isSymbol(value) {
  return typeof value == 'symbol' ||
    (isObjectLike(value) && baseGetTag(value) == symbolTag);
}

module.exports = isSymbol;


/***/ }),

/***/ 321:
/***/ (function(module, exports, __webpack_require__) {

var isSymbol = __webpack_require__(320);

/** Used as references for various `Number` constants. */
var INFINITY = 1 / 0;

/**
 * Converts `value` to a string key if it's not a string or symbol.
 *
 * @private
 * @param {*} value The value to inspect.
 * @returns {string|symbol} Returns the key.
 */
function toKey(value) {
  if (typeof value == 'string' || isSymbol(value)) {
    return value;
  }
  var result = (value + '');
  return (result == '0' && (1 / value) == -INFINITY) ? '-0' : result;
}

module.exports = toKey;


/***/ }),

/***/ 338:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(339)
}
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(341)
/* template */
var __vue_template__ = __webpack_require__(342)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-1b3dd966"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/main/components/project/archived.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-1b3dd966", Component.options)
  } else {
    hotAPI.reload("data-v-1b3dd966", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 339:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(340);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(1)("3653cc78", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../node_modules/css-loader/index.js!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1b3dd966\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/sass-loader/lib/loader.js!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./archived.vue", function() {
     var newContent = require("!!../../../../../../node_modules/css-loader/index.js!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1b3dd966\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/sass-loader/lib/loader.js!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./archived.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 340:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)(false);
// imports


// module
exports.push([module.i, "\n.project-archived .tableFill[data-v-1b3dd966] {\n  margin: 12px 12px 20px;\n}\n", ""]);

// exports


/***/ }),

/***/ 341:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__DrawerTabsContainer__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__DrawerTabsContainer___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__DrawerTabsContainer__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__mixins_task__ = __webpack_require__(30);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/**
 * 项目已归档任务
 */
/* harmony default export */ __webpack_exports__["default"] = ({
    name: 'ProjectArchived',
    components: { DrawerTabsContainer: __WEBPACK_IMPORTED_MODULE_0__DrawerTabsContainer___default.a },
    props: {
        projectid: {
            default: 0
        },
        canload: {
            type: Boolean,
            default: true
        }
    },
    mixins: [__WEBPACK_IMPORTED_MODULE_1__mixins_task__["a" /* default */]],
    data: function data() {
        return {
            loadYet: false,

            loadIng: 0,

            columns: [],

            lists: [],
            listPage: 1,
            listTotal: 0,
            noDataText: ""
        };
    },
    mounted: function mounted() {
        var _this = this;

        if (this.canload) {
            this.loadYet = true;
            this.getLists(true);
        }
        $A.setOnTaskInfoListener('components/project/archived', function (act, detail) {
            if (detail.projectid != _this.projectid) {
                return;
            }
            //
            _this.lists.some(function (task, i) {
                if (task.id == detail.id) {
                    _this.lists.splice(i, 1, detail);
                    return true;
                }
            });
            //
            switch (act) {
                case "delete": // 删除任务
                case "unarchived":
                    // 取消归档
                    _this.lists.some(function (task, i) {
                        if (task.id == detail.id) {
                            _this.lists.splice(i, 1);
                            return true;
                        }
                    });
                    break;

                case "archived":
                    // 归档
                    var has = false;
                    _this.lists.some(function (task) {
                        if (task.id == detail.id) {
                            return has = true;
                        }
                    });
                    if (!has) {
                        _this.lists.unshift(detail);
                    }
                    break;
            }
        });
    },


    watch: {
        projectid: function projectid() {
            if (this.loadYet) {
                this.getLists(true);
            }
        },
        canload: function canload(val) {
            if (val && !this.loadYet) {
                this.loadYet = true;
                this.getLists(true);
            }
        }
    },

    methods: {
        initLanguage: function initLanguage() {
            var _this2 = this;

            this.noDataText = this.$L("数据加载中.....");
            this.columns = [{
                "title": this.$L("任务名称"),
                "key": 'title',
                "minWidth": 120,
                render: function render(h, params) {
                    return _this2.renderTaskTitle(h, params);
                }
            }, {
                "title": this.$L("创建人"),
                "key": 'createuser',
                "minWidth": 80,
                render: function render(h, params) {
                    return h('UserView', {
                        props: {
                            username: params.row.createuser
                        }
                    });
                }
            }, {
                "title": this.$L("负责人"),
                "key": 'username',
                "minWidth": 80,
                render: function render(h, params) {
                    return h('UserView', {
                        props: {
                            username: params.row.username
                        }
                    });
                }
            }, {
                "title": this.$L("完成"),
                "minWidth": 70,
                "align": "center",
                render: function render(h, params) {
                    return h('span', params.row.complete ? '√' : '-');
                }
            }, {
                "title": this.$L("归档时间"),
                "width": 160,
                render: function render(h, params) {
                    return h('span', $A.formatDate("Y-m-d H:i:s", params.row.archiveddate));
                }
            }, {
                "title": this.$L("操作"),
                "key": 'action',
                "width": 100,
                "align": 'center',
                render: function render(h, params) {
                    return h('Button', {
                        props: {
                            type: 'primary',
                            size: 'small'
                        },
                        style: {
                            fontSize: '12px'
                        },
                        on: {
                            click: function click() {
                                _this2.$Modal.confirm({
                                    title: _this2.$L('取消归档'),
                                    content: _this2.$L('你确定要取消归档吗？'),
                                    loading: true,
                                    onOk: function onOk() {
                                        $A.apiAjax({
                                            url: 'project/task/edit',
                                            method: 'post',
                                            data: {
                                                act: 'unarchived',
                                                taskid: params.row.id
                                            },
                                            error: function error() {
                                                _this2.$Modal.remove();
                                                alert(_this2.$L('网络繁忙，请稍后再试！'));
                                            },
                                            success: function success(res) {
                                                _this2.$Modal.remove();
                                                _this2.getLists();
                                                setTimeout(function () {
                                                    if (res.ret === 1) {
                                                        _this2.$Message.success(res.msg);
                                                        $A.triggerTaskInfoListener('unarchived', res.data);
                                                        $A.triggerTaskInfoChange(params.row.id);
                                                    } else {
                                                        _this2.$Modal.error({ title: _this2.$L('温馨提示'), content: res.msg });
                                                    }
                                                }, 350);
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    }, _this2.$L('取消归档'));
                }
            }];
        },
        setPage: function setPage(page) {
            this.listPage = page;
            this.getLists();
        },
        setPageSize: function setPageSize(size) {
            if (Math.max($A.runNum(this.listPageSize), 10) != size) {
                this.listPageSize = size;
                this.getLists();
            }
        },
        getLists: function getLists(resetLoad) {
            var _this3 = this;

            if (resetLoad === true) {
                this.listPage = 1;
            }
            if (this.projectid == 0) {
                this.lists = [];
                this.listTotal = 0;
                this.noDataText = this.$L("没有相关的数据");
                return;
            }
            this.loadIng++;
            this.noDataText = this.$L("数据加载中.....");
            $A.apiAjax({
                url: 'project/task/lists',
                data: {
                    page: Math.max(this.listPage, 1),
                    pagesize: Math.max($A.runNum(this.listPageSize), 10),
                    projectid: this.projectid,
                    archived: '已归档'
                },
                complete: function complete() {
                    _this3.loadIng--;
                },
                error: function error() {
                    _this3.noDataText = _this3.$L("数据加载失败！");
                },
                success: function success(res) {
                    if (res.ret === 1) {
                        _this3.lists = res.data.lists;
                        _this3.listTotal = res.data.total;
                        _this3.noDataText = _this3.$L("没有相关的数据");
                    } else {
                        _this3.lists = [];
                        _this3.listTotal = 0;
                        _this3.noDataText = res.msg;
                    }
                }
            });
        }
    }
});

/***/ }),

/***/ 342:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("drawer-tabs-container", [
    _c(
      "div",
      { staticClass: "project-archived" },
      [
        _c("Table", {
          ref: "tableRef",
          staticClass: "tableFill",
          attrs: {
            columns: _vm.columns,
            data: _vm.lists,
            loading: _vm.loadIng > 0,
            "no-data-text": _vm.noDataText,
            stripe: ""
          }
        }),
        _vm._v(" "),
        _c("Page", {
          staticClass: "pageBox",
          attrs: {
            total: _vm.listTotal,
            current: _vm.listPage,
            disabled: _vm.loadIng > 0,
            "page-size-opts": [10, 20, 30, 50, 100],
            placement: "top",
            "show-elevator": "",
            "show-sizer": "",
            "show-total": "",
            transfer: "",
            simple: _vm.windowMax768
          },
          on: {
            "on-change": _vm.setPage,
            "on-page-size-change": _vm.setPageSize
          }
        })
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-1b3dd966", module.exports)
  }
}

/***/ }),

/***/ 343:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(344)
}
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(346)
/* template */
var __vue_template__ = __webpack_require__(347)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-720a9bad"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/main/components/project/users.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-720a9bad", Component.options)
  } else {
    hotAPI.reload("data-v-720a9bad", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 344:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(345);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(1)("6db19cd1", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../node_modules/css-loader/index.js!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-720a9bad\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/sass-loader/lib/loader.js!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./users.vue", function() {
     var newContent = require("!!../../../../../../node_modules/css-loader/index.js!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-720a9bad\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/sass-loader/lib/loader.js!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./users.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 345:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)(false);
// imports


// module
exports.push([module.i, "\n.project-users[data-v-720a9bad] {\n  padding: 0 12px;\n}\n.project-users .tableFill[data-v-720a9bad] {\n    margin: 12px 0 20px;\n}\n", ""]);

// exports


/***/ }),

/***/ 346:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__DrawerTabsContainer__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__DrawerTabsContainer___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__DrawerTabsContainer__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
    name: 'ProjectUsers',
    components: { DrawerTabsContainer: __WEBPACK_IMPORTED_MODULE_0__DrawerTabsContainer___default.a },
    props: {
        projectid: {
            default: 0
        },
        canload: {
            type: Boolean,
            default: true
        }
    },
    data: function data() {
        return {
            loadYet: false,

            loadIng: 0,

            columns: [],

            lists: [],
            listPage: 1,
            listTotal: 0,
            noDataText: ""
        };
    },
    mounted: function mounted() {
        if (this.canload) {
            this.loadYet = true;
            this.getLists(true);
        }
    },


    watch: {
        projectid: function projectid() {
            if (this.loadYet) {
                this.getLists(true);
            }
        },
        canload: function canload(val) {
            if (val && !this.loadYet) {
                this.loadYet = true;
                this.getLists(true);
            }
        }
    },

    methods: {
        initLanguage: function initLanguage() {
            var _this = this;

            this.noDataText = this.$L("数据加载中.....");
            this.columns = [{
                "title": this.$L("头像"),
                "minWidth": 60,
                "maxWidth": 100,
                render: function render(h, params) {
                    return h('UserImg', {
                        props: {
                            info: params.row
                        },
                        style: {
                            width: "30px",
                            height: "30px",
                            fontSize: "16px",
                            lineHeight: "30px",
                            borderRadius: "15px",
                            verticalAlign: "middle"
                        }
                    });
                }
            }, {
                "title": this.$L("用户名"),
                "key": 'username',
                "minWidth": 80,
                "ellipsis": true
            }, {
                "title": this.$L("昵称"),
                "minWidth": 80,
                "ellipsis": true,
                render: function render(h, params) {
                    return h('span', params.row.nickname || '-');
                }
            }, {
                "title": this.$L("职位/职称"),
                "minWidth": 100,
                "ellipsis": true,
                render: function render(h, params) {
                    return h('span', params.row.profession || '-');
                }
            }, {
                "title": this.$L("成员角色"),
                "minWidth": 100,
                render: function render(h, params) {
                    return h('span', params.row.isowner ? _this.$L('项目负责人') : _this.$L('成员'));
                }
            }, {
                "title": this.$L("加入时间"),
                "width": 160,
                render: function render(h, params) {
                    return h('span', $A.formatDate("Y-m-d H:i:s", params.row.indate));
                }
            }, {
                "title": this.$L("操作"),
                "key": 'action',
                "width": 80,
                "align": 'center',
                render: function render(h, params) {
                    return h('Button', {
                        props: {
                            type: 'primary',
                            size: 'small'
                        },
                        style: {
                            fontSize: '12px'
                        },
                        on: {
                            click: function click() {
                                _this.$Modal.confirm({
                                    title: _this.$L('移出成员'),
                                    content: _this.$L('你确定要将此成员移出项目吗？'),
                                    loading: true,
                                    onOk: function onOk() {
                                        $A.apiAjax({
                                            url: 'project/users/join',
                                            data: {
                                                act: 'delete',
                                                projectid: params.row.projectid,
                                                username: params.row.username
                                            },
                                            error: function error() {
                                                _this.$Modal.remove();
                                                alert(_this.$L('网络繁忙，请稍后再试！'));
                                            },
                                            success: function success(res) {
                                                _this.$Modal.remove();
                                                _this.getLists();
                                                setTimeout(function () {
                                                    if (res.ret === 1) {
                                                        _this.$Message.success(res.msg);
                                                    } else {
                                                        _this.$Modal.error({ title: _this.$L('温馨提示'), content: res.msg });
                                                    }
                                                }, 350);
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    }, _this.$L('删除'));
                }
            }];
        },
        setPage: function setPage(page) {
            this.listPage = page;
            this.getLists();
        },
        setPageSize: function setPageSize(size) {
            if (Math.max($A.runNum(this.listPageSize), 10) != size) {
                this.listPageSize = size;
                this.getLists();
            }
        },
        getLists: function getLists(resetLoad) {
            var _this2 = this;

            if (resetLoad === true) {
                this.listPage = 1;
            }
            if (this.projectid == 0) {
                this.lists = [];
                this.listTotal = 0;
                this.noDataText = this.$L("没有相关的数据");
                return;
            }
            this.loadIng++;
            this.noDataText = this.$L("数据加载中.....");
            $A.apiAjax({
                url: 'project/users/lists',
                data: {
                    page: Math.max(this.listPage, 1),
                    pagesize: Math.max($A.runNum(this.listPageSize), 10),
                    projectid: this.projectid
                },
                complete: function complete() {
                    _this2.loadIng--;
                },
                error: function error() {
                    _this2.noDataText = _this2.$L("数据加载失败！");
                },
                success: function success(res) {
                    if (res.ret === 1) {
                        _this2.lists = res.data.lists;
                        _this2.listTotal = res.data.total;
                        _this2.noDataText = _this2.$L("没有相关的数据");
                    } else {
                        _this2.lists = [];
                        _this2.listTotal = 0;
                        _this2.noDataText = res.msg;
                    }
                }
            });
        },
        addUser: function addUser() {
            var _this3 = this;

            this.userValue = "";
            this.$Modal.confirm({
                render: function render(h) {
                    return h('div', [h('div', {
                        style: {
                            fontSize: '16px',
                            fontWeight: '500',
                            marginBottom: '20px'
                        }
                    }, _this3.$L('添加成员')), h('UserInput', {
                        props: {
                            value: _this3.userValue,
                            multiple: true,
                            noprojectid: _this3.projectid,
                            placeholder: _this3.$L('请输入昵称/用户名搜索')
                        },
                        on: {
                            input: function input(val) {
                                _this3.userValue = val;
                            }
                        }
                    })]);
                },
                loading: true,
                onOk: function onOk() {
                    if (_this3.userValue) {
                        var username = _this3.userValue;
                        $A.apiAjax({
                            url: 'project/users/join',
                            data: {
                                act: 'join',
                                projectid: _this3.projectid,
                                username: username
                            },
                            error: function error() {
                                _this3.$Modal.remove();
                                alert(_this3.$L('网络繁忙，请稍后再试！'));
                            },
                            success: function success(res) {
                                _this3.$Modal.remove();
                                _this3.getLists();
                                setTimeout(function () {
                                    if (res.ret === 1) {
                                        _this3.$Message.success(res.msg);
                                    } else {
                                        _this3.$Modal.error({ title: _this3.$L('温馨提示'), content: res.msg });
                                    }
                                }, 350);
                            }
                        });
                    } else {
                        _this3.$Modal.remove();
                    }
                }
            });
        }
    }
});

/***/ }),

/***/ 347:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("drawer-tabs-container", [
    _c(
      "div",
      { staticClass: "project-users" },
      [
        _c(
          "Button",
          {
            attrs: {
              loading: _vm.loadIng > 0,
              type: "primary",
              icon: "md-add"
            },
            on: { click: _vm.addUser }
          },
          [_vm._v(_vm._s(_vm.$L("添加成员")))]
        ),
        _vm._v(" "),
        _c("Table", {
          ref: "tableRef",
          staticClass: "tableFill",
          attrs: {
            columns: _vm.columns,
            data: _vm.lists,
            loading: _vm.loadIng > 0,
            "no-data-text": _vm.noDataText,
            stripe: ""
          }
        }),
        _vm._v(" "),
        _c("Page", {
          staticClass: "pageBox",
          attrs: {
            total: _vm.listTotal,
            current: _vm.listPage,
            disabled: _vm.loadIng > 0,
            "page-size-opts": [10, 20, 30, 50, 100],
            placement: "top",
            "show-elevator": "",
            "show-sizer": "",
            "show-total": "",
            transfer: "",
            simple: _vm.windowMax768
          },
          on: {
            "on-change": _vm.setPage,
            "on-page-size-change": _vm.setPageSize
          }
        })
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-720a9bad", module.exports)
  }
}

/***/ }),

/***/ 348:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(349)
}
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(351)
/* template */
var __vue_template__ = __webpack_require__(352)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-4466db4e"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/main/components/project/statistics.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-4466db4e", Component.options)
  } else {
    hotAPI.reload("data-v-4466db4e", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 349:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(350);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(1)("ffb4ffa8", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../node_modules/css-loader/index.js!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-4466db4e\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/sass-loader/lib/loader.js!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./statistics.vue", function() {
     var newContent = require("!!../../../../../../node_modules/css-loader/index.js!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-4466db4e\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/sass-loader/lib/loader.js!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./statistics.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 350:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)(false);
// imports


// module
exports.push([module.i, "\n.project-statistics .tableFill[data-v-4466db4e] {\n  margin: 12px 12px 20px;\n}\n.project-statistics ul.state-overview[data-v-4466db4e] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.project-statistics ul.state-overview > li[data-v-4466db4e] {\n    -webkit-box-flex: 1;\n        -ms-flex: 1;\n            flex: 1;\n    cursor: pointer;\n    margin: 0 10px 5px;\n}\n.project-statistics ul.state-overview > li > div[data-v-4466db4e] {\n      position: relative;\n      -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05);\n              box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05);\n      -webkit-transition: all 0.2s;\n      transition: all 0.2s;\n      border-radius: 6px;\n      color: #ffffff;\n      height: 110px;\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-orient: vertical;\n      -webkit-box-direction: normal;\n          -ms-flex-direction: column;\n              flex-direction: column;\n      -webkit-box-pack: center;\n          -ms-flex-pack: center;\n              justify-content: center;\n      -webkit-box-align: center;\n          -ms-flex-align: center;\n              align-items: center;\n}\n.project-statistics ul.state-overview > li > div.terques[data-v-4466db4e] {\n        background: #17BE6B;\n}\n.project-statistics ul.state-overview > li > div.purple[data-v-4466db4e] {\n        background: #A218A5;\n}\n.project-statistics ul.state-overview > li > div.red[data-v-4466db4e] {\n        background: #ED3F14;\n}\n.project-statistics ul.state-overview > li > div.yellow[data-v-4466db4e] {\n        background: #FF9900;\n}\n.project-statistics ul.state-overview > li > div.blue[data-v-4466db4e] {\n        background: #2D8CF0;\n}\n.project-statistics ul.state-overview > li > div[data-v-4466db4e]:hover {\n        -webkit-box-shadow: 2px 2px 8px 0 rgba(0, 0, 0, 0.38);\n                box-shadow: 2px 2px 8px 0 rgba(0, 0, 0, 0.38);\n}\n.project-statistics ul.state-overview > li > div[data-v-4466db4e]:after {\n        position: absolute;\n        content: \"\";\n        left: 50%;\n        bottom: 3px;\n        width: 0;\n        height: 2px;\n        -webkit-transform: translate(-50%, 0);\n                transform: translate(-50%, 0);\n        background-color: #FFFFFF;\n        border-radius: 2px;\n        -webkit-transition: all 0.3s;\n        transition: all 0.3s;\n        opacity: 0;\n}\n.project-statistics ul.state-overview > li > div > h1[data-v-4466db4e] {\n        font-size: 36px;\n        margin: -2px 0 0;\n        padding: 0;\n        font-weight: 500;\n}\n.project-statistics ul.state-overview > li > div > p[data-v-4466db4e] {\n        font-size: 18px;\n        margin: 0;\n        padding: 0;\n}\n.project-statistics ul.state-overview > li.active > div[data-v-4466db4e]:after {\n      width: 90%;\n      opacity: 1;\n}\n", ""]);

// exports


/***/ }),

/***/ 351:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__DrawerTabsContainer__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__DrawerTabsContainer___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__DrawerTabsContainer__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__mixins_task__ = __webpack_require__(30);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/**
 * 项目统计
 */
/* harmony default export */ __webpack_exports__["default"] = ({
    name: 'ProjectStatistics',
    components: { DrawerTabsContainer: __WEBPACK_IMPORTED_MODULE_0__DrawerTabsContainer___default.a },
    props: {
        projectid: {
            default: 0
        },
        canload: {
            type: Boolean,
            default: true
        }
    },
    mixins: [__WEBPACK_IMPORTED_MODULE_1__mixins_task__["a" /* default */]],
    data: function data() {
        return {
            loadYet: false,

            loadIng: 0,

            columns: [],

            taskType: '未完成',

            lists: [],
            listPage: 1,
            listTotal: 0,
            noDataText: "",

            statistics_unfinished: 0,
            statistics_overdue: 0,
            statistics_complete: 0
        };
    },
    mounted: function mounted() {
        var _this = this;

        if (this.canload) {
            this.loadYet = true;
            this.getLists(true);
        }
        $A.setOnTaskInfoListener('components/project/statistics', function (act, detail) {
            if (detail.projectid != _this.projectid) {
                return;
            }
            //
            _this.lists.some(function (task, i) {
                if (task.id == detail.id) {
                    _this.lists.splice(i, 1, detail);
                    return true;
                }
            });
            //
            switch (act) {
                case "delete": // 删除任务
                case "archived":
                    // 归档
                    _this.lists.some(function (task, i) {
                        if (task.id == detail.id) {
                            _this.lists.splice(i, 1);
                            if (task.complete) {
                                _this.statistics_complete--;
                            } else {
                                _this.statistics_unfinished++;
                            }
                            return true;
                        }
                    });
                    break;

                case "unarchived":
                    // 取消归档
                    var has = false;
                    _this.lists.some(function (task) {
                        if (task.id == detail.id) {
                            if (task.complete) {
                                _this.statistics_complete++;
                            } else {
                                _this.statistics_unfinished--;
                            }
                            return has = true;
                        }
                    });
                    if (!has) {
                        _this.lists.unshift(detail);
                    }
                    break;

                case "complete":
                    // 标记完成
                    _this.statistics_complete++;
                    _this.statistics_unfinished--;
                    break;

                case "unfinished":
                    // 标记未完成
                    _this.statistics_complete--;
                    _this.statistics_unfinished++;
                    break;
            }
        });
    },


    watch: {
        projectid: function projectid() {
            if (this.loadYet) {
                this.getLists(true);
            }
        },
        canload: function canload(val) {
            if (val && !this.loadYet) {
                this.loadYet = true;
                this.getLists(true);
            }
        },
        taskType: function taskType() {
            if (this.loadYet) {
                this.getLists(true);
            }
        }
    },

    methods: {
        initLanguage: function initLanguage() {
            var _this2 = this;

            this.noDataText = this.$L("数据加载中.....");
            this.columns = [{
                "title": this.$L("任务名称"),
                "key": 'title',
                "minWidth": 120,
                render: function render(h, params) {
                    return _this2.renderTaskTitle(h, params);
                }
            }, {
                "title": this.$L("创建人"),
                "key": 'createuser',
                "minWidth": 80,
                render: function render(h, params) {
                    return h('UserView', {
                        props: {
                            username: params.row.createuser
                        }
                    });
                }
            }, {
                "title": this.$L("负责人"),
                "key": 'username',
                "minWidth": 80,
                render: function render(h, params) {
                    return h('UserView', {
                        props: {
                            username: params.row.username
                        }
                    });
                }
            }, {
                "title": this.$L("完成"),
                "minWidth": 70,
                "align": "center",
                render: function render(h, params) {
                    return h('span', params.row.complete ? '√' : '-');
                }
            }, {
                "title": this.$L("创建时间"),
                "width": 160,
                render: function render(h, params) {
                    return h('span', $A.formatDate("Y-m-d H:i:s", params.row.indate));
                }
            }];
        },
        setTaskType: function setTaskType(type) {
            this.taskType = type;
        },
        setPage: function setPage(page) {
            this.listPage = page;
            this.getLists();
        },
        setPageSize: function setPageSize(size) {
            if (Math.max($A.runNum(this.listPageSize), 10) != size) {
                this.listPageSize = size;
                this.getLists();
            }
        },
        getLists: function getLists(resetLoad) {
            var _this3 = this;

            if (resetLoad === true) {
                this.listPage = 1;
            }
            if (this.projectid == 0) {
                this.lists = [];
                this.listTotal = 0;
                this.noDataText = this.$L("没有相关的数据");
                return;
            }
            this.loadIng++;
            var tempType = this.taskType;
            this.noDataText = this.$L("数据加载中.....");
            $A.apiAjax({
                url: 'project/task/lists',
                data: {
                    page: Math.max(this.listPage, 1),
                    pagesize: Math.max($A.runNum(this.listPageSize), 10),
                    projectid: this.projectid,
                    type: this.taskType,
                    statistics: 1
                },
                complete: function complete() {
                    _this3.loadIng--;
                },
                error: function error() {
                    _this3.noDataText = _this3.$L("数据加载失败！");
                },
                success: function success(res) {
                    if (tempType != _this3.taskType) {
                        return;
                    }
                    if (res.ret === 1) {
                        _this3.lists = res.data.lists;
                        _this3.listTotal = res.data.total;
                        _this3.noDataText = _this3.$L("没有相关的数据");
                    } else {
                        _this3.lists = [];
                        _this3.listTotal = 0;
                        _this3.noDataText = res.msg;
                    }
                    _this3.statistics_unfinished = res.data.statistics_unfinished || 0;
                    _this3.statistics_overdue = res.data.statistics_overdue || 0;
                    _this3.statistics_complete = res.data.statistics_complete || 0;
                }
            });
        }
    }
});

/***/ }),

/***/ 352:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("drawer-tabs-container", [
    _c(
      "div",
      { staticClass: "project-statistics" },
      [
        _c("ul", { staticClass: "state-overview" }, [
          _c(
            "li",
            {
              class: [_vm.taskType === "未完成" ? "active" : ""],
              on: {
                click: function($event) {
                  _vm.taskType = "未完成"
                }
              }
            },
            [
              _c("div", { staticClass: "yellow" }, [
                _c("h1", { staticClass: "count" }, [
                  _vm._v(_vm._s(_vm.statistics_unfinished))
                ]),
                _vm._v(" "),
                _c("p", [_vm._v(_vm._s(_vm.$L("未完成任务")))])
              ])
            ]
          ),
          _vm._v(" "),
          _c(
            "li",
            {
              class: [_vm.taskType === "已超期" ? "active" : ""],
              on: {
                click: function($event) {
                  _vm.taskType = "已超期"
                }
              }
            },
            [
              _c("div", { staticClass: "red" }, [
                _c("h1", { staticClass: "count" }, [
                  _vm._v(_vm._s(_vm.statistics_overdue))
                ]),
                _vm._v(" "),
                _c("p", [_vm._v(_vm._s(_vm.$L("超期任务")))])
              ])
            ]
          ),
          _vm._v(" "),
          _c(
            "li",
            {
              class: [_vm.taskType === "已完成" ? "active" : ""],
              on: {
                click: function($event) {
                  _vm.taskType = "已完成"
                }
              }
            },
            [
              _c("div", { staticClass: "terques" }, [
                _c("h1", { staticClass: "count" }, [
                  _vm._v(_vm._s(_vm.statistics_complete))
                ]),
                _vm._v(" "),
                _c("p", [_vm._v(_vm._s(_vm.$L("已完成任务")))])
              ])
            ]
          )
        ]),
        _vm._v(" "),
        _c("Table", {
          ref: "tableRef",
          staticClass: "tableFill",
          attrs: {
            columns: _vm.columns,
            data: _vm.lists,
            loading: _vm.loadIng > 0,
            "no-data-text": _vm.noDataText,
            stripe: ""
          }
        }),
        _vm._v(" "),
        _c("Page", {
          staticClass: "pageBox",
          attrs: {
            total: _vm.listTotal,
            current: _vm.listPage,
            disabled: _vm.loadIng > 0,
            "page-size-opts": [10, 20, 30, 50, 100],
            placement: "top",
            "show-elevator": "",
            "show-sizer": "",
            "show-total": "",
            transfer: "",
            simple: _vm.windowMax768
          },
          on: {
            "on-change": _vm.setPage,
            "on-page-size-change": _vm.setPageSize
          }
        })
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-4466db4e", module.exports)
  }
}

/***/ }),

/***/ 353:
/***/ (function(module, exports, __webpack_require__) {

var castPath = __webpack_require__(428),
    toKey = __webpack_require__(321);

/**
 * The base implementation of `_.get` without support for default values.
 *
 * @private
 * @param {Object} object The object to query.
 * @param {Array|string} path The path of the property to get.
 * @returns {*} Returns the resolved value.
 */
function baseGet(object, path) {
  path = castPath(path, object);

  var index = 0,
      length = path.length;

  while (object != null && index < length) {
    object = object[toKey(path[index++])];
  }
  return (index && index == length) ? object : undefined;
}

module.exports = baseGet;


/***/ }),

/***/ 354:
/***/ (function(module, exports, __webpack_require__) {

var isArray = __webpack_require__(16),
    isSymbol = __webpack_require__(320);

/** Used to match property names within property paths. */
var reIsDeepProp = /\.|\[(?:[^[\]]*|(["'])(?:(?!\1)[^\\]|\\.)*?\1)\]/,
    reIsPlainProp = /^\w*$/;

/**
 * Checks if `value` is a property name and not a property path.
 *
 * @private
 * @param {*} value The value to check.
 * @param {Object} [object] The object to query keys on.
 * @returns {boolean} Returns `true` if `value` is a property name, else `false`.
 */
function isKey(value, object) {
  if (isArray(value)) {
    return false;
  }
  var type = typeof value;
  if (type == 'number' || type == 'symbol' || type == 'boolean' ||
      value == null || isSymbol(value)) {
    return true;
  }
  return reIsPlainProp.test(value) || !reIsDeepProp.test(value) ||
    (object != null && value in Object(object));
}

module.exports = isKey;


/***/ }),

/***/ 427:
/***/ (function(module, exports) {

/**
 * A specialized version of `_.map` for arrays without support for iteratee
 * shorthands.
 *
 * @private
 * @param {Array} [array] The array to iterate over.
 * @param {Function} iteratee The function invoked per iteration.
 * @returns {Array} Returns the new mapped array.
 */
function arrayMap(array, iteratee) {
  var index = -1,
      length = array == null ? 0 : array.length,
      result = Array(length);

  while (++index < length) {
    result[index] = iteratee(array[index], index, array);
  }
  return result;
}

module.exports = arrayMap;


/***/ }),

/***/ 428:
/***/ (function(module, exports, __webpack_require__) {

var isArray = __webpack_require__(16),
    isKey = __webpack_require__(354),
    stringToPath = __webpack_require__(565),
    toString = __webpack_require__(568);

/**
 * Casts `value` to a path array if it's not one.
 *
 * @private
 * @param {*} value The value to inspect.
 * @param {Object} [object] The object to query keys on.
 * @returns {Array} Returns the cast property path array.
 */
function castPath(value, object) {
  if (isArray(value)) {
    return value;
  }
  return isKey(value, object) ? [value] : stringToPath(toString(value));
}

module.exports = castPath;


/***/ }),

/***/ 429:
/***/ (function(module, exports, __webpack_require__) {

var baseIsEqualDeep = __webpack_require__(573),
    isObjectLike = __webpack_require__(8);

/**
 * The base implementation of `_.isEqual` which supports partial comparisons
 * and tracks traversed objects.
 *
 * @private
 * @param {*} value The value to compare.
 * @param {*} other The other value to compare.
 * @param {boolean} bitmask The bitmask flags.
 *  1 - Unordered comparison
 *  2 - Partial comparison
 * @param {Function} [customizer] The function to customize comparisons.
 * @param {Object} [stack] Tracks traversed `value` and `other` objects.
 * @returns {boolean} Returns `true` if the values are equivalent, else `false`.
 */
function baseIsEqual(value, other, bitmask, customizer, stack) {
  if (value === other) {
    return true;
  }
  if (value == null || other == null || (!isObjectLike(value) && !isObjectLike(other))) {
    return value !== value && other !== other;
  }
  return baseIsEqualDeep(value, other, bitmask, customizer, baseIsEqual, stack);
}

module.exports = baseIsEqual;


/***/ }),

/***/ 430:
/***/ (function(module, exports, __webpack_require__) {

var SetCache = __webpack_require__(574),
    arraySome = __webpack_require__(577),
    cacheHas = __webpack_require__(578);

/** Used to compose bitmasks for value comparisons. */
var COMPARE_PARTIAL_FLAG = 1,
    COMPARE_UNORDERED_FLAG = 2;

/**
 * A specialized version of `baseIsEqualDeep` for arrays with support for
 * partial deep comparisons.
 *
 * @private
 * @param {Array} array The array to compare.
 * @param {Array} other The other array to compare.
 * @param {number} bitmask The bitmask flags. See `baseIsEqual` for more details.
 * @param {Function} customizer The function to customize comparisons.
 * @param {Function} equalFunc The function to determine equivalents of values.
 * @param {Object} stack Tracks traversed `array` and `other` objects.
 * @returns {boolean} Returns `true` if the arrays are equivalent, else `false`.
 */
function equalArrays(array, other, bitmask, customizer, equalFunc, stack) {
  var isPartial = bitmask & COMPARE_PARTIAL_FLAG,
      arrLength = array.length,
      othLength = other.length;

  if (arrLength != othLength && !(isPartial && othLength > arrLength)) {
    return false;
  }
  // Check that cyclic values are equal.
  var arrStacked = stack.get(array);
  var othStacked = stack.get(other);
  if (arrStacked && othStacked) {
    return arrStacked == other && othStacked == array;
  }
  var index = -1,
      result = true,
      seen = (bitmask & COMPARE_UNORDERED_FLAG) ? new SetCache : undefined;

  stack.set(array, other);
  stack.set(other, array);

  // Ignore non-index properties.
  while (++index < arrLength) {
    var arrValue = array[index],
        othValue = other[index];

    if (customizer) {
      var compared = isPartial
        ? customizer(othValue, arrValue, index, other, array, stack)
        : customizer(arrValue, othValue, index, array, other, stack);
    }
    if (compared !== undefined) {
      if (compared) {
        continue;
      }
      result = false;
      break;
    }
    // Recursively compare arrays (susceptible to call stack limits).
    if (seen) {
      if (!arraySome(other, function(othValue, othIndex) {
            if (!cacheHas(seen, othIndex) &&
                (arrValue === othValue || equalFunc(arrValue, othValue, bitmask, customizer, stack))) {
              return seen.push(othIndex);
            }
          })) {
        result = false;
        break;
      }
    } else if (!(
          arrValue === othValue ||
            equalFunc(arrValue, othValue, bitmask, customizer, stack)
        )) {
      result = false;
      break;
    }
  }
  stack['delete'](array);
  stack['delete'](other);
  return result;
}

module.exports = equalArrays;


/***/ }),

/***/ 431:
/***/ (function(module, exports, __webpack_require__) {

var isObject = __webpack_require__(9);

/**
 * Checks if `value` is suitable for strict equality comparisons, i.e. `===`.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` if suitable for strict
 *  equality comparisons, else `false`.
 */
function isStrictComparable(value) {
  return value === value && !isObject(value);
}

module.exports = isStrictComparable;


/***/ }),

/***/ 432:
/***/ (function(module, exports) {

/**
 * A specialized version of `matchesProperty` for source values suitable
 * for strict equality comparisons, i.e. `===`.
 *
 * @private
 * @param {string} key The key of the property to get.
 * @param {*} srcValue The value to match.
 * @returns {Function} Returns the new spec function.
 */
function matchesStrictComparable(key, srcValue) {
  return function(object) {
    if (object == null) {
      return false;
    }
    return object[key] === srcValue &&
      (srcValue !== undefined || (key in Object(object)));
  };
}

module.exports = matchesStrictComparable;


/***/ }),

/***/ 433:
/***/ (function(module, exports) {

/**
 * This method returns the first argument it receives.
 *
 * @static
 * @since 0.1.0
 * @memberOf _
 * @category Util
 * @param {*} value Any value.
 * @returns {*} Returns `value`.
 * @example
 *
 * var object = { 'a': 1 };
 *
 * console.log(_.identity(object) === object);
 * // => true
 */
function identity(value) {
  return value;
}

module.exports = identity;


/***/ }),

/***/ 531:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(532);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(1)("0f52eb25", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../node_modules/css-loader/index.js!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-4bac3242\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../../../node_modules/sass-loader/lib/loader.js!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./panel.vue", function() {
     var newContent = require("!!../../../../../../node_modules/css-loader/index.js!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-4bac3242\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../../../node_modules/sass-loader/lib/loader.js!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./panel.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 532:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)(false);
// imports


// module
exports.push([module.i, "\n#project-panel-enter-textarea {\n  background: transparent;\n  background: none;\n  outline: none;\n  border: 0;\n  resize: none;\n  padding: 0;\n  margin: 8px 0;\n  line-height: 22px;\n  border-radius: 0;\n  color: rgba(0, 0, 0, 0.85);\n}\n#project-panel-enter-textarea:focus {\n    border-color: transparent;\n    -webkit-box-shadow: none;\n            box-shadow: none;\n}\n", ""]);

// exports


/***/ }),

/***/ 533:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(534);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(1)("109952df", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../node_modules/css-loader/index.js!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-4bac3242\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/sass-loader/lib/loader.js!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=1!./panel.vue", function() {
     var newContent = require("!!../../../../../../node_modules/css-loader/index.js!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-4bac3242\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/sass-loader/lib/loader.js!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=1!./panel.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 534:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)(false);
// imports


// module
exports.push([module.i, "\n.project-panel .label-box[data-v-4bac3242] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: row;\n          flex-direction: row;\n  -webkit-box-align: start;\n      -ms-flex-align: start;\n          align-items: flex-start;\n  -webkit-box-pack: start;\n      -ms-flex-pack: start;\n          justify-content: flex-start;\n  -ms-flex-wrap: nowrap;\n      flex-wrap: nowrap;\n  overflow-x: auto;\n  overflow-y: hidden;\n  -webkit-overflow-scrolling: touch;\n  width: 100%;\n  height: 100%;\n  padding: 15px;\n  -webkit-transform: translateZ(0);\n          transform: translateZ(0);\n}\n.project-panel .label-box .label-item[data-v-4bac3242] {\n    -webkit-box-flex: 0;\n        -ms-flex-positive: 0;\n            flex-grow: 0;\n    -ms-flex-negative: 0;\n        flex-shrink: 0;\n    -ms-flex-preferred-size: auto;\n        flex-basis: auto;\n    position: relative;\n    overflow: hidden;\n    height: 100%;\n    padding-right: 15px;\n}\n.project-panel .label-box .label-item.label-create[data-v-4bac3242] {\n      cursor: pointer;\n}\n.project-panel .label-box .label-item.label-create:hover .trigger-box[data-v-4bac3242] {\n        -webkit-transform: translate(0, -50%) scale(1.1);\n                transform: translate(0, -50%) scale(1.1);\n}\n.project-panel .label-box .label-item.label-scroll:hover .label-bottom[data-v-4bac3242] {\n      -webkit-transform: translate(-50%, 0);\n              transform: translate(-50%, 0);\n}\n.project-panel .label-box .label-item .label-body[data-v-4bac3242] {\n      width: 300px;\n      height: 100%;\n      border-radius: 0.15rem;\n      background-color: #ebecf0;\n      overflow: hidden;\n      position: relative;\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-orient: vertical;\n      -webkit-box-direction: normal;\n          -ms-flex-direction: column;\n              flex-direction: column;\n}\n.project-panel .label-box .label-item .label-body .title-box[data-v-4bac3242] {\n        padding: 0 12px;\n        font-weight: bold;\n        color: #666666;\n        position: relative;\n        cursor: move;\n        display: -webkit-box;\n        display: -ms-flexbox;\n        display: flex;\n        -webkit-box-align: center;\n            -ms-flex-align: center;\n                align-items: center;\n        width: 100%;\n        height: 42px;\n}\n.project-panel .label-box .label-item .label-body .title-box .title-loading[data-v-4bac3242] {\n          width: 16px;\n          height: 16px;\n          margin-right: 6px;\n}\n.project-panel .label-box .label-item .label-body .title-box h2[data-v-4bac3242] {\n          -webkit-box-flex: 1;\n              -ms-flex: 1;\n                  flex: 1;\n          font-size: 16px;\n          overflow: hidden;\n          text-overflow: ellipsis;\n          white-space: nowrap;\n}\n.project-panel .label-box .label-item .label-body .title-box i[data-v-4bac3242] {\n          font-weight: 500;\n          font-size: 18px;\n          height: 100%;\n          line-height: 42px;\n          width: 42px;\n          text-align: right;\n          cursor: pointer;\n}\n.project-panel .label-box .label-item .label-body .task-box[data-v-4bac3242] {\n        position: relative;\n        -webkit-box-flex: 1;\n            -ms-flex: 1;\n                flex: 1;\n        width: 100%;\n        padding: 0 12px 2px;\n        -webkit-transform: translateZ(0);\n                transform: translateZ(0);\n}\n.project-panel .label-box .label-item .label-body .task-box .task-main[data-v-4bac3242] {\n          display: -webkit-box;\n          display: -ms-flexbox;\n          display: flex;\n          -webkit-box-orient: vertical;\n          -webkit-box-direction: normal;\n              -ms-flex-direction: column;\n                  flex-direction: column;\n}\n.project-panel .label-box .label-item .label-body .task-box .task-main.filtr-persons .task-item[data-v-4bac3242] {\n            display: none;\n}\n.project-panel .label-box .label-item .label-body .task-box .task-main.filtr-persons .task-item.persons-item[data-v-4bac3242] {\n              display: block;\n}\n.project-panel .label-box .label-item .label-body .task-box .task-main.filtr-follower .task-item[data-v-4bac3242] {\n            display: none;\n}\n.project-panel .label-box .label-item .label-body .task-box .task-main.filtr-follower .task-item.follower-item[data-v-4bac3242] {\n              display: block;\n}\n.project-panel .label-box .label-item .label-body .task-box .task-main.filtr-create .task-item[data-v-4bac3242] {\n            display: none;\n}\n.project-panel .label-box .label-item .label-body .task-box .task-main.filtr-create .task-item.create-item[data-v-4bac3242] {\n              display: block;\n}\n.project-panel .label-box .label-item .label-body .task-box .task-item[data-v-4bac3242] {\n          width: 100%;\n}\n.project-panel .label-box .label-item .label-body .task-box .task-item.task-draggable .task-shadow[data-v-4bac3242] {\n            cursor: pointer;\n}\n.project-panel .label-box .label-item .label-body .task-box .task-item.task-draggable .task-shadow[data-v-4bac3242]:hover {\n              -webkit-box-shadow: 0 0 4px 0 rgba(0, 0, 0, 0.38);\n                      box-shadow: 0 0 4px 0 rgba(0, 0, 0, 0.38);\n}\n.project-panel .label-box .label-item .label-body .task-box .task-item .task-shadow[data-v-4bac3242] {\n            margin: 5px 0 4px;\n            padding: 8px 10px 8px 8px;\n            background-color: #ffffff;\n            border-left: 2px solid #BF9F03;\n            border-right: 0;\n            color: #091e42;\n            border-radius: 3px;\n            -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05);\n                    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05);\n            -webkit-transition: all 0.3s;\n            transition: all 0.3s;\n            -webkit-transform: scale(1);\n                    transform: scale(1);\n}\n.project-panel .label-box .label-item .label-body .task-box .task-item .task-shadow.p1[data-v-4bac3242] {\n              border-left-color: #ff0000;\n}\n.project-panel .label-box .label-item .label-body .task-box .task-item .task-shadow.p2[data-v-4bac3242] {\n              border-left-color: #BB9F35;\n}\n.project-panel .label-box .label-item .label-body .task-box .task-item .task-shadow.p3[data-v-4bac3242] {\n              border-left-color: #449EDD;\n}\n.project-panel .label-box .label-item .label-body .task-box .task-item .task-shadow.p4[data-v-4bac3242] {\n              border-left-color: #84A83B;\n}\n.project-panel .label-box .label-item .label-body .task-box .task-item .task-shadow.complete[data-v-4bac3242] {\n              border-left-color: #c1c1c1;\n}\n.project-panel .label-box .label-item .label-body .task-box .task-item .task-shadow.complete .task-title[data-v-4bac3242] {\n                color: #666666;\n                text-decoration: line-through;\n}\n.project-panel .label-box .label-item .label-body .task-box .task-item .task-shadow.complete .task-more .task-status[data-v-4bac3242] {\n                color: #666666;\n}\n.project-panel .label-box .label-item .label-body .task-box .task-item .task-shadow.overdue .task-title[data-v-4bac3242] {\n              font-weight: bold;\n}\n.project-panel .label-box .label-item .label-body .task-box .task-item .task-shadow.overdue .task-more .task-status[data-v-4bac3242] {\n              color: #ff0000;\n}\n.project-panel .label-box .label-item .label-body .task-box .task-item .task-shadow.newtask[data-v-4bac3242] {\n              -webkit-transform: scale(1.5);\n                      transform: scale(1.5);\n}\n.project-panel .label-box .label-item .label-body .task-box .task-item .task-shadow .task-title[data-v-4bac3242] {\n              font-size: 12px;\n              color: #091e42;\n              word-break: break-all;\n}\n.project-panel .label-box .label-item .label-body .task-box .task-item .task-shadow .task-title .ivu-icon[data-v-4bac3242] {\n                font-size: 14px;\n                color: #afafaf;\n                vertical-align: top;\n                padding: 2px 4px;\n                -webkit-transform: scale(0.94);\n                        transform: scale(0.94);\n}\n.project-panel .label-box .label-item .label-body .task-box .task-item .task-shadow .task-more[data-v-4bac3242] {\n              min-height: 30px;\n              display: -webkit-box;\n              display: -ms-flexbox;\n              display: flex;\n              -webkit-box-align: end;\n                  -ms-flex-align: end;\n                      align-items: flex-end;\n}\n.project-panel .label-box .label-item .label-body .task-box .task-item .task-shadow .task-more .task-status[data-v-4bac3242] {\n                color: #19be6b;\n                font-size: 12px;\n                -webkit-box-flex: 1;\n                    -ms-flex: 1;\n                        flex: 1;\n}\n.project-panel .label-box .label-item .label-body .task-box .task-item .task-shadow .task-more .task-persons[data-v-4bac3242] {\n                max-width: 150px;\n}\n.project-panel .label-box .label-item .label-body .task-box .task-item .task-shadow .task-more .task-persons.persons-more[data-v-4bac3242] {\n                  text-align: right;\n}\n.project-panel .label-box .label-item .label-body .task-box .task-item .task-shadow .task-more .task-persons.persons-more .task-userimg[data-v-4bac3242] {\n                    width: 20px;\n                    height: 20px;\n                    margin-left: 4px;\n                    margin-top: 4px;\n}\n.project-panel .label-box .label-item .label-body .task-box .task-item .task-shadow .task-more .task-persons.persons-more .task-userimg .avatar[data-v-4bac3242] {\n                      width: 20px;\n                      height: 20px;\n                      font-size: 12px;\n                      line-height: 20px;\n}\n.project-panel .label-box .label-item .label-body .task-box .task-item .task-shadow .task-more .task-persons .task-userimg[data-v-4bac3242] {\n                  width: 26px;\n                  height: 26px;\n                  vertical-align: bottom;\n}\n.project-panel .label-box .label-item .label-body .task-box .task-item .task-shadow .task-more .task-persons .task-userimg .avatar[data-v-4bac3242] {\n                    width: 26px;\n                    height: 26px;\n                    font-size: 14px;\n                    line-height: 26px;\n                    border-radius: 13px;\n}\n.project-panel .label-box .label-item .label-body .task-box .task-item .task-shadow .subtask-progress[data-v-4bac3242] {\n              position: absolute;\n              top: 0;\n              left: 0;\n              width: 100%;\n              height: 100%;\n              z-index: -1;\n              border-radius: 0 3px 3px 0;\n              overflow: hidden;\n              pointer-events: none;\n}\n.project-panel .label-box .label-item .label-body .task-box .task-item .task-shadow .subtask-progress em[data-v-4bac3242] {\n                display: block;\n                height: 100%;\n                background-color: rgba(3, 150, 242, 0.07);\n}\n.project-panel .label-box .label-item .label-body .trigger-box[data-v-4bac3242] {\n        text-align: center;\n        font-size: 16px;\n        color: #666;\n        width: 100%;\n        position: absolute;\n        top: 50%;\n        -webkit-transform: translate(0, -50%) scale(1);\n                transform: translate(0, -50%) scale(1);\n        -webkit-transition: all 0.3s;\n        transition: all 0.3s;\n}\n.project-panel .label-box .label-item .label-bottom[data-v-4bac3242] {\n      position: absolute;\n      left: 50%;\n      bottom: 14px;\n      z-index: 1;\n      width: 36px;\n      height: 36px;\n      border-radius: 50%;\n      background-color: #2db7f5;\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-align: center;\n          -ms-flex-align: center;\n              align-items: center;\n      -webkit-box-pack: center;\n          -ms-flex-pack: center;\n              justify-content: center;\n      -webkit-transition: -webkit-transform 0.2s;\n      transition: -webkit-transform 0.2s;\n      transition: transform 0.2s;\n      transition: transform 0.2s, -webkit-transform 0.2s;\n      -webkit-transform: translate(-50%, 200%);\n              transform: translate(-50%, 200%);\n      cursor: pointer;\n}\n.project-panel .label-box .label-item .label-bottom .label-bottom-icon[data-v-4bac3242] {\n        color: #ffffff;\n        font-size: 36px;\n}\n", ""]);

// exports


/***/ }),

/***/ 535:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vuedraggable__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vuedraggable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vuedraggable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_WContent__ = __webpack_require__(305);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_WContent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__components_WContent__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_project_task_add__ = __webpack_require__(536);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_project_task_add___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__components_project_task_add__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_project_task_lists__ = __webpack_require__(543);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_project_task_lists___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__components_project_task_lists__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_project_task_files__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_project_task_files___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__components_project_task_files__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_project_task_logs__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_project_task_logs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5__components_project_task_logs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__components_project_archived__ = __webpack_require__(338);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__components_project_archived___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6__components_project_archived__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__components_project_users__ = __webpack_require__(343);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__components_project_users___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7__components_project_users__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__components_project_statistics__ = __webpack_require__(348);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__components_project_statistics___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8__components_project_statistics__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__components_iview_WDrawer__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__components_iview_WDrawer___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9__components_iview_WDrawer__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__components_project_gantt_index__ = __webpack_require__(548);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__components_project_gantt_index___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10__components_project_gantt_index__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__components_project_setting__ = __webpack_require__(558);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__components_project_setting___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_11__components_project_setting__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__components_ScrollerY__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__components_ScrollerY___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_12__components_ScrollerY__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_lodash_orderBy__ = __webpack_require__(563);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_lodash_orderBy___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_13_lodash_orderBy__);
var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

















/* harmony default export */ __webpack_exports__["default"] = ({
    components: {
        ScrollerY: __WEBPACK_IMPORTED_MODULE_12__components_ScrollerY___default.a,
        ProjectSetting: __WEBPACK_IMPORTED_MODULE_11__components_project_setting___default.a,
        ProjectGantt: __WEBPACK_IMPORTED_MODULE_10__components_project_gantt_index___default.a,
        WDrawer: __WEBPACK_IMPORTED_MODULE_9__components_iview_WDrawer___default.a,
        ProjectStatistics: __WEBPACK_IMPORTED_MODULE_8__components_project_statistics___default.a,
        ProjectUsers: __WEBPACK_IMPORTED_MODULE_7__components_project_users___default.a,
        ProjectArchived: __WEBPACK_IMPORTED_MODULE_6__components_project_archived___default.a,
        ProjectTaskLogs: __WEBPACK_IMPORTED_MODULE_5__components_project_task_logs___default.a,
        ProjectTaskFiles: __WEBPACK_IMPORTED_MODULE_4__components_project_task_files___default.a, ProjectTaskLists: __WEBPACK_IMPORTED_MODULE_3__components_project_task_lists___default.a, ProjectAddTask: __WEBPACK_IMPORTED_MODULE_2__components_project_task_add___default.a, draggable: __WEBPACK_IMPORTED_MODULE_0_vuedraggable___default.a, WContent: __WEBPACK_IMPORTED_MODULE_1__components_WContent___default.a },
    data: function data() {
        return {
            loadIng: 0,
            loadDetailed: false,

            projectid: 0,
            projectDetail: {},
            projectLabel: [],
            projectSimpleLabel: [],
            projectSortData: '',
            projectSortDisabled: false,

            projectDrawerShow: false,
            projectDrawerTab: 'lists',

            projectSettingDrawerShow: false,
            projectSettingDrawerTab: 'setting',

            projectGanttShow: false,

            filtrTask: '',
            routeName: ''
        };
    },
    mounted: function mounted() {
        var _this = this;

        this.routeName = this.$route.name;
        $A.setOnTaskInfoListener('pages/project-panel', function (act, detail) {
            if (detail.projectid != _this.projectid) {
                return;
            }
            //
            switch (act) {
                case 'addlabel':
                    // 添加分类
                    var tempLists = _this.projectLabel.filter(function (res) {
                        return res.id == detail.labelid;
                    });
                    if (tempLists.length == 0) {
                        _this.projectLabel.push(Object.assign(detail, { id: detail.labelid }));
                        _this.projectSortData = _this.getProjectSort();
                    }
                    return;

                case 'deletelabel':
                    // 删除分类
                    _this.projectLabel.some(function (label, index) {
                        if (label.id == detail.labelid) {
                            _this.projectLabel.splice(index, 1);
                            _this.projectSortData = _this.getProjectSort();
                            return true;
                        }
                    });
                    return;

                case 'deleteproject':
                    // 删除项目
                    return;

                case "labelsort": // 调整分类排序
                case "tasksort":
                    // 调整任务排序
                    if (detail.__modifyUsername != _this.usrName) {
                        if (_this.routeName == _this.$route.name) {
                            _this.$Modal.confirm({
                                title: _this.$L("更新提示"),
                                content: _this.$L('团队成员（%）调整了%，<br/>更新时间：%。<br/><br/>点击【确定】加载最新数据。', detail.nickname, _this.$L(act == 'labelsort' ? '分类排序' : '任务排序'), $A.formatDate("Y-m-d H:i:s", detail.time)),
                                onOk: function onOk() {
                                    _this.getDetail(true);
                                }
                            });
                        } else {
                            _this.getDetail(true);
                        }
                    }
                    return;
            }
            //
            _this.projectLabel.forEach(function (label) {
                label.taskLists.some(function (task, i) {
                    if (task.id == detail.id) {
                        label.taskLists.splice(i, 1, detail);
                        return true;
                    }
                });
            });
            //
            switch (act) {
                case "delete": // 删除任务
                case "archived":
                    // 归档
                    _this.projectLabel.forEach(function (label) {
                        label.taskLists.some(function (task, i) {
                            if (task.id == detail.id) {
                                label.taskLists.splice(i, 1);
                                return true;
                            }
                        });
                    });
                    _this.projectSortData = _this.getProjectSort();
                    break;

                case "create":
                    // 创建任务
                    _this.projectLabel.some(function (label) {
                        if (label.id == detail.labelid) {
                            var _tempLists = label.taskLists.filter(function (res) {
                                return res.id == detail.id;
                            });
                            if (_tempLists.length == 0) {
                                detail.isNewtask = true;
                                if (detail.insertbottom) {
                                    label.taskLists.push(detail);
                                } else {
                                    label.taskLists.unshift(detail);
                                }
                                _this.$nextTick(function () {
                                    _this.$set(detail, 'isNewtask', false);
                                });
                            }
                            return true;
                        }
                    });
                    break;

                case "unarchived":
                    // 取消归档
                    _this.projectLabel.forEach(function (label) {
                        if (label.id == detail.labelid) {
                            var index = label.taskLists.length;
                            label.taskLists.some(function (task, i) {
                                if (detail.inorder > task.inorder || detail.inorder == task.inorder && detail.id > task.id) {
                                    index = i;
                                    return true;
                                }
                            });
                            label.taskLists.splice(index, 0, detail);
                        }
                    });
                    _this.projectSortData = _this.getProjectSort();
                    break;

                case "complete": // 标记完成
                case "unfinished":
                    // 标记未完成
                    _this.taskNewSort();
                    break;
            }
        }, true);
    },
    activated: function activated() {
        var _this2 = this;

        this.projectid = this.$route.params.projectid;
        if (_typeof(this.$route.params.other) === "object") {
            this.$set(this.projectDetail, 'title', $A.getObject(this.$route.params.other, 'title'));
        }
        if (this.$route.params.statistics === '已完成') {
            this.projectSettingDrawerTab = 'statistics';
            this.projectSettingDrawerShow = true;
            this.$nextTick(function () {
                _this2.$refs.statistics.setTaskType('已完成');
            });
        }
    },
    deactivated: function deactivated() {
        if ($A.getToken() === false) {
            this.projectid = 0;
        }
        this.projectGanttShow = false;
        this.projectDrawerShow = false;
        this.projectSettingDrawerShow = false;
    },

    watch: {
        projectid: function projectid(val) {
            if ($A.runNum(val) <= 0) {
                return;
            }
            this.projectDetail = {};
            this.projectLabel = [];
            this.projectSimpleLabel = [];
            this.getDetail();
        },
        '$route': function $route(To) {
            if (To.name == 'project-panel') {
                this.projectid = To.params.projectid;
            }
        }
    },
    methods: {
        getDetail: function getDetail(successTip) {
            var _this3 = this;

            this.loadIng++;
            $A.apiAjax({
                url: 'project/detail',
                data: {
                    projectid: this.projectid
                },
                complete: function complete() {
                    _this3.loadIng--;
                    _this3.loadDetailed = true;
                },
                error: function error() {
                    _this3.goBack({ name: 'project' });
                    alert(_this3.$L('网络繁忙，请稍后再试！'));
                },
                success: function success(res) {
                    if (res.ret === 1) {
                        _this3.projectLabel = res.data.label;
                        _this3.taskNewSort();
                        _this3.projectDetail = res.data.project;
                        _this3.projectSimpleLabel = res.data.simpleLabel;
                        _this3.projectSortData = _this3.getProjectSort();
                        if (successTip === true) {
                            _this3.$Message.success(_this3.$L('刷新成功！'));
                        }
                    } else {
                        _this3.$Modal.error({ title: _this3.$L('温馨提示'), content: res.msg });
                    }
                }
            });
        },
        getProjectSort: function getProjectSort() {
            var sortData = "",
                taskData = "";
            this.projectLabel.forEach(function (label) {
                taskData = "";
                label.taskLists.forEach(function (task) {
                    if (taskData) taskData += "-";
                    taskData += task.id;
                });
                if (sortData) sortData += ";";
                sortData += label.id + ":" + taskData;
            });
            return sortData;
        },
        handleLabel: function handleLabel(event, labelDetail) {
            switch (event) {
                case 'refresh':
                    {
                        this.refreshLabel(labelDetail);
                        break;
                    }
                case 'rename':
                    {
                        this.renameLabel(labelDetail);
                        break;
                    }
                case 'delete':
                    {
                        this.deleteLabel(labelDetail);
                        break;
                    }
            }
        },
        refreshLabel: function refreshLabel(item) {
            var _this4 = this;

            this.$set(item, 'loadIng', true);
            $A.apiAjax({
                url: 'project/task/lists',
                data: {
                    projectid: this.projectid,
                    labelid: item.id
                },
                complete: function complete() {
                    _this4.$set(item, 'loadIng', false);
                },
                error: function error() {
                    window.location.reload();
                },
                success: function success(res) {
                    if (res.ret === 1) {
                        _this4.$set(item, 'taskLists', res.data.lists);
                    } else {
                        window.location.reload();
                    }
                }
            });
        },
        renameLabel: function renameLabel(item) {
            var _this5 = this;

            this.renameValue = "";
            this.$Modal.confirm({
                render: function render(h) {
                    return h('div', [h('div', {
                        style: {
                            fontSize: '16px',
                            fontWeight: '500',
                            marginBottom: '20px'
                        }
                    }, _this5.$L('重命名列表')), h('Input', {
                        props: {
                            value: _this5.renameValue,
                            autofocus: true,
                            placeholder: _this5.$L('请输入新的列表名称')
                        },
                        on: {
                            input: function input(val) {
                                _this5.renameValue = val;
                            }
                        }
                    })]);
                },
                loading: true,
                onOk: function onOk() {
                    if (_this5.renameValue) {
                        _this5.$set(item, 'loadIng', true);
                        var title = _this5.renameValue;
                        $A.apiAjax({
                            url: 'project/label/rename',
                            data: {
                                projectid: _this5.projectid,
                                labelid: item.id,
                                title: title
                            },
                            complete: function complete() {
                                _this5.$set(item, 'loadIng', false);
                            },
                            error: function error() {
                                _this5.$Modal.remove();
                                alert(_this5.$L('网络繁忙，请稍后再试！'));
                            },
                            success: function success(res) {
                                _this5.$Modal.remove();
                                _this5.$set(item, 'title', title);
                                setTimeout(function () {
                                    if (res.ret === 1) {
                                        _this5.$Message.success(res.msg);
                                    } else {
                                        _this5.$Modal.error({ title: _this5.$L('温馨提示'), content: res.msg });
                                    }
                                }, 350);
                            }
                        });
                    } else {
                        _this5.$Modal.remove();
                    }
                }
            });
        },
        deleteLabel: function deleteLabel(item) {
            var _this6 = this;

            var redTip = item.taskLists.length > 0 ? '<div style="color:red;font-weight:500">' + this.$L('注：将同时删除列表下所有任务') + '</div>' : '';
            this.$Modal.confirm({
                title: this.$L('删除列表'),
                content: '<div>' + this.$L('你确定要删除此列表吗？') + '</div>' + redTip,
                loading: true,
                onOk: function onOk() {
                    $A.apiAjax({
                        url: 'project/label/delete',
                        data: {
                            projectid: _this6.projectid,
                            labelid: item.id
                        },
                        error: function error() {
                            _this6.$Modal.remove();
                            alert(_this6.$L('网络繁忙，请稍后再试！'));
                        },
                        success: function success(res) {
                            _this6.$Modal.remove();
                            _this6.projectLabel.some(function (label, index) {
                                if (label.id == item.id) {
                                    _this6.projectLabel.splice(index, 1);
                                    _this6.projectSortData = _this6.getProjectSort();
                                    return true;
                                }
                            });
                            setTimeout(function () {
                                if (res.ret === 1) {
                                    _this6.$Message.success(res.msg);
                                    $A.triggerTaskInfoListener('deletelabel', { labelid: item.id, projectid: item.projectid });
                                } else {
                                    _this6.$Modal.error({ title: _this6.$L('温馨提示'), content: res.msg });
                                }
                            }, 350);
                        }
                    });
                }
            });
        },
        addLabel: function addLabel() {
            var _this7 = this;

            this.labelValue = "";
            this.$Modal.confirm({
                render: function render(h) {
                    return h('div', [h('div', {
                        style: {
                            fontSize: '16px',
                            fontWeight: '500',
                            marginBottom: '20px'
                        }
                    }, _this7.$L('添加列表')), h('Input', {
                        props: {
                            value: _this7.labelValue,
                            autofocus: true,
                            placeholder: _this7.$L('请输入列表名称')
                        },
                        on: {
                            input: function input(val) {
                                _this7.labelValue = val;
                            }
                        }
                    })]);
                },
                loading: true,
                onOk: function onOk() {
                    if (_this7.labelValue) {
                        var data = {
                            projectid: _this7.projectid,
                            title: _this7.labelValue
                        };
                        $A.apiAjax({
                            url: 'project/label/add',
                            data: data,
                            error: function error() {
                                _this7.$Modal.remove();
                                alert(_this7.$L('网络繁忙，请稍后再试！'));
                            },
                            success: function success(res) {
                                _this7.$Modal.remove();
                                _this7.projectLabel.push(res.data);
                                _this7.projectSortData = _this7.getProjectSort();
                                $A.triggerTaskInfoListener('addlabel', Object.assign(data, { labelid: res.data.id }));
                                setTimeout(function () {
                                    if (res.ret === 1) {
                                        _this7.$Message.success(res.msg);
                                    } else {
                                        _this7.$Modal.error({ title: _this7.$L('温馨提示'), content: res.msg });
                                    }
                                }, 350);
                            }
                        });
                    } else {
                        _this7.$Modal.remove();
                    }
                }
            });
        },
        addTaskSuccess: function addTaskSuccess(taskDetail, label) {
            var _this8 = this;

            if (label.taskLists instanceof Array) {
                taskDetail.isNewtask = true;
                if (taskDetail.insertbottom) {
                    label.taskLists.push(taskDetail);
                } else {
                    label.taskLists.unshift(taskDetail);
                }
                this.$nextTick(function () {
                    _this8.$set(taskDetail, 'isNewtask', false);
                });
            } else {
                this.refreshLabel(label);
            }
        },
        openProjectDrawer: function openProjectDrawer(tab) {
            if (tab == 'projectGanttShow') {
                this.projectGanttShow = !this.projectGanttShow;
                return;
            } else if (tab == 'openProjectSettingDrawer') {
                this.openProjectSettingDrawer('setting');
                return;
            }
            this.projectDrawerTab = tab;
            this.projectDrawerShow = true;
        },
        openProjectSettingDrawer: function openProjectSettingDrawer(tab) {
            this.projectSettingDrawerTab = tab;
            this.projectSettingDrawerShow = true;
        },
        projectSortUpdate: function projectSortUpdate(isLabel) {
            var _this9 = this;

            var oldSort = this.projectSortData;
            var newSort = this.getProjectSort();
            if (oldSort == newSort) {
                return;
            }
            this.projectSortData = newSort;
            this.projectSortDisabled = true;
            this.loadIng++;
            $A.apiAjax({
                url: 'project/sort',
                data: {
                    projectid: this.projectid,
                    oldsort: oldSort,
                    newsort: newSort,
                    label: isLabel === true ? 1 : 0
                },
                complete: function complete() {
                    _this9.projectSortDisabled = false;
                    _this9.loadIng--;
                },
                error: function error() {
                    _this9.getDetail();
                    alert(_this9.$L('网络繁忙，请稍后再试！'));
                },
                success: function success(res) {
                    if (res.ret === 1) {
                        _this9.projectLabel.forEach(function (label) {
                            var length = label.taskLists.length;
                            label.taskLists.forEach(function (task, index) {
                                task.inorder = length - index;
                            });
                        });
                        _this9.taskNewSort();
                        //
                        _this9.$Message.success(res.msg);
                        $A.triggerTaskInfoListener(isLabel ? 'labelsort' : 'tasksort', { projectid: _this9.projectid, nickname: $A.getNickName(), time: Math.round(new Date().getTime() / 1000) });
                    } else {
                        _this9.getDetail();
                        _this9.$Modal.error({ title: _this9.$L('温馨提示'), content: res.msg });
                    }
                }
            });
        },
        projectMouse: function projectMouse(label) {
            var hasScroll = false;
            var el = this.$refs['box_' + label.id];
            if (el && el.length > 0) {
                el = el[0].$el;
                hasScroll = el.scrollHeight > el.offsetHeight;
            }
            this.$set(label, 'hasScroll', hasScroll);
        },
        projectBoxScroll: function projectBoxScroll(e, label) {
            this.$set(label, 'endScroll', e.scrollE < 50);
        },
        projectFocus: function projectFocus(label) {
            var el = this.$refs['add_' + label.id];
            if (el && el.length > 0) {
                el[0].setFocus();
            }
            el = this.$refs['box_' + label.id];
            if (el && el.length > 0) {
                el[0].scrollToBottom(false);
            }
        },
        subtaskProgress: function subtaskProgress(task) {
            var subtask = task.subtask,
                complete = task.complete;

            if (subtask.length === 0) {
                return complete ? 100 : 0;
            }
            var completeLists = subtask.filter(function (item) {
                return item.status == 'complete';
            });
            return parseFloat((completeLists.length / subtask.length * 100).toFixed(2));
        },
        openTaskModal: function openTaskModal(taskDetail) {
            this.taskDetail(taskDetail);
        },
        taskNewSort: function taskNewSort() {
            var _this10 = this;

            this.$nextTick(function () {
                _this10.projectLabel.forEach(function (item) {
                    item.taskLists = _this10.taskReturnNewSort(item.taskLists);
                });
            });
        },
        taskReturnNewSort: function taskReturnNewSort(lists) {
            var tmpLists = __WEBPACK_IMPORTED_MODULE_13_lodash_orderBy___default()(lists, ['complete', 'inorder'], ['asc', 'desc']);
            var array = [];
            array.unshift.apply(array, _toConsumableArray(tmpLists.filter(function (_ref) {
                var complete = _ref.complete;
                return complete;
            })));
            array.unshift.apply(array, _toConsumableArray(tmpLists.filter(function (_ref2) {
                var complete = _ref2.complete;
                return !complete;
            })));
            return array;
        },
        isPersonsTask: function isPersonsTask(task) {
            var _this11 = this;

            return task.persons && !!task.persons.find(function (_ref3) {
                var username = _ref3.username;
                return username == _this11.usrInfo.username;
            });
        },
        isFollowerTask: function isFollowerTask(task) {
            return task.follower && task.follower.indexOf(this.usrInfo.username) !== -1;
        },
        isCreateTask: function isCreateTask(task) {
            return task.createuser == this.usrInfo.username;
        }
    }
});

/***/ }),

/***/ 536:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(537)
  __webpack_require__(539)
}
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(541)
/* template */
var __vue_template__ = __webpack_require__(542)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-7dae0bfc"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/main/components/project/task/add.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-7dae0bfc", Component.options)
  } else {
    hotAPI.reload("data-v-7dae0bfc", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 537:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(538);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(1)("1187e5ab", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../../node_modules/css-loader/index.js!../../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-7dae0bfc\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../../../../node_modules/sass-loader/lib/loader.js!../../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./add.vue", function() {
     var newContent = require("!!../../../../../../../node_modules/css-loader/index.js!../../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-7dae0bfc\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../../../../node_modules/sass-loader/lib/loader.js!../../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./add.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 538:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)(false);
// imports


// module
exports.push([module.i, "\n.enter-module-btn-drop-list .ivu-dropdown-item {\n  padding: 5px 16px;\n  font-size: 12px !important;\n}\n", ""]);

// exports


/***/ }),

/***/ 539:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(540);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(1)("42fdf46b", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../../node_modules/css-loader/index.js!../../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-7dae0bfc\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../../node_modules/sass-loader/lib/loader.js!../../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=1!./add.vue", function() {
     var newContent = require("!!../../../../../../../node_modules/css-loader/index.js!../../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-7dae0bfc\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../../node_modules/sass-loader/lib/loader.js!../../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=1!./add.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 540:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)(false);
// imports


// module
exports.push([module.i, "\n.task-input-box[data-v-7dae0bfc] {\n  position: relative;\n  margin-top: 5px;\n  margin-bottom: 20px;\n  min-height: 70px;\n}\n.task-input-box .input-placeholder[data-v-7dae0bfc],\n  .task-input-box .input-enter[data-v-7dae0bfc] {\n    position: absolute;\n    top: 0;\n    left: 0;\n    right: 0;\n    width: 100%;\n}\n.task-input-box .input-placeholder[data-v-7dae0bfc] {\n    z-index: 1;\n    height: 40px;\n    line-height: 40px;\n    color: rgba(0, 0, 0, 0.36);\n    padding-left: 12px;\n    padding-right: 12px;\n}\n.task-input-box .input-enter[data-v-7dae0bfc] {\n    z-index: 2;\n    position: relative;\n    background-color: transparent;\n}\n.task-input-box .input-enter .input-enter-textarea[data-v-7dae0bfc] {\n      border-radius: 4px;\n      padding-left: 12px;\n      padding-right: 12px;\n      color: rgba(0, 0, 0, 0.85);\n}\n.task-input-box .input-enter .input-enter-textarea.bright[data-v-7dae0bfc] {\n        background-color: rgba(46, 73, 136, 0.08);\n}\n.task-input-box .input-enter .input-enter-textarea.highlight[data-v-7dae0bfc] {\n        background-color: #ffffff;\n}\n.task-input-box .input-enter .input-enter-module[data-v-7dae0bfc] {\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      width: 100%;\n      margin-top: 8px;\n      -webkit-box-align: center;\n          -ms-flex-align: center;\n              align-items: center;\n}\n.task-input-box .input-enter .input-enter-module .enter-module-icon[data-v-7dae0bfc] {\n        display: inline-block;\n        width: 16px;\n        height: 16px;\n        margin-right: 5px;\n        border-radius: 4px;\n        vertical-align: middle;\n        cursor: pointer;\n}\n.task-input-box .input-enter .input-enter-module .enter-module-icon.p1[data-v-7dae0bfc] {\n          background-color: #ff0000;\n}\n.task-input-box .input-enter .input-enter-module .enter-module-icon.p2[data-v-7dae0bfc] {\n          background-color: #BB9F35;\n}\n.task-input-box .input-enter .input-enter-module .enter-module-icon.p3[data-v-7dae0bfc] {\n          background-color: #449EDD;\n}\n.task-input-box .input-enter .input-enter-module .enter-module-icon.p4[data-v-7dae0bfc] {\n          background-color: #84A83B;\n}\n.task-input-box .input-enter .input-enter-module .enter-module-icon.user[data-v-7dae0bfc] {\n          width: 24px;\n          height: 24px;\n          margin-left: 10px;\n          margin-right: 10px;\n}\n.task-input-box .input-enter .input-enter-module .enter-module-icon.user .avatar[data-v-7dae0bfc] {\n            width: 24px;\n            height: 24px;\n            font-size: 14px;\n            line-height: 24px;\n            border-radius: 12px;\n}\n.task-input-box .input-enter .input-enter-module .enter-module-icon.user i[data-v-7dae0bfc] {\n            line-height: 24px;\n            font-size: 16px;\n}\n.task-input-box .input-enter .input-enter-module .enter-module-icon i[data-v-7dae0bfc] {\n          width: 100%;\n          height: 100%;\n          color: #ffffff;\n          line-height: 16px;\n          font-size: 14px;\n          -webkit-transform: scale(0.85);\n                  transform: scale(0.85);\n          vertical-align: 0;\n}\n.task-input-box .input-enter .input-enter-module .enter-module-flex[data-v-7dae0bfc] {\n        -webkit-box-flex: 1;\n            -ms-flex: 1;\n                flex: 1;\n}\n.task-input-box .input-enter .input-enter-module .enter-module-btn button[data-v-7dae0bfc] {\n        font-size: 12px;\n}\n.task-input-box .input-enter .input-enter-module .enter-module-btn .enter-module-btn-1[data-v-7dae0bfc] {\n        border-top-right-radius: 0;\n        border-bottom-right-radius: 0;\n}\n.task-input-box .input-enter .input-enter-module .enter-module-btn .enter-module-btn-2[data-v-7dae0bfc] {\n        padding: 0 2px;\n        border-top-left-radius: 0;\n        border-bottom-left-radius: 0;\n}\n.task-input-box .input-enter .input-enter-module .enter-module-btn .enter-module-btn-drop[data-v-7dae0bfc] {\n        margin-left: -4px;\n        border-left: 1px solid #c0daff;\n}\n.task-input-box .load-box[data-v-7dae0bfc] {\n    position: absolute;\n    top: 0;\n    left: 0;\n    right: 0;\n    bottom: 0;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n    -webkit-box-pack: center;\n        -ms-flex-pack: center;\n            justify-content: center;\n    z-index: 9;\n}\n.task-input-box .load-box .load-box-main[data-v-7dae0bfc] {\n      width: 24px;\n      height: 24px;\n}\n", ""]);

// exports


/***/ }),

/***/ 541:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: 'ProjectAddTask',
    props: {
        placeholder: {
            type: String,
            default: ''
        },
        projectid: {
            type: Number,
            default: 0
        },
        labelid: {
            type: Number,
            default: 0
        }
    },
    data: function data() {
        return {
            loadIng: 0,

            addText: '',
            addLevel: 2,
            addUserInfo: {},
            addFocus: false,

            nameTipDisabled: false
        };
    },
    mounted: function mounted() {
        this.addUserInfo = $A.cloneData(this.usrInfo);
    },

    methods: {
        changeUser: function changeUser(user) {
            if (typeof user.username === "undefined") {
                this.addUserInfo = $A.cloneData(this.usrInfo);
            } else {
                this.addUserInfo = user;
            }
        },
        dropAdd: function dropAdd(name) {
            if (name == 'insertbottom') {
                this.clickAdd(true);
            }
        },
        clickAdd: function clickAdd() {
            var _this = this;

            var insertbottom = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

            var addText = this.addText.trim();
            if ($A.count(addText) == 0 || this.loadIng > 0) {
                return;
            }
            this.loadIng++;
            $A.apiAjax({
                url: 'project/task/add',
                data: {
                    projectid: this.projectid,
                    labelid: this.labelid,
                    title: addText,
                    level: this.addLevel,
                    username: this.addUserInfo.username,
                    insertbottom: insertbottom ? 1 : 0
                },
                complete: function complete() {
                    _this.loadIng--;
                },
                error: function error() {
                    alert(_this.$L('网络繁忙，请稍后再试！'));
                },
                success: function success(res) {
                    if (res.ret === 1) {
                        _this.addText = '';
                        _this.addFocus = false;
                        _this.$Message.success(res.msg);
                        res.data.insertbottom = insertbottom;
                        _this.$emit('on-add-success', res.data);
                        $A.triggerTaskInfoListener('create', res.data);
                        $A.triggerTaskInfoChange(res.data.id);
                    } else {
                        _this.$Modal.error({ title: _this.$L('温馨提示'), content: res.msg });
                    }
                }
            });
        },
        addKeydown: function addKeydown(e) {
            if (e.keyCode == 13) {
                if (e.shiftKey) {
                    return;
                }
                this.clickAdd(false);
                e.preventDefault();
            }
        },
        setFocus: function setFocus() {
            this.$refs.addInput.focus();
        },
        onFocus: function onFocus(focus) {
            this.addFocus = focus;
            this.$emit('on-focus', focus);
        }
    }
});

/***/ }),

/***/ 542:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "task-input-box" }, [
    !_vm.addText
      ? _c(
          "div",
          { staticClass: "input-placeholder" },
          [
            _c("Icon", { attrs: { type: "md-create", size: "18" } }),
            _vm._v(
              " " +
                _vm._s(
                  _vm.addFocus
                    ? "" + _vm.$L("输入任务，回车即可保存")
                    : _vm.placeholder
                ) +
                "\n    "
            )
          ],
          1
        )
      : _vm._e(),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "input-enter" },
      [
        _c("Input", {
          ref: "addInput",
          staticClass: "input-enter-textarea",
          class: { bright: _vm.addFocus === true, highlight: !!_vm.addText },
          attrs: {
            type: "textarea",
            "element-id": "project-panel-enter-textarea",
            autosize: { minRows: 1, maxRows: 6 },
            maxlength: 255
          },
          on: {
            "on-focus": function($event) {
              return _vm.onFocus(true)
            },
            "on-blur": function($event) {
              return _vm.onFocus(false)
            },
            "on-keydown": _vm.addKeydown
          },
          model: {
            value: _vm.addText,
            callback: function($$v) {
              _vm.addText = $$v
            },
            expression: "addText"
          }
        }),
        _vm._v(" "),
        !!_vm.addText
          ? _c(
              "div",
              { staticClass: "input-enter-module" },
              [
                _c(
                  "Tooltip",
                  {
                    attrs: {
                      content: _vm.$L("重要且紧急"),
                      placement: "bottom",
                      transfer: ""
                    }
                  },
                  [
                    _c(
                      "div",
                      {
                        staticClass: "enter-module-icon p1",
                        on: {
                          click: function($event) {
                            _vm.addLevel = 1
                          }
                        }
                      },
                      [
                        _vm.addLevel == "1"
                          ? _c("Icon", { attrs: { type: "md-checkmark" } })
                          : _vm._e()
                      ],
                      1
                    )
                  ]
                ),
                _vm._v(" "),
                _c(
                  "Tooltip",
                  {
                    attrs: {
                      content: _vm.$L("重要不紧急"),
                      placement: "bottom",
                      transfer: ""
                    }
                  },
                  [
                    _c(
                      "div",
                      {
                        staticClass: "enter-module-icon p2",
                        on: {
                          click: function($event) {
                            _vm.addLevel = 2
                          }
                        }
                      },
                      [
                        _vm.addLevel == "2"
                          ? _c("Icon", { attrs: { type: "md-checkmark" } })
                          : _vm._e()
                      ],
                      1
                    )
                  ]
                ),
                _vm._v(" "),
                _c(
                  "Tooltip",
                  {
                    attrs: {
                      content: _vm.$L("紧急不重要"),
                      placement: "bottom",
                      transfer: ""
                    }
                  },
                  [
                    _c(
                      "div",
                      {
                        staticClass: "enter-module-icon p3",
                        on: {
                          click: function($event) {
                            _vm.addLevel = 3
                          }
                        }
                      },
                      [
                        _vm.addLevel == "3"
                          ? _c("Icon", { attrs: { type: "md-checkmark" } })
                          : _vm._e()
                      ],
                      1
                    )
                  ]
                ),
                _vm._v(" "),
                _c(
                  "Tooltip",
                  {
                    attrs: {
                      content: _vm.$L("不重要不紧急"),
                      placement: "bottom",
                      transfer: ""
                    }
                  },
                  [
                    _c(
                      "div",
                      {
                        staticClass: "enter-module-icon p4",
                        on: {
                          click: function($event) {
                            _vm.addLevel = 4
                          }
                        }
                      },
                      [
                        _vm.addLevel == "4"
                          ? _c("Icon", { attrs: { type: "md-checkmark" } })
                          : _vm._e()
                      ],
                      1
                    )
                  ]
                ),
                _vm._v(" "),
                _c("div", { staticClass: "enter-module-flex" }),
                _vm._v(" "),
                _c(
                  "Poptip",
                  {
                    attrs: { placement: "bottom", transfer: "" },
                    on: {
                      "on-popper-show": function($event) {
                        _vm.nameTipDisabled = true
                      },
                      "on-popper-hide": function($event) {
                        _vm.nameTipDisabled = false
                      }
                    }
                  },
                  [
                    _c(
                      "Tooltip",
                      {
                        attrs: {
                          placement: "bottom",
                          disabled: _vm.nameTipDisabled
                        }
                      },
                      [
                        _c(
                          "div",
                          { staticClass: "enter-module-icon user" },
                          [
                            _c("UserImg", {
                              staticClass: "avatar",
                              attrs: { info: _vm.addUserInfo }
                            })
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { attrs: { slot: "content" }, slot: "content" },
                          [
                            _vm._v(
                              "\n                        " +
                                _vm._s(_vm.$L("负责人")) +
                                ": "
                            ),
                            _c("UserView", {
                              attrs: { username: _vm.addUserInfo.username }
                            })
                          ],
                          1
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c("div", { attrs: { slot: "content" }, slot: "content" }, [
                      _c(
                        "div",
                        { staticStyle: { width: "240px" } },
                        [
                          _vm._v(
                            "\n                        " +
                              _vm._s(_vm.$L("选择负责人")) +
                              "\n                        "
                          ),
                          _c("UserInput", {
                            staticStyle: { margin: "5px 0 3px" },
                            attrs: {
                              projectid: _vm.projectid,
                              placeholder: _vm.$L("留空默认: 自己")
                            },
                            on: { change: _vm.changeUser },
                            model: {
                              value: _vm.addUserInfo.username,
                              callback: function($$v) {
                                _vm.$set(_vm.addUserInfo, "username", $$v)
                              },
                              expression: "addUserInfo.username"
                            }
                          })
                        ],
                        1
                      )
                    ])
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "enter-module-btn" },
                  [
                    _c(
                      "Button",
                      {
                        staticClass: "enter-module-btn-1",
                        attrs: { type: "info", size: "small" },
                        on: {
                          click: function($event) {
                            return _vm.clickAdd(false)
                          }
                        }
                      },
                      [_vm._v(_vm._s(_vm.$L("添加任务")))]
                    ),
                    _vm._v(" "),
                    _c(
                      "Dropdown",
                      {
                        staticClass: "enter-module-btn-drop",
                        attrs: { placement: "bottom-end", transfer: "" },
                        on: { "on-click": _vm.dropAdd }
                      },
                      [
                        _c(
                          "Button",
                          {
                            staticClass: "enter-module-btn-2",
                            attrs: { type: "info", size: "small" }
                          },
                          [_c("Icon", { attrs: { type: "ios-arrow-down" } })],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "DropdownMenu",
                          {
                            staticClass: "enter-module-btn-drop-list",
                            attrs: { slot: "list" },
                            slot: "list"
                          },
                          [
                            _c(
                              "DropdownItem",
                              { attrs: { name: "insertbottom" } },
                              [_vm._v(_vm._s(_vm.$L("添加至列表结尾")))]
                            )
                          ],
                          1
                        )
                      ],
                      1
                    )
                  ],
                  1
                )
              ],
              1
            )
          : _vm._e()
      ],
      1
    ),
    _vm._v(" "),
    _vm.loadIng > 0
      ? _c(
          "div",
          {
            staticClass: "load-box",
            on: {
              click: function($event) {
                $event.stopPropagation()
              }
            }
          },
          [_c("div", { staticClass: "load-box-main" }, [_c("w-loading")], 1)]
        )
      : _vm._e()
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-7dae0bfc", module.exports)
  }
}

/***/ }),

/***/ 543:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(544)
}
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(546)
/* template */
var __vue_template__ = __webpack_require__(547)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-11cf9cb6"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/main/components/project/task/lists.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-11cf9cb6", Component.options)
  } else {
    hotAPI.reload("data-v-11cf9cb6", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 544:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(545);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(1)("82725c66", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../../node_modules/css-loader/index.js!../../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-11cf9cb6\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../../node_modules/sass-loader/lib/loader.js!../../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./lists.vue", function() {
     var newContent = require("!!../../../../../../../node_modules/css-loader/index.js!../../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-11cf9cb6\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../../node_modules/sass-loader/lib/loader.js!../../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./lists.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 545:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)(false);
// imports


// module
exports.push([module.i, "\n.project-task-lists[data-v-11cf9cb6] {\n  margin: 0 12px;\n}\n.project-task-lists .tableFill[data-v-11cf9cb6] {\n    margin: 12px 0 20px;\n}\n", ""]);

// exports


/***/ }),

/***/ 546:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__DrawerTabsContainer__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__DrawerTabsContainer___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__DrawerTabsContainer__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__mixins_task__ = __webpack_require__(30);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/**
 * 项目任务列表
 */
/* harmony default export */ __webpack_exports__["default"] = ({
    name: 'ProjectTaskLists',
    components: { DrawerTabsContainer: __WEBPACK_IMPORTED_MODULE_0__DrawerTabsContainer___default.a },
    props: {
        projectid: {
            default: 0
        },
        canload: {
            type: Boolean,
            default: true
        },
        labelLists: {
            type: Array
        }
    },
    mixins: [__WEBPACK_IMPORTED_MODULE_1__mixins_task__["a" /* default */]],
    data: function data() {
        return {
            keys: {},
            sorts: { key: '', order: '' },

            loadYet: false,

            loadIng: 0,
            exportLoad: 0,

            columns: [],

            lists: [],
            listPage: 1,
            listTotal: 0,
            noDataText: ""
        };
    },
    mounted: function mounted() {
        var _this = this;

        if (this.canload) {
            this.loadYet = true;
            this.getLists(true);
        }
        $A.setOnTaskInfoListener('components/project/task/lists', function (act, detail) {
            if (detail.projectid != _this.projectid) {
                return;
            }
            //
            _this.lists.some(function (task, i) {
                if (task.id == detail.id) {
                    _this.lists.splice(i, 1, detail);
                    return true;
                }
            });
            //
            switch (act) {
                case "username": // 负责人
                case "delete": // 删除任务
                case "archived":
                    // 归档
                    _this.lists.some(function (task, i) {
                        if (task.id == detail.id) {
                            _this.lists.splice(i, 1);
                            return true;
                        }
                    });
                    break;

                case "unarchived":
                    // 取消归档
                    var has = false;
                    _this.lists.some(function (task) {
                        if (task.id == detail.id) {
                            return has = true;
                        }
                    });
                    if (!has) {
                        _this.lists.unshift(detail);
                    }
                    break;
            }
        });
    },


    watch: {
        projectid: function projectid() {
            if (this.loadYet) {
                this.getLists(true);
            }
        },
        canload: function canload(val) {
            if (val && !this.loadYet) {
                this.loadYet = true;
                this.getLists(true);
            }
        }
    },

    methods: {
        initLanguage: function initLanguage() {
            var _this2 = this;

            this.noDataText = this.$L("数据加载中.....");
            this.columns = [{
                "title": this.$L("任务名称"),
                "key": 'title',
                "minWidth": 120,
                render: function render(h, params) {
                    return _this2.renderTaskTitle(h, params);
                }
            }, {
                "title": this.$L("阶段"),
                "key": 'labelid',
                "minWidth": 80,
                "sortable": true,
                render: function render(h, params) {
                    var labelid = params.row.labelid;
                    var labelDetail = _this2.labelLists.find(function (item) {
                        return item.id === labelid;
                    });
                    return h('span', labelDetail ? labelDetail.title : labelid);
                }
            }, {
                "title": this.$L("计划时间"),
                "key": 'enddate',
                "width": 160,
                "align": "center",
                "sortable": true,
                render: function render(h, params) {
                    if (!params.row.startdate && !params.row.enddate) {
                        return h('span', '-');
                    }
                    return h('div', {
                        style: {
                            fontSize: '12px',
                            lineHeight: '14px'
                        }
                    }, [h('div', params.row.startdate ? $A.formatDate("Y-m-d H:i:s", params.row.startdate) : '-'), h('div', params.row.enddate ? $A.formatDate("Y-m-d H:i:s", params.row.enddate) : '-')]);
                }
            }, {
                "title": this.$L("负责人"),
                "key": 'username',
                "minWidth": 90,
                "sortable": true,
                render: function render(h, params) {
                    return h('UserView', {
                        props: {
                            username: params.row.username
                        }
                    });
                }
            }, {
                "title": this.$L("优先级"),
                "key": 'level',
                "align": "center",
                "minWidth": 90,
                "maxWidth": 100,
                "sortable": true,
                render: function render(h, params) {
                    var level = params.row.level;
                    var color = void 0;
                    switch (level) {
                        case 1:
                            color = "#ff0000";
                            break;
                        case 2:
                            color = "#BB9F35";
                            break;
                        case 3:
                            color = "#449EDD";
                            break;
                        case 4:
                            color = "#84A83B";
                            break;
                    }
                    return h('span', {
                        style: {
                            color: color
                        }
                    }, "P" + level);
                }
            }, {
                "title": this.$L("状态"),
                "key": 'type',
                "align": "center",
                "minWidth": 80,
                "maxWidth": 100,
                "sortable": true,
                render: function render(h, params) {
                    var color = void 0;
                    var status = void 0;
                    if (params.row.overdue) {
                        color = "#ff0000";
                        status = _this2.$L("已超期");
                    } else if (params.row.complete) {
                        color = "";
                        status = _this2.$L("已完成");
                    } else {
                        color = "#19be6b";
                        status = _this2.$L("未完成");
                    }
                    return h('span', {
                        style: {
                            color: color
                        }
                    }, status);
                }
            }, {
                "title": this.$L("创建时间"),
                "key": 'indate',
                "width": 160,
                "sortable": true,
                render: function render(h, params) {
                    return h('span', $A.formatDate("Y-m-d H:i:s", params.row.indate));
                }
            }];
        },
        exportTab: function exportTab() {
            var _this3 = this;

            var whereData = $A.cloneData(this.keys);
            whereData.page = Math.max(this.listPage, 1);
            whereData.pagesize = Math.max($A.runNum(this.listPageSize), 10);
            whereData.projectid = this.projectid;
            whereData.sorts = $A.cloneData(this.sorts);
            whereData.export = 1;
            this.exportLoad++;
            $A.apiAjax({
                url: 'project/task/lists',
                data: whereData,
                complete: function complete() {
                    _this3.exportLoad--;
                },
                error: function error() {
                    alert(_this3.$L('网络繁忙，请稍后再试！'));
                },
                success: function success(res) {
                    if (res.ret === 1) {
                        _this3.$Modal.info({
                            okText: _this3.$L('关闭'),
                            render: function render(h) {
                                return h('div', [h('div', {
                                    style: {
                                        fontSize: '16px',
                                        fontWeight: '500',
                                        marginBottom: '20px'
                                    }
                                }, _this3.$L('导出结果')), h('a', {
                                    attrs: { href: res.data.url, target: '_blank' }
                                }, _this3.$L('点击下载 (%)', res.data.size + " KB"))]);
                            }
                        });
                    } else {
                        _this3.$Modal.error({ title: _this3.$L('温馨提示'), content: res.msg });
                    }
                }
            });
        },
        sreachTab: function sreachTab(clear) {
            if (clear === true) {
                this.keys = {};
            }
            this.getLists(true);
        },
        sortChange: function sortChange(info) {
            this.sorts = { key: info.key, order: info.order };
            this.getLists(true);
        },
        setPage: function setPage(page) {
            this.listPage = page;
            this.getLists();
        },
        setPageSize: function setPageSize(size) {
            if (Math.max($A.runNum(this.listPageSize), 10) != size) {
                this.listPageSize = size;
                this.getLists();
            }
        },
        getLists: function getLists(resetLoad) {
            var _this4 = this;

            if (resetLoad === true) {
                this.listPage = 1;
            }
            if (this.projectid == 0) {
                this.lists = [];
                this.listTotal = 0;
                this.noDataText = this.$L("没有相关的数据");
                return;
            }
            this.loadIng++;
            var whereData = $A.cloneData(this.keys);
            whereData.page = Math.max(this.listPage, 1);
            whereData.pagesize = Math.max($A.runNum(this.listPageSize), 10);
            whereData.projectid = this.projectid;
            whereData.sorts = $A.cloneData(this.sorts);
            this.noDataText = this.$L("数据加载中.....");
            $A.apiAjax({
                url: 'project/task/lists',
                data: whereData,
                complete: function complete() {
                    _this4.loadIng--;
                },
                error: function error() {
                    _this4.noDataText = _this4.$L("数据加载失败！");
                },
                success: function success(res) {
                    if (res.ret === 1) {
                        _this4.lists = res.data.lists;
                        _this4.listTotal = res.data.total;
                        _this4.noDataText = _this4.$L("没有相关的数据");
                    } else {
                        _this4.lists = [];
                        _this4.listTotal = 0;
                        _this4.noDataText = res.msg;
                    }
                }
            });
        }
    }
});

/***/ }),

/***/ 547:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("drawer-tabs-container", [
    _c(
      "div",
      { staticClass: "project-task-lists" },
      [
        _c("Row", { staticClass: "sreachBox" }, [
          _c("div", { staticClass: "item" }, [
            _c(
              "div",
              { staticClass: "item-4" },
              [
                _c("sreachTitle", { attrs: { val: _vm.keys.type } }, [
                  _vm._v(_vm._s(_vm.$L("状态")))
                ]),
                _vm._v(" "),
                _c(
                  "Select",
                  {
                    attrs: { placeholder: _vm.$L("全部") },
                    model: {
                      value: _vm.keys.type,
                      callback: function($$v) {
                        _vm.$set(_vm.keys, "type", $$v)
                      },
                      expression: "keys.type"
                    }
                  },
                  [
                    _c("Option", { attrs: { value: "" } }, [
                      _vm._v(_vm._s(_vm.$L("全部")))
                    ]),
                    _vm._v(" "),
                    _c("Option", { attrs: { value: "未完成" } }, [
                      _vm._v(_vm._s(_vm.$L("未完成")))
                    ]),
                    _vm._v(" "),
                    _c("Option", { attrs: { value: "已超期" } }, [
                      _vm._v(_vm._s(_vm.$L("已超期")))
                    ]),
                    _vm._v(" "),
                    _c("Option", { attrs: { value: "已完成" } }, [
                      _vm._v(_vm._s(_vm.$L("已完成")))
                    ])
                  ],
                  1
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "item-4" },
              [
                _c("sreachTitle", { attrs: { val: _vm.keys.username } }, [
                  _vm._v(_vm._s(_vm.$L("负责人")))
                ]),
                _vm._v(" "),
                _c("Input", {
                  attrs: { placeholder: _vm.$L("用户名") },
                  model: {
                    value: _vm.keys.username,
                    callback: function($$v) {
                      _vm.$set(_vm.keys, "username", $$v)
                    },
                    expression: "keys.username"
                  }
                })
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "item-4" },
              [
                _c("sreachTitle", { attrs: { val: _vm.keys.level } }, [
                  _vm._v(_vm._s(_vm.$L("级别")))
                ]),
                _vm._v(" "),
                _c(
                  "Select",
                  {
                    attrs: { placeholder: _vm.$L("全部") },
                    model: {
                      value: _vm.keys.level,
                      callback: function($$v) {
                        _vm.$set(_vm.keys, "level", $$v)
                      },
                      expression: "keys.level"
                    }
                  },
                  [
                    _c("Option", { attrs: { value: "" } }, [
                      _vm._v(_vm._s(_vm.$L("全部")))
                    ]),
                    _vm._v(" "),
                    _c("Option", { attrs: { value: "1" } }, [_vm._v("P1")]),
                    _vm._v(" "),
                    _c("Option", { attrs: { value: "2" } }, [_vm._v("P2")]),
                    _vm._v(" "),
                    _c("Option", { attrs: { value: "3" } }, [_vm._v("P3")]),
                    _vm._v(" "),
                    _c("Option", { attrs: { value: "4" } }, [_vm._v("P4")])
                  ],
                  1
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "item-4" },
              [
                _c("sreachTitle", { attrs: { val: _vm.keys.labelid } }, [
                  _vm._v(_vm._s(_vm.$L("阶段")))
                ]),
                _vm._v(" "),
                _c(
                  "Select",
                  {
                    attrs: { placeholder: _vm.$L("全部") },
                    model: {
                      value: _vm.keys.labelid,
                      callback: function($$v) {
                        _vm.$set(_vm.keys, "labelid", $$v)
                      },
                      expression: "keys.labelid"
                    }
                  },
                  [
                    _c("Option", { attrs: { value: "" } }, [
                      _vm._v(_vm._s(_vm.$L("全部")))
                    ]),
                    _vm._v(" "),
                    _vm._l(_vm.labelLists, function(item) {
                      return _c(
                        "Option",
                        { key: item.id, attrs: { value: item.id } },
                        [_vm._v(_vm._s(item.title))]
                      )
                    })
                  ],
                  2
                )
              ],
              1
            )
          ]),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "item item-button" },
            [
              _c(
                "Button",
                {
                  staticClass: "left-btn",
                  attrs: {
                    type: "info",
                    icon: "md-swap",
                    loading: _vm.exportLoad > 0
                  },
                  on: { click: _vm.exportTab }
                },
                [_vm._v(_vm._s(_vm.$L("导出列表")))]
              ),
              _vm._v(" "),
              _vm.$A.objImplode(_vm.keys) != ""
                ? _c(
                    "Button",
                    {
                      attrs: { type: "text" },
                      on: {
                        click: function($event) {
                          return _vm.sreachTab(true)
                        }
                      }
                    },
                    [_vm._v(_vm._s(_vm.$L("取消筛选")))]
                  )
                : _vm._e(),
              _vm._v(" "),
              _c(
                "Button",
                {
                  attrs: {
                    type: "primary",
                    icon: "md-search",
                    loading: _vm.loadIng > 0
                  },
                  on: { click: _vm.sreachTab }
                },
                [_vm._v(_vm._s(_vm.$L("搜索")))]
              )
            ],
            1
          )
        ]),
        _vm._v(" "),
        _c("Table", {
          ref: "tableRef",
          staticClass: "tableFill",
          attrs: {
            columns: _vm.columns,
            data: _vm.lists,
            loading: _vm.loadIng > 0,
            "no-data-text": _vm.noDataText,
            stripe: ""
          },
          on: { "on-sort-change": _vm.sortChange }
        }),
        _vm._v(" "),
        _c("Page", {
          staticClass: "pageBox",
          attrs: {
            total: _vm.listTotal,
            current: _vm.listPage,
            disabled: _vm.loadIng > 0,
            "page-size-opts": [10, 20, 30, 50, 100],
            placement: "top",
            "show-elevator": "",
            "show-sizer": "",
            "show-total": "",
            transfer: "",
            simple: _vm.windowMax768
          },
          on: {
            "on-change": _vm.setPage,
            "on-page-size-change": _vm.setPageSize
          }
        })
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-11cf9cb6", module.exports)
  }
}

/***/ }),

/***/ 548:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(549)
}
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(551)
/* template */
var __vue_template__ = __webpack_require__(557)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/main/components/project/gantt/index.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-91cfb088", Component.options)
  } else {
    hotAPI.reload("data-v-91cfb088", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 549:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(550);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(1)("87129f5a", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../../node_modules/css-loader/index.js!../../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-91cfb088\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../../../../node_modules/sass-loader/lib/loader.js!../../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./index.vue", function() {
     var newContent = require("!!../../../../../../../node_modules/css-loader/index.js!../../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-91cfb088\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../../../../node_modules/sass-loader/lib/loader.js!../../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./index.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 550:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)(false);
// imports


// module
exports.push([module.i, "\n.project-gstc-gantt {\n  position: absolute;\n  top: 15px;\n  left: 15px;\n  right: 15px;\n  bottom: 15px;\n  z-index: 1;\n  -webkit-transform: translateZ(0);\n          transform: translateZ(0);\n  background-color: #fdfdfd;\n  border-radius: 3px;\n  overflow: hidden;\n}\n.project-gstc-gantt .project-gstc-dropdown-filtr {\n    position: absolute;\n    top: 38px;\n    left: 222px;\n}\n.project-gstc-gantt .project-gstc-dropdown-filtr .project-gstc-dropdown-icon {\n      cursor: pointer;\n      color: #999;\n      font-size: 20px;\n}\n.project-gstc-gantt .project-gstc-dropdown-filtr .project-gstc-dropdown-icon.filtr {\n        color: #058ce4;\n}\n.project-gstc-gantt .project-gstc-close {\n    position: absolute;\n    top: 8px;\n    left: 12px;\n    cursor: pointer;\n}\n.project-gstc-gantt .project-gstc-close:hover i {\n      -webkit-transform: scale(1) rotate(45deg);\n              transform: scale(1) rotate(45deg);\n}\n.project-gstc-gantt .project-gstc-close i {\n      color: #666666;\n      font-size: 28px;\n      -webkit-transform: scale(0.92);\n              transform: scale(0.92);\n      -webkit-transition: all .2s;\n      transition: all .2s;\n}\n.project-gstc-gantt .project-gstc-edit {\n    position: absolute;\n    bottom: 6px;\n    right: 6px;\n    background: #ffffff;\n    border-radius: 4px;\n    opacity: 0;\n    -webkit-transform: translate(120%, 0);\n            transform: translate(120%, 0);\n    -webkit-transition: all 0.2s;\n    transition: all 0.2s;\n}\n.project-gstc-gantt .project-gstc-edit.visible {\n      opacity: 1;\n      -webkit-transform: translate(0, 0);\n              transform: translate(0, 0);\n}\n.project-gstc-gantt .project-gstc-edit.info .project-gstc-edit-info {\n      display: block;\n}\n.project-gstc-gantt .project-gstc-edit.info .project-gstc-edit-small {\n      display: none;\n}\n.project-gstc-gantt .project-gstc-edit .project-gstc-edit-info {\n      display: none;\n      border: 1px solid #e4e4e4;\n      background: #ffffff;\n      padding: 6px;\n      border-radius: 4px;\n      width: 500px;\n}\n.project-gstc-gantt .project-gstc-edit .project-gstc-edit-info .project-gstc-edit-btns {\n        margin: 12px 6px 4px;\n        display: -webkit-box;\n        display: -ms-flexbox;\n        display: flex;\n        -webkit-box-align: center;\n            -ms-flex-align: center;\n                align-items: center;\n        -webkit-box-pack: end;\n            -ms-flex-pack: end;\n                justify-content: flex-end;\n}\n.project-gstc-gantt .project-gstc-edit .project-gstc-edit-info .project-gstc-edit-btns .ivu-btn {\n          margin-right: 8px;\n          font-size: 13px;\n}\n.project-gstc-gantt .project-gstc-edit .project-gstc-edit-info .project-gstc-edit-btns .zoom {\n          font-size: 20px;\n          color: #444444;\n          cursor: pointer;\n}\n.project-gstc-gantt .project-gstc-edit .project-gstc-edit-info .project-gstc-edit-btns .zoom:hover {\n            color: #57a3f3;\n}\n.project-gstc-gantt .project-gstc-edit .project-gstc-edit-small {\n      border: 1px solid #e4e4e4;\n      background: #ffffff;\n      padding: 6px 12px;\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-align: center;\n          -ms-flex-align: center;\n              align-items: center;\n}\n.project-gstc-gantt .project-gstc-edit .project-gstc-edit-small .project-gstc-edit-text {\n        cursor: pointer;\n        text-decoration: underline;\n        color: #444444;\n        margin-right: 8px;\n}\n.project-gstc-gantt .project-gstc-edit .project-gstc-edit-small .project-gstc-edit-text:hover {\n          color: #57a3f3;\n}\n.project-gstc-gantt .project-gstc-edit .project-gstc-edit-small .ivu-btn {\n        margin-left: 4px;\n        font-size: 13px;\n}\n", ""]);

// exports


/***/ }),

/***/ 551:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__gantt_index__ = __webpack_require__(552);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__gantt_index___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__gantt_index__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/**
 * 甘特图
 */
/* harmony default export */ __webpack_exports__["default"] = ({
    name: 'ProjectGantt',
    components: { GanttView: __WEBPACK_IMPORTED_MODULE_0__gantt_index___default.a },
    props: {
        projectLabel: {
            default: []
        }
    },

    data: function data() {
        return {
            loadFinish: false,

            lists: [],

            editColumns: [],
            editData: [],
            editShowInfo: false,
            editLoad: 0,

            filtrProjectId: 0
        };
    },
    mounted: function mounted() {
        this.editColumns = [{
            title: this.$L('任务名称'),
            key: 'label',
            minWidth: 150,
            ellipsis: true
        }, {
            title: this.$L('原计划时间'),
            minWidth: 135,
            align: 'center',
            render: function render(h, params) {
                if (params.row.notime === true) {
                    return h('span', '-');
                }
                return h('div', {
                    style: {}
                }, [h('div', $A.formatDate('Y-m-d H:i', Math.round(params.row.backTime.start / 1000))), h('div', $A.formatDate('Y-m-d H:i', Math.round(params.row.backTime.end / 1000)))]);
            }
        }, {
            title: this.$L('新计划时间'),
            minWidth: 135,
            align: 'center',
            render: function render(h, params) {
                return h('div', {
                    style: {}
                }, [h('div', $A.formatDate('Y-m-d H:i', Math.round(params.row.newTime.start / 1000))), h('div', $A.formatDate('Y-m-d H:i', Math.round(params.row.newTime.end / 1000)))]);
            }
        }];
        //
        this.initData();
        this.loadFinish = true;
    },


    watch: {
        projectLabel: {
            handler: function handler() {
                this.initData();
            },

            deep: true
        }
    },

    methods: {
        initData: function initData() {
            var _this = this;

            this.lists = [];
            this.projectLabel.forEach(function (item) {
                if (_this.filtrProjectId > 0) {
                    if (item.id != _this.filtrProjectId) {
                        return;
                    }
                }
                item.taskLists.forEach(function (taskData) {
                    var notime = taskData.startdate == 0 || taskData.enddate == 0;
                    var times = _this.getTimeObj(taskData);
                    var start = times.start;
                    var end = times.end;
                    //
                    var color = '#058ce4';
                    if (taskData.complete) {
                        color = '#c1c1c1';
                    } else {
                        if (taskData.level === 1) {
                            color = '#ff0000';
                        } else if (taskData.level === 2) {
                            color = '#BB9F35';
                        } else if (taskData.level === 3) {
                            color = '#449EDD';
                        } else if (taskData.level === 4) {
                            color = '#84A83B';
                        }
                    }
                    //
                    var tempTime = { start: start, end: end };
                    var findData = _this.editData.find(function (t) {
                        return t.id == taskData.id;
                    });
                    if (findData) {
                        findData.backTime = $A.cloneData(tempTime);
                        tempTime = $A.cloneData(findData.newTime);
                    }
                    //
                    _this.lists.push({
                        id: taskData.id,
                        label: taskData.title,
                        time: tempTime,
                        notime: notime,
                        style: { background: color }
                    });
                });
            });
            //
            if (this.lists.length == 0 && this.filtrProjectId == 0) {
                this.$Modal.warning({
                    title: this.$L("温馨提示"),
                    content: this.$L('任务列表为空，请先添加任务。'),
                    onOk: function onOk() {
                        _this.$emit('on-close');
                    }
                });
            }
        },
        updateTime: function updateTime(item) {
            var original = this.getRawTime(item.id);
            if (Math.abs(original.end - item.time.end) > 1000 || Math.abs(original.start - item.time.start) > 1000) {
                //修改时间（变化超过1秒钟)
                var backTime = $A.cloneData(original);
                var newTime = $A.cloneData(item.time);
                var findData = this.editData.find(function (_ref) {
                    var id = _ref.id;
                    return id == item.id;
                });
                if (findData) {
                    findData.newTime = newTime;
                } else {
                    this.editData.push({
                        id: item.id,
                        label: item.label,
                        notime: item.notime,
                        backTime: backTime,
                        newTime: newTime
                    });
                }
            }
        },
        clickItem: function clickItem(item) {
            this.taskDetail(item.id);
        },
        editSubmit: function editSubmit(save) {
            var _this2 = this;

            var triggerTask = [];
            this.editData.forEach(function (item) {
                if (save) {
                    _this2.editLoad++;
                    var timeStart = $A.formatDate('Y-m-d H:i', Math.round(item.newTime.start / 1000));
                    var timeEnd = $A.formatDate('Y-m-d H:i', Math.round(item.newTime.end / 1000));
                    var ajaxData = {
                        act: 'plannedtime',
                        taskid: item.id,
                        content: timeStart + "," + timeEnd
                    };
                    $A.apiAjax({
                        url: 'project/task/edit',
                        method: 'post',
                        data: ajaxData,
                        error: function error() {
                            _this2.lists.some(function (task) {
                                if (task.id == item.id) {
                                    _this2.$set(task, 'time', item.backTime);
                                    return true;
                                }
                            });
                        },
                        success: function success(res) {
                            if (res.ret === 1) {
                                triggerTask.push({
                                    status: 'await',
                                    act: ajaxData.act,
                                    taskid: ajaxData.taskid,
                                    data: res.data
                                });
                            } else {
                                _this2.lists.some(function (task) {
                                    if (task.id == item.id) {
                                        _this2.$set(task, 'time', item.backTime);
                                        return true;
                                    }
                                });
                            }
                        },
                        afterComplete: function afterComplete() {
                            _this2.editLoad--;
                            if (_this2.editLoad <= 0) {
                                triggerTask.forEach(function (info) {
                                    if (info.status == 'await') {
                                        info.status = 'trigger';
                                        $A.triggerTaskInfoListener(info.act, info.data);
                                        $A.triggerTaskInfoChange(info.taskid);
                                    }
                                });
                            }
                        }
                    });
                } else {
                    _this2.lists.some(function (task) {
                        if (task.id == item.id) {
                            _this2.$set(task, 'time', item.backTime);
                            return true;
                        }
                    });
                }
            });
            this.editData = [];
        },
        getRawTime: function getRawTime(taskId) {
            var _this3 = this;

            var times = null;
            this.projectLabel.some(function (item) {
                item.taskLists.some(function (taskData) {
                    if (taskData.id == taskId) {
                        times = _this3.getTimeObj(taskData);
                        return true;
                    }
                });
                if (times) {
                    return true;
                }
            });
            return times;
        },
        getTimeObj: function getTimeObj(taskData) {
            var start = taskData.startdate || taskData.indate;
            var end = taskData.enddate || taskData.indate + 86400;
            if (end == start) {
                end = Math.round(new Date($A.formatDate('Y-m-d 23:59:59', end)).getTime() / 1000);
            }
            end = Math.max(end, start + 60);
            start *= 1000;
            end *= 1000;
            return { start: start, end: end };
        },
        tapProject: function tapProject(e) {
            this.filtrProjectId = $A.runNum(e);
            this.initData();
        }
    }
});

/***/ }),

/***/ 552:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(553)
}
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(555)
/* template */
var __vue_template__ = __webpack_require__(556)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-ab75349c"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/main/components/gantt/index.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-ab75349c", Component.options)
  } else {
    hotAPI.reload("data-v-ab75349c", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 553:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(554);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(1)("1f2ff96e", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../node_modules/css-loader/index.js!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-ab75349c\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/sass-loader/lib/loader.js!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./index.vue", function() {
     var newContent = require("!!../../../../../../node_modules/css-loader/index.js!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-ab75349c\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/sass-loader/lib/loader.js!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./index.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 554:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)(false);
// imports


// module
exports.push([module.i, "\n.wook-gantt[data-v-ab75349c] {\n  position: absolute;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: row;\n          flex-direction: row;\n  -webkit-box-align: self-start;\n      -ms-flex-align: self-start;\n          align-items: self-start;\n  color: #747a81;\n}\n.wook-gantt *[data-v-ab75349c] {\n    -webkit-box-sizing: border-box;\n            box-sizing: border-box;\n}\n.wook-gantt .gantt-left[data-v-ab75349c] {\n    -webkit-box-flex: 0;\n        -ms-flex-positive: 0;\n            flex-grow: 0;\n    -ms-flex-negative: 0;\n        flex-shrink: 0;\n    height: 100%;\n    background-color: #ffffff;\n    position: relative;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n}\n.wook-gantt .gantt-left[data-v-ab75349c]:after {\n      content: \"\";\n      position: absolute;\n      top: 0;\n      right: 0;\n      bottom: 0;\n      width: 1px;\n      background-color: rgba(237, 241, 242, 0.75);\n}\n.wook-gantt .gantt-left .gantt-title[data-v-ab75349c] {\n      height: 76px;\n      -webkit-box-flex: 0;\n          -ms-flex-positive: 0;\n              flex-grow: 0;\n      -ms-flex-negative: 0;\n          flex-shrink: 0;\n      background-color: #F9FAFB;\n      padding-left: 12px;\n      overflow: hidden;\n}\n.wook-gantt .gantt-left .gantt-title .gantt-title-text[data-v-ab75349c] {\n        line-height: 100px;\n        max-width: 200px;\n        overflow: hidden;\n        text-overflow: ellipsis;\n        white-space: nowrap;\n        font-weight: 600;\n}\n.wook-gantt .gantt-left .gantt-item[data-v-ab75349c] {\n      -webkit-transform: translateZ(0);\n              transform: translateZ(0);\n      max-height: 100%;\n      overflow: auto;\n      -ms-overflow-style: none;\n}\n.wook-gantt .gantt-left .gantt-item[data-v-ab75349c]::-webkit-scrollbar {\n        display: none;\n}\n.wook-gantt .gantt-left .gantt-item > li[data-v-ab75349c] {\n        height: 40px;\n        border-bottom: 1px solid rgba(237, 241, 242, 0.75);\n        position: relative;\n        display: -webkit-box;\n        display: -ms-flexbox;\n        display: flex;\n        -webkit-box-align: center;\n            -ms-flex-align: center;\n                align-items: center;\n}\n.wook-gantt .gantt-left .gantt-item > li:hover .item-icon[data-v-ab75349c] {\n          display: -webkit-box;\n          display: -ms-flexbox;\n          display: flex;\n}\n.wook-gantt .gantt-left .gantt-item > li .item-title[data-v-ab75349c] {\n          -webkit-box-flex: 1;\n              -ms-flex: 1;\n                  flex: 1;\n          padding: 0 12px;\n          cursor: default;\n          overflow: hidden;\n          text-overflow: ellipsis;\n          white-space: nowrap;\n}\n.wook-gantt .gantt-left .gantt-item > li .item-icon[data-v-ab75349c] {\n          display: none;\n          -webkit-box-align: center;\n              -ms-flex-align: center;\n                  align-items: center;\n          -webkit-box-pack: center;\n              -ms-flex-pack: center;\n                  justify-content: center;\n          width: 32px;\n          margin-right: 2px;\n          font-size: 16px;\n          color: #888888;\n}\n.wook-gantt .gantt-right[data-v-ab75349c] {\n    -webkit-box-flex: 1;\n        -ms-flex: 1;\n            flex: 1;\n    height: 100%;\n    background-color: #ffffff;\n    position: relative;\n    overflow: hidden;\n}\n.wook-gantt .gantt-right .gantt-chart[data-v-ab75349c] {\n      position: absolute;\n      top: 0;\n      left: 0;\n      right: 0;\n      bottom: 0;\n      -webkit-transform: translateZ(0);\n              transform: translateZ(0);\n}\n.wook-gantt .gantt-right .gantt-chart .gantt-month[data-v-ab75349c] {\n        display: -webkit-box;\n        display: -ms-flexbox;\n        display: flex;\n        -webkit-box-align: center;\n            -ms-flex-align: center;\n                align-items: center;\n        position: absolute;\n        top: 0;\n        left: 0;\n        right: 0;\n        z-index: 1;\n        height: 26px;\n        line-height: 20px;\n        font-size: 14px;\n        background-color: #F9FAFB;\n}\n.wook-gantt .gantt-right .gantt-chart .gantt-month > li[data-v-ab75349c] {\n          -webkit-box-flex: 0;\n              -ms-flex-positive: 0;\n                  flex-grow: 0;\n          -ms-flex-negative: 0;\n              flex-shrink: 0;\n          height: 100%;\n          position: relative;\n          overflow: hidden;\n}\n.wook-gantt .gantt-right .gantt-chart .gantt-month > li[data-v-ab75349c]:after {\n            content: \"\";\n            position: absolute;\n            top: 0;\n            right: 0;\n            width: 1px;\n            height: 100%;\n            background-color: rgba(237, 241, 242, 0.75);\n}\n.wook-gantt .gantt-right .gantt-chart .gantt-month > li .month-format[data-v-ab75349c] {\n            overflow: hidden;\n            white-space: nowrap;\n            padding: 6px 6px 0;\n}\n.wook-gantt .gantt-right .gantt-chart .gantt-date[data-v-ab75349c] {\n        display: -webkit-box;\n        display: -ms-flexbox;\n        display: flex;\n        -webkit-box-align: center;\n            -ms-flex-align: center;\n                align-items: center;\n        position: absolute;\n        top: 26px;\n        left: 0;\n        right: 0;\n        bottom: 0;\n        z-index: 2;\n        cursor: move;\n}\n.wook-gantt .gantt-right .gantt-chart .gantt-date[data-v-ab75349c]:before {\n          content: \"\";\n          position: absolute;\n          top: 0;\n          left: 0;\n          right: 0;\n          height: 50px;\n          background-color: #F9FAFB;\n}\n.wook-gantt .gantt-right .gantt-chart .gantt-date > li[data-v-ab75349c] {\n          -webkit-box-flex: 0;\n              -ms-flex-positive: 0;\n                  flex-grow: 0;\n          -ms-flex-negative: 0;\n              flex-shrink: 0;\n          height: 100%;\n          position: relative;\n          overflow: hidden;\n}\n.wook-gantt .gantt-right .gantt-chart .gantt-date > li[data-v-ab75349c]:after {\n            content: \"\";\n            position: absolute;\n            top: 0;\n            right: 0;\n            width: 1px;\n            height: 100%;\n            background-color: rgba(237, 241, 242, 0.75);\n}\n.wook-gantt .gantt-right .gantt-chart .gantt-date > li .date-format[data-v-ab75349c] {\n            overflow: hidden;\n            white-space: nowrap;\n            display: -webkit-box;\n            display: -ms-flexbox;\n            display: flex;\n            -webkit-box-orient: vertical;\n            -webkit-box-direction: normal;\n                -ms-flex-direction: column;\n                    flex-direction: column;\n            -webkit-box-align: center;\n                -ms-flex-align: center;\n                    align-items: center;\n            -webkit-box-pack: center;\n                -ms-flex-pack: center;\n                    justify-content: center;\n            height: 44px;\n}\n.wook-gantt .gantt-right .gantt-chart .gantt-date > li .date-format .format-day[data-v-ab75349c] {\n              line-height: 28px;\n              font-size: 18px;\n}\n.wook-gantt .gantt-right .gantt-chart .gantt-date > li .date-format .format-wook[data-v-ab75349c] {\n              line-height: 16px;\n              font-weight: 300;\n              font-size: 13px;\n}\n.wook-gantt .gantt-right .gantt-chart .gantt-timeline[data-v-ab75349c] {\n        position: absolute;\n        top: 76px;\n        left: 0;\n        right: 0;\n        bottom: 0;\n        z-index: 3;\n        overflow-x: hidden;\n        overflow-y: auto;\n}\n.wook-gantt .gantt-right .gantt-chart .gantt-timeline > li[data-v-ab75349c] {\n          cursor: default;\n          height: 40px;\n          border-bottom: 1px solid rgba(237, 241, 242, 0.75);\n          position: relative;\n}\n.wook-gantt .gantt-right .gantt-chart .gantt-timeline > li .timeline-item[data-v-ab75349c] {\n            position: absolute;\n            top: 0;\n            -ms-touch-action: none;\n                touch-action: none;\n            pointer-events: auto;\n            padding: 4px;\n            margin-top: 4px;\n            background: #e74c3c;\n            border-radius: 18px;\n            color: #fff;\n            display: -webkit-box;\n            display: -ms-flexbox;\n            display: flex;\n            -webkit-box-align: center;\n                -ms-flex-align: center;\n                    align-items: center;\n            will-change: contents;\n            height: 32px;\n}\n.wook-gantt .gantt-right .gantt-chart .gantt-timeline > li .timeline-item .timeline-title[data-v-ab75349c] {\n              -ms-touch-action: none;\n                  touch-action: none;\n              -webkit-box-flex: 1;\n                  -ms-flex-positive: 1;\n                      flex-grow: 1;\n              overflow: hidden;\n              text-overflow: ellipsis;\n              white-space: nowrap;\n              margin-left: 4px;\n              margin-right: 10px;\n}\n.wook-gantt .gantt-right .gantt-chart .gantt-timeline > li .timeline-item .timeline-resizer[data-v-ab75349c] {\n              height: 22px;\n              -ms-touch-action: none;\n                  touch-action: none;\n              width: 8px;\n              background: rgba(255, 255, 255, 0.1);\n              cursor: ew-resize;\n              -ms-flex-negative: 0;\n                  flex-shrink: 0;\n              will-change: visibility;\n              position: absolute;\n              top: 5px;\n              right: 5px;\n}\n", ""]);

// exports


/***/ }),

/***/ 555:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: 'GanttView',
    props: {
        lists: {
            type: Array
        },
        menuWidth: {
            type: Number,
            default: 300
        },
        itemWidth: {
            type: Number,
            default: 100
        }
    },
    data: function data() {
        return {
            mouseType: '',
            mouseWidth: 0,
            mouseScaleWidth: 0,

            dateWidth: 100,
            ganttWidth: 0,

            mouseItem: null,
            mouseBak: {},

            dateMove: null
        };
    },
    mounted: function mounted() {
        this.dateWidth = this.itemWidth;
        this.$refs.ganttRight.addEventListener('mousewheel', this.handleScroll, false);
        document.addEventListener('mousemove', this.itemMouseMove);
        document.addEventListener('mouseup', this.itemMouseUp);
        window.addEventListener("resize", this.handleResize, false);
        this.handleResize();
    },
    beforeDestroy: function beforeDestroy() {
        this.$refs.ganttRight.removeEventListener('mousewheel', this.handleScroll, false);
        document.removeEventListener('mousemove', this.itemMouseMove);
        document.removeEventListener('mouseup', this.itemMouseUp);
        window.removeEventListener("resize", this.handleResize, false);
    },

    watch: {
        itemWidth: function itemWidth(val) {
            this.dateWidth = val;
        }
    },
    computed: {
        monthNum: function monthNum() {
            var ganttWidth = this.ganttWidth,
                dateWidth = this.dateWidth;

            return Math.floor(ganttWidth / dateWidth / 30) + 2;
        },
        monthStyle: function monthStyle() {
            var mouseWidth = this.mouseWidth,
                dateWidth = this.dateWidth;

            return function (index) {
                var mouseDay = mouseWidth == 0 ? 0 : mouseWidth / dateWidth;
                var date = new Date();
                //今天00:00:00
                var nowDay = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0);
                //当前时间
                var curDay = new Date(nowDay.getTime() + mouseDay * 86400000);
                //当月最后一天
                var lastDay = new Date(curDay.getFullYear(), curDay.getMonth() + 1, 0, 23, 59, 59);
                //相差天数
                var diffDay = (lastDay - curDay) / 1000 / 60 / 60 / 24;
                //
                var width = dateWidth * diffDay;
                if (index > 0) {
                    lastDay = new Date(curDay.getFullYear(), curDay.getMonth() + 1 + index, 0);
                    width = lastDay.getDate() * dateWidth;
                }
                return {
                    width: width + 'px'
                };
            };
        },
        monthFormat: function monthFormat() {
            var mouseWidth = this.mouseWidth,
                dateWidth = this.dateWidth;

            return function (index) {
                var mouseDay = mouseWidth == 0 ? 0 : mouseWidth / dateWidth;
                var date = new Date();
                //开始位置时间（今天00:00:00）
                var nowDay = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0);
                //当前时间
                var curDay = new Date(nowDay.getTime() + mouseDay * 86400000);
                //
                if (index > 0) {
                    curDay = new Date(curDay.getFullYear(), curDay.getMonth() + 1 + index, 0);
                }
                return $A.formatDate("Y-m", curDay);
            };
        },
        dateNum: function dateNum() {
            var ganttWidth = this.ganttWidth,
                dateWidth = this.dateWidth;

            return Math.floor(ganttWidth / dateWidth) + 2;
        },
        dateStyle: function dateStyle() {
            var mouseWidth = this.mouseWidth,
                dateWidth = this.dateWidth;

            return function (index) {
                var style = {};
                //
                var mouseDay = mouseWidth == 0 ? 0 : mouseWidth / dateWidth;
                var mouseData = Math.floor(mouseDay) + index;
                if (mouseDay == Math.floor(mouseDay)) {
                    mouseData--;
                }
                var j = mouseWidth == 0 ? index - 1 : mouseData;
                var date = new Date(new Date().getTime() + j * 86400000);
                if ([0, 6].indexOf(date.getDay()) !== -1) {
                    style.backgroundColor = '#f9fafb';
                }
                //
                var width = dateWidth;
                if (index == 0) {
                    width = Math.abs((mouseWidth % width - width) % width);
                }
                style.width = width + 'px';
                return style;
            };
        },
        dateFormat: function dateFormat() {
            var mouseWidth = this.mouseWidth,
                dateWidth = this.dateWidth;

            return function (index, type) {
                var mouseDay = mouseWidth == 0 ? 0 : mouseWidth / dateWidth;
                var mouseData = Math.floor(mouseDay) + index;
                if (mouseDay == Math.floor(mouseDay)) {
                    mouseData--;
                }
                var j = mouseWidth == 0 ? index - 1 : mouseData;
                var date = new Date(new Date().getTime() + j * 86400000);
                if (type == 'day') {
                    return date.getDate();
                } else if (type == 'wook') {
                    return this.$L('\u661F\u671F' + '日一二三四五六'.charAt(date.getDay()));
                } else {
                    return date;
                }
            };
        },
        itemStyle: function itemStyle() {
            var mouseWidth = this.mouseWidth,
                dateWidth = this.dateWidth,
                ganttWidth = this.ganttWidth;

            return function (item) {
                var _item$time = item.time,
                    start = _item$time.start,
                    end = _item$time.end;
                var style = item.style,
                    moveX = item.moveX,
                    moveW = item.moveW;

                var date = new Date();
                //开始位置时间戳（今天00:00:00时间戳）
                var nowTime = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0).getTime();
                //距离开始位置多少天
                var diffStartDay = (start - nowTime) / 1000 / 60 / 60 / 24;
                var diffEndDay = (end - nowTime) / 1000 / 60 / 60 / 24;
                //
                var left = dateWidth * diffStartDay + mouseWidth * -1;
                var width = dateWidth * (diffEndDay - diffStartDay);
                if (typeof moveX === "number") {
                    left += moveX;
                }
                if (typeof moveW === "number") {
                    width += moveW;
                }
                //
                var customStyle = {
                    left: Math.min(Math.max(left, width * -1.2), ganttWidth * 1.2).toFixed(2) + 'px',
                    width: width.toFixed(2) + 'px'
                };
                if (left < 0 && Math.abs(left) < width) {
                    customStyle.paddingLeft = Math.abs(left).toFixed(2) + 'px';
                }
                if (left + width > ganttWidth && left < ganttWidth) {
                    customStyle.paddingRight = Math.abs(left + width - ganttWidth).toFixed(2) + 'px';
                }
                if ((typeof style === 'undefined' ? 'undefined' : _typeof(style)) === "object") {
                    return Object.assign(customStyle, style);
                }
                return customStyle;
            };
        }
    },
    methods: {
        itemScrollListener: function itemScrollListener(e) {
            if (this.mouseType == 'timeline') {
                return;
            }
            this.$refs.ganttTimeline.scrollTop = e.target.scrollTop;
        },
        timelineScrollListener: function timelineScrollListener(e) {
            if (this.mouseType == 'item') {
                return;
            }
            this.$refs.ganttItem.scrollTop = e.target.scrollTop;
        },
        handleScroll: function handleScroll(e) {
            e.preventDefault();
            if (e.ctrlKey) {
                //缩放
                this.dateWidth = Math.min(600, Math.max(24, this.dateWidth - Math.floor(e.deltaY)));
                this.mouseWidth = this.ganttWidth / 2 * ((this.dateWidth - 100) / 100) + this.dateWidth / 100 * this.mouseScaleWidth;
                return;
            }
            if (e.deltaY != 0) {
                var ganttTimeline = this.$refs.ganttTimeline;
                var newTop = ganttTimeline.scrollTop + e.deltaY;
                if (newTop < 0) {
                    newTop = 0;
                } else if (newTop > ganttTimeline.scrollHeight - ganttTimeline.clientHeight) {
                    newTop = ganttTimeline.scrollHeight - ganttTimeline.clientHeight;
                }
                if (ganttTimeline.scrollTop != newTop) {
                    this.mouseType = 'timeline';
                    ganttTimeline.scrollTop = newTop;
                }
            }
            if (e.deltaX != 0) {
                this.mouseWidth += e.deltaX;
                this.mouseScaleWidth += e.deltaX * (100 / this.dateWidth);
            }
        },
        handleResize: function handleResize() {
            this.ganttWidth = this.$refs.ganttTimeline.clientWidth;
        },
        dateMouseDown: function dateMouseDown(e) {
            e.preventDefault();
            this.mouseItem = null;
            this.dateMove = {
                clientX: e.clientX
            };
        },
        itemMouseDown: function itemMouseDown(e, item) {
            e.preventDefault();
            var type = 'moveX';
            if (e.target.className == 'timeline-resizer') {
                type = 'moveW';
            }
            if (typeof item[type] !== "number") {
                this.$set(item, type, 0);
            }
            this.mouseBak = {
                type: type,
                clientX: e.clientX,
                value: item[type]
            };
            this.mouseItem = item;
            this.dateMove = null;
        },
        itemMouseMove: function itemMouseMove(e) {
            if (this.mouseItem != null) {
                e.preventDefault();
                var diff = e.clientX - this.mouseBak.clientX;
                this.$set(this.mouseItem, this.mouseBak.type, this.mouseBak.value + diff);
            } else if (this.dateMove != null) {
                e.preventDefault();
                var moveX = (this.dateMove.clientX - e.clientX) * 5;
                this.dateMove.clientX = e.clientX;
                this.mouseWidth += moveX;
                this.mouseScaleWidth += moveX * (100 / this.dateWidth);
            }
        },
        itemMouseUp: function itemMouseUp(e) {
            if (this.mouseItem != null) {
                var _mouseItem$time = this.mouseItem.time,
                    start = _mouseItem$time.start,
                    end = _mouseItem$time.end;

                var isM = false;
                //一个宽度的时间
                var oneWidthTime = 86400000 / this.dateWidth;
                //修改起止时间
                if (typeof this.mouseItem.moveX === "number" && this.mouseItem.moveX != 0) {
                    var moveTime = this.mouseItem.moveX * oneWidthTime;
                    this.$set(this.mouseItem.time, 'start', start + moveTime);
                    this.$set(this.mouseItem.time, 'end', end + moveTime);
                    this.$set(this.mouseItem, 'moveX', 0);
                    isM = true;
                }
                //修改结束时间
                if (typeof this.mouseItem.moveW === "number" && this.mouseItem.moveW != 0) {
                    var _moveTime = this.mouseItem.moveW * oneWidthTime;
                    this.$set(this.mouseItem.time, 'end', end + _moveTime);
                    this.$set(this.mouseItem, 'moveW', 0);
                    isM = true;
                }
                //
                if (isM) {
                    this.$emit("on-change", this.mouseItem);
                } else if (e.target.className == 'timeline-title') {
                    this.clickItem(this.mouseItem);
                }
                this.mouseItem = null;
            } else if (this.dateMove != null) {
                this.dateMove = null;
            }
        },
        scrollPosition: function scrollPosition(pos) {
            var date = new Date();
            //今天00:00:00
            var nowDay = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0);
            //一个宽度的时间
            var oneWidthTime = 86400000 / this.dateWidth;
            //
            var moveWidth = (this.lists[pos].time.start - nowDay) / oneWidthTime - this.dateWidth - this.mouseWidth;
            this.mouseWidth += moveWidth;
            this.mouseScaleWidth += moveWidth * (100 / this.dateWidth);
        },
        clickItem: function clickItem(item) {
            this.$emit("on-click", item);
        }
    }
});

/***/ }),

/***/ 556:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "wook-gantt" }, [
    _c(
      "div",
      { staticClass: "gantt-left", style: { width: _vm.menuWidth + "px" } },
      [
        _c("div", { staticClass: "gantt-title" }, [
          _c("div", { staticClass: "gantt-title-text" }, [
            _vm._v(_vm._s(_vm.$L("任务名称")))
          ])
        ]),
        _vm._v(" "),
        _c(
          "ul",
          {
            ref: "ganttItem",
            staticClass: "gantt-item",
            on: {
              scroll: _vm.itemScrollListener,
              mouseenter: function($event) {
                _vm.mouseType = "item"
              }
            }
          },
          _vm._l(_vm.lists, function(item, key) {
            return _c(
              "li",
              { key: key },
              [
                _c(
                  "div",
                  {
                    staticClass: "item-title",
                    on: {
                      click: function($event) {
                        return _vm.clickItem(item)
                      }
                    }
                  },
                  [_vm._v(_vm._s(item.label))]
                ),
                _vm._v(" "),
                _c("Icon", {
                  staticClass: "item-icon",
                  attrs: { type: "ios-locate-outline" },
                  on: {
                    click: function($event) {
                      return _vm.scrollPosition(key)
                    }
                  }
                })
              ],
              1
            )
          }),
          0
        )
      ]
    ),
    _vm._v(" "),
    _c("div", { ref: "ganttRight", staticClass: "gantt-right" }, [
      _c("div", { staticClass: "gantt-chart" }, [
        _c(
          "ul",
          { staticClass: "gantt-month" },
          _vm._l(_vm.monthNum, function(item, key) {
            return _c("li", { key: key, style: _vm.monthStyle(key) }, [
              _c("div", { staticClass: "month-format" }, [
                _vm._v(_vm._s(_vm.monthFormat(key)))
              ])
            ])
          }),
          0
        ),
        _vm._v(" "),
        _c(
          "ul",
          { staticClass: "gantt-date", on: { mousedown: _vm.dateMouseDown } },
          _vm._l(_vm.dateNum, function(item, key) {
            return _c("li", { key: key, style: _vm.dateStyle(key) }, [
              _c("div", { staticClass: "date-format" }, [
                _c("div", { staticClass: "format-day" }, [
                  _vm._v(_vm._s(_vm.dateFormat(key, "day")))
                ]),
                _vm._v(" "),
                _vm.dateWidth > 46
                  ? _c("div", { staticClass: "format-wook" }, [
                      _vm._v(_vm._s(_vm.dateFormat(key, "wook")))
                    ])
                  : _vm._e()
              ])
            ])
          }),
          0
        ),
        _vm._v(" "),
        _c(
          "ul",
          {
            ref: "ganttTimeline",
            staticClass: "gantt-timeline",
            on: {
              scroll: _vm.timelineScrollListener,
              mouseenter: function($event) {
                _vm.mouseType = "timeline"
              }
            }
          },
          _vm._l(_vm.lists, function(item, key) {
            return _c("li", { key: key }, [
              _c(
                "div",
                {
                  staticClass: "timeline-item",
                  style: _vm.itemStyle(item),
                  on: {
                    mousedown: function($event) {
                      return _vm.itemMouseDown($event, item)
                    }
                  }
                },
                [
                  _c("div", { staticClass: "timeline-title" }, [
                    _vm._v(_vm._s(item.label))
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "timeline-resizer" })
                ]
              )
            ])
          }),
          0
        )
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-ab75349c", module.exports)
  }
}

/***/ }),

/***/ 557:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "project-gstc-gantt" },
    [
      _c("GanttView", {
        attrs: {
          lists: _vm.lists,
          menuWidth: _vm.windowMax768 ? 180 : 260,
          itemWidth: 80
        },
        on: { "on-change": _vm.updateTime, "on-click": _vm.clickItem }
      }),
      _vm._v(" "),
      _c(
        "Dropdown",
        {
          staticClass: "project-gstc-dropdown-filtr",
          style: _vm.windowMax768 ? { left: "142px" } : {},
          on: { "on-click": _vm.tapProject }
        },
        [
          _c("Icon", {
            staticClass: "project-gstc-dropdown-icon",
            class: { filtr: _vm.filtrProjectId > 0 },
            attrs: { type: "md-funnel" }
          }),
          _vm._v(" "),
          _c(
            "DropdownMenu",
            { attrs: { slot: "list" }, slot: "list" },
            [
              _c(
                "DropdownItem",
                {
                  class: { "dropdown-active": _vm.filtrProjectId == 0 },
                  attrs: { name: 0 }
                },
                [_vm._v(_vm._s(_vm.$L("全部")))]
              ),
              _vm._v(" "),
              _vm._l(_vm.projectLabel, function(item, index) {
                return _c(
                  "DropdownItem",
                  {
                    key: index,
                    class: { "dropdown-active": _vm.filtrProjectId == item.id },
                    attrs: { name: item.id }
                  },
                  [
                    _vm._v(
                      _vm._s(item.title) +
                        " (" +
                        _vm._s(item.taskLists.length) +
                        ")"
                    )
                  ]
                )
              })
            ],
            2
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass: "project-gstc-close",
          on: {
            click: function($event) {
              return _vm.$emit("on-close")
            }
          }
        },
        [_c("Icon", { attrs: { type: "md-close" } })],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass: "project-gstc-edit",
          class: { info: _vm.editShowInfo, visible: _vm.editData.length > 0 }
        },
        [
          _c(
            "div",
            { staticClass: "project-gstc-edit-info" },
            [
              _c("Table", {
                staticClass: "tableFill",
                attrs: {
                  size: "small",
                  "max-height": "600",
                  columns: _vm.editColumns,
                  data: _vm.editData
                }
              }),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "project-gstc-edit-btns" },
                [
                  _c(
                    "Button",
                    {
                      attrs: {
                        loading: _vm.editLoad > 0,
                        size: "small",
                        type: "text"
                      },
                      on: {
                        click: function($event) {
                          return _vm.editSubmit(false)
                        }
                      }
                    },
                    [_vm._v(_vm._s(_vm.$L("取消")))]
                  ),
                  _vm._v(" "),
                  _c(
                    "Button",
                    {
                      attrs: {
                        loading: _vm.editLoad > 0,
                        size: "small",
                        type: "primary"
                      },
                      on: {
                        click: function($event) {
                          return _vm.editSubmit(true)
                        }
                      }
                    },
                    [_vm._v(_vm._s(_vm.$L("保存")))]
                  ),
                  _vm._v(" "),
                  _c("Icon", {
                    staticClass: "zoom",
                    attrs: { type: "md-arrow-dropright" },
                    on: {
                      click: function($event) {
                        _vm.editShowInfo = false
                      }
                    }
                  })
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "project-gstc-edit-small" },
            [
              _c(
                "div",
                {
                  staticClass: "project-gstc-edit-text",
                  on: {
                    click: function($event) {
                      _vm.editShowInfo = true
                    }
                  }
                },
                [
                  _vm._v(
                    _vm._s(_vm.$L("未保存计划时间")) +
                      ": " +
                      _vm._s(_vm.editData.length)
                  )
                ]
              ),
              _vm._v(" "),
              _c(
                "Button",
                {
                  attrs: {
                    loading: _vm.editLoad > 0,
                    size: "small",
                    type: "text"
                  },
                  on: {
                    click: function($event) {
                      return _vm.editSubmit(false)
                    }
                  }
                },
                [_vm._v(_vm._s(_vm.$L("取消")))]
              ),
              _vm._v(" "),
              _c(
                "Button",
                {
                  attrs: {
                    loading: _vm.editLoad > 0,
                    size: "small",
                    type: "primary"
                  },
                  on: {
                    click: function($event) {
                      return _vm.editSubmit(true)
                    }
                  }
                },
                [_vm._v(_vm._s(_vm.$L("保存")))]
              )
            ],
            1
          )
        ]
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-91cfb088", module.exports)
  }
}

/***/ }),

/***/ 558:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(559)
}
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(561)
/* template */
var __vue_template__ = __webpack_require__(562)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-6f0e28b5"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/main/components/project/setting.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-6f0e28b5", Component.options)
  } else {
    hotAPI.reload("data-v-6f0e28b5", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 559:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(560);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(1)("6310644a", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../node_modules/css-loader/index.js!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-6f0e28b5\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/sass-loader/lib/loader.js!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./setting.vue", function() {
     var newContent = require("!!../../../../../../node_modules/css-loader/index.js!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-6f0e28b5\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/sass-loader/lib/loader.js!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./setting.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 560:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)(false);
// imports


// module
exports.push([module.i, "\n.project-setting[data-v-6f0e28b5] {\n  padding: 0 12px;\n}\n.project-setting .project-setting-title[data-v-6f0e28b5] {\n    padding: 12px;\n    font-size: 14px;\n    font-weight: 600;\n}\n.project-setting .project-setting-group[data-v-6f0e28b5] {\n    display: inline-block;\n}\n.project-setting .form-placeholder[data-v-6f0e28b5] {\n    font-size: 12px;\n    color: #999999;\n}\n.project-setting .form-placeholder[data-v-6f0e28b5]:hover {\n    color: #000000;\n}\n", ""]);

// exports


/***/ }),

/***/ 561:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__DrawerTabsContainer__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__DrawerTabsContainer___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__DrawerTabsContainer__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
    name: 'ProjectSetting',
    components: { DrawerTabsContainer: __WEBPACK_IMPORTED_MODULE_0__DrawerTabsContainer___default.a },
    props: {
        projectid: {
            default: 0
        },
        canload: {
            type: Boolean,
            default: true
        }
    },
    data: function data() {
        return {
            loadYet: false,

            loadIng: 0,

            formSystem: {}
        };
    },
    mounted: function mounted() {
        if (this.canload) {
            this.loadYet = true;
            this.getSetting();
        }
    },


    watch: {
        projectid: function projectid() {
            if (this.loadYet) {
                this.getSetting();
            }
        },
        canload: function canload(val) {
            if (val && !this.loadYet) {
                this.loadYet = true;
                this.getSetting();
            }
        }
    },

    methods: {
        getSetting: function getSetting(save) {
            var _this = this;

            this.loadIng++;
            $A.apiAjax({
                url: 'project/setting?act=' + (save ? 'save' : 'get'),
                data: Object.assign(this.formSystem, {
                    projectid: this.projectid
                }),
                complete: function complete() {
                    _this.loadIng--;
                },
                success: function success(res) {
                    if (res.ret === 1) {
                        _this.formSystem = res.data;
                        _this.formSystem__reset = $A.cloneData(_this.formSystem);
                        if (save) {
                            _this.$Message.success(_this.$L('修改成功'));
                            _this.$emit('on-change', res.data);
                        }
                    } else {
                        if (save) {
                            _this.$Modal.error({ title: _this.$L('温馨提示'), content: res.msg });
                        }
                    }
                }
            });
        },
        handleSubmit: function handleSubmit(name) {
            var _this2 = this;

            this.$refs[name].validate(function (valid) {
                if (valid) {
                    switch (name) {
                        case "formSystem":
                            {
                                _this2.getSetting(true);
                                break;
                            }
                    }
                }
            });
        },
        handleReset: function handleReset(name) {
            if (typeof this[name + '__reset'] !== "undefined") {
                this[name] = $A.cloneData(this[name + '__reset']);
                return;
            }
            this.$refs[name].resetFields();
        }
    }
});

/***/ }),

/***/ 562:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("drawer-tabs-container", [
    _c(
      "div",
      { staticClass: "project-setting" },
      [
        _c(
          "Form",
          {
            ref: "formSystem",
            attrs: { model: _vm.formSystem, "label-width": 110 },
            nativeOn: {
              submit: function($event) {
                $event.preventDefault()
              }
            }
          },
          [
            _c("div", { staticClass: "project-setting-title" }, [
              _vm._v(_vm._s(_vm.$L("项目信息")) + ":")
            ]),
            _vm._v(" "),
            _c(
              "FormItem",
              { attrs: { label: _vm.$L("项目简介") } },
              [
                _c("Input", {
                  staticStyle: { "max-width": "450px" },
                  attrs: {
                    type: "textarea",
                    autosize: { minRows: 3, maxRows: 20 }
                  },
                  model: {
                    value: _vm.formSystem.project_desc,
                    callback: function($$v) {
                      _vm.$set(_vm.formSystem, "project_desc", $$v)
                    },
                    expression: "formSystem.project_desc"
                  }
                })
              ],
              1
            ),
            _vm._v(" "),
            _c("div", { staticClass: "project-setting-title" }, [
              _vm._v(_vm._s(_vm.$L("项目权限")) + ":")
            ]),
            _vm._v(" "),
            _c(
              "FormItem",
              { attrs: { prop: "project_role_export" } },
              [
                _c(
                  "div",
                  { attrs: { slot: "label" }, slot: "label" },
                  [
                    _c(
                      "Tooltip",
                      {
                        attrs: {
                          content: _vm.$L("任务列表导出Excel"),
                          transfer: ""
                        }
                      },
                      [_vm._v(_vm._s(_vm.$L("导出列表")))]
                    )
                  ],
                  1
                ),
                _vm._v(" "),
                _c("Checkbox", { attrs: { value: true, disabled: "" } }, [
                  _vm._v(_vm._s(_vm.$L("项目负责人")))
                ]),
                _vm._v(" "),
                _c(
                  "CheckboxGroup",
                  {
                    staticClass: "project-setting-group",
                    model: {
                      value: _vm.formSystem.project_role_export,
                      callback: function($$v) {
                        _vm.$set(_vm.formSystem, "project_role_export", $$v)
                      },
                      expression: "formSystem.project_role_export"
                    }
                  },
                  [
                    _c("Checkbox", { attrs: { label: "member" } }, [
                      _vm._v(_vm._s(_vm.$L("项目成员")))
                    ])
                  ],
                  1
                )
              ],
              1
            ),
            _vm._v(" "),
            _c("div", { staticClass: "project-setting-title" }, [
              _vm._v(_vm._s(_vm.$L("任务权限")) + ":")
            ]),
            _vm._v(" "),
            _c(
              "FormItem",
              { attrs: { label: _vm.$L("添加任务") } },
              [
                _c("Checkbox", { attrs: { value: true, disabled: "" } }, [
                  _vm._v(_vm._s(_vm.$L("项目负责人")))
                ]),
                _vm._v(" "),
                _c(
                  "CheckboxGroup",
                  {
                    staticClass: "project-setting-group",
                    model: {
                      value: _vm.formSystem.add_role,
                      callback: function($$v) {
                        _vm.$set(_vm.formSystem, "add_role", $$v)
                      },
                      expression: "formSystem.add_role"
                    }
                  },
                  [
                    _c("Checkbox", { attrs: { label: "member" } }, [
                      _vm._v(_vm._s(_vm.$L("项目成员")))
                    ])
                  ],
                  1
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "FormItem",
              { attrs: { label: _vm.$L("修改任务") } },
              [
                _c("Checkbox", { attrs: { value: true, disabled: "" } }, [
                  _vm._v(_vm._s(_vm.$L("项目负责人")))
                ]),
                _vm._v(" "),
                _c(
                  "CheckboxGroup",
                  {
                    staticClass: "project-setting-group",
                    model: {
                      value: _vm.formSystem.edit_role,
                      callback: function($$v) {
                        _vm.$set(_vm.formSystem, "edit_role", $$v)
                      },
                      expression: "formSystem.edit_role"
                    }
                  },
                  [
                    _c("Checkbox", { attrs: { label: "owner" } }, [
                      _vm._v(_vm._s(_vm.$L("任务负责人")))
                    ]),
                    _vm._v(" "),
                    _c("Checkbox", { attrs: { label: "member" } }, [
                      _vm._v(_vm._s(_vm.$L("项目成员")))
                    ])
                  ],
                  1
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "FormItem",
              { attrs: { label: _vm.$L("标记完成") } },
              [
                _c("Checkbox", { attrs: { value: true, disabled: "" } }, [
                  _vm._v(_vm._s(_vm.$L("项目负责人")))
                ]),
                _vm._v(" "),
                _c(
                  "CheckboxGroup",
                  {
                    staticClass: "project-setting-group",
                    model: {
                      value: _vm.formSystem.complete_role,
                      callback: function($$v) {
                        _vm.$set(_vm.formSystem, "complete_role", $$v)
                      },
                      expression: "formSystem.complete_role"
                    }
                  },
                  [
                    _c("Checkbox", { attrs: { label: "owner" } }, [
                      _vm._v(_vm._s(_vm.$L("任务负责人")))
                    ]),
                    _vm._v(" "),
                    _c("Checkbox", { attrs: { label: "member" } }, [
                      _vm._v(_vm._s(_vm.$L("项目成员")))
                    ])
                  ],
                  1
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "FormItem",
              { attrs: { label: _vm.$L("归档任务") } },
              [
                _c("Checkbox", { attrs: { value: true, disabled: "" } }, [
                  _vm._v(_vm._s(_vm.$L("项目负责人")))
                ]),
                _vm._v(" "),
                _c(
                  "CheckboxGroup",
                  {
                    staticClass: "project-setting-group",
                    model: {
                      value: _vm.formSystem.archived_role,
                      callback: function($$v) {
                        _vm.$set(_vm.formSystem, "archived_role", $$v)
                      },
                      expression: "formSystem.archived_role"
                    }
                  },
                  [
                    _c("Checkbox", { attrs: { label: "owner" } }, [
                      _vm._v(_vm._s(_vm.$L("任务负责人")))
                    ]),
                    _vm._v(" "),
                    _c("Checkbox", { attrs: { label: "member" } }, [
                      _vm._v(_vm._s(_vm.$L("项目成员")))
                    ])
                  ],
                  1
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "FormItem",
              { attrs: { label: _vm.$L("删除任务") } },
              [
                _c("Checkbox", { attrs: { value: true, disabled: "" } }, [
                  _vm._v(_vm._s(_vm.$L("项目负责人")))
                ]),
                _vm._v(" "),
                _c(
                  "CheckboxGroup",
                  {
                    staticClass: "project-setting-group",
                    model: {
                      value: _vm.formSystem.del_role,
                      callback: function($$v) {
                        _vm.$set(_vm.formSystem, "del_role", $$v)
                      },
                      expression: "formSystem.del_role"
                    }
                  },
                  [
                    _c("Checkbox", { attrs: { label: "owner" } }, [
                      _vm._v(_vm._s(_vm.$L("任务负责人")))
                    ]),
                    _vm._v(" "),
                    _c("Checkbox", { attrs: { label: "member" } }, [
                      _vm._v(_vm._s(_vm.$L("项目成员")))
                    ])
                  ],
                  1
                )
              ],
              1
            ),
            _vm._v(" "),
            _c("div", { staticClass: "project-setting-title" }, [
              _vm._v(_vm._s(_vm.$L("面板显示")) + ":")
            ]),
            _vm._v(" "),
            _c("FormItem", { attrs: { label: _vm.$L("显示已完成") } }, [
              _c(
                "div",
                [
                  _c(
                    "RadioGroup",
                    {
                      model: {
                        value: _vm.formSystem.complete_show,
                        callback: function($$v) {
                          _vm.$set(_vm.formSystem, "complete_show", $$v)
                        },
                        expression: "formSystem.complete_show"
                      }
                    },
                    [
                      _c("Radio", { attrs: { label: "show" } }, [
                        _vm._v(_vm._s(_vm.$L("显示")))
                      ]),
                      _vm._v(" "),
                      _c("Radio", { attrs: { label: "hide" } }, [
                        _vm._v(_vm._s(_vm.$L("隐藏")))
                      ])
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _vm.formSystem.complete_show == "show"
                ? _c("div", { staticClass: "form-placeholder" }, [
                    _vm._v(
                      "\n                    " +
                        _vm._s(_vm.$L("项目面板显示已完成的任务。")) +
                        "\n                "
                    )
                  ])
                : _c("div", { staticClass: "form-placeholder" }, [
                    _vm._v(
                      "\n                    " +
                        _vm._s(_vm.$L("项目面板隐藏已完成的任务。")) +
                        "\n                "
                    )
                  ])
            ]),
            _vm._v(" "),
            _c(
              "FormItem",
              [
                _c(
                  "Button",
                  {
                    attrs: { loading: _vm.loadIng > 0, type: "primary" },
                    on: {
                      click: function($event) {
                        return _vm.handleSubmit("formSystem")
                      }
                    }
                  },
                  [_vm._v(_vm._s(_vm.$L("提交")))]
                ),
                _vm._v(" "),
                _c(
                  "Button",
                  {
                    staticStyle: { "margin-left": "8px" },
                    attrs: { loading: _vm.loadIng > 0 },
                    on: {
                      click: function($event) {
                        return _vm.handleReset("formSystem")
                      }
                    }
                  },
                  [_vm._v(_vm._s(_vm.$L("重置")))]
                )
              ],
              1
            )
          ],
          1
        )
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-6f0e28b5", module.exports)
  }
}

/***/ }),

/***/ 563:
/***/ (function(module, exports, __webpack_require__) {

var baseOrderBy = __webpack_require__(564),
    isArray = __webpack_require__(16);

/**
 * This method is like `_.sortBy` except that it allows specifying the sort
 * orders of the iteratees to sort by. If `orders` is unspecified, all values
 * are sorted in ascending order. Otherwise, specify an order of "desc" for
 * descending or "asc" for ascending sort order of corresponding values.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Collection
 * @param {Array|Object} collection The collection to iterate over.
 * @param {Array[]|Function[]|Object[]|string[]} [iteratees=[_.identity]]
 *  The iteratees to sort by.
 * @param {string[]} [orders] The sort orders of `iteratees`.
 * @param- {Object} [guard] Enables use as an iteratee for methods like `_.reduce`.
 * @returns {Array} Returns the new sorted array.
 * @example
 *
 * var users = [
 *   { 'user': 'fred',   'age': 48 },
 *   { 'user': 'barney', 'age': 34 },
 *   { 'user': 'fred',   'age': 40 },
 *   { 'user': 'barney', 'age': 36 }
 * ];
 *
 * // Sort by `user` in ascending order and by `age` in descending order.
 * _.orderBy(users, ['user', 'age'], ['asc', 'desc']);
 * // => objects for [['barney', 36], ['barney', 34], ['fred', 48], ['fred', 40]]
 */
function orderBy(collection, iteratees, orders, guard) {
  if (collection == null) {
    return [];
  }
  if (!isArray(iteratees)) {
    iteratees = iteratees == null ? [] : [iteratees];
  }
  orders = guard ? undefined : orders;
  if (!isArray(orders)) {
    orders = orders == null ? [] : [orders];
  }
  return baseOrderBy(collection, iteratees, orders);
}

module.exports = orderBy;


/***/ }),

/***/ 564:
/***/ (function(module, exports, __webpack_require__) {

var arrayMap = __webpack_require__(427),
    baseGet = __webpack_require__(353),
    baseIteratee = __webpack_require__(570),
    baseMap = __webpack_require__(592),
    baseSortBy = __webpack_require__(598),
    baseUnary = __webpack_require__(20),
    compareMultiple = __webpack_require__(599),
    identity = __webpack_require__(433),
    isArray = __webpack_require__(16);

/**
 * The base implementation of `_.orderBy` without param guards.
 *
 * @private
 * @param {Array|Object} collection The collection to iterate over.
 * @param {Function[]|Object[]|string[]} iteratees The iteratees to sort by.
 * @param {string[]} orders The sort orders of `iteratees`.
 * @returns {Array} Returns the new sorted array.
 */
function baseOrderBy(collection, iteratees, orders) {
  if (iteratees.length) {
    iteratees = arrayMap(iteratees, function(iteratee) {
      if (isArray(iteratee)) {
        return function(value) {
          return baseGet(value, iteratee.length === 1 ? iteratee[0] : iteratee);
        }
      }
      return iteratee;
    });
  } else {
    iteratees = [identity];
  }

  var index = -1;
  iteratees = arrayMap(iteratees, baseUnary(baseIteratee));

  var result = baseMap(collection, function(value, key, collection) {
    var criteria = arrayMap(iteratees, function(iteratee) {
      return iteratee(value);
    });
    return { 'criteria': criteria, 'index': ++index, 'value': value };
  });

  return baseSortBy(result, function(object, other) {
    return compareMultiple(object, other, orders);
  });
}

module.exports = baseOrderBy;


/***/ }),

/***/ 565:
/***/ (function(module, exports, __webpack_require__) {

var memoizeCapped = __webpack_require__(566);

/** Used to match property names within property paths. */
var rePropName = /[^.[\]]+|\[(?:(-?\d+(?:\.\d+)?)|(["'])((?:(?!\2)[^\\]|\\.)*?)\2)\]|(?=(?:\.|\[\])(?:\.|\[\]|$))/g;

/** Used to match backslashes in property paths. */
var reEscapeChar = /\\(\\)?/g;

/**
 * Converts `string` to a property path array.
 *
 * @private
 * @param {string} string The string to convert.
 * @returns {Array} Returns the property path array.
 */
var stringToPath = memoizeCapped(function(string) {
  var result = [];
  if (string.charCodeAt(0) === 46 /* . */) {
    result.push('');
  }
  string.replace(rePropName, function(match, number, quote, subString) {
    result.push(quote ? subString.replace(reEscapeChar, '$1') : (number || match));
  });
  return result;
});

module.exports = stringToPath;


/***/ }),

/***/ 566:
/***/ (function(module, exports, __webpack_require__) {

var memoize = __webpack_require__(567);

/** Used as the maximum memoize cache size. */
var MAX_MEMOIZE_SIZE = 500;

/**
 * A specialized version of `_.memoize` which clears the memoized function's
 * cache when it exceeds `MAX_MEMOIZE_SIZE`.
 *
 * @private
 * @param {Function} func The function to have its output memoized.
 * @returns {Function} Returns the new memoized function.
 */
function memoizeCapped(func) {
  var result = memoize(func, function(key) {
    if (cache.size === MAX_MEMOIZE_SIZE) {
      cache.clear();
    }
    return key;
  });

  var cache = result.cache;
  return result;
}

module.exports = memoizeCapped;


/***/ }),

/***/ 567:
/***/ (function(module, exports, __webpack_require__) {

var MapCache = __webpack_require__(54);

/** Error message constants. */
var FUNC_ERROR_TEXT = 'Expected a function';

/**
 * Creates a function that memoizes the result of `func`. If `resolver` is
 * provided, it determines the cache key for storing the result based on the
 * arguments provided to the memoized function. By default, the first argument
 * provided to the memoized function is used as the map cache key. The `func`
 * is invoked with the `this` binding of the memoized function.
 *
 * **Note:** The cache is exposed as the `cache` property on the memoized
 * function. Its creation may be customized by replacing the `_.memoize.Cache`
 * constructor with one whose instances implement the
 * [`Map`](http://ecma-international.org/ecma-262/7.0/#sec-properties-of-the-map-prototype-object)
 * method interface of `clear`, `delete`, `get`, `has`, and `set`.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Function
 * @param {Function} func The function to have its output memoized.
 * @param {Function} [resolver] The function to resolve the cache key.
 * @returns {Function} Returns the new memoized function.
 * @example
 *
 * var object = { 'a': 1, 'b': 2 };
 * var other = { 'c': 3, 'd': 4 };
 *
 * var values = _.memoize(_.values);
 * values(object);
 * // => [1, 2]
 *
 * values(other);
 * // => [3, 4]
 *
 * object.a = 2;
 * values(object);
 * // => [1, 2]
 *
 * // Modify the result cache.
 * values.cache.set(object, ['a', 'b']);
 * values(object);
 * // => ['a', 'b']
 *
 * // Replace `_.memoize.Cache`.
 * _.memoize.Cache = WeakMap;
 */
function memoize(func, resolver) {
  if (typeof func != 'function' || (resolver != null && typeof resolver != 'function')) {
    throw new TypeError(FUNC_ERROR_TEXT);
  }
  var memoized = function() {
    var args = arguments,
        key = resolver ? resolver.apply(this, args) : args[0],
        cache = memoized.cache;

    if (cache.has(key)) {
      return cache.get(key);
    }
    var result = func.apply(this, args);
    memoized.cache = cache.set(key, result) || cache;
    return result;
  };
  memoized.cache = new (memoize.Cache || MapCache);
  return memoized;
}

// Expose `MapCache`.
memoize.Cache = MapCache;

module.exports = memoize;


/***/ }),

/***/ 568:
/***/ (function(module, exports, __webpack_require__) {

var baseToString = __webpack_require__(569);

/**
 * Converts `value` to a string. An empty string is returned for `null`
 * and `undefined` values. The sign of `-0` is preserved.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to convert.
 * @returns {string} Returns the converted string.
 * @example
 *
 * _.toString(null);
 * // => ''
 *
 * _.toString(-0);
 * // => '-0'
 *
 * _.toString([1, 2, 3]);
 * // => '1,2,3'
 */
function toString(value) {
  return value == null ? '' : baseToString(value);
}

module.exports = toString;


/***/ }),

/***/ 569:
/***/ (function(module, exports, __webpack_require__) {

var Symbol = __webpack_require__(18),
    arrayMap = __webpack_require__(427),
    isArray = __webpack_require__(16),
    isSymbol = __webpack_require__(320);

/** Used as references for various `Number` constants. */
var INFINITY = 1 / 0;

/** Used to convert symbols to primitives and strings. */
var symbolProto = Symbol ? Symbol.prototype : undefined,
    symbolToString = symbolProto ? symbolProto.toString : undefined;

/**
 * The base implementation of `_.toString` which doesn't convert nullish
 * values to empty strings.
 *
 * @private
 * @param {*} value The value to process.
 * @returns {string} Returns the string.
 */
function baseToString(value) {
  // Exit early for strings to avoid a performance hit in some environments.
  if (typeof value == 'string') {
    return value;
  }
  if (isArray(value)) {
    // Recursively convert values (susceptible to call stack limits).
    return arrayMap(value, baseToString) + '';
  }
  if (isSymbol(value)) {
    return symbolToString ? symbolToString.call(value) : '';
  }
  var result = (value + '');
  return (result == '0' && (1 / value) == -INFINITY) ? '-0' : result;
}

module.exports = baseToString;


/***/ }),

/***/ 570:
/***/ (function(module, exports, __webpack_require__) {

var baseMatches = __webpack_require__(571),
    baseMatchesProperty = __webpack_require__(584),
    identity = __webpack_require__(433),
    isArray = __webpack_require__(16),
    property = __webpack_require__(589);

/**
 * The base implementation of `_.iteratee`.
 *
 * @private
 * @param {*} [value=_.identity] The value to convert to an iteratee.
 * @returns {Function} Returns the iteratee.
 */
function baseIteratee(value) {
  // Don't store the `typeof` result in a variable to avoid a JIT bug in Safari 9.
  // See https://bugs.webkit.org/show_bug.cgi?id=156034 for more details.
  if (typeof value == 'function') {
    return value;
  }
  if (value == null) {
    return identity;
  }
  if (typeof value == 'object') {
    return isArray(value)
      ? baseMatchesProperty(value[0], value[1])
      : baseMatches(value);
  }
  return property(value);
}

module.exports = baseIteratee;


/***/ }),

/***/ 571:
/***/ (function(module, exports, __webpack_require__) {

var baseIsMatch = __webpack_require__(572),
    getMatchData = __webpack_require__(583),
    matchesStrictComparable = __webpack_require__(432);

/**
 * The base implementation of `_.matches` which doesn't clone `source`.
 *
 * @private
 * @param {Object} source The object of property values to match.
 * @returns {Function} Returns the new spec function.
 */
function baseMatches(source) {
  var matchData = getMatchData(source);
  if (matchData.length == 1 && matchData[0][2]) {
    return matchesStrictComparable(matchData[0][0], matchData[0][1]);
  }
  return function(object) {
    return object === source || baseIsMatch(object, source, matchData);
  };
}

module.exports = baseMatches;


/***/ }),

/***/ 572:
/***/ (function(module, exports, __webpack_require__) {

var Stack = __webpack_require__(55),
    baseIsEqual = __webpack_require__(429);

/** Used to compose bitmasks for value comparisons. */
var COMPARE_PARTIAL_FLAG = 1,
    COMPARE_UNORDERED_FLAG = 2;

/**
 * The base implementation of `_.isMatch` without support for iteratee shorthands.
 *
 * @private
 * @param {Object} object The object to inspect.
 * @param {Object} source The object of property values to match.
 * @param {Array} matchData The property names, values, and compare flags to match.
 * @param {Function} [customizer] The function to customize comparisons.
 * @returns {boolean} Returns `true` if `object` is a match, else `false`.
 */
function baseIsMatch(object, source, matchData, customizer) {
  var index = matchData.length,
      length = index,
      noCustomizer = !customizer;

  if (object == null) {
    return !length;
  }
  object = Object(object);
  while (index--) {
    var data = matchData[index];
    if ((noCustomizer && data[2])
          ? data[1] !== object[data[0]]
          : !(data[0] in object)
        ) {
      return false;
    }
  }
  while (++index < length) {
    data = matchData[index];
    var key = data[0],
        objValue = object[key],
        srcValue = data[1];

    if (noCustomizer && data[2]) {
      if (objValue === undefined && !(key in object)) {
        return false;
      }
    } else {
      var stack = new Stack;
      if (customizer) {
        var result = customizer(objValue, srcValue, key, object, source, stack);
      }
      if (!(result === undefined
            ? baseIsEqual(srcValue, objValue, COMPARE_PARTIAL_FLAG | COMPARE_UNORDERED_FLAG, customizer, stack)
            : result
          )) {
        return false;
      }
    }
  }
  return true;
}

module.exports = baseIsMatch;


/***/ }),

/***/ 573:
/***/ (function(module, exports, __webpack_require__) {

var Stack = __webpack_require__(55),
    equalArrays = __webpack_require__(430),
    equalByTag = __webpack_require__(579),
    equalObjects = __webpack_require__(582),
    getTag = __webpack_require__(21),
    isArray = __webpack_require__(16),
    isBuffer = __webpack_require__(34),
    isTypedArray = __webpack_require__(63);

/** Used to compose bitmasks for value comparisons. */
var COMPARE_PARTIAL_FLAG = 1;

/** `Object#toString` result references. */
var argsTag = '[object Arguments]',
    arrayTag = '[object Array]',
    objectTag = '[object Object]';

/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * A specialized version of `baseIsEqual` for arrays and objects which performs
 * deep comparisons and tracks traversed objects enabling objects with circular
 * references to be compared.
 *
 * @private
 * @param {Object} object The object to compare.
 * @param {Object} other The other object to compare.
 * @param {number} bitmask The bitmask flags. See `baseIsEqual` for more details.
 * @param {Function} customizer The function to customize comparisons.
 * @param {Function} equalFunc The function to determine equivalents of values.
 * @param {Object} [stack] Tracks traversed `object` and `other` objects.
 * @returns {boolean} Returns `true` if the objects are equivalent, else `false`.
 */
function baseIsEqualDeep(object, other, bitmask, customizer, equalFunc, stack) {
  var objIsArr = isArray(object),
      othIsArr = isArray(other),
      objTag = objIsArr ? arrayTag : getTag(object),
      othTag = othIsArr ? arrayTag : getTag(other);

  objTag = objTag == argsTag ? objectTag : objTag;
  othTag = othTag == argsTag ? objectTag : othTag;

  var objIsObj = objTag == objectTag,
      othIsObj = othTag == objectTag,
      isSameTag = objTag == othTag;

  if (isSameTag && isBuffer(object)) {
    if (!isBuffer(other)) {
      return false;
    }
    objIsArr = true;
    objIsObj = false;
  }
  if (isSameTag && !objIsObj) {
    stack || (stack = new Stack);
    return (objIsArr || isTypedArray(object))
      ? equalArrays(object, other, bitmask, customizer, equalFunc, stack)
      : equalByTag(object, other, objTag, bitmask, customizer, equalFunc, stack);
  }
  if (!(bitmask & COMPARE_PARTIAL_FLAG)) {
    var objIsWrapped = objIsObj && hasOwnProperty.call(object, '__wrapped__'),
        othIsWrapped = othIsObj && hasOwnProperty.call(other, '__wrapped__');

    if (objIsWrapped || othIsWrapped) {
      var objUnwrapped = objIsWrapped ? object.value() : object,
          othUnwrapped = othIsWrapped ? other.value() : other;

      stack || (stack = new Stack);
      return equalFunc(objUnwrapped, othUnwrapped, bitmask, customizer, stack);
    }
  }
  if (!isSameTag) {
    return false;
  }
  stack || (stack = new Stack);
  return equalObjects(object, other, bitmask, customizer, equalFunc, stack);
}

module.exports = baseIsEqualDeep;


/***/ }),

/***/ 574:
/***/ (function(module, exports, __webpack_require__) {

var MapCache = __webpack_require__(54),
    setCacheAdd = __webpack_require__(575),
    setCacheHas = __webpack_require__(576);

/**
 *
 * Creates an array cache object to store unique values.
 *
 * @private
 * @constructor
 * @param {Array} [values] The values to cache.
 */
function SetCache(values) {
  var index = -1,
      length = values == null ? 0 : values.length;

  this.__data__ = new MapCache;
  while (++index < length) {
    this.add(values[index]);
  }
}

// Add methods to `SetCache`.
SetCache.prototype.add = SetCache.prototype.push = setCacheAdd;
SetCache.prototype.has = setCacheHas;

module.exports = SetCache;


/***/ }),

/***/ 575:
/***/ (function(module, exports) {

/** Used to stand-in for `undefined` hash values. */
var HASH_UNDEFINED = '__lodash_hash_undefined__';

/**
 * Adds `value` to the array cache.
 *
 * @private
 * @name add
 * @memberOf SetCache
 * @alias push
 * @param {*} value The value to cache.
 * @returns {Object} Returns the cache instance.
 */
function setCacheAdd(value) {
  this.__data__.set(value, HASH_UNDEFINED);
  return this;
}

module.exports = setCacheAdd;


/***/ }),

/***/ 576:
/***/ (function(module, exports) {

/**
 * Checks if `value` is in the array cache.
 *
 * @private
 * @name has
 * @memberOf SetCache
 * @param {*} value The value to search for.
 * @returns {number} Returns `true` if `value` is found, else `false`.
 */
function setCacheHas(value) {
  return this.__data__.has(value);
}

module.exports = setCacheHas;


/***/ }),

/***/ 577:
/***/ (function(module, exports) {

/**
 * A specialized version of `_.some` for arrays without support for iteratee
 * shorthands.
 *
 * @private
 * @param {Array} [array] The array to iterate over.
 * @param {Function} predicate The function invoked per iteration.
 * @returns {boolean} Returns `true` if any element passes the predicate check,
 *  else `false`.
 */
function arraySome(array, predicate) {
  var index = -1,
      length = array == null ? 0 : array.length;

  while (++index < length) {
    if (predicate(array[index], index, array)) {
      return true;
    }
  }
  return false;
}

module.exports = arraySome;


/***/ }),

/***/ 578:
/***/ (function(module, exports) {

/**
 * Checks if a `cache` value for `key` exists.
 *
 * @private
 * @param {Object} cache The cache to query.
 * @param {string} key The key of the entry to check.
 * @returns {boolean} Returns `true` if an entry for `key` exists, else `false`.
 */
function cacheHas(cache, key) {
  return cache.has(key);
}

module.exports = cacheHas;


/***/ }),

/***/ 579:
/***/ (function(module, exports, __webpack_require__) {

var Symbol = __webpack_require__(18),
    Uint8Array = __webpack_require__(59),
    eq = __webpack_require__(33),
    equalArrays = __webpack_require__(430),
    mapToArray = __webpack_require__(580),
    setToArray = __webpack_require__(581);

/** Used to compose bitmasks for value comparisons. */
var COMPARE_PARTIAL_FLAG = 1,
    COMPARE_UNORDERED_FLAG = 2;

/** `Object#toString` result references. */
var boolTag = '[object Boolean]',
    dateTag = '[object Date]',
    errorTag = '[object Error]',
    mapTag = '[object Map]',
    numberTag = '[object Number]',
    regexpTag = '[object RegExp]',
    setTag = '[object Set]',
    stringTag = '[object String]',
    symbolTag = '[object Symbol]';

var arrayBufferTag = '[object ArrayBuffer]',
    dataViewTag = '[object DataView]';

/** Used to convert symbols to primitives and strings. */
var symbolProto = Symbol ? Symbol.prototype : undefined,
    symbolValueOf = symbolProto ? symbolProto.valueOf : undefined;

/**
 * A specialized version of `baseIsEqualDeep` for comparing objects of
 * the same `toStringTag`.
 *
 * **Note:** This function only supports comparing values with tags of
 * `Boolean`, `Date`, `Error`, `Number`, `RegExp`, or `String`.
 *
 * @private
 * @param {Object} object The object to compare.
 * @param {Object} other The other object to compare.
 * @param {string} tag The `toStringTag` of the objects to compare.
 * @param {number} bitmask The bitmask flags. See `baseIsEqual` for more details.
 * @param {Function} customizer The function to customize comparisons.
 * @param {Function} equalFunc The function to determine equivalents of values.
 * @param {Object} stack Tracks traversed `object` and `other` objects.
 * @returns {boolean} Returns `true` if the objects are equivalent, else `false`.
 */
function equalByTag(object, other, tag, bitmask, customizer, equalFunc, stack) {
  switch (tag) {
    case dataViewTag:
      if ((object.byteLength != other.byteLength) ||
          (object.byteOffset != other.byteOffset)) {
        return false;
      }
      object = object.buffer;
      other = other.buffer;

    case arrayBufferTag:
      if ((object.byteLength != other.byteLength) ||
          !equalFunc(new Uint8Array(object), new Uint8Array(other))) {
        return false;
      }
      return true;

    case boolTag:
    case dateTag:
    case numberTag:
      // Coerce booleans to `1` or `0` and dates to milliseconds.
      // Invalid dates are coerced to `NaN`.
      return eq(+object, +other);

    case errorTag:
      return object.name == other.name && object.message == other.message;

    case regexpTag:
    case stringTag:
      // Coerce regexes to strings and treat strings, primitives and objects,
      // as equal. See http://www.ecma-international.org/ecma-262/7.0/#sec-regexp.prototype.tostring
      // for more details.
      return object == (other + '');

    case mapTag:
      var convert = mapToArray;

    case setTag:
      var isPartial = bitmask & COMPARE_PARTIAL_FLAG;
      convert || (convert = setToArray);

      if (object.size != other.size && !isPartial) {
        return false;
      }
      // Assume cyclic values are equal.
      var stacked = stack.get(object);
      if (stacked) {
        return stacked == other;
      }
      bitmask |= COMPARE_UNORDERED_FLAG;

      // Recursively compare objects (susceptible to call stack limits).
      stack.set(object, other);
      var result = equalArrays(convert(object), convert(other), bitmask, customizer, equalFunc, stack);
      stack['delete'](object);
      return result;

    case symbolTag:
      if (symbolValueOf) {
        return symbolValueOf.call(object) == symbolValueOf.call(other);
      }
  }
  return false;
}

module.exports = equalByTag;


/***/ }),

/***/ 580:
/***/ (function(module, exports) {

/**
 * Converts `map` to its key-value pairs.
 *
 * @private
 * @param {Object} map The map to convert.
 * @returns {Array} Returns the key-value pairs.
 */
function mapToArray(map) {
  var index = -1,
      result = Array(map.size);

  map.forEach(function(value, key) {
    result[++index] = [key, value];
  });
  return result;
}

module.exports = mapToArray;


/***/ }),

/***/ 581:
/***/ (function(module, exports) {

/**
 * Converts `set` to an array of its values.
 *
 * @private
 * @param {Object} set The set to convert.
 * @returns {Array} Returns the values.
 */
function setToArray(set) {
  var index = -1,
      result = Array(set.size);

  set.forEach(function(value) {
    result[++index] = value;
  });
  return result;
}

module.exports = setToArray;


/***/ }),

/***/ 582:
/***/ (function(module, exports, __webpack_require__) {

var getAllKeys = __webpack_require__(60);

/** Used to compose bitmasks for value comparisons. */
var COMPARE_PARTIAL_FLAG = 1;

/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * A specialized version of `baseIsEqualDeep` for objects with support for
 * partial deep comparisons.
 *
 * @private
 * @param {Object} object The object to compare.
 * @param {Object} other The other object to compare.
 * @param {number} bitmask The bitmask flags. See `baseIsEqual` for more details.
 * @param {Function} customizer The function to customize comparisons.
 * @param {Function} equalFunc The function to determine equivalents of values.
 * @param {Object} stack Tracks traversed `object` and `other` objects.
 * @returns {boolean} Returns `true` if the objects are equivalent, else `false`.
 */
function equalObjects(object, other, bitmask, customizer, equalFunc, stack) {
  var isPartial = bitmask & COMPARE_PARTIAL_FLAG,
      objProps = getAllKeys(object),
      objLength = objProps.length,
      othProps = getAllKeys(other),
      othLength = othProps.length;

  if (objLength != othLength && !isPartial) {
    return false;
  }
  var index = objLength;
  while (index--) {
    var key = objProps[index];
    if (!(isPartial ? key in other : hasOwnProperty.call(other, key))) {
      return false;
    }
  }
  // Check that cyclic values are equal.
  var objStacked = stack.get(object);
  var othStacked = stack.get(other);
  if (objStacked && othStacked) {
    return objStacked == other && othStacked == object;
  }
  var result = true;
  stack.set(object, other);
  stack.set(other, object);

  var skipCtor = isPartial;
  while (++index < objLength) {
    key = objProps[index];
    var objValue = object[key],
        othValue = other[key];

    if (customizer) {
      var compared = isPartial
        ? customizer(othValue, objValue, key, other, object, stack)
        : customizer(objValue, othValue, key, object, other, stack);
    }
    // Recursively compare objects (susceptible to call stack limits).
    if (!(compared === undefined
          ? (objValue === othValue || equalFunc(objValue, othValue, bitmask, customizer, stack))
          : compared
        )) {
      result = false;
      break;
    }
    skipCtor || (skipCtor = key == 'constructor');
  }
  if (result && !skipCtor) {
    var objCtor = object.constructor,
        othCtor = other.constructor;

    // Non `Object` object instances with different constructors are not equal.
    if (objCtor != othCtor &&
        ('constructor' in object && 'constructor' in other) &&
        !(typeof objCtor == 'function' && objCtor instanceof objCtor &&
          typeof othCtor == 'function' && othCtor instanceof othCtor)) {
      result = false;
    }
  }
  stack['delete'](object);
  stack['delete'](other);
  return result;
}

module.exports = equalObjects;


/***/ }),

/***/ 583:
/***/ (function(module, exports, __webpack_require__) {

var isStrictComparable = __webpack_require__(431),
    keys = __webpack_require__(19);

/**
 * Gets the property names, values, and compare flags of `object`.
 *
 * @private
 * @param {Object} object The object to query.
 * @returns {Array} Returns the match data of `object`.
 */
function getMatchData(object) {
  var result = keys(object),
      length = result.length;

  while (length--) {
    var key = result[length],
        value = object[key];

    result[length] = [key, value, isStrictComparable(value)];
  }
  return result;
}

module.exports = getMatchData;


/***/ }),

/***/ 584:
/***/ (function(module, exports, __webpack_require__) {

var baseIsEqual = __webpack_require__(429),
    get = __webpack_require__(585),
    hasIn = __webpack_require__(586),
    isKey = __webpack_require__(354),
    isStrictComparable = __webpack_require__(431),
    matchesStrictComparable = __webpack_require__(432),
    toKey = __webpack_require__(321);

/** Used to compose bitmasks for value comparisons. */
var COMPARE_PARTIAL_FLAG = 1,
    COMPARE_UNORDERED_FLAG = 2;

/**
 * The base implementation of `_.matchesProperty` which doesn't clone `srcValue`.
 *
 * @private
 * @param {string} path The path of the property to get.
 * @param {*} srcValue The value to match.
 * @returns {Function} Returns the new spec function.
 */
function baseMatchesProperty(path, srcValue) {
  if (isKey(path) && isStrictComparable(srcValue)) {
    return matchesStrictComparable(toKey(path), srcValue);
  }
  return function(object) {
    var objValue = get(object, path);
    return (objValue === undefined && objValue === srcValue)
      ? hasIn(object, path)
      : baseIsEqual(srcValue, objValue, COMPARE_PARTIAL_FLAG | COMPARE_UNORDERED_FLAG);
  };
}

module.exports = baseMatchesProperty;


/***/ }),

/***/ 585:
/***/ (function(module, exports, __webpack_require__) {

var baseGet = __webpack_require__(353);

/**
 * Gets the value at `path` of `object`. If the resolved value is
 * `undefined`, the `defaultValue` is returned in its place.
 *
 * @static
 * @memberOf _
 * @since 3.7.0
 * @category Object
 * @param {Object} object The object to query.
 * @param {Array|string} path The path of the property to get.
 * @param {*} [defaultValue] The value returned for `undefined` resolved values.
 * @returns {*} Returns the resolved value.
 * @example
 *
 * var object = { 'a': [{ 'b': { 'c': 3 } }] };
 *
 * _.get(object, 'a[0].b.c');
 * // => 3
 *
 * _.get(object, ['a', '0', 'b', 'c']);
 * // => 3
 *
 * _.get(object, 'a.b.c', 'default');
 * // => 'default'
 */
function get(object, path, defaultValue) {
  var result = object == null ? undefined : baseGet(object, path);
  return result === undefined ? defaultValue : result;
}

module.exports = get;


/***/ }),

/***/ 586:
/***/ (function(module, exports, __webpack_require__) {

var baseHasIn = __webpack_require__(587),
    hasPath = __webpack_require__(588);

/**
 * Checks if `path` is a direct or inherited property of `object`.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Object
 * @param {Object} object The object to query.
 * @param {Array|string} path The path to check.
 * @returns {boolean} Returns `true` if `path` exists, else `false`.
 * @example
 *
 * var object = _.create({ 'a': _.create({ 'b': 2 }) });
 *
 * _.hasIn(object, 'a');
 * // => true
 *
 * _.hasIn(object, 'a.b');
 * // => true
 *
 * _.hasIn(object, ['a', 'b']);
 * // => true
 *
 * _.hasIn(object, 'b');
 * // => false
 */
function hasIn(object, path) {
  return object != null && hasPath(object, path, baseHasIn);
}

module.exports = hasIn;


/***/ }),

/***/ 587:
/***/ (function(module, exports) {

/**
 * The base implementation of `_.hasIn` without support for deep paths.
 *
 * @private
 * @param {Object} [object] The object to query.
 * @param {Array|string} key The key to check.
 * @returns {boolean} Returns `true` if `key` exists, else `false`.
 */
function baseHasIn(object, key) {
  return object != null && key in Object(object);
}

module.exports = baseHasIn;


/***/ }),

/***/ 588:
/***/ (function(module, exports, __webpack_require__) {

var castPath = __webpack_require__(428),
    isArguments = __webpack_require__(61),
    isArray = __webpack_require__(16),
    isIndex = __webpack_require__(62),
    isLength = __webpack_require__(35),
    toKey = __webpack_require__(321);

/**
 * Checks if `path` exists on `object`.
 *
 * @private
 * @param {Object} object The object to query.
 * @param {Array|string} path The path to check.
 * @param {Function} hasFunc The function to check properties.
 * @returns {boolean} Returns `true` if `path` exists, else `false`.
 */
function hasPath(object, path, hasFunc) {
  path = castPath(path, object);

  var index = -1,
      length = path.length,
      result = false;

  while (++index < length) {
    var key = toKey(path[index]);
    if (!(result = object != null && hasFunc(object, key))) {
      break;
    }
    object = object[key];
  }
  if (result || ++index != length) {
    return result;
  }
  length = object == null ? 0 : object.length;
  return !!length && isLength(length) && isIndex(key, length) &&
    (isArray(object) || isArguments(object));
}

module.exports = hasPath;


/***/ }),

/***/ 589:
/***/ (function(module, exports, __webpack_require__) {

var baseProperty = __webpack_require__(590),
    basePropertyDeep = __webpack_require__(591),
    isKey = __webpack_require__(354),
    toKey = __webpack_require__(321);

/**
 * Creates a function that returns the value at `path` of a given object.
 *
 * @static
 * @memberOf _
 * @since 2.4.0
 * @category Util
 * @param {Array|string} path The path of the property to get.
 * @returns {Function} Returns the new accessor function.
 * @example
 *
 * var objects = [
 *   { 'a': { 'b': 2 } },
 *   { 'a': { 'b': 1 } }
 * ];
 *
 * _.map(objects, _.property('a.b'));
 * // => [2, 1]
 *
 * _.map(_.sortBy(objects, _.property(['a', 'b'])), 'a.b');
 * // => [1, 2]
 */
function property(path) {
  return isKey(path) ? baseProperty(toKey(path)) : basePropertyDeep(path);
}

module.exports = property;


/***/ }),

/***/ 590:
/***/ (function(module, exports) {

/**
 * The base implementation of `_.property` without support for deep paths.
 *
 * @private
 * @param {string} key The key of the property to get.
 * @returns {Function} Returns the new accessor function.
 */
function baseProperty(key) {
  return function(object) {
    return object == null ? undefined : object[key];
  };
}

module.exports = baseProperty;


/***/ }),

/***/ 591:
/***/ (function(module, exports, __webpack_require__) {

var baseGet = __webpack_require__(353);

/**
 * A specialized version of `baseProperty` which supports deep paths.
 *
 * @private
 * @param {Array|string} path The path of the property to get.
 * @returns {Function} Returns the new accessor function.
 */
function basePropertyDeep(path) {
  return function(object) {
    return baseGet(object, path);
  };
}

module.exports = basePropertyDeep;


/***/ }),

/***/ 592:
/***/ (function(module, exports, __webpack_require__) {

var baseEach = __webpack_require__(593),
    isArrayLike = __webpack_require__(32);

/**
 * The base implementation of `_.map` without support for iteratee shorthands.
 *
 * @private
 * @param {Array|Object} collection The collection to iterate over.
 * @param {Function} iteratee The function invoked per iteration.
 * @returns {Array} Returns the new mapped array.
 */
function baseMap(collection, iteratee) {
  var index = -1,
      result = isArrayLike(collection) ? Array(collection.length) : [];

  baseEach(collection, function(value, key, collection) {
    result[++index] = iteratee(value, key, collection);
  });
  return result;
}

module.exports = baseMap;


/***/ }),

/***/ 593:
/***/ (function(module, exports, __webpack_require__) {

var baseForOwn = __webpack_require__(594),
    createBaseEach = __webpack_require__(597);

/**
 * The base implementation of `_.forEach` without support for iteratee shorthands.
 *
 * @private
 * @param {Array|Object} collection The collection to iterate over.
 * @param {Function} iteratee The function invoked per iteration.
 * @returns {Array|Object} Returns `collection`.
 */
var baseEach = createBaseEach(baseForOwn);

module.exports = baseEach;


/***/ }),

/***/ 594:
/***/ (function(module, exports, __webpack_require__) {

var baseFor = __webpack_require__(595),
    keys = __webpack_require__(19);

/**
 * The base implementation of `_.forOwn` without support for iteratee shorthands.
 *
 * @private
 * @param {Object} object The object to iterate over.
 * @param {Function} iteratee The function invoked per iteration.
 * @returns {Object} Returns `object`.
 */
function baseForOwn(object, iteratee) {
  return object && baseFor(object, iteratee, keys);
}

module.exports = baseForOwn;


/***/ }),

/***/ 595:
/***/ (function(module, exports, __webpack_require__) {

var createBaseFor = __webpack_require__(596);

/**
 * The base implementation of `baseForOwn` which iterates over `object`
 * properties returned by `keysFunc` and invokes `iteratee` for each property.
 * Iteratee functions may exit iteration early by explicitly returning `false`.
 *
 * @private
 * @param {Object} object The object to iterate over.
 * @param {Function} iteratee The function invoked per iteration.
 * @param {Function} keysFunc The function to get the keys of `object`.
 * @returns {Object} Returns `object`.
 */
var baseFor = createBaseFor();

module.exports = baseFor;


/***/ }),

/***/ 596:
/***/ (function(module, exports) {

/**
 * Creates a base function for methods like `_.forIn` and `_.forOwn`.
 *
 * @private
 * @param {boolean} [fromRight] Specify iterating from right to left.
 * @returns {Function} Returns the new base function.
 */
function createBaseFor(fromRight) {
  return function(object, iteratee, keysFunc) {
    var index = -1,
        iterable = Object(object),
        props = keysFunc(object),
        length = props.length;

    while (length--) {
      var key = props[fromRight ? length : ++index];
      if (iteratee(iterable[key], key, iterable) === false) {
        break;
      }
    }
    return object;
  };
}

module.exports = createBaseFor;


/***/ }),

/***/ 597:
/***/ (function(module, exports, __webpack_require__) {

var isArrayLike = __webpack_require__(32);

/**
 * Creates a `baseEach` or `baseEachRight` function.
 *
 * @private
 * @param {Function} eachFunc The function to iterate over a collection.
 * @param {boolean} [fromRight] Specify iterating from right to left.
 * @returns {Function} Returns the new base function.
 */
function createBaseEach(eachFunc, fromRight) {
  return function(collection, iteratee) {
    if (collection == null) {
      return collection;
    }
    if (!isArrayLike(collection)) {
      return eachFunc(collection, iteratee);
    }
    var length = collection.length,
        index = fromRight ? length : -1,
        iterable = Object(collection);

    while ((fromRight ? index-- : ++index < length)) {
      if (iteratee(iterable[index], index, iterable) === false) {
        break;
      }
    }
    return collection;
  };
}

module.exports = createBaseEach;


/***/ }),

/***/ 598:
/***/ (function(module, exports) {

/**
 * The base implementation of `_.sortBy` which uses `comparer` to define the
 * sort order of `array` and replaces criteria objects with their corresponding
 * values.
 *
 * @private
 * @param {Array} array The array to sort.
 * @param {Function} comparer The function to define sort order.
 * @returns {Array} Returns `array`.
 */
function baseSortBy(array, comparer) {
  var length = array.length;

  array.sort(comparer);
  while (length--) {
    array[length] = array[length].value;
  }
  return array;
}

module.exports = baseSortBy;


/***/ }),

/***/ 599:
/***/ (function(module, exports, __webpack_require__) {

var compareAscending = __webpack_require__(600);

/**
 * Used by `_.orderBy` to compare multiple properties of a value to another
 * and stable sort them.
 *
 * If `orders` is unspecified, all values are sorted in ascending order. Otherwise,
 * specify an order of "desc" for descending or "asc" for ascending sort order
 * of corresponding values.
 *
 * @private
 * @param {Object} object The object to compare.
 * @param {Object} other The other object to compare.
 * @param {boolean[]|string[]} orders The order to sort by for each property.
 * @returns {number} Returns the sort order indicator for `object`.
 */
function compareMultiple(object, other, orders) {
  var index = -1,
      objCriteria = object.criteria,
      othCriteria = other.criteria,
      length = objCriteria.length,
      ordersLength = orders.length;

  while (++index < length) {
    var result = compareAscending(objCriteria[index], othCriteria[index]);
    if (result) {
      if (index >= ordersLength) {
        return result;
      }
      var order = orders[index];
      return result * (order == 'desc' ? -1 : 1);
    }
  }
  // Fixes an `Array#sort` bug in the JS engine embedded in Adobe applications
  // that causes it, under certain circumstances, to provide the same value for
  // `object` and `other`. See https://github.com/jashkenas/underscore/pull/1247
  // for more details.
  //
  // This also ensures a stable sort in V8 and other engines.
  // See https://bugs.chromium.org/p/v8/issues/detail?id=90 for more details.
  return object.index - other.index;
}

module.exports = compareMultiple;


/***/ }),

/***/ 600:
/***/ (function(module, exports, __webpack_require__) {

var isSymbol = __webpack_require__(320);

/**
 * Compares values to sort them in ascending order.
 *
 * @private
 * @param {*} value The value to compare.
 * @param {*} other The other value to compare.
 * @returns {number} Returns the sort order indicator for `value`.
 */
function compareAscending(value, other) {
  if (value !== other) {
    var valIsDefined = value !== undefined,
        valIsNull = value === null,
        valIsReflexive = value === value,
        valIsSymbol = isSymbol(value);

    var othIsDefined = other !== undefined,
        othIsNull = other === null,
        othIsReflexive = other === other,
        othIsSymbol = isSymbol(other);

    if ((!othIsNull && !othIsSymbol && !valIsSymbol && value > other) ||
        (valIsSymbol && othIsDefined && othIsReflexive && !othIsNull && !othIsSymbol) ||
        (valIsNull && othIsDefined && othIsReflexive) ||
        (!valIsDefined && othIsReflexive) ||
        !valIsReflexive) {
      return 1;
    }
    if ((!valIsNull && !valIsSymbol && !othIsSymbol && value < other) ||
        (othIsSymbol && valIsDefined && valIsReflexive && !valIsNull && !valIsSymbol) ||
        (othIsNull && valIsDefined && valIsReflexive) ||
        (!othIsDefined && valIsReflexive) ||
        !othIsReflexive) {
      return -1;
    }
  }
  return 0;
}

module.exports = compareAscending;


/***/ }),

/***/ 601:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "w-main project-panel" },
    [
      _c("v-title", [
        _vm._v(
          _vm._s(_vm.$L("项目面板")) +
            "-" +
            _vm._s(_vm.$L("轻量级的团队在线协作"))
        )
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "w-nav" }, [
        _c("div", { staticClass: "nav-row" }, [
          _c("div", { staticClass: "w-nav-left" }, [
            _c("div", { staticClass: "page-nav-left" }, [
              _c("span", { staticClass: "bold" }, [
                _vm._v(_vm._s(_vm.projectDetail.title))
              ]),
              _vm._v(" "),
              _vm.loadIng > 0
                ? _c(
                    "div",
                    { staticClass: "page-nav-loading" },
                    [_c("w-loading")],
                    1
                  )
                : _c("div", { staticClass: "page-nav-refresh" }, [
                    _c(
                      "em",
                      {
                        on: {
                          click: function($event) {
                            return _vm.getDetail(true)
                          }
                        }
                      },
                      [_vm._v(_vm._s(_vm.$L("刷新")))]
                    )
                  ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "w-nav-flex" }),
          _vm._v(" "),
          _c("div", { staticClass: "w-nav-right" }, [
            _c(
              "span",
              {
                staticClass: "ft hover",
                class: { active: _vm.filtrTask != "" }
              },
              [
                _c(
                  "Dropdown",
                  {
                    attrs: { transfer: "" },
                    on: {
                      "on-click": function(res) {
                        _vm.filtrTask = res
                      }
                    }
                  },
                  [
                    _c("Icon", {
                      staticClass: "icon",
                      attrs: { type: "md-funnel" }
                    }),
                    _vm._v(
                      " " +
                        _vm._s(_vm.$L("筛选")) +
                        "\n                        "
                    ),
                    _c(
                      "DropdownMenu",
                      { attrs: { slot: "list" }, slot: "list" },
                      [
                        _c(
                          "DropdownItem",
                          {
                            class: { "dropdown-active": _vm.filtrTask == "" },
                            attrs: { name: "" }
                          },
                          [_vm._v(_vm._s(_vm.$L("全部任务")))]
                        ),
                        _vm._v(" "),
                        _c(
                          "DropdownItem",
                          {
                            class: {
                              "dropdown-active": _vm.filtrTask == "persons"
                            },
                            attrs: { name: "persons" }
                          },
                          [_vm._v(_vm._s(_vm.$L("我负责的任务")))]
                        ),
                        _vm._v(" "),
                        _c(
                          "DropdownItem",
                          {
                            class: {
                              "dropdown-active": _vm.filtrTask == "follower"
                            },
                            attrs: { name: "follower" }
                          },
                          [_vm._v(_vm._s(_vm.$L("我关注的任务")))]
                        ),
                        _vm._v(" "),
                        _c(
                          "DropdownItem",
                          {
                            class: {
                              "dropdown-active": _vm.filtrTask == "create"
                            },
                            attrs: { name: "create" }
                          },
                          [_vm._v(_vm._s(_vm.$L("我创建的任务")))]
                        )
                      ],
                      1
                    )
                  ],
                  1
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "span",
              { staticClass: "m768-show-i" },
              [
                _c(
                  "Dropdown",
                  {
                    attrs: { trigger: "click", transfer: "" },
                    on: { "on-click": _vm.openProjectDrawer }
                  },
                  [
                    _c("Icon", { attrs: { type: "md-menu", size: "18" } }),
                    _vm._v(" "),
                    _c(
                      "DropdownMenu",
                      { attrs: { slot: "list" }, slot: "list" },
                      [
                        _c("DropdownItem", { attrs: { name: "lists" } }, [
                          _vm._v(_vm._s(_vm.$L("列表")))
                        ]),
                        _vm._v(" "),
                        _c(
                          "DropdownItem",
                          { attrs: { name: "projectGanttShow" } },
                          [_vm._v(_vm._s(_vm.$L("甘特图")))]
                        ),
                        _vm._v(" "),
                        _c("DropdownItem", { attrs: { name: "files" } }, [
                          _vm._v(_vm._s(_vm.$L("文件")))
                        ]),
                        _vm._v(" "),
                        _c("DropdownItem", { attrs: { name: "logs" } }, [
                          _vm._v(_vm._s(_vm.$L("动态")))
                        ]),
                        _vm._v(" "),
                        _c(
                          "DropdownItem",
                          { attrs: { name: "openProjectSettingDrawer" } },
                          [_vm._v(_vm._s(_vm.$L("设置")))]
                        )
                      ],
                      1
                    )
                  ],
                  1
                )
              ],
              1
            ),
            _vm._v(" "),
            _c("span", { staticClass: "m768-hide-i" }, [
              _c(
                "span",
                {
                  staticClass: "ft hover",
                  on: {
                    click: function($event) {
                      return _vm.openProjectDrawer("lists")
                    }
                  }
                },
                [
                  _c("i", { staticClass: "ft icon" }, [_vm._v("")]),
                  _vm._v(" " + _vm._s(_vm.$L("列表")))
                ]
              ),
              _vm._v(" "),
              _c(
                "span",
                {
                  staticClass: "ft hover",
                  class: { active: _vm.projectGanttShow },
                  on: {
                    click: function($event) {
                      _vm.projectGanttShow = !_vm.projectGanttShow
                    }
                  }
                },
                [
                  _c("i", { staticClass: "ft icon" }, [_vm._v("")]),
                  _vm._v(" " + _vm._s(_vm.$L("甘特图")))
                ]
              ),
              _vm._v(" "),
              _c(
                "span",
                {
                  staticClass: "ft hover",
                  on: {
                    click: function($event) {
                      return _vm.openProjectDrawer("files")
                    }
                  }
                },
                [
                  _c("i", { staticClass: "ft icon" }, [_vm._v("")]),
                  _vm._v(" " + _vm._s(_vm.$L("文件")))
                ]
              ),
              _vm._v(" "),
              _c(
                "span",
                {
                  staticClass: "ft hover",
                  on: {
                    click: function($event) {
                      return _vm.openProjectDrawer("logs")
                    }
                  }
                },
                [
                  _c("i", { staticClass: "ft icon" }, [_vm._v("")]),
                  _vm._v(" " + _vm._s(_vm.$L("动态")))
                ]
              ),
              _vm._v(" "),
              _c(
                "span",
                {
                  staticClass: "ft hover",
                  on: {
                    click: function($event) {
                      return _vm.openProjectSettingDrawer("setting")
                    }
                  }
                },
                [
                  _c("i", { staticClass: "ft icon" }, [_vm._v("")]),
                  _vm._v(" " + _vm._s(_vm.$L("设置")))
                ]
              )
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c(
        "w-content",
        [
          _c(
            "draggable",
            {
              staticClass: "label-box",
              style: {
                visibility: _vm.projectGanttShow ? "hidden" : "visible"
              },
              attrs: {
                draggable: ".label-draggable",
                animation: 150,
                disabled: _vm.projectSortDisabled || _vm.windowMax768
              },
              on: {
                sort: function($event) {
                  return _vm.projectSortUpdate(true)
                }
              },
              model: {
                value: _vm.projectLabel,
                callback: function($$v) {
                  _vm.projectLabel = $$v
                },
                expression: "projectLabel"
              }
            },
            [
              _vm._l(_vm.projectLabel, function(label) {
                return _vm.projectLabel.length > 0
                  ? _c(
                      "div",
                      {
                        key: label.id,
                        staticClass: "label-item label-draggable",
                        class: {
                          "label-scroll":
                            label.hasScroll === true && label.endScroll !== true
                        },
                        on: {
                          mouseenter: function($event) {
                            return _vm.projectMouse(label)
                          }
                        }
                      },
                      [
                        _c(
                          "div",
                          { staticClass: "label-body" },
                          [
                            _c(
                              "div",
                              { staticClass: "title-box" },
                              [
                                label.loadIng === true
                                  ? _c(
                                      "div",
                                      { staticClass: "title-loading" },
                                      [_c("w-loading")],
                                      1
                                    )
                                  : _vm._e(),
                                _vm._v(" "),
                                _c("h2", [_vm._v(_vm._s(label.title))]),
                                _vm._v(" "),
                                _c(
                                  "Dropdown",
                                  {
                                    attrs: { trigger: "click", transfer: "" },
                                    on: {
                                      "on-click": function($event) {
                                        return _vm.handleLabel($event, label)
                                      }
                                    }
                                  },
                                  [
                                    _c("Icon", { attrs: { type: "ios-more" } }),
                                    _vm._v(" "),
                                    _c(
                                      "DropdownMenu",
                                      { attrs: { slot: "list" }, slot: "list" },
                                      [
                                        _c(
                                          "Dropdown-item",
                                          { attrs: { name: "refresh" } },
                                          [_vm._v(_vm._s(_vm.$L("刷新列表")))]
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "Dropdown-item",
                                          { attrs: { name: "rename" } },
                                          [_vm._v(_vm._s(_vm.$L("重命名")))]
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "Dropdown-item",
                                          { attrs: { name: "delete" } },
                                          [_vm._v(_vm._s(_vm.$L("删除")))]
                                        )
                                      ],
                                      1
                                    )
                                  ],
                                  1
                                )
                              ],
                              1
                            ),
                            _vm._v(" "),
                            _c(
                              "ScrollerY",
                              {
                                ref: "box_" + label.id,
                                refInFor: true,
                                staticClass: "task-box",
                                on: {
                                  "on-scroll": function($event) {
                                    return _vm.projectBoxScroll($event, label)
                                  }
                                }
                              },
                              [
                                _c(
                                  "draggable",
                                  {
                                    staticClass: "task-main",
                                    class: [
                                      _vm.filtrTask
                                        ? "filtr-" + _vm.filtrTask
                                        : ""
                                    ],
                                    attrs: {
                                      group: "task",
                                      draggable: ".task-draggable",
                                      animation: 150,
                                      disabled:
                                        _vm.projectSortDisabled ||
                                        _vm.windowMax768
                                    },
                                    on: {
                                      sort: function($event) {
                                        return _vm.projectSortUpdate(false)
                                      },
                                      remove: function($event) {
                                        return _vm.projectSortUpdate(false)
                                      }
                                    },
                                    model: {
                                      value: label.taskLists,
                                      callback: function($$v) {
                                        _vm.$set(label, "taskLists", $$v)
                                      },
                                      expression: "label.taskLists"
                                    }
                                  },
                                  [
                                    _vm._l(label.taskLists, function(task) {
                                      return _c(
                                        "div",
                                        {
                                          key: task.id,
                                          staticClass:
                                            "task-item task-draggable",
                                          class: {
                                            "persons-item": _vm.isPersonsTask(
                                              task
                                            ),
                                            "follower-item": _vm.isFollowerTask(
                                              task
                                            ),
                                            "create-item": _vm.isCreateTask(
                                              task
                                            )
                                          }
                                        },
                                        [
                                          _c(
                                            "div",
                                            {
                                              staticClass: "task-shadow",
                                              class: [
                                                "p" + task.level,
                                                task.complete ? "complete" : "",
                                                task.overdue ? "overdue" : "",
                                                task.isNewtask === true
                                                  ? "newtask"
                                                  : ""
                                              ],
                                              on: {
                                                click: function($event) {
                                                  return _vm.openTaskModal(task)
                                                }
                                              }
                                            },
                                            [
                                              _c(
                                                "div",
                                                {
                                                  staticClass:
                                                    "subtask-progress"
                                                },
                                                [
                                                  _c("em", {
                                                    style: {
                                                      width:
                                                        _vm.subtaskProgress(
                                                          task
                                                        ) + "%"
                                                    }
                                                  })
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                { staticClass: "task-title" },
                                                [
                                                  _vm._v(_vm._s(task.title)),
                                                  task.desc
                                                    ? _c("Icon", {
                                                        attrs: {
                                                          type:
                                                            "ios-list-box-outline"
                                                        }
                                                      })
                                                    : _vm._e()
                                                ],
                                                1
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "div",
                                                { staticClass: "task-more" },
                                                [
                                                  task.overdue
                                                    ? _c(
                                                        "div",
                                                        {
                                                          staticClass:
                                                            "task-status"
                                                        },
                                                        [
                                                          _vm._v(
                                                            _vm._s(
                                                              _vm.$L("已超期")
                                                            )
                                                          )
                                                        ]
                                                      )
                                                    : task.complete
                                                    ? _c(
                                                        "div",
                                                        {
                                                          staticClass:
                                                            "task-status"
                                                        },
                                                        [
                                                          _vm._v(
                                                            _vm._s(
                                                              _vm.$L("已完成")
                                                            )
                                                          )
                                                        ]
                                                      )
                                                    : _c(
                                                        "div",
                                                        {
                                                          staticClass:
                                                            "task-status"
                                                        },
                                                        [
                                                          _vm._v(
                                                            _vm._s(
                                                              _vm.$L("未完成")
                                                            )
                                                          )
                                                        ]
                                                      ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "div",
                                                    {
                                                      staticClass:
                                                        "task-persons",
                                                      class: {
                                                        "persons-more":
                                                          task.persons.length >
                                                          1
                                                      }
                                                    },
                                                    _vm._l(
                                                      task.persons,
                                                      function(person, iper) {
                                                        return _c(
                                                          "Tooltip",
                                                          {
                                                            key: iper,
                                                            staticClass:
                                                              "task-userimg",
                                                            attrs: {
                                                              content:
                                                                person.nickname ||
                                                                person.username,
                                                              transfer: ""
                                                            }
                                                          },
                                                          [
                                                            _c("UserImg", {
                                                              staticClass:
                                                                "avatar",
                                                              attrs: {
                                                                info: person
                                                              }
                                                            })
                                                          ],
                                                          1
                                                        )
                                                      }
                                                    ),
                                                    1
                                                  )
                                                ]
                                              )
                                            ]
                                          )
                                        ]
                                      )
                                    }),
                                    _vm._v(" "),
                                    _c(
                                      "div",
                                      {
                                        attrs: { slot: "footer" },
                                        slot: "footer"
                                      },
                                      [
                                        _c("project-add-task", {
                                          ref: "add_" + label.id,
                                          refInFor: true,
                                          attrs: {
                                            placeholder:
                                              _vm.$L("添加任务至") +
                                              '"' +
                                              label.title +
                                              '"',
                                            projectid: label.projectid,
                                            labelid: label.id
                                          },
                                          on: {
                                            "on-add-success": function($event) {
                                              return _vm.addTaskSuccess(
                                                $event,
                                                label
                                              )
                                            }
                                          }
                                        })
                                      ],
                                      1
                                    )
                                  ],
                                  2
                                )
                              ],
                              1
                            )
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          {
                            staticClass: "label-bottom",
                            on: {
                              click: function($event) {
                                return _vm.projectFocus(label)
                              }
                            }
                          },
                          [
                            _c("Icon", {
                              staticClass: "label-bottom-icon",
                              attrs: { type: "ios-add" }
                            })
                          ],
                          1
                        )
                      ]
                    )
                  : _vm._e()
              }),
              _vm._v(" "),
              _vm.loadDetailed
                ? _c(
                    "div",
                    {
                      staticClass: "label-item label-create",
                      attrs: { slot: "footer" },
                      on: { click: _vm.addLabel },
                      slot: "footer"
                    },
                    [
                      _c("div", { staticClass: "label-body" }, [
                        _c("div", { staticClass: "trigger-box ft hover" }, [
                          _c("i", { staticClass: "ft icon" }, [_vm._v("")]),
                          _vm._v(_vm._s(_vm.$L("添加一个新列表")))
                        ])
                      ])
                    ]
                  )
                : _vm._e()
            ],
            2
          ),
          _vm._v(" "),
          _vm.projectGanttShow
            ? _c("project-gantt", {
                attrs: { projectLabel: _vm.projectLabel },
                on: {
                  "on-close": function($event) {
                    _vm.projectGanttShow = false
                  }
                }
              })
            : _vm._e()
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "WDrawer",
        {
          attrs: { maxWidth: "1080" },
          model: {
            value: _vm.projectDrawerShow,
            callback: function($$v) {
              _vm.projectDrawerShow = $$v
            },
            expression: "projectDrawerShow"
          }
        },
        [
          _vm.projectDrawerShow
            ? _c(
                "Tabs",
                {
                  model: {
                    value: _vm.projectDrawerTab,
                    callback: function($$v) {
                      _vm.projectDrawerTab = $$v
                    },
                    expression: "projectDrawerTab"
                  }
                },
                [
                  _c(
                    "TabPane",
                    { attrs: { label: _vm.$L("任务列表"), name: "lists" } },
                    [
                      _c("project-task-lists", {
                        attrs: {
                          canload:
                            _vm.projectDrawerShow &&
                            _vm.projectDrawerTab == "lists",
                          projectid: _vm.projectid,
                          labelLists: _vm.projectSimpleLabel
                        }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "TabPane",
                    { attrs: { label: _vm.$L("文件列表"), name: "files" } },
                    [
                      _c("project-task-files", {
                        attrs: {
                          canload:
                            _vm.projectDrawerShow &&
                            _vm.projectDrawerTab == "files",
                          projectid: _vm.projectid
                        }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "TabPane",
                    { attrs: { label: _vm.$L("项目动态"), name: "logs" } },
                    [
                      _c("project-task-logs", {
                        attrs: {
                          canload:
                            _vm.projectDrawerShow &&
                            _vm.projectDrawerTab == "logs",
                          projectid: _vm.projectid
                        }
                      })
                    ],
                    1
                  )
                ],
                1
              )
            : _vm._e()
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "WDrawer",
        {
          attrs: { maxWidth: "1000" },
          model: {
            value: _vm.projectSettingDrawerShow,
            callback: function($$v) {
              _vm.projectSettingDrawerShow = $$v
            },
            expression: "projectSettingDrawerShow"
          }
        },
        [
          _vm.projectSettingDrawerShow
            ? _c(
                "Tabs",
                {
                  model: {
                    value: _vm.projectSettingDrawerTab,
                    callback: function($$v) {
                      _vm.projectSettingDrawerTab = $$v
                    },
                    expression: "projectSettingDrawerTab"
                  }
                },
                [
                  _c(
                    "TabPane",
                    { attrs: { label: _vm.$L("项目设置"), name: "setting" } },
                    [
                      _c("project-setting", {
                        attrs: {
                          canload:
                            _vm.projectSettingDrawerShow &&
                            _vm.projectSettingDrawerTab == "setting",
                          projectid: _vm.projectid
                        },
                        on: { "on-change": _vm.getDetail }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "TabPane",
                    {
                      attrs: { label: _vm.$L("已归档任务"), name: "archived" }
                    },
                    [
                      _c("project-archived", {
                        attrs: {
                          canload:
                            _vm.projectSettingDrawerShow &&
                            _vm.projectSettingDrawerTab == "archived",
                          projectid: _vm.projectid
                        }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "TabPane",
                    {
                      attrs: { label: _vm.$L("项目统计"), name: "statistics" }
                    },
                    [
                      _c("project-statistics", {
                        ref: "statistics",
                        attrs: {
                          canload:
                            _vm.projectSettingDrawerShow &&
                            _vm.projectSettingDrawerTab == "statistics",
                          projectid: _vm.projectid
                        }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "TabPane",
                    { attrs: { label: _vm.$L("成员管理"), name: "member" } },
                    [
                      _c("project-users", {
                        attrs: {
                          canload:
                            _vm.projectSettingDrawerShow &&
                            _vm.projectSettingDrawerTab == "member",
                          projectid: _vm.projectid
                        }
                      })
                    ],
                    1
                  )
                ],
                1
              )
            : _vm._e()
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-4bac3242", module.exports)
  }
}

/***/ })

});