webpackJsonp([15],{

/***/ 326:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(327)
  __webpack_require__(329)
}
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(331)
/* template */
var __vue_template__ = __webpack_require__(332)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-c6b4dc88"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/main/components/TEditor.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-c6b4dc88", Component.options)
  } else {
    hotAPI.reload("data-v-c6b4dc88", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 327:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(328);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(1)("b8965312", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-c6b4dc88\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./TEditor.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-c6b4dc88\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./TEditor.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 328:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)(false);
// imports


// module
exports.push([module.i, "\n.teditor-box textarea {\n  opacity: 0;\n}\n.teditor-box .tox-tinymce {\n  -webkit-box-shadow: none;\n          box-shadow: none;\n  -webkit-box-sizing: border-box;\n          box-sizing: border-box;\n  border-color: #dddee1;\n  border-radius: 4px;\n  overflow: hidden;\n}\n.teditor-box .tox-tinymce .tox-statusbar span.tox-statusbar__branding a {\n    display: none;\n}\n.teditor-transfer {\n  background-color: #ffffff;\n}\n.teditor-transfer .tox-toolbar > div:last-child > button:last-child {\n    margin-right: 64px;\n}\n.teditor-transfer .ivu-modal-header {\n    display: none;\n}\n.teditor-transfer .ivu-modal-close {\n    top: 7px;\n    z-index: 2;\n}\n.teditor-transfer .teditor-transfer-body {\n    position: absolute;\n    top: 0;\n    left: 0;\n    width: 100%;\n    height: 100%;\n    padding: 0;\n    margin: 0;\n}\n.teditor-transfer .teditor-transfer-body textarea {\n      opacity: 0;\n}\n.teditor-transfer .teditor-transfer-body .tox-tinymce {\n      border: 0;\n}\n.teditor-transfer .teditor-transfer-body .tox-tinymce .tox-statusbar span.tox-statusbar__branding a {\n        display: none;\n}\n.tox.tox-silver-sink {\n  z-index: 13000;\n}\n", ""]);

// exports


/***/ }),

/***/ 329:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(330);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(1)("4b0c3b6f", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-c6b4dc88\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=1!./TEditor.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-c6b4dc88\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=1!./TEditor.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 330:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)(false);
// imports


// module
exports.push([module.i, "\n.teditor-loadstyle[data-v-c6b4dc88] {\n  width: 100%;\n  height: 180px;\n  overflow: hidden;\n  position: relative;\n}\n.teditor-loadedstyle[data-v-c6b4dc88] {\n  width: 100%;\n  max-height: inherit;\n  overflow: inherit;\n  position: relative;\n}\n.upload-control[data-v-c6b4dc88] {\n  display: none;\n  width: 0;\n  height: 0;\n  overflow: hidden;\n}\n", ""]);

// exports


/***/ }),

/***/ 331:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_tinymce_tinymce__ = __webpack_require__(53);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_tinymce_tinymce___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_tinymce_tinymce__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ImgUpload__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ImgUpload___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__ImgUpload__);
var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
    name: 'TEditor',
    components: { ImgUpload: __WEBPACK_IMPORTED_MODULE_1__ImgUpload___default.a },
    props: {
        id: {
            type: String,
            default: function _default() {
                return "tinymce_" + Math.round(Math.random() * 10000);
            }
        },
        value: {
            default: ''
        },
        height: {
            default: 360
        },
        htmlClass: {
            default: '',
            type: String
        },
        plugins: {
            type: Array,
            default: function _default() {
                return ['advlist autolink lists link image charmap print preview hr anchor pagebreak imagetools', 'searchreplace visualblocks visualchars code', 'insertdatetime media nonbreaking save table contextmenu directionality', 'emoticons paste textcolor colorpicker imagetools codesample'];
            }
        },
        toolbar: {
            type: String,
            default: ' undo redo | styleselect | uploadImages | uploadFiles | bold italic underline forecolor backcolor | alignleft aligncenter alignright | bullist numlist outdent indent | link image emoticons media codesample | preview screenload'
        },
        other_options: {
            type: Object,
            default: function _default() {
                return {};
            }
        },
        readonly: {
            type: Boolean,
            default: false
        }
    },
    data: function data() {
        return {
            content: '',
            editor: null,
            editorT: null,
            cTinyMce: null,
            checkerTimeout: null,
            isTyping: false,

            spinShow: true,
            transfer: false,

            uploadIng: 0,
            uploadFormat: ['jpg', 'jpeg', 'png', 'gif', 'doc', 'docx', 'xls', 'xlsx', 'ppt', 'pptx', 'txt', 'esp', 'pdf', 'rar', 'zip', 'gz', 'ai', 'avi', 'bmp', 'cdr', 'eps', 'mov', 'mp3', 'mp4', 'pr', 'psd', 'svg', 'tif'],
            actionUrl: $A.apiUrl('system/fileupload'),
            params: { token: $A.getToken() },
            maxSize: 204800
        };
    },
    mounted: function mounted() {
        this.content = this.value;
        this.init();
    },
    activated: function activated() {
        this.content = this.value;
        this.init();
    },
    deactivated: function deactivated() {
        if (this.editor !== null) {
            this.editor.destroy();
        }
        this.spinShow = true;
        $A(this.$refs.myTextarea).show();
    },

    watch: {
        value: function value(newValue) {
            if (newValue == null) {
                newValue = "";
            }
            if (!this.isTyping) {
                if (this.getEditor() !== null) {
                    this.getEditor().setContent(newValue);
                } else {
                    this.content = newValue;
                }
            }
        },
        readonly: function readonly(value) {
            if (this.editor !== null) {
                if (value) {
                    this.editor.setMode('readonly');
                } else {
                    this.editor.setMode('design');
                }
            }
        }
    },
    methods: {
        init: function init() {
            var _this = this;

            this.$nextTick(function () {
                __WEBPACK_IMPORTED_MODULE_0_tinymce_tinymce___default.a.init(_this.concatAssciativeArrays(_this.options(false), _this.other_options));
            });
        },
        initTransfer: function initTransfer() {
            var _this2 = this;

            this.$nextTick(function () {
                __WEBPACK_IMPORTED_MODULE_0_tinymce_tinymce___default.a.init(_this2.concatAssciativeArrays(_this2.options(true), _this2.other_options));
            });
        },
        options: function options(isFull) {
            var _this3 = this;

            return {
                selector: (isFull ? '#T_' : '#') + this.id,
                base_url: $A.serverUrl('js/build'),
                language: "zh_CN",
                toolbar: this.toolbar,
                plugins: this.plugins,
                save_onsavecallback: function save_onsavecallback(e) {
                    _this3.$emit('editorSave', e);
                },
                paste_data_images: true,
                menu: {
                    view: {
                        title: 'View',
                        items: 'code | visualaid visualchars visualblocks | spellchecker | preview fullscreen screenload | showcomments'
                    },
                    insert: {
                        title: "Insert",
                        items: "image link media addcomment pageembed template codesample inserttable | charmap emoticons hr | pagebreak nonbreaking anchor toc | insertdatetime | uploadImages browseImages | uploadFiles"
                    }
                },
                codesample_languages: [{ text: "HTML/VUE/XML", value: "markup" }, { text: "JavaScript", value: "javascript" }, { text: "CSS", value: "css" }, { text: "PHP", value: "php" }, { text: "Ruby", value: "ruby" }, { text: "Python", value: "python" }, { text: "Java", value: "java" }, { text: "C", value: "c" }, { text: "C#", value: "csharp" }, { text: "C++", value: "cpp" }],
                height: isFull ? '100%' : $A.rightExists(this.height, '%') ? this.height : $A.runNum(this.height) || 360,
                resize: !isFull,
                convert_urls: false,
                toolbar_mode: 'sliding',
                toolbar_drawer: 'floating',
                setup: function setup(editor) {
                    editor.ui.registry.addMenuButton('uploadImages', {
                        text: _this3.$L('图片'),
                        tooltip: _this3.$L('上传/浏览 图片'),
                        fetch: function fetch(callback) {
                            var items = [{
                                type: 'menuitem',
                                text: _this3.$L('上传图片'),
                                onAction: function onAction() {
                                    _this3.$refs.myUpload.handleClick();
                                }
                            }, {
                                type: 'menuitem',
                                text: _this3.$L('浏览图片'),
                                onAction: function onAction() {
                                    _this3.$refs.myUpload.browsePicture();
                                }
                            }];
                            callback(items);
                        }
                    });
                    editor.ui.registry.addMenuItem('uploadImages', {
                        text: _this3.$L('上传图片'),
                        onAction: function onAction() {
                            _this3.$refs.myUpload.handleClick();
                        }
                    });
                    editor.ui.registry.addMenuItem('browseImages', {
                        text: _this3.$L('浏览图片'),
                        onAction: function onAction() {
                            _this3.$refs.myUpload.browsePicture();
                        }
                    });
                    editor.ui.registry.addButton('uploadFiles', {
                        text: _this3.$L('文件'),
                        tooltip: _this3.$L('上传文件'),
                        onAction: function onAction() {
                            if (_this3.handleBeforeUpload()) {
                                _this3.$refs.fileUpload.handleClick();
                            }
                        }
                    });
                    editor.ui.registry.addMenuItem('uploadFiles', {
                        text: _this3.$L('上传文件'),
                        onAction: function onAction() {
                            if (_this3.handleBeforeUpload()) {
                                _this3.$refs.fileUpload.handleClick();
                            }
                        }
                    });
                    if (isFull) {
                        editor.ui.registry.addButton('screenload', {
                            icon: 'fullscreen',
                            tooltip: _this3.$L('退出全屏'),
                            onAction: function onAction() {
                                _this3.closeFull();
                            }
                        });
                        editor.ui.registry.addMenuItem('screenload', {
                            text: _this3.$L('退出全屏'),
                            onAction: function onAction() {
                                _this3.closeFull();
                            }
                        });
                        editor.on('Init', function (e) {
                            _this3.editorT = editor;
                            _this3.editorT.setContent(_this3.content);
                            if (_this3.readonly) {
                                _this3.editorT.setMode('readonly');
                            } else {
                                _this3.editorT.setMode('design');
                            }
                        });
                    } else {
                        editor.ui.registry.addButton('screenload', {
                            icon: 'fullscreen',
                            tooltip: _this3.$L('全屏'),
                            onAction: function onAction() {
                                _this3.content = editor.getContent();
                                _this3.transfer = true;
                                _this3.initTransfer();
                            }
                        });
                        editor.ui.registry.addMenuItem('screenload', {
                            text: _this3.$L('全屏'),
                            onAction: function onAction() {
                                _this3.content = editor.getContent();
                                _this3.transfer = true;
                                _this3.initTransfer();
                            }
                        });
                        editor.on('Init', function (e) {
                            _this3.spinShow = false;
                            _this3.editor = editor;
                            _this3.editor.setContent(_this3.content);
                            if (_this3.readonly) {
                                _this3.editor.setMode('readonly');
                            } else {
                                _this3.editor.setMode('design');
                            }
                            _this3.$emit('editorInit', _this3.editor);
                        });
                        editor.on('KeyUp', function (e) {
                            if (_this3.editor !== null) {
                                _this3.submitNewContent();
                            }
                        });
                        editor.on('Change', function (e) {
                            if (_this3.editor !== null) {
                                if (_this3.getContent() !== _this3.value) {
                                    _this3.submitNewContent();
                                }
                                _this3.$emit('editorChange', e);
                            }
                        });
                    }
                }
            };
        },
        closeFull: function closeFull() {
            this.content = this.getContent();
            this.$emit('input', this.content);
            this.transfer = false;
            if (this.editorT != null) {
                this.editorT.destroy();
                this.editorT = null;
            }
        },
        transferChange: function transferChange(visible) {
            if (!visible && this.editorT != null) {
                this.content = this.editorT.getContent();
                this.$emit('input', this.content);
                this.editorT.destroy();
                this.editorT = null;
            }
        },
        getEditor: function getEditor() {
            return this.transfer ? this.editorT : this.editor;
        },
        concatAssciativeArrays: function concatAssciativeArrays(array1, array2) {
            if (array2.length === 0) return array1;
            if (array1.length === 0) return array2;
            var dest = [];
            for (var key in array1) {
                if (array1.hasOwnProperty(key)) {
                    dest[key] = array1[key];
                }
            }
            for (var _key in array2) {
                if (array2.hasOwnProperty(_key)) {
                    dest[_key] = array2[_key];
                }
            }
            return dest;
        },
        submitNewContent: function submitNewContent() {
            var _this4 = this;

            this.isTyping = true;
            if (this.checkerTimeout !== null) {
                clearTimeout(this.checkerTimeout);
            }
            this.checkerTimeout = setTimeout(function () {
                _this4.isTyping = false;
            }, 300);
            this.$emit('input', this.getContent());
        },
        insertContent: function insertContent(content) {
            if (this.getEditor() !== null) {
                this.getEditor().insertContent(content);
            } else {
                this.content += content;
            }
        },
        getContent: function getContent() {
            if (this.getEditor() === null) {
                return "";
            }
            return this.getEditor().getContent();
        },
        insertImage: function insertImage(src) {
            this.insertContent('<img src="' + src + '">');
        },
        editorImage: function editorImage(lists) {
            for (var i = 0; i < lists.length; i++) {
                var item = lists[i];
                if ((typeof item === 'undefined' ? 'undefined' : _typeof(item)) === 'object' && typeof item.url === "string") {
                    this.insertImage(item.url);
                }
            }
        },


        /********************文件上传部分************************/

        handleProgress: function handleProgress() {
            //开始上传
            this.uploadIng++;
        },
        handleSuccess: function handleSuccess(res, file) {
            //上传完成
            this.uploadIng--;
            if (res.ret === 1) {
                this.insertContent('<a href="' + res.data.url + '" target="_blank">' + res.data.name + ' (' + $A.bytesToSize(res.data.size * 1024) + ')</a>');
            } else {
                this.$Modal.warning({
                    title: this.$L('上传失败'),
                    content: this.$L('文件 % 上传失败，%', file.name, res.msg)
                });
            }
        },
        handleError: function handleError() {
            //上传错误
            this.uploadIng--;
        },
        handleFormatError: function handleFormatError(file) {
            //上传类型错误
            this.$Modal.warning({
                title: this.$L('文件格式不正确'),
                content: this.$L('文件 % 格式不正确，仅支持上传：%', file.name, this.uploadFormat.join(','))
            });
        },
        handleMaxSize: function handleMaxSize(file) {
            //上传大小错误
            this.$Modal.warning({
                title: this.$L('超出文件大小限制'),
                content: this.$L('文件 % 太大，不能超过%。', file.name, $A.bytesToSize(this.maxSize * 1024))
            });
        },
        handleBeforeUpload: function handleBeforeUpload() {
            //上传前判断
            this.params = {
                token: $A.getToken()
            };
            return true;
        }
    }
});

/***/ }),

/***/ 332:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "div",
        {
          staticClass: "teditor-box",
          class: [_vm.spinShow ? "teditor-loadstyle" : "teditor-loadedstyle"]
        },
        [
          _c("textarea", { ref: "myTextarea", attrs: { id: _vm.id } }, [
            _vm._v(_vm._s(_vm.content))
          ]),
          _vm._v(" "),
          _vm.spinShow
            ? _c(
                "Spin",
                { attrs: { fix: "" } },
                [
                  _c("Icon", {
                    staticClass: "upload-control-spin-icon-load",
                    attrs: { type: "ios-loading", size: "18" }
                  }),
                  _vm._v(" "),
                  _c("div", [_vm._v(_vm._s(_vm.$L("加载组件中...")))])
                ],
                1
              )
            : _vm._e(),
          _vm._v(" "),
          _c("ImgUpload", {
            ref: "myUpload",
            staticClass: "upload-control",
            attrs: { type: "callback", uploadIng: _vm.uploadIng, num: "50" },
            on: {
              "update:uploadIng": function($event) {
                _vm.uploadIng = $event
              },
              "update:upload-ing": function($event) {
                _vm.uploadIng = $event
              },
              "on-callback": _vm.editorImage
            }
          }),
          _vm._v(" "),
          _c("Upload", {
            ref: "fileUpload",
            staticClass: "upload-control",
            attrs: {
              name: "files",
              action: _vm.actionUrl,
              data: _vm.params,
              multiple: "",
              format: _vm.uploadFormat,
              "show-upload-list": false,
              "max-size": _vm.maxSize,
              "on-progress": _vm.handleProgress,
              "on-success": _vm.handleSuccess,
              "on-error": _vm.handleError,
              "on-format-error": _vm.handleFormatError,
              "on-exceeded-size": _vm.handleMaxSize,
              "before-upload": _vm.handleBeforeUpload
            }
          })
        ],
        1
      ),
      _vm._v(" "),
      _vm.uploadIng > 0
        ? _c(
            "Spin",
            { attrs: { fix: "" } },
            [
              _c("Icon", {
                staticClass: "upload-control-spin-icon-load",
                attrs: { type: "ios-loading" }
              }),
              _vm._v(" "),
              _c("div", [_vm._v(_vm._s(_vm.$L("正在上传文件...")))])
            ],
            1
          )
        : _vm._e(),
      _vm._v(" "),
      _c(
        "Modal",
        {
          staticClass: "teditor-transfer",
          attrs: { "footer-hide": "", fullscreen: "", transfer: "" },
          on: { "on-visible-change": _vm.transferChange },
          model: {
            value: _vm.transfer,
            callback: function($$v) {
              _vm.transfer = $$v
            },
            expression: "transfer"
          }
        },
        [
          _c(
            "div",
            { attrs: { slot: "close" }, slot: "close" },
            [
              _c("Button", { attrs: { type: "primary", size: "small" } }, [
                _vm._v(_vm._s(_vm.$L("完成")))
              ])
            ],
            1
          ),
          _vm._v(" "),
          _c("div", { staticClass: "teditor-transfer-body" }, [
            _c("textarea", { attrs: { id: "T_" + _vm.id } }, [
              _vm._v(_vm._s(_vm.content))
            ])
          ]),
          _vm._v(" "),
          _vm.uploadIng > 0
            ? _c(
                "Spin",
                { attrs: { fix: "" } },
                [
                  _c("Icon", {
                    staticClass: "upload-control-spin-icon-load",
                    attrs: { type: "ios-loading" }
                  }),
                  _vm._v(" "),
                  _c("div", [_vm._v(_vm._s(_vm.$L("正在上传文件...")))])
                ],
                1
              )
            : _vm._e()
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-c6b4dc88", module.exports)
  }
}

/***/ })

});