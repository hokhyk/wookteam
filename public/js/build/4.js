webpackJsonp([4],{

/***/ 289:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(512)
}
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(514)
/* template */
var __vue_template__ = __webpack_require__(530)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-426c4faa"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/main/pages/project.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-426c4faa", Component.options)
  } else {
    hotAPI.reload("data-v-426c4faa", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 305:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(306)
}
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(308)
/* template */
var __vue_template__ = __webpack_require__(309)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-35be3d57"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/main/components/WContent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-35be3d57", Component.options)
  } else {
    hotAPI.reload("data-v-35be3d57", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 306:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(307);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(1)("5ee96958", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-35be3d57\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./WContent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-35be3d57\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./WContent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 307:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)(false);
// imports


// module
exports.push([module.i, "\n.w-content[data-v-35be3d57] {\n  position: absolute;\n  top: 72px;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  overflow: auto;\n  background-repeat: no-repeat;\n  background-position: center;\n  background-color: #EEEEEE;\n  background-size: cover;\n}\n.w-content .w-container[data-v-35be3d57] {\n    min-height: 500px;\n}\n", ""]);

// exports


/***/ }),

/***/ 308:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: 'WContent',
    data: function data() {
        return {
            bgid: -1
        };
    },
    mounted: function mounted() {
        this.bgid = $A.runNum(this.usrInfo.bgid);
    },

    watch: {
        usrInfo: {
            handler: function handler(info) {
                this.bgid = $A.runNum(info.bgid);
            },

            deep: true
        }
    },
    methods: {
        getBgUrl: function getBgUrl(id, thumb) {
            if (id < 0) {
                return 'none';
            }
            id = Math.max(1, parseInt(id));
            return 'url(' + window.location.origin + '/images/bg/' + (thumb ? 'thumb/' : '') + id + '.jpg' + ')';
        }
    }
});

/***/ }),

/***/ 309:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {
      staticClass: "w-content",
      style: "background-image:" + _vm.getBgUrl(_vm.bgid)
    },
    [_vm._t("default")],
    2
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-35be3d57", module.exports)
  }
}

/***/ }),

/***/ 319:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony default export */ __webpack_exports__["a"] = ({
    methods: {
        openProject: function openProject(projectid, otherParam) {
            var statistics = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : '';

            this.goForward({ name: 'project-panel', params: { projectid: projectid, statistics: statistics, other: otherParam || {} } });
        },
        outProject: function outProject(projectid, successCallback) {
            var _this = this;

            this.$Modal.confirm({
                title: this.$L('退出项目'),
                content: this.$L('你确定要退出此项目吗？'),
                loading: true,
                onOk: function onOk() {
                    $A.apiAjax({
                        url: 'project/out?projectid=' + projectid,
                        error: function error() {
                            _this.$Modal.remove();
                            alert(_this.$L('网络繁忙，请稍后再试！'));
                        },
                        success: function success(res) {
                            _this.$Modal.remove();
                            typeof successCallback === "function" && successCallback();
                            setTimeout(function () {
                                if (res.ret === 1) {
                                    _this.$Message.success(res.msg);
                                } else {
                                    _this.$Modal.error({ title: _this.$L('温馨提示'), content: res.msg });
                                }
                            }, 350);
                        }
                    });
                }
            });
        },
        favorProject: function favorProject(act, projectid, successCallback) {
            var _this2 = this;

            $A.apiAjax({
                url: 'project/favor',
                data: {
                    act: act,
                    projectid: projectid
                },
                error: function error() {
                    _this2.$Modal.remove();
                    alert(_this2.$L('网络繁忙，请稍后再试！'));
                },
                success: function success(res) {
                    _this2.$Modal.remove();
                    typeof successCallback === "function" && successCallback();
                    setTimeout(function () {
                        if (res.ret === 1) {
                            _this2.$Message.success(res.msg);
                        } else {
                            _this2.$Modal.error({ title: _this2.$L('温馨提示'), content: res.msg });
                        }
                    }, 350);
                }
            });
        },
        deleteProject: function deleteProject(projectid, successCallback) {
            var _this3 = this;

            this.$Modal.confirm({
                title: this.$L('删除项目'),
                content: this.$L('你确定要删除此项目吗？'),
                loading: true,
                onOk: function onOk() {
                    $A.apiAjax({
                        url: 'project/delete?projectid=' + projectid,
                        error: function error() {
                            _this3.$Modal.remove();
                            alert(_this3.$L('网络繁忙，请稍后再试！'));
                        },
                        success: function success(res) {
                            _this3.$Modal.remove();
                            typeof successCallback === "function" && successCallback();
                            setTimeout(function () {
                                if (res.ret === 1) {
                                    _this3.$Message.success(res.msg);
                                    $A.triggerTaskInfoListener('deleteproject', { projectid: projectid });
                                } else {
                                    _this3.$Modal.error({ title: _this3.$L('温馨提示'), content: res.msg });
                                }
                            }, 350);
                        }
                    });
                }
            });
        }
    }
});

/***/ }),

/***/ 333:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(334)
}
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(336)
/* template */
var __vue_template__ = __webpack_require__(337)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/main/components/TagInput.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-63d616a5", Component.options)
  } else {
    hotAPI.reload("data-v-63d616a5", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 334:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(335);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(1)("72d872d6", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-63d616a5\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./TagInput.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-63d616a5\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./TagInput.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 335:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)(false);
// imports


// module
exports.push([module.i, "\n.tags-wrap {\n  display: inline-block;\n  width: 100%;\n  min-height: 32px;\n  padding: 2px 7px;\n  border: 1px solid #dddee1;\n  border-radius: 4px;\n  color: #495060;\n  background: #fff;\n  position: relative;\n  cursor: text;\n  vertical-align: middle;\n  line-height: normal;\n  -webkit-transition: border .2s ease-in-out, background .2s ease-in-out, -webkit-box-shadow .2s ease-in-out;\n  transition: border .2s ease-in-out, background .2s ease-in-out, -webkit-box-shadow .2s ease-in-out;\n}\n.tags-wrap .tags-item, .tags-wrap .tags-input {\n    position: relative;\n    float: left;\n    color: #495060;\n    background-color: #f1f8ff;\n    border-radius: 3px;\n    line-height: 22px;\n    margin: 2px 6px 2px 0;\n    padding: 0 20px 0 6px;\n}\n.tags-wrap .tags-item .tags-content, .tags-wrap .tags-input .tags-content {\n      line-height: 22px;\n}\n.tags-wrap .tags-item .tags-del, .tags-wrap .tags-input .tags-del {\n      width: 20px;\n      height: 22px;\n      text-align: center;\n      cursor: pointer;\n      position: absolute;\n      top: -1px;\n      right: 0;\n}\n.tags-wrap .tags-input {\n    max-width: 80%;\n    padding: 0;\n    background-color: inherit;\n    border: none;\n    color: inherit;\n    height: 22px;\n    line-height: 22px;\n    -webkit-appearance: none;\n    outline: none;\n    resize: none;\n    overflow: hidden;\n}\n.tags-wrap .tags-input::-webkit-input-placeholder {\n    color: #bbbbbb;\n}\n.tags-wrap .tags-input::-moz-placeholder {\n    color: #bbbbbb;\n}\n.tags-wrap .tags-input::-ms-input-placeholder {\n    color: #bbbbbb;\n}\n.tags-wrap .tags-input::placeholder {\n    color: #bbbbbb;\n}\n.tags-wrap .tags-placeholder {\n    position: absolute;\n    left: 0;\n    top: 0;\n    z-index: -1;\n    color: #ffffff00;\n}\n.tags-wrap::after {\n  content: \"\";\n  display: block;\n  height: 0;\n  clear: both;\n}\n", ""]);

// exports


/***/ }),

/***/ 336:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: 'TagInput',
    props: {
        value: {
            default: ''
        },
        cut: {
            default: ','
        },
        disabled: {
            type: Boolean,
            default: false
        },
        readonly: {
            type: Boolean,
            default: false
        },
        placeholder: {
            default: ''
        },
        max: {
            default: 0
        }
    },
    data: function data() {
        var disSource = [];
        this.value.split(",").forEach(function (item) {
            if (item) {
                disSource.push(item);
            }
        });
        return {
            minWidth: 80,

            tis: '',
            tisTimeout: null,

            showPlaceholder: true,

            content: '',

            disSource: disSource
        };
    },
    mounted: function mounted() {
        this.wayMinWidth();
    },

    watch: {
        placeholder: function placeholder() {
            this.wayMinWidth();
        },
        value: function value(val) {
            var disSource = [];
            if ($A.count(val) > 0) {
                val.split(",").forEach(function (item) {
                    if (item) {
                        disSource.push(item);
                    }
                });
            }
            this.disSource = disSource;
        },
        disSource: function disSource(val) {
            var _this = this;

            var temp = '';
            val.forEach(function (item) {
                if (temp != '') {
                    temp += _this.cut;
                }
                temp += item;
            });
            this.$emit('input', temp);
        }
    },
    methods: {
        wayMinWidth: function wayMinWidth() {
            var _this2 = this;

            this.showPlaceholder = true;
            this.$nextTick(function () {
                if (_this2.$refs.myPlaceholder) {
                    _this2.minWidth = Math.max(_this2.minWidth, _this2.$refs.myPlaceholder.offsetWidth);
                }
                setTimeout(function () {
                    try {
                        _this2.minWidth = Math.max(_this2.minWidth, _this2.$refs.myPlaceholder.offsetWidth);
                        _this2.showPlaceholder = false;
                    } catch (e) {}
                    if (!$A(_this2.$refs.myPlaceholder).is(":visible")) {
                        _this2.wayMinWidth();
                    }
                }, 500);
            });
        },
        pasteText: function pasteText(e) {
            e.preventDefault();
            var content = (e.clipboardData || window.clipboardData).getData('text');
            this.addTag(false, content);
        },
        clickWrap: function clickWrap() {
            this.$refs.myTextarea.focus();
        },
        downEnter: function downEnter(e) {
            e.preventDefault();
        },
        addTag: function addTag(e, content) {
            var _this3 = this;

            if (e.keyCode === 13 || e === false) {
                if (content.trim() != '' && this.disSource.indexOf(content.trim()) === -1) {
                    this.disSource.push(content.trim());
                }
                this.content = '';
            } else {
                if (this.max > 0 && this.disSource.length >= this.max) {
                    this.content = '';
                    this.tis = '最多只能添加' + this.max + '个';
                    clearInterval(this.tisTimeout);
                    this.tisTimeout = setTimeout(function () {
                        _this3.tis = '';
                    }, 2000);
                    return;
                }
                var temp = content.trim();
                var cutPos = temp.length - this.cut.length;
                if (temp != '' && temp.substring(cutPos) === this.cut) {
                    temp = temp.substring(0, cutPos);
                    if (temp.trim() != '' && this.disSource.indexOf(temp.trim()) === -1) {
                        this.disSource.push(temp.trim());
                    }
                    this.content = '';
                }
            }
        },
        delTag: function delTag(index) {
            if (index === false) {
                if (this.content !== '') {
                    return;
                }
                index = this.disSource.length - 1;
            }
            this.disSource.splice(index, 1);
        }
    }
});

/***/ }),

/***/ 337:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {
      staticClass: "tags-wrap",
      on: {
        paste: function($event) {
          return _vm.pasteText($event)
        },
        click: _vm.clickWrap
      }
    },
    [
      _vm._l(_vm.disSource, function(text, index) {
        return _c("div", { staticClass: "tags-item" }, [
          _c(
            "span",
            {
              staticClass: "tags-content",
              on: {
                click: function($event) {
                  $event.stopPropagation()
                }
              }
            },
            [_vm._v(_vm._s(text))]
          ),
          _c(
            "span",
            {
              staticClass: "tags-del",
              on: {
                click: function($event) {
                  $event.stopPropagation()
                  return _vm.delTag(index)
                }
              }
            },
            [_vm._v("×")]
          )
        ])
      }),
      _vm._v(" "),
      _c("textarea", {
        directives: [
          {
            name: "model",
            rawName: "v-model",
            value: _vm.content,
            expression: "content"
          }
        ],
        ref: "myTextarea",
        staticClass: "tags-input",
        style: { minWidth: _vm.minWidth + "px" },
        attrs: {
          placeholder: _vm.tis || _vm.placeholder,
          disabled: _vm.disabled,
          readonly: _vm.readonly
        },
        domProps: { value: _vm.content },
        on: {
          keydown: [
            function($event) {
              if (
                !$event.type.indexOf("key") &&
                _vm._k($event.keyCode, "enter", 13, $event.key, "Enter")
              ) {
                return null
              }
              return _vm.downEnter($event)
            },
            function($event) {
              if (
                !$event.type.indexOf("key") &&
                _vm._k($event.keyCode, "delete", [8, 46], $event.key, [
                  "Backspace",
                  "Delete",
                  "Del"
                ])
              ) {
                return null
              }
              return _vm.delTag(false)
            }
          ],
          keyup: function($event) {
            return _vm.addTag($event, _vm.content)
          },
          blur: function($event) {
            return _vm.addTag(false, _vm.content)
          },
          input: function($event) {
            if ($event.target.composing) {
              return
            }
            _vm.content = $event.target.value
          }
        }
      }),
      _vm._v(" "),
      _vm.showPlaceholder || _vm.tis !== ""
        ? _c(
            "span",
            { ref: "myPlaceholder", staticClass: "tags-placeholder" },
            [_vm._v(_vm._s(_vm.tis || _vm.placeholder))]
          )
        : _vm._e()
    ],
    2
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-63d616a5", module.exports)
  }
}

/***/ }),

/***/ 338:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(339)
}
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(341)
/* template */
var __vue_template__ = __webpack_require__(342)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-1b3dd966"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/main/components/project/archived.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-1b3dd966", Component.options)
  } else {
    hotAPI.reload("data-v-1b3dd966", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 339:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(340);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(1)("3653cc78", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../node_modules/css-loader/index.js!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1b3dd966\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/sass-loader/lib/loader.js!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./archived.vue", function() {
     var newContent = require("!!../../../../../../node_modules/css-loader/index.js!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-1b3dd966\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/sass-loader/lib/loader.js!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./archived.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 340:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)(false);
// imports


// module
exports.push([module.i, "\n.project-archived .tableFill[data-v-1b3dd966] {\n  margin: 12px 12px 20px;\n}\n", ""]);

// exports


/***/ }),

/***/ 341:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__DrawerTabsContainer__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__DrawerTabsContainer___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__DrawerTabsContainer__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__mixins_task__ = __webpack_require__(30);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/**
 * 项目已归档任务
 */
/* harmony default export */ __webpack_exports__["default"] = ({
    name: 'ProjectArchived',
    components: { DrawerTabsContainer: __WEBPACK_IMPORTED_MODULE_0__DrawerTabsContainer___default.a },
    props: {
        projectid: {
            default: 0
        },
        canload: {
            type: Boolean,
            default: true
        }
    },
    mixins: [__WEBPACK_IMPORTED_MODULE_1__mixins_task__["a" /* default */]],
    data: function data() {
        return {
            loadYet: false,

            loadIng: 0,

            columns: [],

            lists: [],
            listPage: 1,
            listTotal: 0,
            noDataText: ""
        };
    },
    mounted: function mounted() {
        var _this = this;

        if (this.canload) {
            this.loadYet = true;
            this.getLists(true);
        }
        $A.setOnTaskInfoListener('components/project/archived', function (act, detail) {
            if (detail.projectid != _this.projectid) {
                return;
            }
            //
            _this.lists.some(function (task, i) {
                if (task.id == detail.id) {
                    _this.lists.splice(i, 1, detail);
                    return true;
                }
            });
            //
            switch (act) {
                case "delete": // 删除任务
                case "unarchived":
                    // 取消归档
                    _this.lists.some(function (task, i) {
                        if (task.id == detail.id) {
                            _this.lists.splice(i, 1);
                            return true;
                        }
                    });
                    break;

                case "archived":
                    // 归档
                    var has = false;
                    _this.lists.some(function (task) {
                        if (task.id == detail.id) {
                            return has = true;
                        }
                    });
                    if (!has) {
                        _this.lists.unshift(detail);
                    }
                    break;
            }
        });
    },


    watch: {
        projectid: function projectid() {
            if (this.loadYet) {
                this.getLists(true);
            }
        },
        canload: function canload(val) {
            if (val && !this.loadYet) {
                this.loadYet = true;
                this.getLists(true);
            }
        }
    },

    methods: {
        initLanguage: function initLanguage() {
            var _this2 = this;

            this.noDataText = this.$L("数据加载中.....");
            this.columns = [{
                "title": this.$L("任务名称"),
                "key": 'title',
                "minWidth": 120,
                render: function render(h, params) {
                    return _this2.renderTaskTitle(h, params);
                }
            }, {
                "title": this.$L("创建人"),
                "key": 'createuser',
                "minWidth": 80,
                render: function render(h, params) {
                    return h('UserView', {
                        props: {
                            username: params.row.createuser
                        }
                    });
                }
            }, {
                "title": this.$L("负责人"),
                "key": 'username',
                "minWidth": 80,
                render: function render(h, params) {
                    return h('UserView', {
                        props: {
                            username: params.row.username
                        }
                    });
                }
            }, {
                "title": this.$L("完成"),
                "minWidth": 70,
                "align": "center",
                render: function render(h, params) {
                    return h('span', params.row.complete ? '√' : '-');
                }
            }, {
                "title": this.$L("归档时间"),
                "width": 160,
                render: function render(h, params) {
                    return h('span', $A.formatDate("Y-m-d H:i:s", params.row.archiveddate));
                }
            }, {
                "title": this.$L("操作"),
                "key": 'action',
                "width": 100,
                "align": 'center',
                render: function render(h, params) {
                    return h('Button', {
                        props: {
                            type: 'primary',
                            size: 'small'
                        },
                        style: {
                            fontSize: '12px'
                        },
                        on: {
                            click: function click() {
                                _this2.$Modal.confirm({
                                    title: _this2.$L('取消归档'),
                                    content: _this2.$L('你确定要取消归档吗？'),
                                    loading: true,
                                    onOk: function onOk() {
                                        $A.apiAjax({
                                            url: 'project/task/edit',
                                            method: 'post',
                                            data: {
                                                act: 'unarchived',
                                                taskid: params.row.id
                                            },
                                            error: function error() {
                                                _this2.$Modal.remove();
                                                alert(_this2.$L('网络繁忙，请稍后再试！'));
                                            },
                                            success: function success(res) {
                                                _this2.$Modal.remove();
                                                _this2.getLists();
                                                setTimeout(function () {
                                                    if (res.ret === 1) {
                                                        _this2.$Message.success(res.msg);
                                                        $A.triggerTaskInfoListener('unarchived', res.data);
                                                        $A.triggerTaskInfoChange(params.row.id);
                                                    } else {
                                                        _this2.$Modal.error({ title: _this2.$L('温馨提示'), content: res.msg });
                                                    }
                                                }, 350);
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    }, _this2.$L('取消归档'));
                }
            }];
        },
        setPage: function setPage(page) {
            this.listPage = page;
            this.getLists();
        },
        setPageSize: function setPageSize(size) {
            if (Math.max($A.runNum(this.listPageSize), 10) != size) {
                this.listPageSize = size;
                this.getLists();
            }
        },
        getLists: function getLists(resetLoad) {
            var _this3 = this;

            if (resetLoad === true) {
                this.listPage = 1;
            }
            if (this.projectid == 0) {
                this.lists = [];
                this.listTotal = 0;
                this.noDataText = this.$L("没有相关的数据");
                return;
            }
            this.loadIng++;
            this.noDataText = this.$L("数据加载中.....");
            $A.apiAjax({
                url: 'project/task/lists',
                data: {
                    page: Math.max(this.listPage, 1),
                    pagesize: Math.max($A.runNum(this.listPageSize), 10),
                    projectid: this.projectid,
                    archived: '已归档'
                },
                complete: function complete() {
                    _this3.loadIng--;
                },
                error: function error() {
                    _this3.noDataText = _this3.$L("数据加载失败！");
                },
                success: function success(res) {
                    if (res.ret === 1) {
                        _this3.lists = res.data.lists;
                        _this3.listTotal = res.data.total;
                        _this3.noDataText = _this3.$L("没有相关的数据");
                    } else {
                        _this3.lists = [];
                        _this3.listTotal = 0;
                        _this3.noDataText = res.msg;
                    }
                }
            });
        }
    }
});

/***/ }),

/***/ 342:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("drawer-tabs-container", [
    _c(
      "div",
      { staticClass: "project-archived" },
      [
        _c("Table", {
          ref: "tableRef",
          staticClass: "tableFill",
          attrs: {
            columns: _vm.columns,
            data: _vm.lists,
            loading: _vm.loadIng > 0,
            "no-data-text": _vm.noDataText,
            stripe: ""
          }
        }),
        _vm._v(" "),
        _c("Page", {
          staticClass: "pageBox",
          attrs: {
            total: _vm.listTotal,
            current: _vm.listPage,
            disabled: _vm.loadIng > 0,
            "page-size-opts": [10, 20, 30, 50, 100],
            placement: "top",
            "show-elevator": "",
            "show-sizer": "",
            "show-total": "",
            transfer: "",
            simple: _vm.windowMax768
          },
          on: {
            "on-change": _vm.setPage,
            "on-page-size-change": _vm.setPageSize
          }
        })
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-1b3dd966", module.exports)
  }
}

/***/ }),

/***/ 343:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(344)
}
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(346)
/* template */
var __vue_template__ = __webpack_require__(347)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-720a9bad"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/main/components/project/users.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-720a9bad", Component.options)
  } else {
    hotAPI.reload("data-v-720a9bad", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 344:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(345);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(1)("6db19cd1", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../node_modules/css-loader/index.js!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-720a9bad\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/sass-loader/lib/loader.js!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./users.vue", function() {
     var newContent = require("!!../../../../../../node_modules/css-loader/index.js!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-720a9bad\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/sass-loader/lib/loader.js!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./users.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 345:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)(false);
// imports


// module
exports.push([module.i, "\n.project-users[data-v-720a9bad] {\n  padding: 0 12px;\n}\n.project-users .tableFill[data-v-720a9bad] {\n    margin: 12px 0 20px;\n}\n", ""]);

// exports


/***/ }),

/***/ 346:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__DrawerTabsContainer__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__DrawerTabsContainer___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__DrawerTabsContainer__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
    name: 'ProjectUsers',
    components: { DrawerTabsContainer: __WEBPACK_IMPORTED_MODULE_0__DrawerTabsContainer___default.a },
    props: {
        projectid: {
            default: 0
        },
        canload: {
            type: Boolean,
            default: true
        }
    },
    data: function data() {
        return {
            loadYet: false,

            loadIng: 0,

            columns: [],

            lists: [],
            listPage: 1,
            listTotal: 0,
            noDataText: ""
        };
    },
    mounted: function mounted() {
        if (this.canload) {
            this.loadYet = true;
            this.getLists(true);
        }
    },


    watch: {
        projectid: function projectid() {
            if (this.loadYet) {
                this.getLists(true);
            }
        },
        canload: function canload(val) {
            if (val && !this.loadYet) {
                this.loadYet = true;
                this.getLists(true);
            }
        }
    },

    methods: {
        initLanguage: function initLanguage() {
            var _this = this;

            this.noDataText = this.$L("数据加载中.....");
            this.columns = [{
                "title": this.$L("头像"),
                "minWidth": 60,
                "maxWidth": 100,
                render: function render(h, params) {
                    return h('UserImg', {
                        props: {
                            info: params.row
                        },
                        style: {
                            width: "30px",
                            height: "30px",
                            fontSize: "16px",
                            lineHeight: "30px",
                            borderRadius: "15px",
                            verticalAlign: "middle"
                        }
                    });
                }
            }, {
                "title": this.$L("用户名"),
                "key": 'username',
                "minWidth": 80,
                "ellipsis": true
            }, {
                "title": this.$L("昵称"),
                "minWidth": 80,
                "ellipsis": true,
                render: function render(h, params) {
                    return h('span', params.row.nickname || '-');
                }
            }, {
                "title": this.$L("职位/职称"),
                "minWidth": 100,
                "ellipsis": true,
                render: function render(h, params) {
                    return h('span', params.row.profession || '-');
                }
            }, {
                "title": this.$L("成员角色"),
                "minWidth": 100,
                render: function render(h, params) {
                    return h('span', params.row.isowner ? _this.$L('项目负责人') : _this.$L('成员'));
                }
            }, {
                "title": this.$L("加入时间"),
                "width": 160,
                render: function render(h, params) {
                    return h('span', $A.formatDate("Y-m-d H:i:s", params.row.indate));
                }
            }, {
                "title": this.$L("操作"),
                "key": 'action',
                "width": 80,
                "align": 'center',
                render: function render(h, params) {
                    return h('Button', {
                        props: {
                            type: 'primary',
                            size: 'small'
                        },
                        style: {
                            fontSize: '12px'
                        },
                        on: {
                            click: function click() {
                                _this.$Modal.confirm({
                                    title: _this.$L('移出成员'),
                                    content: _this.$L('你确定要将此成员移出项目吗？'),
                                    loading: true,
                                    onOk: function onOk() {
                                        $A.apiAjax({
                                            url: 'project/users/join',
                                            data: {
                                                act: 'delete',
                                                projectid: params.row.projectid,
                                                username: params.row.username
                                            },
                                            error: function error() {
                                                _this.$Modal.remove();
                                                alert(_this.$L('网络繁忙，请稍后再试！'));
                                            },
                                            success: function success(res) {
                                                _this.$Modal.remove();
                                                _this.getLists();
                                                setTimeout(function () {
                                                    if (res.ret === 1) {
                                                        _this.$Message.success(res.msg);
                                                    } else {
                                                        _this.$Modal.error({ title: _this.$L('温馨提示'), content: res.msg });
                                                    }
                                                }, 350);
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    }, _this.$L('删除'));
                }
            }];
        },
        setPage: function setPage(page) {
            this.listPage = page;
            this.getLists();
        },
        setPageSize: function setPageSize(size) {
            if (Math.max($A.runNum(this.listPageSize), 10) != size) {
                this.listPageSize = size;
                this.getLists();
            }
        },
        getLists: function getLists(resetLoad) {
            var _this2 = this;

            if (resetLoad === true) {
                this.listPage = 1;
            }
            if (this.projectid == 0) {
                this.lists = [];
                this.listTotal = 0;
                this.noDataText = this.$L("没有相关的数据");
                return;
            }
            this.loadIng++;
            this.noDataText = this.$L("数据加载中.....");
            $A.apiAjax({
                url: 'project/users/lists',
                data: {
                    page: Math.max(this.listPage, 1),
                    pagesize: Math.max($A.runNum(this.listPageSize), 10),
                    projectid: this.projectid
                },
                complete: function complete() {
                    _this2.loadIng--;
                },
                error: function error() {
                    _this2.noDataText = _this2.$L("数据加载失败！");
                },
                success: function success(res) {
                    if (res.ret === 1) {
                        _this2.lists = res.data.lists;
                        _this2.listTotal = res.data.total;
                        _this2.noDataText = _this2.$L("没有相关的数据");
                    } else {
                        _this2.lists = [];
                        _this2.listTotal = 0;
                        _this2.noDataText = res.msg;
                    }
                }
            });
        },
        addUser: function addUser() {
            var _this3 = this;

            this.userValue = "";
            this.$Modal.confirm({
                render: function render(h) {
                    return h('div', [h('div', {
                        style: {
                            fontSize: '16px',
                            fontWeight: '500',
                            marginBottom: '20px'
                        }
                    }, _this3.$L('添加成员')), h('UserInput', {
                        props: {
                            value: _this3.userValue,
                            multiple: true,
                            noprojectid: _this3.projectid,
                            placeholder: _this3.$L('请输入昵称/用户名搜索')
                        },
                        on: {
                            input: function input(val) {
                                _this3.userValue = val;
                            }
                        }
                    })]);
                },
                loading: true,
                onOk: function onOk() {
                    if (_this3.userValue) {
                        var username = _this3.userValue;
                        $A.apiAjax({
                            url: 'project/users/join',
                            data: {
                                act: 'join',
                                projectid: _this3.projectid,
                                username: username
                            },
                            error: function error() {
                                _this3.$Modal.remove();
                                alert(_this3.$L('网络繁忙，请稍后再试！'));
                            },
                            success: function success(res) {
                                _this3.$Modal.remove();
                                _this3.getLists();
                                setTimeout(function () {
                                    if (res.ret === 1) {
                                        _this3.$Message.success(res.msg);
                                    } else {
                                        _this3.$Modal.error({ title: _this3.$L('温馨提示'), content: res.msg });
                                    }
                                }, 350);
                            }
                        });
                    } else {
                        _this3.$Modal.remove();
                    }
                }
            });
        }
    }
});

/***/ }),

/***/ 347:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("drawer-tabs-container", [
    _c(
      "div",
      { staticClass: "project-users" },
      [
        _c(
          "Button",
          {
            attrs: {
              loading: _vm.loadIng > 0,
              type: "primary",
              icon: "md-add"
            },
            on: { click: _vm.addUser }
          },
          [_vm._v(_vm._s(_vm.$L("添加成员")))]
        ),
        _vm._v(" "),
        _c("Table", {
          ref: "tableRef",
          staticClass: "tableFill",
          attrs: {
            columns: _vm.columns,
            data: _vm.lists,
            loading: _vm.loadIng > 0,
            "no-data-text": _vm.noDataText,
            stripe: ""
          }
        }),
        _vm._v(" "),
        _c("Page", {
          staticClass: "pageBox",
          attrs: {
            total: _vm.listTotal,
            current: _vm.listPage,
            disabled: _vm.loadIng > 0,
            "page-size-opts": [10, 20, 30, 50, 100],
            placement: "top",
            "show-elevator": "",
            "show-sizer": "",
            "show-total": "",
            transfer: "",
            simple: _vm.windowMax768
          },
          on: {
            "on-change": _vm.setPage,
            "on-page-size-change": _vm.setPageSize
          }
        })
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-720a9bad", module.exports)
  }
}

/***/ }),

/***/ 348:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(349)
}
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(351)
/* template */
var __vue_template__ = __webpack_require__(352)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-4466db4e"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/main/components/project/statistics.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-4466db4e", Component.options)
  } else {
    hotAPI.reload("data-v-4466db4e", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 349:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(350);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(1)("ffb4ffa8", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../node_modules/css-loader/index.js!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-4466db4e\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/sass-loader/lib/loader.js!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./statistics.vue", function() {
     var newContent = require("!!../../../../../../node_modules/css-loader/index.js!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-4466db4e\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/sass-loader/lib/loader.js!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./statistics.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 350:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)(false);
// imports


// module
exports.push([module.i, "\n.project-statistics .tableFill[data-v-4466db4e] {\n  margin: 12px 12px 20px;\n}\n.project-statistics ul.state-overview[data-v-4466db4e] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.project-statistics ul.state-overview > li[data-v-4466db4e] {\n    -webkit-box-flex: 1;\n        -ms-flex: 1;\n            flex: 1;\n    cursor: pointer;\n    margin: 0 10px 5px;\n}\n.project-statistics ul.state-overview > li > div[data-v-4466db4e] {\n      position: relative;\n      -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05);\n              box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05);\n      -webkit-transition: all 0.2s;\n      transition: all 0.2s;\n      border-radius: 6px;\n      color: #ffffff;\n      height: 110px;\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-orient: vertical;\n      -webkit-box-direction: normal;\n          -ms-flex-direction: column;\n              flex-direction: column;\n      -webkit-box-pack: center;\n          -ms-flex-pack: center;\n              justify-content: center;\n      -webkit-box-align: center;\n          -ms-flex-align: center;\n              align-items: center;\n}\n.project-statistics ul.state-overview > li > div.terques[data-v-4466db4e] {\n        background: #17BE6B;\n}\n.project-statistics ul.state-overview > li > div.purple[data-v-4466db4e] {\n        background: #A218A5;\n}\n.project-statistics ul.state-overview > li > div.red[data-v-4466db4e] {\n        background: #ED3F14;\n}\n.project-statistics ul.state-overview > li > div.yellow[data-v-4466db4e] {\n        background: #FF9900;\n}\n.project-statistics ul.state-overview > li > div.blue[data-v-4466db4e] {\n        background: #2D8CF0;\n}\n.project-statistics ul.state-overview > li > div[data-v-4466db4e]:hover {\n        -webkit-box-shadow: 2px 2px 8px 0 rgba(0, 0, 0, 0.38);\n                box-shadow: 2px 2px 8px 0 rgba(0, 0, 0, 0.38);\n}\n.project-statistics ul.state-overview > li > div[data-v-4466db4e]:after {\n        position: absolute;\n        content: \"\";\n        left: 50%;\n        bottom: 3px;\n        width: 0;\n        height: 2px;\n        -webkit-transform: translate(-50%, 0);\n                transform: translate(-50%, 0);\n        background-color: #FFFFFF;\n        border-radius: 2px;\n        -webkit-transition: all 0.3s;\n        transition: all 0.3s;\n        opacity: 0;\n}\n.project-statistics ul.state-overview > li > div > h1[data-v-4466db4e] {\n        font-size: 36px;\n        margin: -2px 0 0;\n        padding: 0;\n        font-weight: 500;\n}\n.project-statistics ul.state-overview > li > div > p[data-v-4466db4e] {\n        font-size: 18px;\n        margin: 0;\n        padding: 0;\n}\n.project-statistics ul.state-overview > li.active > div[data-v-4466db4e]:after {\n      width: 90%;\n      opacity: 1;\n}\n", ""]);

// exports


/***/ }),

/***/ 351:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__DrawerTabsContainer__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__DrawerTabsContainer___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__DrawerTabsContainer__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__mixins_task__ = __webpack_require__(30);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/**
 * 项目统计
 */
/* harmony default export */ __webpack_exports__["default"] = ({
    name: 'ProjectStatistics',
    components: { DrawerTabsContainer: __WEBPACK_IMPORTED_MODULE_0__DrawerTabsContainer___default.a },
    props: {
        projectid: {
            default: 0
        },
        canload: {
            type: Boolean,
            default: true
        }
    },
    mixins: [__WEBPACK_IMPORTED_MODULE_1__mixins_task__["a" /* default */]],
    data: function data() {
        return {
            loadYet: false,

            loadIng: 0,

            columns: [],

            taskType: '未完成',

            lists: [],
            listPage: 1,
            listTotal: 0,
            noDataText: "",

            statistics_unfinished: 0,
            statistics_overdue: 0,
            statistics_complete: 0
        };
    },
    mounted: function mounted() {
        var _this = this;

        if (this.canload) {
            this.loadYet = true;
            this.getLists(true);
        }
        $A.setOnTaskInfoListener('components/project/statistics', function (act, detail) {
            if (detail.projectid != _this.projectid) {
                return;
            }
            //
            _this.lists.some(function (task, i) {
                if (task.id == detail.id) {
                    _this.lists.splice(i, 1, detail);
                    return true;
                }
            });
            //
            switch (act) {
                case "delete": // 删除任务
                case "archived":
                    // 归档
                    _this.lists.some(function (task, i) {
                        if (task.id == detail.id) {
                            _this.lists.splice(i, 1);
                            if (task.complete) {
                                _this.statistics_complete--;
                            } else {
                                _this.statistics_unfinished++;
                            }
                            return true;
                        }
                    });
                    break;

                case "unarchived":
                    // 取消归档
                    var has = false;
                    _this.lists.some(function (task) {
                        if (task.id == detail.id) {
                            if (task.complete) {
                                _this.statistics_complete++;
                            } else {
                                _this.statistics_unfinished--;
                            }
                            return has = true;
                        }
                    });
                    if (!has) {
                        _this.lists.unshift(detail);
                    }
                    break;

                case "complete":
                    // 标记完成
                    _this.statistics_complete++;
                    _this.statistics_unfinished--;
                    break;

                case "unfinished":
                    // 标记未完成
                    _this.statistics_complete--;
                    _this.statistics_unfinished++;
                    break;
            }
        });
    },


    watch: {
        projectid: function projectid() {
            if (this.loadYet) {
                this.getLists(true);
            }
        },
        canload: function canload(val) {
            if (val && !this.loadYet) {
                this.loadYet = true;
                this.getLists(true);
            }
        },
        taskType: function taskType() {
            if (this.loadYet) {
                this.getLists(true);
            }
        }
    },

    methods: {
        initLanguage: function initLanguage() {
            var _this2 = this;

            this.noDataText = this.$L("数据加载中.....");
            this.columns = [{
                "title": this.$L("任务名称"),
                "key": 'title',
                "minWidth": 120,
                render: function render(h, params) {
                    return _this2.renderTaskTitle(h, params);
                }
            }, {
                "title": this.$L("创建人"),
                "key": 'createuser',
                "minWidth": 80,
                render: function render(h, params) {
                    return h('UserView', {
                        props: {
                            username: params.row.createuser
                        }
                    });
                }
            }, {
                "title": this.$L("负责人"),
                "key": 'username',
                "minWidth": 80,
                render: function render(h, params) {
                    return h('UserView', {
                        props: {
                            username: params.row.username
                        }
                    });
                }
            }, {
                "title": this.$L("完成"),
                "minWidth": 70,
                "align": "center",
                render: function render(h, params) {
                    return h('span', params.row.complete ? '√' : '-');
                }
            }, {
                "title": this.$L("创建时间"),
                "width": 160,
                render: function render(h, params) {
                    return h('span', $A.formatDate("Y-m-d H:i:s", params.row.indate));
                }
            }];
        },
        setTaskType: function setTaskType(type) {
            this.taskType = type;
        },
        setPage: function setPage(page) {
            this.listPage = page;
            this.getLists();
        },
        setPageSize: function setPageSize(size) {
            if (Math.max($A.runNum(this.listPageSize), 10) != size) {
                this.listPageSize = size;
                this.getLists();
            }
        },
        getLists: function getLists(resetLoad) {
            var _this3 = this;

            if (resetLoad === true) {
                this.listPage = 1;
            }
            if (this.projectid == 0) {
                this.lists = [];
                this.listTotal = 0;
                this.noDataText = this.$L("没有相关的数据");
                return;
            }
            this.loadIng++;
            var tempType = this.taskType;
            this.noDataText = this.$L("数据加载中.....");
            $A.apiAjax({
                url: 'project/task/lists',
                data: {
                    page: Math.max(this.listPage, 1),
                    pagesize: Math.max($A.runNum(this.listPageSize), 10),
                    projectid: this.projectid,
                    type: this.taskType,
                    statistics: 1
                },
                complete: function complete() {
                    _this3.loadIng--;
                },
                error: function error() {
                    _this3.noDataText = _this3.$L("数据加载失败！");
                },
                success: function success(res) {
                    if (tempType != _this3.taskType) {
                        return;
                    }
                    if (res.ret === 1) {
                        _this3.lists = res.data.lists;
                        _this3.listTotal = res.data.total;
                        _this3.noDataText = _this3.$L("没有相关的数据");
                    } else {
                        _this3.lists = [];
                        _this3.listTotal = 0;
                        _this3.noDataText = res.msg;
                    }
                    _this3.statistics_unfinished = res.data.statistics_unfinished || 0;
                    _this3.statistics_overdue = res.data.statistics_overdue || 0;
                    _this3.statistics_complete = res.data.statistics_complete || 0;
                }
            });
        }
    }
});

/***/ }),

/***/ 352:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("drawer-tabs-container", [
    _c(
      "div",
      { staticClass: "project-statistics" },
      [
        _c("ul", { staticClass: "state-overview" }, [
          _c(
            "li",
            {
              class: [_vm.taskType === "未完成" ? "active" : ""],
              on: {
                click: function($event) {
                  _vm.taskType = "未完成"
                }
              }
            },
            [
              _c("div", { staticClass: "yellow" }, [
                _c("h1", { staticClass: "count" }, [
                  _vm._v(_vm._s(_vm.statistics_unfinished))
                ]),
                _vm._v(" "),
                _c("p", [_vm._v(_vm._s(_vm.$L("未完成任务")))])
              ])
            ]
          ),
          _vm._v(" "),
          _c(
            "li",
            {
              class: [_vm.taskType === "已超期" ? "active" : ""],
              on: {
                click: function($event) {
                  _vm.taskType = "已超期"
                }
              }
            },
            [
              _c("div", { staticClass: "red" }, [
                _c("h1", { staticClass: "count" }, [
                  _vm._v(_vm._s(_vm.statistics_overdue))
                ]),
                _vm._v(" "),
                _c("p", [_vm._v(_vm._s(_vm.$L("超期任务")))])
              ])
            ]
          ),
          _vm._v(" "),
          _c(
            "li",
            {
              class: [_vm.taskType === "已完成" ? "active" : ""],
              on: {
                click: function($event) {
                  _vm.taskType = "已完成"
                }
              }
            },
            [
              _c("div", { staticClass: "terques" }, [
                _c("h1", { staticClass: "count" }, [
                  _vm._v(_vm._s(_vm.statistics_complete))
                ]),
                _vm._v(" "),
                _c("p", [_vm._v(_vm._s(_vm.$L("已完成任务")))])
              ])
            ]
          )
        ]),
        _vm._v(" "),
        _c("Table", {
          ref: "tableRef",
          staticClass: "tableFill",
          attrs: {
            columns: _vm.columns,
            data: _vm.lists,
            loading: _vm.loadIng > 0,
            "no-data-text": _vm.noDataText,
            stripe: ""
          }
        }),
        _vm._v(" "),
        _c("Page", {
          staticClass: "pageBox",
          attrs: {
            total: _vm.listTotal,
            current: _vm.listPage,
            disabled: _vm.loadIng > 0,
            "page-size-opts": [10, 20, 30, 50, 100],
            placement: "top",
            "show-elevator": "",
            "show-sizer": "",
            "show-total": "",
            transfer: "",
            simple: _vm.windowMax768
          },
          on: {
            "on-change": _vm.setPage,
            "on-page-size-change": _vm.setPageSize
          }
        })
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-4466db4e", module.exports)
  }
}

/***/ }),

/***/ 512:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(513);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(1)("6f61a682", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-426c4faa\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./project.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-426c4faa\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./project.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 513:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)(false);
// imports


// module
exports.push([module.i, "\n.project ul.project-list[data-v-426c4faa] {\n  padding: 5px;\n  max-width: 2200px;\n  margin: 0 auto;\n}\n.project ul.project-list li[data-v-426c4faa] {\n    float: left;\n    width: 20%;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n}\n@media (max-width: 2000px) {\n.project ul.project-list li[data-v-426c4faa] {\n        width: 25%;\n}\n}\n@media (max-width: 1400px) {\n.project ul.project-list li[data-v-426c4faa] {\n        width: 33.33%;\n}\n}\n@media (max-width: 1080px) {\n.project ul.project-list li[data-v-426c4faa] {\n        width: 50%;\n}\n}\n@media (max-width: 640px) {\n.project ul.project-list li[data-v-426c4faa] {\n        width: 100%;\n}\n}\n.project ul.project-list li .project-item[data-v-426c4faa] {\n      -webkit-box-flex: 1;\n          -ms-flex: 1;\n              flex: 1;\n      margin: 10px;\n      width: 100%;\n      height: 313px;\n      padding: 20px;\n      background-color: #ffffff;\n      border-radius: 4px;\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-orient: vertical;\n      -webkit-box-direction: normal;\n          -ms-flex-direction: column;\n              flex-direction: column;\n}\n.project ul.project-list li .project-item .project-head[data-v-426c4faa] {\n        display: -webkit-box;\n        display: -ms-flexbox;\n        display: flex;\n        -webkit-box-orient: horizontal;\n        -webkit-box-direction: normal;\n            -ms-flex-direction: row;\n                flex-direction: row;\n}\n.project ul.project-list li .project-item .project-head .project-loading[data-v-426c4faa] {\n          width: 18px;\n          height: 18px;\n          margin-right: 6px;\n          margin-top: 3px;\n}\n.project ul.project-list li .project-item .project-head .project-title[data-v-426c4faa] {\n          -webkit-box-flex: 1;\n              -ms-flex: 1;\n                  flex: 1;\n          font-size: 16px;\n          padding-right: 6px;\n          overflow: hidden;\n          text-overflow: ellipsis;\n          white-space: nowrap;\n          color: #333333;\n          cursor: pointer;\n}\n.project ul.project-list li .project-item .project-head .project-setting[data-v-426c4faa] {\n          width: 30px;\n          text-align: right;\n}\n.project ul.project-list li .project-item .project-head .project-setting .project-setting-icon[data-v-426c4faa] {\n            cursor: pointer;\n            color: #333333;\n}\n.project ul.project-list li .project-item .project-head .project-setting .project-setting-icon[data-v-426c4faa]:hover {\n              color: #0396f2;\n}\n.project ul.project-list li .project-item .project-num[data-v-426c4faa] {\n        -webkit-box-flex: 1;\n            -ms-flex: 1;\n                flex: 1;\n        padding: 34px 0;\n        display: -webkit-box;\n        display: -ms-flexbox;\n        display: flex;\n        -webkit-box-orient: horizontal;\n        -webkit-box-direction: normal;\n            -ms-flex-direction: row;\n                flex-direction: row;\n        -webkit-box-align: center;\n            -ms-flex-align: center;\n                align-items: center;\n        -webkit-box-pack: center;\n            -ms-flex-pack: center;\n                justify-content: center;\n        cursor: pointer;\n        position: relative;\n}\n.project ul.project-list li .project-item .project-num[data-v-426c4faa]:before {\n          content: \"\";\n          position: absolute;\n          width: 1px;\n          height: 60%;\n          background-color: #EFEFEF;\n}\n.project ul.project-list li .project-item .project-num .project-circle[data-v-426c4faa] {\n          -webkit-box-flex: 1;\n              -ms-flex: 1;\n                  flex: 1;\n          text-align: center;\n          margin-right: 10px;\n}\n.project ul.project-list li .project-item .project-num .project-circle .project-circle-box[data-v-426c4faa] {\n            display: -webkit-box;\n            display: -ms-flexbox;\n            display: flex;\n            -webkit-box-orient: vertical;\n            -webkit-box-direction: normal;\n                -ms-flex-direction: column;\n                    flex-direction: column;\n            -webkit-box-align: center;\n                -ms-flex-align: center;\n                    align-items: center;\n            -webkit-box-pack: center;\n                -ms-flex-pack: center;\n                    justify-content: center;\n}\n.project ul.project-list li .project-item .project-num .project-circle .project-circle-box .project-circle-num[data-v-426c4faa] {\n              display: -webkit-box;\n              display: -ms-flexbox;\n              display: flex;\n              -webkit-box-align: end;\n                  -ms-flex-align: end;\n                      align-items: flex-end;\n              font-weight: 600;\n}\n.project ul.project-list li .project-item .project-num .project-circle .project-circle-box .project-circle-num em[data-v-426c4faa] {\n                color: #62C5FE;\n                font-size: 26px;\n}\n.project ul.project-list li .project-item .project-num .project-circle .project-circle-box .project-circle-num span[data-v-426c4faa] {\n                color: #666666;\n}\n.project ul.project-list li .project-item .project-num .project-circle .project-circle-box .project-circle-num span[data-v-426c4faa]:before {\n                  content: \"/\";\n}\n.project ul.project-list li .project-item .project-num .project-circle .project-circle-box .project-circle-title[data-v-426c4faa] {\n              font-size: 12px;\n              padding-top: 4px;\n              color: #999999;\n}\n.project ul.project-list li .project-item .project-num .project-situation[data-v-426c4faa] {\n          -webkit-box-flex: 1;\n              -ms-flex: 1;\n                  flex: 1;\n          position: relative;\n}\n.project ul.project-list li .project-item .project-num .project-situation ul[data-v-426c4faa] {\n            display: -webkit-box;\n            display: -ms-flexbox;\n            display: flex;\n            -webkit-box-orient: vertical;\n            -webkit-box-direction: normal;\n                -ms-flex-direction: column;\n                    flex-direction: column;\n            position: absolute;\n            top: 50%;\n            left: 50%;\n            -webkit-transform: translate(-50%, -50%);\n                    transform: translate(-50%, -50%);\n}\n.project ul.project-list li .project-item .project-num .project-situation ul > li[data-v-426c4faa] {\n              width: 100%;\n              color: #BBBBBB;\n              font-size: 12px;\n              white-space: nowrap;\n              display: -webkit-box;\n              display: -ms-flexbox;\n              display: flex;\n              -webkit-box-align: center;\n                  -ms-flex-align: center;\n                      align-items: center;\n              padding: 6px 0;\n              line-height: 20px;\n}\n.project ul.project-list li .project-item .project-num .project-situation ul > li > em[data-v-426c4faa] {\n                padding-left: 14px;\n                font-size: 18px;\n                color: #666666;\n                font-weight: 500;\n}\n.project ul.project-list li .project-item .project-bottom[data-v-426c4faa] {\n        display: -webkit-box;\n        display: -ms-flexbox;\n        display: flex;\n        -webkit-box-orient: vertical;\n        -webkit-box-direction: normal;\n            -ms-flex-direction: column;\n                flex-direction: column;\n        border-top: 1px solid #EFEFEF;\n        padding: 18px 0;\n        cursor: default;\n        position: relative;\n}\n.project ul.project-list li .project-item .project-bottom .project-iconbtn[data-v-426c4faa] {\n          -webkit-box-flex: 1;\n              -ms-flex: 1;\n                  flex: 1;\n          width: 50%;\n          text-align: center;\n          display: -webkit-box;\n          display: -ms-flexbox;\n          display: flex;\n          -webkit-box-align: center;\n              -ms-flex-align: center;\n                  align-items: center;\n          padding: 4px 0;\n}\n.project ul.project-list li .project-item .project-bottom .project-iconbtn.project-people[data-v-426c4faa] {\n            width: auto;\n            min-width: 36px;\n            position: absolute;\n            bottom: 18px;\n            right: 0;\n            cursor: pointer;\n            -webkit-box-pack: end;\n                -ms-flex-pack: end;\n                    justify-content: flex-end;\n}\n.project ul.project-list li .project-item .project-bottom .project-iconbtn.project-people .userimg-icon[data-v-426c4faa],\n            .project ul.project-list li .project-item .project-bottom .project-iconbtn.project-people .userimg-count[data-v-426c4faa] {\n              width: 36px;\n              height: 36px;\n              border-radius: 18px;\n              margin-left: -16px;\n              border: 2px solid #ffffff;\n}\n.project ul.project-list li .project-item .project-bottom .project-iconbtn.project-people .userimg-count[data-v-426c4faa] {\n              -webkit-transform: scale(1);\n                      transform: scale(1);\n              color: #ffffff;\n              font-size: 16px;\n              font-weight: 500;\n              line-height: 32px;\n              background-color: #62C5FE;\n}\n.project ul.project-list li .project-item .project-bottom .project-iconbtn .project-iconbtn-icon[data-v-426c4faa] {\n            font-size: 16px;\n            margin-right: 6px;\n            color: #999;\n}\n.project ul.project-list li .project-item .project-bottom .project-iconbtn .project-iconbtn-text[data-v-426c4faa] {\n            color: #999999;\n            cursor: pointer;\n}\n.project ul.project-list li .project-item .project-bottom .project-iconbtn .project-iconbtn-text[data-v-426c4faa]:hover {\n              color: #0396f2;\n}\n.project ul.project-list[data-v-426c4faa]:before, .project ul.project-list[data-v-426c4faa]:after {\n    display: table;\n    content: \"\";\n}\n.project ul.project-list[data-v-426c4faa]:after {\n    clear: both;\n}\n", ""]);

// exports


/***/ }),

/***/ 514:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_TagInput__ = __webpack_require__(333);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_TagInput___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__components_TagInput__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_WContent__ = __webpack_require__(305);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_WContent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__components_WContent__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_project_archived__ = __webpack_require__(338);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_project_archived___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__components_project_archived__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_project_users__ = __webpack_require__(343);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_project_users___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__components_project_users__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_project_statistics__ = __webpack_require__(348);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_project_statistics___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5__components_project_statistics__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__components_project_my_favor__ = __webpack_require__(515);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__components_project_my_favor___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6__components_project_my_favor__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__components_project_my_join__ = __webpack_require__(520);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__components_project_my_join___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7__components_project_my_join__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__components_project_my_manage__ = __webpack_require__(525);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__components_project_my_manage___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8__components_project_my_manage__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__mixins_project__ = __webpack_require__(319);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__components_iview_WDrawer__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__components_iview_WDrawer___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10__components_iview_WDrawer__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



__WEBPACK_IMPORTED_MODULE_0_vue___default.a.component('TagInput', __WEBPACK_IMPORTED_MODULE_1__components_TagInput___default.a);










/* harmony default export */ __webpack_exports__["default"] = ({
    components: {
        WDrawer: __WEBPACK_IMPORTED_MODULE_10__components_iview_WDrawer___default.a,
        ProjectMyManage: __WEBPACK_IMPORTED_MODULE_8__components_project_my_manage___default.a,
        ProjectMyJoin: __WEBPACK_IMPORTED_MODULE_7__components_project_my_join___default.a,
        ProjectMyFavor: __WEBPACK_IMPORTED_MODULE_6__components_project_my_favor___default.a, ProjectStatistics: __WEBPACK_IMPORTED_MODULE_5__components_project_statistics___default.a, ProjectUsers: __WEBPACK_IMPORTED_MODULE_4__components_project_users___default.a, ProjectArchived: __WEBPACK_IMPORTED_MODULE_3__components_project_archived___default.a, WContent: __WEBPACK_IMPORTED_MODULE_2__components_WContent___default.a },
    mixins: [__WEBPACK_IMPORTED_MODULE_9__mixins_project__["a" /* default */]],
    data: function data() {
        return {
            loadIng: 0,

            addShow: false,
            formAdd: {
                title: '',
                labels: [],
                template: 0
            },
            ruleAdd: {},

            labelLists: [],

            lists: [],
            listPage: 1,
            listTotal: 0,
            listPageSize: 20,

            projectDrawerShow: false,
            projectDrawerTab: 'archived',

            projectListDrawerShow: false,
            projectListDrawerTab: 'myjoin',

            handleProjectId: 0
        };
    },
    mounted: function mounted() {
        var _this = this;

        this.getLists(true);
        //
        $A.setOnTaskInfoListener('pages/project', function (act, detail) {
            var item = _this.lists.find(function (item) {
                return item.id == detail.projectid;
            });
            if (!item) {
                return;
            }
            var persons = detail.persons ? !!detail.persons.find(function (_ref) {
                var username = _ref.username;
                return username == _this.usrName;
            }) : null;
            var unfinishedNum = function unfinishedNum(add) {
                if (add) {
                    item.unfinished++;
                    persons === true && item.self_count++;
                } else {
                    item.unfinished--;
                    persons === true && item.self_count--;
                }
            };
            var completeNum = function completeNum(add) {
                if (add) {
                    item.complete++;
                    persons === true && item.self_complete++;
                } else {
                    item.complete--;
                    persons === true && item.self_complete--;
                }
            };
            switch (act) {
                case 'deleteproject': // 删除项目
                case 'deletelabel':
                    // 删除分类
                    _this.getLists(true);
                    break;
                case "create":
                    // 创建任务
                    unfinishedNum(true);
                    break;
                case "delete": // 删除任务
                case "archived":
                    // 归档
                    if (detail.complete) {
                        completeNum();
                    } else {
                        unfinishedNum();
                    }
                    break;
                case "unarchived":
                    // 取消归档
                    if (detail.complete) {
                        completeNum(true);
                    } else {
                        unfinishedNum(true);
                    }
                    break;
                case "complete":
                    // 标记完成
                    completeNum(true);
                    unfinishedNum();
                    break;
                case "unfinished":
                    // 标记未完成
                    completeNum();
                    unfinishedNum(true);
                    break;
            }
        }, true);
    },
    deactivated: function deactivated() {
        this.addShow = false;
        this.projectDrawerShow = false;
        this.projectListDrawerShow = false;
    },

    watch: {
        usrName: function usrName() {
            this.usrLogin && this.getLists(true);
        }
    },
    methods: {
        initLanguage: function initLanguage() {
            this.labelLists = [{
                label: this.$L('空白模板'),
                value: []
            }, {
                label: this.$L('软件开发'),
                value: [this.$L('产品规划'), this.$L('前端开发'), this.$L('后端开发'), this.$L('测试'), this.$L('发布'), this.$L('其它')]
            }, {
                label: this.$L('产品开发'),
                value: [this.$L('产品计划'), this.$L('正在设计'), this.$L('正在研发'), this.$L('测试'), this.$L('准备发布'), this.$L('发布成功')]
            }];
            this.ruleAdd = {
                title: [{ required: true, message: this.$L('请填写项目名称！'), trigger: 'change' }, { type: 'string', min: 2, message: this.$L('项目名称至少2个字！'), trigger: 'change' }]
            };
        },
        setPage: function setPage(page) {
            this.listPage = page;
            this.getLists();
        },
        setPageSize: function setPageSize(size) {
            if (Math.max($A.runNum(this.listPageSize), 20) != size) {
                this.listPageSize = size;
                this.getLists();
            }
        },
        getLists: function getLists(resetLoad) {
            var _this2 = this;

            if (resetLoad === true) {
                this.listPage = 1;
            }
            this.loadIng++;
            $A.apiAjax({
                url: 'project/lists',
                data: {
                    page: Math.max(this.listPage, 1),
                    pagesize: Math.max($A.runNum(this.listPageSize), 20)
                },
                complete: function complete() {
                    _this2.loadIng--;
                },
                success: function success(res) {
                    if (res.ret === 1) {
                        _this2.lists = res.data.lists;
                        _this2.listTotal = res.data.total;
                    } else {
                        _this2.lists = [];
                        _this2.listTotal = 0;
                    }
                }
            });
        },
        addLabels: function addLabels() {
            var _this3 = this;

            this.labelsValue = "";
            this.$Modal.confirm({
                render: function render(h) {
                    return h('div', [h('div', {
                        style: {
                            fontSize: '16px',
                            fontWeight: '500',
                            marginBottom: '20px'
                        }
                    }, _this3.$L('添加流程')), h('TagInput', {
                        props: {
                            value: _this3.labelsValue,
                            autofocus: true,
                            placeholder: _this3.$L('请输入流程名称，多个可用英文逗号分隔。')
                        },
                        on: {
                            input: function input(val) {
                                _this3.labelsValue = val;
                            }
                        }
                    })]);
                },
                onOk: function onOk() {
                    if (_this3.labelsValue) {
                        var array = $A.trim(_this3.labelsValue).split(",");
                        array.forEach(function (name) {
                            if ($A.trim(name)) {
                                _this3.formAdd.labels.push($A.trim(name));
                            }
                        });
                    }
                }
            });
        },
        onAdd: function onAdd() {
            var _this4 = this;

            this.$refs.add.validate(function (valid) {
                if (valid) {
                    _this4.loadIng++;
                    $A.apiAjax({
                        url: 'project/add',
                        data: _this4.formAdd,
                        complete: function complete() {
                            _this4.loadIng--;
                        },
                        success: function success(res) {
                            if (res.ret === 1) {
                                _this4.addShow = false;
                                _this4.$Message.success(res.msg);
                                _this4.$refs.add.resetFields();
                                _this4.$set(_this4.formAdd, 'template', 0);
                                //
                                _this4.getLists(true);
                            } else {
                                _this4.$Modal.error({ title: _this4.$L('温馨提示'), content: res.msg });
                            }
                        }
                    });
                }
            });
        },
        openComplete: function openComplete(item) {
            if (item.complete > 0) {
                this.openProject(item.id, item, '已完成');
            } else {
                this.handleProject('open', item);
            }
        },
        handleProject: function handleProject(event, item) {
            var _this5 = this;

            if (item) {
                this.handleProjectId = item.id;
            }
            switch (event) {
                case 'favor':
                    {
                        this.favorProject('add', item.id);
                        break;
                    }
                case 'rename':
                    {
                        this.renameProject(item);
                        break;
                    }
                case 'transfer':
                    {
                        this.transferProject(item);
                        break;
                    }
                case 'delete':
                    {
                        this.deleteProject(item.id, function () {
                            _this5.getLists();
                        });
                        break;
                    }
                case 'out':
                    {
                        this.outProject(item.id, function () {
                            _this5.getLists();
                        });
                        break;
                    }

                case 'open':
                    {
                        this.openProject(item.id, item);
                        break;
                    }
                case 'archived':
                case 'member':
                case 'statistics':
                    {
                        this.projectDrawerShow = true;
                        this.projectDrawerTab = event;
                        break;
                    }
                case 'myjoin':
                case 'myfavor':
                case 'mycreate':
                    {
                        this.projectListDrawerShow = true;
                        this.projectListDrawerTab = event;
                        break;
                    }
            }
        },
        renameProject: function renameProject(item) {
            var _this6 = this;

            this.renameValue = "";
            this.$Modal.confirm({
                render: function render(h) {
                    return h('div', [h('div', {
                        style: {
                            fontSize: '16px',
                            fontWeight: '500',
                            marginBottom: '20px'
                        }
                    }, _this6.$L('重命名项目')), h('Input', {
                        props: {
                            value: _this6.renameValue,
                            autofocus: true,
                            placeholder: _this6.$L('请输入新的项目名称')
                        },
                        on: {
                            input: function input(val) {
                                _this6.renameValue = val;
                            }
                        }
                    })]);
                },
                loading: true,
                onOk: function onOk() {
                    if (_this6.renameValue) {
                        _this6.$set(item, 'loadIng', true);
                        var title = _this6.renameValue;
                        $A.apiAjax({
                            url: 'project/rename',
                            data: {
                                projectid: item.id,
                                title: title
                            },
                            complete: function complete() {
                                _this6.$set(item, 'loadIng', false);
                            },
                            error: function error() {
                                _this6.$Modal.remove();
                                alert(_this6.$L('网络繁忙，请稍后再试！'));
                            },
                            success: function success(res) {
                                _this6.$Modal.remove();
                                _this6.$set(item, 'title', title);
                                setTimeout(function () {
                                    if (res.ret === 1) {
                                        _this6.$Message.success(res.msg);
                                    } else {
                                        _this6.$Modal.error({ title: _this6.$L('温馨提示'), content: res.msg });
                                    }
                                }, 350);
                            }
                        });
                    } else {
                        _this6.$Modal.remove();
                    }
                }
            });
        },
        transferProject: function transferProject(item) {
            var _this7 = this;

            this.transferValue = "";
            this.$Modal.confirm({
                render: function render(h) {
                    return h('div', [h('div', {
                        style: {
                            fontSize: '16px',
                            fontWeight: '500',
                            marginBottom: '20px'
                        }
                    }, _this7.$L('移交项目')), h('UserInput', {
                        props: {
                            value: _this7.transferValue,
                            nousername: item.username,
                            placeholder: _this7.$L('请输入昵称/用户名搜索')
                        },
                        on: {
                            input: function input(val) {
                                _this7.transferValue = val;
                            }
                        }
                    })]);
                },
                loading: true,
                onOk: function onOk() {
                    if (_this7.transferValue) {
                        _this7.$set(item, 'loadIng', true);
                        var username = _this7.transferValue;
                        $A.apiAjax({
                            url: 'project/transfer',
                            data: {
                                projectid: item.id,
                                username: username
                            },
                            complete: function complete() {
                                _this7.$set(item, 'loadIng', false);
                            },
                            error: function error() {
                                _this7.$Modal.remove();
                                alert(_this7.$L('网络繁忙，请稍后再试！'));
                            },
                            success: function success(res) {
                                _this7.$Modal.remove();
                                _this7.getLists();
                                setTimeout(function () {
                                    if (res.ret === 1) {
                                        _this7.$Message.success(res.msg);
                                    } else {
                                        _this7.$Modal.error({ title: _this7.$L('温馨提示'), content: res.msg });
                                    }
                                }, 350);
                            }
                        });
                    } else {
                        _this7.$Modal.remove();
                    }
                }
            });
        },
        selfProportion: function selfProportion(complete, count) {
            if (count <= 0) {
                return 100;
            }
            return Math.round(complete / count * 100);
        }
    }
});

/***/ }),

/***/ 515:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(516)
}
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(518)
/* template */
var __vue_template__ = __webpack_require__(519)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-6c4e3706"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/main/components/project/my/favor.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-6c4e3706", Component.options)
  } else {
    hotAPI.reload("data-v-6c4e3706", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 516:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(517);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(1)("66fbcb3b", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../../node_modules/css-loader/index.js!../../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-6c4e3706\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../../node_modules/sass-loader/lib/loader.js!../../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./favor.vue", function() {
     var newContent = require("!!../../../../../../../node_modules/css-loader/index.js!../../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-6c4e3706\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../../node_modules/sass-loader/lib/loader.js!../../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./favor.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 517:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)(false);
// imports


// module
exports.push([module.i, "\n.project-my-favor .tableFill[data-v-6c4e3706] {\n  margin: 12px 12px 20px;\n}\n", ""]);

// exports


/***/ }),

/***/ 518:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__mixins_project__ = __webpack_require__(319);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__DrawerTabsContainer__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__DrawerTabsContainer___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__DrawerTabsContainer__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
    name: 'ProjectMyFavor',
    components: { DrawerTabsContainer: __WEBPACK_IMPORTED_MODULE_1__DrawerTabsContainer___default.a },
    props: {
        canload: {
            type: Boolean,
            default: true
        }
    },
    mixins: [__WEBPACK_IMPORTED_MODULE_0__mixins_project__["a" /* default */]],
    data: function data() {
        return {
            loadYet: false,

            loadIng: 0,

            columns: [],

            lists: [],
            listPage: 1,
            listTotal: 0,
            noDataText: ""
        };
    },
    mounted: function mounted() {
        if (this.canload) {
            this.loadYet = true;
            this.getLists(true);
        }
    },


    watch: {
        canload: function canload(val) {
            if (val && !this.loadYet) {
                this.loadYet = true;
                this.getLists(true);
            }
        }
    },

    methods: {
        initLanguage: function initLanguage() {
            var _this = this;

            this.noDataText = this.$L("数据加载中.....");
            this.columns = [{
                "title": this.$L("项目名称"),
                "key": 'title',
                "minWidth": 100,
                render: function render(h, params) {
                    return h('a', {
                        attrs: {
                            href: 'javascript:void(0)'
                        },
                        on: {
                            click: function click() {
                                _this.openProject(params.row.id);
                            }
                        }
                    }, params.row.title);
                }
            }, {
                "title": this.$L("收藏时间"),
                "minWidth": 160,
                render: function render(h, params) {
                    return h('span', $A.formatDate("Y-m-d H:i:s", params.row.uindate));
                }
            }, {
                "title": this.$L("操作"),
                "key": 'action',
                "width": 80,
                "align": 'center',
                render: function render(h, params) {
                    return h('Button', {
                        props: {
                            type: 'primary',
                            size: 'small'
                        },
                        style: {
                            fontSize: '12px'
                        },
                        on: {
                            click: function click() {
                                _this.$Modal.confirm({
                                    title: _this.$L('取消收藏'),
                                    content: _this.$L('你确定要取消收藏此项目吗？'),
                                    loading: true,
                                    onOk: function onOk() {
                                        _this.favorProject('cancel', params.row.id, function () {
                                            _this.getLists();
                                        });
                                    }
                                });
                            }
                        }
                    }, _this.$L('取消'));
                }
            }];
        },
        setPage: function setPage(page) {
            this.listPage = page;
            this.getLists();
        },
        setPageSize: function setPageSize(size) {
            if (Math.max($A.runNum(this.listPageSize), 10) != size) {
                this.listPageSize = size;
                this.getLists();
            }
        },
        getLists: function getLists(resetLoad) {
            var _this2 = this;

            if (resetLoad === true) {
                this.listPage = 1;
            }
            this.loadIng++;
            this.noDataText = this.$L("数据加载中.....");
            $A.apiAjax({
                url: 'project/lists',
                data: {
                    act: 'favor',
                    page: Math.max(this.listPage, 1),
                    pagesize: Math.max($A.runNum(this.listPageSize), 10)
                },
                complete: function complete() {
                    _this2.loadIng--;
                },
                error: function error() {
                    _this2.noDataText = _this2.$L("数据加载失败！");
                },
                success: function success(res) {
                    if (res.ret === 1) {
                        _this2.lists = res.data.lists;
                        _this2.listTotal = res.data.total;
                        _this2.noDataText = _this2.$L("没有相关的数据");
                    } else {
                        _this2.lists = [];
                        _this2.listTotal = 0;
                        _this2.noDataText = res.msg;
                    }
                }
            });
        }
    }
});

/***/ }),

/***/ 519:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("drawer-tabs-container", [
    _c(
      "div",
      { staticClass: "project-my-favor" },
      [
        _c("Table", {
          ref: "tableRef",
          staticClass: "tableFill",
          attrs: {
            columns: _vm.columns,
            data: _vm.lists,
            loading: _vm.loadIng > 0,
            "no-data-text": _vm.noDataText,
            stripe: ""
          }
        }),
        _vm._v(" "),
        _c("Page", {
          staticClass: "pageBox",
          attrs: {
            total: _vm.listTotal,
            current: _vm.listPage,
            disabled: _vm.loadIng > 0,
            "page-size-opts": [10, 20, 30, 50, 100],
            placement: "top",
            "show-elevator": "",
            "show-sizer": "",
            "show-total": "",
            transfer: "",
            simple: _vm.windowMax768
          },
          on: {
            "on-change": _vm.setPage,
            "on-page-size-change": _vm.setPageSize
          }
        })
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-6c4e3706", module.exports)
  }
}

/***/ }),

/***/ 520:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(521)
}
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(523)
/* template */
var __vue_template__ = __webpack_require__(524)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-3770ef92"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/main/components/project/my/join.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-3770ef92", Component.options)
  } else {
    hotAPI.reload("data-v-3770ef92", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 521:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(522);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(1)("5e5666bc", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../../node_modules/css-loader/index.js!../../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-3770ef92\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../../node_modules/sass-loader/lib/loader.js!../../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./join.vue", function() {
     var newContent = require("!!../../../../../../../node_modules/css-loader/index.js!../../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-3770ef92\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../../node_modules/sass-loader/lib/loader.js!../../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./join.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 522:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)(false);
// imports


// module
exports.push([module.i, "\n.project-my-join .tableFill[data-v-3770ef92] {\n  margin: 12px 12px 20px;\n}\n", ""]);

// exports


/***/ }),

/***/ 523:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__mixins_project__ = __webpack_require__(319);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__DrawerTabsContainer__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__DrawerTabsContainer___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__DrawerTabsContainer__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
    name: 'ProjectMyJoin',
    components: { DrawerTabsContainer: __WEBPACK_IMPORTED_MODULE_1__DrawerTabsContainer___default.a },
    props: {
        canload: {
            type: Boolean,
            default: true
        }
    },
    mixins: [__WEBPACK_IMPORTED_MODULE_0__mixins_project__["a" /* default */]],
    data: function data() {
        return {
            loadYet: false,

            loadIng: 0,

            columns: [],

            lists: [],
            listPage: 1,
            listTotal: 0,
            noDataText: ""
        };
    },
    mounted: function mounted() {
        if (this.canload) {
            this.loadYet = true;
            this.getLists(true);
        }
    },


    watch: {
        canload: function canload(val) {
            if (val && !this.loadYet) {
                this.loadYet = true;
                this.getLists(true);
            }
        }
    },

    methods: {
        initLanguage: function initLanguage() {
            var _this = this;

            this.noDataText = this.$L("数据加载中.....");
            this.columns = [{
                "title": this.$L("项目名称"),
                "key": 'title',
                "minWidth": 100,
                render: function render(h, params) {
                    return h('a', {
                        attrs: {
                            href: 'javascript:void(0)'
                        },
                        on: {
                            click: function click() {
                                _this.openProject(params.row.id);
                            }
                        }
                    }, params.row.title);
                }
            }, {
                "title": this.$L("加入时间"),
                "minWidth": 160,
                render: function render(h, params) {
                    return h('span', $A.formatDate("Y-m-d H:i:s", params.row.uindate));
                }
            }, {
                "title": this.$L("操作"),
                "key": 'action',
                "width": 80,
                "align": 'center',
                render: function render(h, params) {
                    return h('Button', {
                        props: {
                            type: 'primary',
                            size: 'small'
                        },
                        style: {
                            fontSize: '12px'
                        },
                        on: {
                            click: function click() {
                                _this.outProject(params.row.id, function () {
                                    _this.getLists();
                                });
                            }
                        }
                    }, _this.$L('退出'));
                }
            }];
        },
        setPage: function setPage(page) {
            this.listPage = page;
            this.getLists();
        },
        setPageSize: function setPageSize(size) {
            if (Math.max($A.runNum(this.listPageSize), 10) != size) {
                this.listPageSize = size;
                this.getLists();
            }
        },
        getLists: function getLists(resetLoad) {
            var _this2 = this;

            if (resetLoad === true) {
                this.listPage = 1;
            }
            this.loadIng++;
            this.noDataText = this.$L("数据加载中.....");
            $A.apiAjax({
                url: 'project/lists',
                data: {
                    act: 'join',
                    page: Math.max(this.listPage, 1),
                    pagesize: Math.max($A.runNum(this.listPageSize), 10)
                },
                complete: function complete() {
                    _this2.loadIng--;
                },
                error: function error() {
                    _this2.noDataText = _this2.$L("数据加载失败！");
                },
                success: function success(res) {
                    if (res.ret === 1) {
                        _this2.lists = res.data.lists;
                        _this2.listTotal = res.data.total;
                        _this2.noDataText = _this2.$L("没有相关的数据");
                    } else {
                        _this2.lists = [];
                        _this2.listTotal = 0;
                        _this2.noDataText = res.msg;
                    }
                }
            });
        }
    }
});

/***/ }),

/***/ 524:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("drawer-tabs-container", [
    _c(
      "div",
      { staticClass: "project-my-join" },
      [
        _c("Table", {
          ref: "tableRef",
          staticClass: "tableFill",
          attrs: {
            columns: _vm.columns,
            data: _vm.lists,
            loading: _vm.loadIng > 0,
            "no-data-text": _vm.noDataText,
            stripe: ""
          }
        }),
        _vm._v(" "),
        _c("Page", {
          staticClass: "pageBox",
          attrs: {
            total: _vm.listTotal,
            current: _vm.listPage,
            disabled: _vm.loadIng > 0,
            "page-size-opts": [10, 20, 30, 50, 100],
            placement: "top",
            "show-elevator": "",
            "show-sizer": "",
            "show-total": "",
            transfer: "",
            simple: _vm.windowMax768
          },
          on: {
            "on-change": _vm.setPage,
            "on-page-size-change": _vm.setPageSize
          }
        })
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-3770ef92", module.exports)
  }
}

/***/ }),

/***/ 525:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(526)
}
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(528)
/* template */
var __vue_template__ = __webpack_require__(529)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-d009b366"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/main/components/project/my/manage.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-d009b366", Component.options)
  } else {
    hotAPI.reload("data-v-d009b366", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 526:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(527);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(1)("3c32c461", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../../node_modules/css-loader/index.js!../../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-d009b366\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../../node_modules/sass-loader/lib/loader.js!../../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./manage.vue", function() {
     var newContent = require("!!../../../../../../../node_modules/css-loader/index.js!../../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-d009b366\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../../node_modules/sass-loader/lib/loader.js!../../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./manage.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 527:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)(false);
// imports


// module
exports.push([module.i, "\n.project-my-manage .tableFill[data-v-d009b366] {\n  margin: 12px 12px 20px;\n}\n", ""]);

// exports


/***/ }),

/***/ 528:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__mixins_project__ = __webpack_require__(319);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__DrawerTabsContainer__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__DrawerTabsContainer___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__DrawerTabsContainer__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
    name: 'ProjectMyManage',
    components: { DrawerTabsContainer: __WEBPACK_IMPORTED_MODULE_1__DrawerTabsContainer___default.a },
    props: {
        canload: {
            type: Boolean,
            default: true
        }
    },
    mixins: [__WEBPACK_IMPORTED_MODULE_0__mixins_project__["a" /* default */]],
    data: function data() {
        return {
            loadYet: false,

            loadIng: 0,

            columns: [],

            lists: [],
            listPage: 1,
            listTotal: 0,
            noDataText: ""
        };
    },
    mounted: function mounted() {
        if (this.canload) {
            this.loadYet = true;
            this.getLists(true);
        }
    },


    watch: {
        canload: function canload(val) {
            if (val && !this.loadYet) {
                this.loadYet = true;
                this.getLists(true);
            }
        }
    },

    methods: {
        initLanguage: function initLanguage() {
            var _this = this;

            this.noDataText = this.$L("数据加载中.....");
            this.columns = [{
                "title": this.$L("项目名称"),
                "key": 'title',
                "minWidth": 100,
                render: function render(h, params) {
                    return h('a', {
                        attrs: {
                            href: 'javascript:void(0)'
                        },
                        on: {
                            click: function click() {
                                _this.openProject(params.row.id);
                            }
                        }
                    }, params.row.title);
                }
            }, {
                "title": this.$L("创建时间"),
                "minWidth": 160,
                render: function render(h, params) {
                    return h('span', $A.formatDate("Y-m-d H:i:s", params.row.indate));
                }
            }, {
                "title": this.$L("操作"),
                "key": 'action',
                "width": 80,
                "align": 'center',
                render: function render(h, params) {
                    return h('Button', {
                        props: {
                            type: 'primary',
                            size: 'small'
                        },
                        style: {
                            fontSize: '12px'
                        },
                        on: {
                            click: function click() {
                                _this.deleteProject(params.row.id, function () {
                                    _this.getLists();
                                });
                            }
                        }
                    }, _this.$L('删除'));
                }
            }];
        },
        setPage: function setPage(page) {
            this.listPage = page;
            this.getLists();
        },
        setPageSize: function setPageSize(size) {
            if (Math.max($A.runNum(this.listPageSize), 10) != size) {
                this.listPageSize = size;
                this.getLists();
            }
        },
        getLists: function getLists(resetLoad) {
            var _this2 = this;

            if (resetLoad === true) {
                this.listPage = 1;
            }
            this.loadIng++;
            this.noDataText = this.$L("数据加载中.....");
            $A.apiAjax({
                url: 'project/lists',
                data: {
                    act: 'manage',
                    page: Math.max(this.listPage, 1),
                    pagesize: Math.max($A.runNum(this.listPageSize), 10)
                },
                complete: function complete() {
                    _this2.loadIng--;
                },
                error: function error() {
                    _this2.noDataText = _this2.$L("数据加载失败！");
                },
                success: function success(res) {
                    if (res.ret === 1) {
                        _this2.lists = res.data.lists;
                        _this2.listTotal = res.data.total;
                        _this2.noDataText = _this2.$L("没有相关的数据");
                    } else {
                        _this2.lists = [];
                        _this2.listTotal = 0;
                        _this2.noDataText = res.msg;
                    }
                }
            });
        }
    }
});

/***/ }),

/***/ 529:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("drawer-tabs-container", [
    _c(
      "div",
      { staticClass: "project-my-manage" },
      [
        _c("Table", {
          ref: "tableRef",
          staticClass: "tableFill",
          attrs: {
            columns: _vm.columns,
            data: _vm.lists,
            loading: _vm.loadIng > 0,
            "no-data-text": _vm.noDataText,
            stripe: ""
          }
        }),
        _vm._v(" "),
        _c("Page", {
          staticClass: "pageBox",
          attrs: {
            total: _vm.listTotal,
            current: _vm.listPage,
            disabled: _vm.loadIng > 0,
            "page-size-opts": [10, 20, 30, 50, 100],
            placement: "top",
            "show-elevator": "",
            "show-sizer": "",
            "show-total": "",
            transfer: "",
            simple: _vm.windowMax768
          },
          on: {
            "on-change": _vm.setPage,
            "on-page-size-change": _vm.setPageSize
          }
        })
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-d009b366", module.exports)
  }
}

/***/ }),

/***/ 530:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "w-main project" },
    [
      _c("v-title", [
        _vm._v(
          _vm._s(_vm.$L("项目")) + "-" + _vm._s(_vm.$L("轻量级的团队在线协作"))
        )
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "w-nav" }, [
        _c("div", { staticClass: "nav-row" }, [
          _c("div", { staticClass: "w-nav-left" }, [
            _c("div", { staticClass: "page-nav-left" }, [
              _c(
                "span",
                {
                  staticClass: "hover",
                  on: {
                    click: function($event) {
                      _vm.addShow = true
                    }
                  }
                },
                [
                  _c("i", { staticClass: "ft icon" }, [_vm._v("")]),
                  _vm._v(" " + _vm._s(_vm.$L("新建项目")))
                ]
              ),
              _vm._v(" "),
              _vm.loadIng > 0
                ? _c(
                    "div",
                    { staticClass: "page-nav-loading" },
                    [_c("w-loading")],
                    1
                  )
                : _c("div", { staticClass: "page-nav-refresh" }, [
                    _c(
                      "em",
                      {
                        on: {
                          click: function($event) {
                            return _vm.getLists(true)
                          }
                        }
                      },
                      [_vm._v(_vm._s(_vm.$L("刷新")))]
                    )
                  ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "w-nav-flex" }),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "w-nav-right m768-show" },
            [
              _c(
                "Dropdown",
                {
                  attrs: { trigger: "click", transfer: "" },
                  on: { "on-click": _vm.handleProject }
                },
                [
                  _c("Icon", { attrs: { type: "md-menu", size: "18" } }),
                  _vm._v(" "),
                  _c(
                    "DropdownMenu",
                    { attrs: { slot: "list" }, slot: "list" },
                    [
                      _c("DropdownItem", { attrs: { name: "myjoin" } }, [
                        _vm._v(_vm._s(_vm.$L("参与的项目")))
                      ]),
                      _vm._v(" "),
                      _c("DropdownItem", { attrs: { name: "myfavor" } }, [
                        _vm._v(_vm._s(_vm.$L("收藏的项目")))
                      ]),
                      _vm._v(" "),
                      _c("DropdownItem", { attrs: { name: "mycreate" } }, [
                        _vm._v(_vm._s(_vm.$L("我管理的项目")))
                      ])
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c("div", { staticClass: "w-nav-right m768-hide" }, [
            _c(
              "span",
              {
                staticClass: "ft hover",
                on: {
                  click: function($event) {
                    return _vm.handleProject("myjoin", null)
                  }
                }
              },
              [
                _c("i", { staticClass: "ft icon" }, [_vm._v("")]),
                _vm._v(" " + _vm._s(_vm.$L("参与的项目")))
              ]
            ),
            _vm._v(" "),
            _c(
              "span",
              {
                staticClass: "ft hover",
                on: {
                  click: function($event) {
                    return _vm.handleProject("myfavor", null)
                  }
                }
              },
              [
                _c("i", { staticClass: "ft icon" }, [_vm._v("")]),
                _vm._v(" " + _vm._s(_vm.$L("收藏的项目")))
              ]
            ),
            _vm._v(" "),
            _c(
              "span",
              {
                staticClass: "ft hover",
                on: {
                  click: function($event) {
                    return _vm.handleProject("mycreate", null)
                  }
                }
              },
              [
                _c("i", { staticClass: "ft icon" }, [_vm._v("")]),
                _vm._v(" " + _vm._s(_vm.$L("我管理的项目")))
              ]
            )
          ])
        ])
      ]),
      _vm._v(" "),
      _c(
        "w-content",
        [
          _c(
            "ul",
            { staticClass: "project-list" },
            _vm._l(_vm.lists, function(item) {
              return _c("li", [
                _c("div", { staticClass: "project-item" }, [
                  _c("div", { staticClass: "project-head" }, [
                    item.loadIng === true
                      ? _c(
                          "div",
                          { staticClass: "project-loading" },
                          [_c("w-loading")],
                          1
                        )
                      : _vm._e(),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "project-title",
                        on: {
                          click: function($event) {
                            return _vm.handleProject("open", item)
                          }
                        }
                      },
                      [_vm._v(_vm._s(item.title))]
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "project-setting" },
                      [
                        _c(
                          "Dropdown",
                          {
                            staticClass: "right-info",
                            attrs: { trigger: "click", transfer: "" },
                            on: {
                              "on-click": function($event) {
                                return _vm.handleProject($event, item)
                              }
                            }
                          },
                          [
                            _c("Icon", {
                              staticClass: "project-setting-icon",
                              attrs: { type: "md-settings", size: "16" }
                            }),
                            _vm._v(" "),
                            _c(
                              "Dropdown-menu",
                              { attrs: { slot: "list" }, slot: "list" },
                              [
                                _c(
                                  "Dropdown-item",
                                  { attrs: { name: "open" } },
                                  [_vm._v(_vm._s(_vm.$L("打开")))]
                                ),
                                _vm._v(" "),
                                _c(
                                  "Dropdown-item",
                                  { attrs: { name: "favor" } },
                                  [_vm._v(_vm._s(_vm.$L("收藏")))]
                                ),
                                _vm._v(" "),
                                item.isowner
                                  ? _c(
                                      "Dropdown-item",
                                      { attrs: { name: "rename" } },
                                      [_vm._v(_vm._s(_vm.$L("重命名")))]
                                    )
                                  : _vm._e(),
                                _vm._v(" "),
                                item.isowner
                                  ? _c(
                                      "Dropdown-item",
                                      { attrs: { name: "transfer" } },
                                      [_vm._v(_vm._s(_vm.$L("移交项目")))]
                                    )
                                  : _vm._e(),
                                _vm._v(" "),
                                item.isowner
                                  ? _c(
                                      "Dropdown-item",
                                      { attrs: { name: "delete" } },
                                      [_vm._v(_vm._s(_vm.$L("删除")))]
                                    )
                                  : _c(
                                      "Dropdown-item",
                                      { attrs: { name: "out" } },
                                      [_vm._v(_vm._s(_vm.$L("退出")))]
                                    )
                              ],
                              1
                            )
                          ],
                          1
                        )
                      ],
                      1
                    )
                  ]),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      staticClass: "project-num",
                      on: {
                        click: function($event) {
                          return _vm.handleProject("open", item)
                        }
                      }
                    },
                    [
                      _c(
                        "div",
                        { staticClass: "project-circle" },
                        [
                          _c(
                            "i-circle",
                            {
                              attrs: {
                                size: 100,
                                "trail-width": 8,
                                "stroke-width": 8,
                                percent: _vm.selfProportion(
                                  item.self_complete,
                                  item.self_count
                                ),
                                "stroke-linecap": "round",
                                "stroke-color": "#62C5FE"
                              }
                            },
                            [
                              _c("div", { staticClass: "project-circle-box" }, [
                                _c(
                                  "div",
                                  { staticClass: "project-circle-num" },
                                  [
                                    _c("em", [
                                      _vm._v(_vm._s(item.self_complete))
                                    ]),
                                    _vm._v(" "),
                                    _c("span", [
                                      _vm._v(_vm._s(item.self_count))
                                    ])
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  { staticClass: "project-circle-title" },
                                  [_vm._v(_vm._s(_vm.$L("个人总计")))]
                                )
                              ])
                            ]
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c("div", { staticClass: "project-situation" }, [
                        _c("ul", [
                          _c("li", [
                            _vm._v(_vm._s(_vm.$L("项目总任务数"))),
                            _c("em", [
                              _vm._v(_vm._s(item.complete + item.unfinished))
                            ])
                          ]),
                          _vm._v(" "),
                          _c("li", [
                            _vm._v(_vm._s(_vm.$L("项目已完成数"))),
                            _c("em", [_vm._v(_vm._s(item.complete))])
                          ]),
                          _vm._v(" "),
                          _c("li", [
                            _vm._v(_vm._s(_vm.$L("项目未完成数"))),
                            _c("em", [_vm._v(_vm._s(item.unfinished))])
                          ])
                        ])
                      ])
                    ]
                  ),
                  _vm._v(" "),
                  _c("div", { staticClass: "project-bottom" }, [
                    _c(
                      "div",
                      { staticClass: "project-iconbtn" },
                      [
                        _c("Icon", {
                          staticClass: "project-iconbtn-icon",
                          attrs: { type: "md-stats" }
                        }),
                        _vm._v(" "),
                        _c(
                          "div",
                          {
                            staticClass: "project-iconbtn-text",
                            on: {
                              click: function($event) {
                                $event.stopPropagation()
                                return _vm.handleProject("statistics", item)
                              }
                            }
                          },
                          [_vm._v(_vm._s(_vm.$L("项目统计")))]
                        )
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "project-iconbtn" },
                      [
                        _c("Icon", {
                          staticClass: "project-iconbtn-icon",
                          attrs: { type: "md-filing" }
                        }),
                        _vm._v(" "),
                        _c(
                          "div",
                          {
                            staticClass: "project-iconbtn-text",
                            on: {
                              click: function($event) {
                                $event.stopPropagation()
                                return _vm.handleProject("archived", item)
                              }
                            }
                          },
                          [_vm._v(_vm._s(_vm.$L("已归档任务")))]
                        )
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "project-iconbtn project-people",
                        on: {
                          click: function($event) {
                            $event.stopPropagation()
                            return _vm.handleProject("member", item)
                          }
                        }
                      },
                      [
                        _vm._l(item.people_lists, function(uItem, uKey) {
                          return _c("UserImg", {
                            key: uKey,
                            staticClass: "userimg-icon",
                            attrs: {
                              info: uItem,
                              "two-words": "",
                              "show-title": ""
                            }
                          })
                        }),
                        _vm._v(" "),
                        item.people_count > 99
                          ? _c(
                              "div",
                              {
                                staticClass: "userimg-count",
                                attrs: { title: item.people_count }
                              },
                              [_vm._v("99+")]
                            )
                          : item.people_count > 5
                          ? _c("div", { staticClass: "userimg-count" }, [
                              _vm._v(_vm._s(item.people_count))
                            ])
                          : _vm._e()
                      ],
                      2
                    )
                  ])
                ])
              ])
            }),
            0
          ),
          _vm._v(" "),
          _vm.listTotal > 0
            ? _c("Page", {
                staticClass: "pageBox",
                attrs: {
                  total: _vm.listTotal,
                  current: _vm.listPage,
                  disabled: _vm.loadIng > 0,
                  pageSize: _vm.listPageSize,
                  "page-size-opts": [20, 40, 60, 100],
                  placement: "top",
                  transfer: "",
                  "show-elevator": "",
                  "show-sizer": "",
                  "show-total": "",
                  simple: _vm.windowMax768
                },
                on: {
                  "on-change": _vm.setPage,
                  "on-page-size-change": _vm.setPageSize
                }
              })
            : _vm._e()
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "Modal",
        {
          attrs: {
            title: _vm.$L("新建项目"),
            closable: false,
            "mask-closable": false,
            "class-name": "simple-modal"
          },
          model: {
            value: _vm.addShow,
            callback: function($$v) {
              _vm.addShow = $$v
            },
            expression: "addShow"
          }
        },
        [
          _c(
            "Form",
            {
              ref: "add",
              attrs: {
                model: _vm.formAdd,
                rules: _vm.ruleAdd,
                "label-width": 80
              },
              nativeOn: {
                submit: function($event) {
                  $event.preventDefault()
                }
              }
            },
            [
              _c(
                "FormItem",
                { attrs: { prop: "title", label: _vm.$L("项目名称") } },
                [
                  _c("Input", {
                    attrs: { type: "text" },
                    model: {
                      value: _vm.formAdd.title,
                      callback: function($$v) {
                        _vm.$set(_vm.formAdd, "title", $$v)
                      },
                      expression: "formAdd.title"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "FormItem",
                { attrs: { prop: "labels", label: _vm.$L("项目模板") } },
                [
                  _c(
                    "Select",
                    {
                      on: {
                        "on-change": function(res) {
                          _vm.$set(
                            _vm.formAdd,
                            "labels",
                            _vm.labelLists[res].value
                          )
                        }
                      },
                      model: {
                        value: _vm.formAdd.template,
                        callback: function($$v) {
                          _vm.$set(_vm.formAdd, "template", $$v)
                        },
                        expression: "formAdd.template"
                      }
                    },
                    _vm._l(_vm.labelLists, function(item, index) {
                      return _c(
                        "Option",
                        { key: index, attrs: { value: index } },
                        [_vm._v(_vm._s(item.label))]
                      )
                    }),
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c("FormItem", { attrs: { label: _vm.$L("项目流程") } }, [
                _c(
                  "div",
                  { staticStyle: { "line-height": "38px" } },
                  _vm._l(_vm.formAdd.labels, function(item, index) {
                    return _c(
                      "span",
                      [
                        index > 0 ? _c("span", [_vm._v(">")]) : _vm._e(),
                        _vm._v(" "),
                        _c(
                          "Tag",
                          {
                            attrs: {
                              closable: "",
                              size: "large",
                              color: "primary"
                            },
                            on: {
                              "on-close": function() {
                                _vm.formAdd.labels.splice(index, 1)
                              }
                            }
                          },
                          [_vm._v(_vm._s(item))]
                        )
                      ],
                      1
                    )
                  }),
                  0
                ),
                _vm._v(" "),
                _vm.formAdd.labels.length > 0
                  ? _c("div", { staticStyle: { "margin-top": "4px" } })
                  : _vm._e(),
                _vm._v(" "),
                _c(
                  "div",
                  { staticStyle: { "margin-bottom": "-16px" } },
                  [
                    _c(
                      "Button",
                      {
                        attrs: { icon: "ios-add", type: "dashed" },
                        on: { click: _vm.addLabels }
                      },
                      [_vm._v(_vm._s(_vm.$L("添加流程")))]
                    )
                  ],
                  1
                )
              ])
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "div",
            { attrs: { slot: "footer" }, slot: "footer" },
            [
              _c(
                "Button",
                {
                  attrs: { type: "default" },
                  on: {
                    click: function($event) {
                      _vm.addShow = false
                    }
                  }
                },
                [_vm._v(_vm._s(_vm.$L("取消")))]
              ),
              _vm._v(" "),
              _c(
                "Button",
                {
                  attrs: { type: "primary", loading: _vm.loadIng > 0 },
                  on: { click: _vm.onAdd }
                },
                [_vm._v(_vm._s(_vm.$L("添加")))]
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "WDrawer",
        {
          attrs: { maxWidth: "1000" },
          model: {
            value: _vm.projectDrawerShow,
            callback: function($$v) {
              _vm.projectDrawerShow = $$v
            },
            expression: "projectDrawerShow"
          }
        },
        [
          _vm.projectDrawerShow
            ? _c(
                "Tabs",
                {
                  model: {
                    value: _vm.projectDrawerTab,
                    callback: function($$v) {
                      _vm.projectDrawerTab = $$v
                    },
                    expression: "projectDrawerTab"
                  }
                },
                [
                  _c(
                    "TabPane",
                    {
                      attrs: { label: _vm.$L("已归档任务"), name: "archived" }
                    },
                    [
                      _c("project-archived", {
                        attrs: {
                          canload:
                            _vm.projectDrawerShow &&
                            _vm.projectDrawerTab == "archived",
                          projectid: _vm.handleProjectId
                        }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "TabPane",
                    {
                      attrs: { label: _vm.$L("项目统计"), name: "statistics" }
                    },
                    [
                      _c("project-statistics", {
                        attrs: {
                          canload:
                            _vm.projectDrawerShow &&
                            _vm.projectDrawerTab == "statistics",
                          projectid: _vm.handleProjectId
                        }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "TabPane",
                    { attrs: { label: _vm.$L("成员管理"), name: "member" } },
                    [
                      _c("project-users", {
                        attrs: {
                          canload:
                            _vm.projectDrawerShow &&
                            _vm.projectDrawerTab == "member",
                          projectid: _vm.handleProjectId
                        }
                      })
                    ],
                    1
                  )
                ],
                1
              )
            : _vm._e()
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "WDrawer",
        {
          attrs: { maxWidth: "740" },
          model: {
            value: _vm.projectListDrawerShow,
            callback: function($$v) {
              _vm.projectListDrawerShow = $$v
            },
            expression: "projectListDrawerShow"
          }
        },
        [
          _vm.projectListDrawerShow
            ? _c(
                "Tabs",
                {
                  model: {
                    value: _vm.projectListDrawerTab,
                    callback: function($$v) {
                      _vm.projectListDrawerTab = $$v
                    },
                    expression: "projectListDrawerTab"
                  }
                },
                [
                  _c(
                    "TabPane",
                    { attrs: { label: _vm.$L("参与的项目"), name: "myjoin" } },
                    [
                      _c("project-my-join", {
                        attrs: {
                          canload:
                            _vm.projectListDrawerShow &&
                            _vm.projectListDrawerTab == "myjoin"
                        }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "TabPane",
                    { attrs: { label: _vm.$L("收藏的项目"), name: "myfavor" } },
                    [
                      _c("project-my-favor", {
                        attrs: {
                          canload:
                            _vm.projectListDrawerShow &&
                            _vm.projectListDrawerTab == "myfavor"
                        }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "TabPane",
                    {
                      attrs: { label: _vm.$L("管理的项目"), name: "mycreate" }
                    },
                    [
                      _c("project-my-manage", {
                        attrs: {
                          canload:
                            _vm.projectListDrawerShow &&
                            _vm.projectListDrawerTab == "mycreate"
                        }
                      })
                    ],
                    1
                  )
                ],
                1
              )
            : _vm._e()
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-426c4faa", module.exports)
  }
}

/***/ })

});