webpackJsonp([8],{

/***/ 287:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(446)
  __webpack_require__(448)
}
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(450)
/* template */
var __vue_template__ = __webpack_require__(451)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-effe19ba"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/main/pages/index.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-effe19ba", Component.options)
  } else {
    hotAPI.reload("data-v-effe19ba", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 325:
/***/ (function(module, exports) {

module.exports = "/images/statics/images/logo-white.png?069beff1";

/***/ }),

/***/ 446:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(447);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(1)("2a20ec4c", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-effe19ba\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./index.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-effe19ba\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./index.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 447:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)(false);
// imports


// module
exports.push([module.i, "\n.login-header {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n      -ms-flex-align: center;\n          align-items: center;\n}\n.login-header .login-header-item {\n    height: 20px;\n    line-height: 20px;\n    font-size: 14px;\n    color: #444444;\n    overflow: hidden;\n    text-overflow: ellipsis;\n    white-space: nowrap;\n    padding-right: 12px;\n    cursor: pointer;\n}\n.login-header .login-header-item.active {\n      font-size: 16px;\n      color: #17233d;\n      font-weight: 500;\n}\n.login-code {\n  margin: -4px -7px;\n  height: 30px;\n  overflow: hidden;\n  cursor: pointer;\n}\n.login-code img {\n    height: 100%;\n}\n", ""]);

// exports


/***/ }),

/***/ 448:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(449);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(1)("22a60908", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-effe19ba\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=1!./index.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-effe19ba\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=1!./index.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 449:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)(false);
// imports


// module
exports.push([module.i, "\n.index[data-v-effe19ba] {\n  position: absolute;\n  color: #000000;\n  top: 0;\n  left: 0;\n  min-width: 100%;\n  min-height: 100%;\n  padding: 0;\n  margin: 0;\n}\n.index .header[data-v-effe19ba] {\n    position: relative;\n    z-index: 3;\n    height: 50px;\n    padding-top: 12px;\n    max-width: 1280px;\n    margin: 0 auto;\n}\n.index .header .z-row[data-v-effe19ba] {\n      color: #fff;\n      height: 50px;\n      position: relative;\n      z-index: 2;\n      max-width: 1680px;\n      margin: 0 auto;\n}\n.index .header .z-row .header-col-sub[data-v-effe19ba] {\n        width: 500px;\n}\n.index .header .z-row .header-col-sub h2[data-v-effe19ba] {\n          position: relative;\n          padding: 1rem 0 0 1rem;\n          display: -webkit-box;\n          display: -ms-flexbox;\n          display: flex;\n          -webkit-box-align: end;\n              -ms-flex-align: end;\n                  align-items: flex-end;\n}\n.index .header .z-row .header-col-sub h2 img[data-v-effe19ba] {\n            width: 150px;\n            margin-right: 6px;\n}\n.index .header .z-row .header-col-sub h2 span[data-v-effe19ba] {\n            font-size: 12px;\n            font-weight: normal;\n            color: rgba(255, 255, 255, 0.85);\n            line-height: 14px;\n}\n.index .header .z-row .z-1 dl[data-v-effe19ba] {\n        position: absolute;\n        right: 20px;\n        top: 0;\n        font-size: 14px;\n}\n.index .header .z-row .z-1 dl dd[data-v-effe19ba] {\n          line-height: 50px;\n          color: #fff;\n          cursor: pointer;\n          margin-right: 1px;\n}\n.index .header .z-row .z-1 dl dd .right-enterprise[data-v-effe19ba] {\n            padding: 1px 10px;\n            font-size: 12px;\n            color: #f6ca9d;\n            background: #1d1e23;\n            background: -webkit-gradient(linear, left top, right top, from(#1d1e23), to(#3f4045));\n            background: linear-gradient(90deg, #1d1e23, #3f4045);\n            border: none;\n}\n.index .header .z-row .z-1 dl dd .right-info[data-v-effe19ba] {\n            display: inline-block;\n            cursor: pointer;\n            margin-left: 12px;\n            color: #ffffff;\n}\n.index .header .z-row .z-1 dl dd .right-info .right-icon[data-v-effe19ba] {\n              font-size: 26px;\n              vertical-align: middle;\n}\n.index .header .z-row .z-1 dl dd .right-info .right-img[data-v-effe19ba] {\n              vertical-align: middle;\n}\n.index .welbg[data-v-effe19ba] {\n    position: absolute;\n    z-index: 1;\n    top: 0;\n    left: 0;\n    right: 0;\n    height: 762px;\n    overflow: hidden;\n    padding-top: 480px;\n    margin-top: -480px;\n    background: #2d8cf0;\n    -webkit-transform: skewY(-2deg);\n            transform: skewY(-2deg);\n    -webkit-box-shadow: 0 2px 244px 0 rgba(56, 132, 255, 0.4);\n            box-shadow: 0 2px 244px 0 rgba(56, 132, 255, 0.4);\n}\n.index .welbg .second[data-v-effe19ba] {\n      position: relative;\n      margin-top: 582px;\n      height: 220px;\n}\n.index .welbg .second .bg[data-v-effe19ba] {\n        -webkit-transform: skewY(-2.5deg);\n                transform: skewY(-2.5deg);\n        display: block;\n        position: absolute;\n        top: 0;\n        left: 0;\n        right: 0;\n        bottom: 0;\n        background: #1F65D6;\n}\n.index .welcome[data-v-effe19ba] {\n    position: relative;\n    z-index: 2;\n    height: 700px;\n    display: block;\n    overflow: hidden;\n    color: #FFFFFF;\n    padding-top: 480px;\n    margin-top: -480px;\n}\n.index .welcome .unslider-arrow[data-v-effe19ba] {\n      display: none;\n}\n.index .welcome .banner[data-v-effe19ba] {\n      padding-top: 60px;\n      height: 460px;\n      max-width: 1200px;\n      margin: 0 auto;\n}\n.index .welcome .banner .banner-carousel[data-v-effe19ba] {\n        max-width: 685px;\n        border-radius: 5px;\n        overflow: hidden;\n}\n.index .welcome .banner img[data-v-effe19ba] {\n        height: 400px;\n        border: 5px solid #fff;\n        display: table;\n}\n.index .welcome .z-8[data-v-effe19ba] {\n      text-align: center;\n}\n.index .welcome h3[data-v-effe19ba] {\n      color: rgba(255, 255, 255, 0.8);\n      font-size: 40px;\n      font-weight: normal;\n      text-align: center;\n      margin: 30px auto;\n      width: 380px;\n}\n.index .welcome .start[data-v-effe19ba] {\n      display: inline-block;\n      width: 160px;\n      height: 50px;\n      line-height: 50px;\n      text-align: center;\n      font-weight: normal;\n      cursor: pointer;\n      font-size: 20px;\n      background: #fff;\n      color: #0396f2;\n      border-radius: 4px;\n      border: 0;\n}\n.index .welcome .start[data-v-effe19ba]:hover, .index .welcome .start[data-v-effe19ba]:focus {\n        background: #f6f6f6;\n        color: #0396f2;\n}\n.index .welcome .second[data-v-effe19ba] {\n      position: relative;\n      height: 220px;\n      text-align: center;\n}\n.index .welcome .second .bg[data-v-effe19ba] {\n        display: block;\n        position: absolute;\n        top: 0;\n        left: 0;\n        right: 0;\n        bottom: 0;\n}\n.index .welcome .second .z-row[data-v-effe19ba] {\n        z-index: 2;\n        position: relative;\n        font-size: 22px;\n        max-width: 1400px;\n        margin: 0 auto;\n        line-height: 220px;\n}\n.index .welcome .second i[data-v-effe19ba] {\n        color: rgba(255, 255, 255, 0.85);\n        margin-right: 6px;\n}\n.index .welcome .second a[data-v-effe19ba] {\n        color: #fff;\n}\n.index .welcome .second a[data-v-effe19ba]:hover, .index .welcome .second a[data-v-effe19ba]:visited {\n          color: #fff;\n}\n.index .block[data-v-effe19ba] {\n    max-width: 1200px;\n    margin: 30px auto;\n    padding-top: 50px;\n    border: 1px solid transparent;\n}\n.index .block .wrap-left[data-v-effe19ba], .index .block .wrap-right[data-v-effe19ba] {\n      line-height: 36px;\n      color: #666;\n      font-size: 16px;\n}\n.index .block .wrap-left[data-v-effe19ba] {\n      margin: 20px 30px 0 0;\n}\n.index .block .wrap-right[data-v-effe19ba] {\n      margin: 20px 0 0 30px;\n}\n.index .block i[data-v-effe19ba] {\n      color: rgba(248, 14, 21, 0.7);\n      margin-right: 6px;\n}\n.index .block img[data-v-effe19ba] {\n      border: 5px solid #fff;\n      border-radius: 10px;\n      width: 100%;\n}\n.index .p-footer[data-v-effe19ba] {\n    margin: 20px 0;\n    text-align: center;\n    color: #333;\n}\n.index .p-footer a[data-v-effe19ba], .index .p-footer span[data-v-effe19ba] {\n      color: #333;\n      margin-left: 10px;\n}\n@media (max-width: 768px) {\n.index .header .z-row .header-col-sub h2[data-v-effe19ba] {\n      padding: 12px 0 0 12px;\n}\n.index .header .z-row .header-col-sub h2 span[data-v-effe19ba] {\n        display: none;\n}\n.index .welbg[data-v-effe19ba] {\n      display: none;\n}\n.index .welcome[data-v-effe19ba] {\n      height: auto;\n      background: #2d8cf0;\n      -webkit-transform: skewY(-2deg);\n              transform: skewY(-2deg);\n      -webkit-box-shadow: 0 2px 244px 0 rgba(56, 132, 255, 0.4);\n              box-shadow: 0 2px 244px 0 rgba(56, 132, 255, 0.4);\n}\n.index .welcome .banner[data-v-effe19ba] {\n        height: auto;\n        -webkit-transform: skewY(2deg);\n                transform: skewY(2deg);\n}\n.index .welcome .banner .z-row[data-v-effe19ba] {\n          -webkit-box-orient: vertical;\n          -webkit-box-direction: normal;\n              -ms-flex-direction: column;\n                  flex-direction: column;\n          padding-bottom: 52px;\n}\n.index .welcome .banner .z-row > div[data-v-effe19ba] {\n            width: 90%;\n            margin: 0 auto;\n}\n.index .welcome .banner h3[data-v-effe19ba] {\n          font-size: 24px;\n          width: auto;\n}\n.index .welcome .banner img[data-v-effe19ba] {\n          max-width: 100%;\n          height: auto;\n}\n.index .welcome .second[data-v-effe19ba] {\n        height: 120px;\n        display: -webkit-box;\n        display: -ms-flexbox;\n        display: flex;\n        -webkit-box-align: center;\n            -ms-flex-align: center;\n                align-items: center;\n        -webkit-transform: skewY(2deg);\n                transform: skewY(2deg);\n}\n.index .welcome .second .bg[data-v-effe19ba] {\n          -webkit-transform: skewY(-4.5deg);\n                  transform: skewY(-4.5deg);\n          background: #1F65D6;\n}\n.index .welcome .second .z-row[data-v-effe19ba] {\n          height: auto;\n          line-height: 36px;\n          font-size: 16px;\n          display: block;\n}\n.index .welcome .second .z-row .z-6[data-v-effe19ba] {\n            width: 30%;\n            margin: 0 5%;\n            white-space: nowrap;\n}\n.index .block[data-v-effe19ba] {\n      -webkit-box-orient: vertical;\n      -webkit-box-direction: normal;\n          -ms-flex-direction: column;\n              flex-direction: column;\n      margin: 24px auto;\n      padding-top: 24px;\n      max-width: 90%;\n      border: 0;\n}\n.index .block > div[data-v-effe19ba] {\n        width: 96%;\n        margin: 0 auto;\n}\n.index .block .wrap-left[data-v-effe19ba],\n      .index .block .wrap-right[data-v-effe19ba] {\n        margin: 6px 0;\n        line-height: 28px;\n}\n}\n", ""]);

// exports


/***/ }),

/***/ 450:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    data: function data() {
        return {
            loadIng: 0,
            loginShow: false,
            loginType: 'login',

            codeNeed: false,
            codeUrl: $A.apiUrl('users/login/codeimg'),

            formLogin: {
                username: '',
                userpass: '',
                userpass2: '',
                code: ''
            },
            ruleLogin: {},

            systemConfig: $A.jsonParse($A.storage("systemSetting")),

            fromUrl: ''
        };
    },
    mounted: function mounted() {
        //
    },
    activated: function activated() {
        this.getSetting();
        this.fromUrl = decodeURIComponent($A.getObject(this.$route.query, 'from'));
        if (this.fromUrl) {
            this.loginChack();
        }
    },
    deactivated: function deactivated() {
        this.loginShow = false;
    },


    watch: {
        loginShow: function loginShow(val) {
            if (val) {
                this.getSetting();
            } else {
                this.loginType = 'login';
            }
        }
    },

    methods: {
        initLanguage: function initLanguage() {
            var _this = this;

            this.ruleLogin = {
                username: [{ required: true, message: this.$L('请填写用户名！'), trigger: 'change' }, { type: 'string', min: 2, message: this.$L('用户名长度至少2位！'), trigger: 'change' }],
                userpass: [{ required: true, message: this.$L('请填写登录密码！'), trigger: 'change' }, { type: 'string', min: 6, message: this.$L('密码错长度至少6位！'), trigger: 'change' }],
                userpass2: [{ required: true, message: this.$L('请填写确认密码！'), trigger: 'change' }, { type: 'string', min: 6, message: this.$L('确认密码错长度至少6位！'), trigger: 'change' }, {
                    validator: function validator(rule, value, callback) {
                        if (value !== _this.formLogin.userpass) {
                            callback(new Error(_this.$L('两次密码输入不一致！')));
                        } else {
                            callback();
                        }
                    },
                    required: true,
                    trigger: 'change'
                }]
            };
        },
        getSetting: function getSetting() {
            var _this2 = this;

            $A.apiAjax({
                url: 'system/setting',
                error: function error() {
                    $A.storage("systemSetting", {});
                },
                success: function success(res) {
                    if (res.ret === 1) {
                        _this2.systemConfig = res.data;
                        $A.storage("systemSetting", _this2.systemConfig);
                    } else {
                        $A.storage("systemSetting", {});
                    }
                    if (_this2.systemConfig.loginWin == 'direct') {
                        _this2.loginChack();
                    }
                }
            });
        },
        loginChack: function loginChack() {
            if ($A.getToken() !== false) {
                this.goForward({ path: '/todo' }, true);
            } else {
                this.loginShow = true;
            }
        },
        refreshCode: function refreshCode() {
            this.codeUrl = $A.apiUrl('users/login/codeimg?_=' + Math.random());
        },
        enterpriseOpen: function enterpriseOpen() {
            this.goForward({ path: '/plans' });
        },
        onBlur: function onBlur() {
            var _this3 = this;

            if (this.loginType != 'login') {
                this.codeNeed = false;
                return;
            }
            this.loadIng++;
            $A.ajax({
                url: $A.apiUrl('users/login/needcode'),
                data: {
                    username: this.formLogin.username
                },
                complete: function complete() {
                    _this3.loadIng--;
                },
                success: function success(res) {
                    _this3.codeNeed = res.ret === 1;
                }
            });
        },
        onLogin: function onLogin() {
            var _this4 = this;

            this.$refs.login.validate(function (valid) {
                if (valid) {
                    _this4.loadIng++;
                    $A.ajax({
                        url: $A.apiUrl('users/login?type=' + _this4.loginType),
                        data: _this4.formLogin,
                        complete: function complete() {
                            _this4.loadIng--;
                        },
                        success: function success(res) {
                            if (res.ret === 1) {
                                $A.storage("userInfo", res.data);
                                $A.setToken(res.data.token);
                                $A.triggerUserInfoListener(res.data);
                                //
                                _this4.loadIng--;
                                _this4.loginShow = false;
                                _this4.$refs.login.resetFields();
                                _this4.$Message.success(_this4.$L('登录成功'));
                                if (_this4.fromUrl) {
                                    window.location.replace(_this4.fromUrl);
                                } else {
                                    _this4.goForward({ path: '/todo' }, true);
                                }
                            } else {
                                _this4.$Modal.error({
                                    title: _this4.$L("温馨提示"),
                                    content: res.msg
                                });
                                if (res.data.code === 'need') {
                                    _this4.codeNeed = true;
                                    _this4.refreshCode();
                                }
                            }
                        }
                    });
                }
            });
        }
    }
});

/***/ }),

/***/ 451:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "w-box index" },
    [
      _c("v-title", [_vm._v(_vm._s(_vm.$L("轻量级的团队在线协作")))]),
      _vm._v(" "),
      _vm._m(0),
      _vm._v(" "),
      _c("div", { staticClass: "header" }, [
        _c("div", { staticClass: "z-row" }, [
          _c("div", { staticClass: "header-col-sub" }, [
            _c("h2", [
              _vm.systemConfig.logo
                ? _c("img", { attrs: { src: _vm.systemConfig.logo } })
                : _c("img", {
                    attrs: {
                      src: __webpack_require__(325)
                    }
                  }),
              _vm._v(" "),
              _c("span", [_vm._v(_vm._s(_vm.$L("轻量级的团队在线协作")))])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "z-1" }, [
            _c("dl", [
              _c(
                "dd",
                [
                  _vm.systemConfig.enterprise == "show"
                    ? _c(
                        "Button",
                        {
                          staticClass: "right-enterprise",
                          attrs: { type: "success", size: "small" },
                          on: { click: _vm.enterpriseOpen }
                        },
                        [_vm._v(_vm._s(_vm.$L("企业版")))]
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.systemConfig.github == "show"
                    ? _c(
                        "a",
                        {
                          staticClass: "right-info",
                          attrs: {
                            href: "https://gitee.com/aipaw/wookteam",
                            target: "_blank"
                          }
                        },
                        [
                          _c("img", {
                            staticClass: "right-img",
                            attrs: {
                              src:
                                "https://gitee.com/aipaw/wookteam/badge/star.svg?theme=gvp",
                              alt: "star"
                            }
                          })
                        ]
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  _vm.systemConfig.github == "show"
                    ? _c(
                        "a",
                        {
                          staticClass: "right-info",
                          attrs: {
                            target: "_blank",
                            href: "https://github.com/kuaifan/wookteam"
                          }
                        },
                        [
                          _c("Icon", {
                            staticClass: "right-icon",
                            attrs: { type: "logo-github" }
                          })
                        ],
                        1
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  _c(
                    "Dropdown",
                    {
                      staticClass: "right-info",
                      attrs: { trigger: "hover", transfer: "" },
                      on: { "on-click": _vm.setLanguage }
                    },
                    [
                      _c(
                        "div",
                        [
                          _c("Icon", {
                            staticClass: "right-icon",
                            attrs: { type: "md-globe" }
                          }),
                          _vm._v(" "),
                          _c("Icon", { attrs: { type: "md-arrow-dropdown" } })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "Dropdown-menu",
                        { attrs: { slot: "list" }, slot: "list" },
                        [
                          _c(
                            "Dropdown-item",
                            {
                              attrs: {
                                name: "zh",
                                selected: _vm.getLanguage() === "zh"
                              }
                            },
                            [_vm._v("中文")]
                          ),
                          _vm._v(" "),
                          _c(
                            "Dropdown-item",
                            {
                              attrs: {
                                name: "en",
                                selected: _vm.getLanguage() === "en"
                              }
                            },
                            [_vm._v("English")]
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "welcome" }, [
        _c("div", { staticClass: "banner" }, [
          _c("div", { staticClass: "z-row" }, [
            _c(
              "div",
              { staticClass: "z-16" },
              [
                _c(
                  "Carousel",
                  {
                    staticClass: "banner-carousel",
                    attrs: { autoplay: "", loop: "", "autoplay-speed": 5000 }
                  },
                  [
                    _c("CarouselItem", [
                      _c("img", {
                        attrs: {
                          src: __webpack_require__(452)
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("CarouselItem", [
                      _c("img", {
                        attrs: {
                          src: __webpack_require__(453)
                        }
                      })
                    ])
                  ],
                  1
                )
              ],
              1
            ),
            _vm._v(" "),
            _c("div", { staticClass: "z-8" }, [
              _c("h3", [_vm._v(_vm._s(_vm.$L("酷团队协作工具就从这里开始")))]),
              _vm._v(" "),
              _c("div", { staticClass: "bl inline-block" }, [
                _c(
                  "span",
                  { staticClass: "start", on: { click: _vm.loginChack } },
                  [_vm._v(_vm._s(_vm.$L("立即登陆")))]
                )
              ])
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "second" }, [
          _c("div", { staticClass: "bg" }),
          _vm._v(" "),
          _c("div", { staticClass: "z-row" }, [
            _c("div", { staticClass: "z-6" }, [
              _c("a", { attrs: { href: "#W_link1" } }, [
                _c("i", { staticClass: "ft icon" }, [_vm._v("")]),
                _vm._v(_vm._s(_vm.$L("待办四象限")))
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "z-6" }, [
              _c("a", { attrs: { href: "#W_link2" } }, [
                _c("i", { staticClass: "ft icon" }, [_vm._v("")]),
                _vm._v(_vm._s(_vm.$L("项目管理")))
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "z-6" }, [
              _c("a", { attrs: { href: "#W_link3" } }, [
                _c("i", { staticClass: "ft icon" }, [_vm._v("")]),
                _vm._v(_vm._s(_vm.$L("在线知识库")))
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "z-6" }, [
              _c("a", { attrs: { href: "#W_link4" } }, [
                _c("i", { staticClass: "ft icon" }, [_vm._v("")]),
                _vm._v(_vm._s(_vm.$L("日程管理")))
              ])
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "z-row block" }, [
        _c("div", { staticClass: "z-6" }, [
          _c("div", { staticClass: "wrap-left", attrs: { id: "W_link1" } }, [
            _c("i", { staticClass: "ft icon" }, [_vm._v("")]),
            _vm._v(
              _vm._s(
                _vm.$L(
                  "待办四象限：突出事情优先级，帮助员工合理安排时间，提高工作效率。"
                )
              )
            )
          ])
        ]),
        _vm._v(" "),
        _vm._m(1)
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "z-row block" }, [
        _vm._m(2),
        _vm._v(" "),
        _c("div", { staticClass: "z-6" }, [
          _c("div", { staticClass: "wrap-right", attrs: { id: "W_link2" } }, [
            _c("i", { staticClass: "ft icon" }, [_vm._v("")]),
            _vm._v(_vm._s(_vm.$L("项目管理：自定义项目看板，可视化任务安排。")))
          ])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "z-row block" }, [
        _c("div", { staticClass: "z-6" }, [
          _c("div", { staticClass: "wrap-left", attrs: { id: "W_link3" } }, [
            _c("i", { staticClass: "ft icon" }, [_vm._v("")]),
            _vm._v(
              _vm._s(
                _vm.$L(
                  "在线知识库：在线流程图，在线文档，以及可视化的目录编排，文档管理无忧。"
                )
              )
            )
          ])
        ]),
        _vm._v(" "),
        _vm._m(3)
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "z-row block" }, [
        _vm._m(4),
        _vm._v(" "),
        _c("div", { staticClass: "z-6" }, [
          _c("div", { staticClass: "wrap-right", attrs: { id: "W_link4" } }, [
            _c("i", { staticClass: "ft icon" }, [_vm._v("")]),
            _vm._v(
              _vm._s(
                _vm.$L(
                  "日程管理：可视化日程管理，快速搞定工作计划，了解工作宏观安排。"
                )
              )
            )
          ])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "p-footer" }, [
        _c("span", {
          domProps: {
            innerHTML: _vm._s(
              _vm.systemConfig.footerText || "WookTeam &copy; 2018-2020"
            )
          }
        })
      ]),
      _vm._v(" "),
      _c(
        "Modal",
        {
          attrs: { "mask-closable": false, "class-name": "simple-modal" },
          model: {
            value: _vm.loginShow,
            callback: function($$v) {
              _vm.loginShow = $$v
            },
            expression: "loginShow"
          }
        },
        [
          _c(
            "Form",
            {
              ref: "login",
              attrs: { model: _vm.formLogin, rules: _vm.ruleLogin },
              nativeOn: {
                submit: function($event) {
                  $event.preventDefault()
                }
              }
            },
            [
              _c(
                "FormItem",
                { attrs: { prop: "username" } },
                [
                  _c(
                    "Input",
                    {
                      attrs: { type: "text", placeholder: _vm.$L("用户名") },
                      on: { "on-enter": _vm.onLogin, "on-blur": _vm.onBlur },
                      model: {
                        value: _vm.formLogin.username,
                        callback: function($$v) {
                          _vm.$set(_vm.formLogin, "username", $$v)
                        },
                        expression: "formLogin.username"
                      }
                    },
                    [
                      _c("Icon", {
                        attrs: { slot: "prepend", type: "ios-person-outline" },
                        slot: "prepend"
                      })
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "FormItem",
                { attrs: { prop: "userpass" } },
                [
                  _c(
                    "Input",
                    {
                      attrs: { type: "password", placeholder: _vm.$L("密码") },
                      on: { "on-enter": _vm.onLogin },
                      model: {
                        value: _vm.formLogin.userpass,
                        callback: function($$v) {
                          _vm.$set(_vm.formLogin, "userpass", $$v)
                        },
                        expression: "formLogin.userpass"
                      }
                    },
                    [
                      _c("Icon", {
                        attrs: { slot: "prepend", type: "ios-lock-outline" },
                        slot: "prepend"
                      })
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _vm.loginType == "reg"
                ? _c(
                    "FormItem",
                    { attrs: { prop: "userpass2" } },
                    [
                      _c(
                        "Input",
                        {
                          attrs: {
                            type: "password",
                            placeholder: _vm.$L("确认密码")
                          },
                          on: { "on-enter": _vm.onLogin },
                          model: {
                            value: _vm.formLogin.userpass2,
                            callback: function($$v) {
                              _vm.$set(_vm.formLogin, "userpass2", $$v)
                            },
                            expression: "formLogin.userpass2"
                          }
                        },
                        [
                          _c("Icon", {
                            attrs: {
                              slot: "prepend",
                              type: "ios-lock-outline"
                            },
                            slot: "prepend"
                          })
                        ],
                        1
                      )
                    ],
                    1
                  )
                : _vm._e(),
              _vm._v(" "),
              _vm.loginType == "login" && _vm.codeNeed
                ? _c(
                    "FormItem",
                    { attrs: { prop: "code" } },
                    [
                      _c(
                        "Input",
                        {
                          attrs: {
                            type: "text",
                            placeholder: _vm.$L("验证码")
                          },
                          on: { "on-enter": _vm.onLogin },
                          model: {
                            value: _vm.formLogin.code,
                            callback: function($$v) {
                              _vm.$set(_vm.formLogin, "code", $$v)
                            },
                            expression: "formLogin.code"
                          }
                        },
                        [
                          _c("Icon", {
                            attrs: {
                              slot: "prepend",
                              type: "ios-checkmark-circle-outline"
                            },
                            slot: "prepend"
                          }),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              staticClass: "login-code",
                              attrs: { slot: "append" },
                              on: { click: _vm.refreshCode },
                              slot: "append"
                            },
                            [_c("img", { attrs: { src: _vm.codeUrl } })]
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                : _vm._e()
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "div",
            {
              staticClass: "login-header",
              attrs: { slot: "header" },
              slot: "header"
            },
            [
              _c(
                "div",
                {
                  staticClass: "login-header-item",
                  class: { active: _vm.loginType == "login" },
                  on: {
                    click: function($event) {
                      _vm.loginType = "login"
                    }
                  }
                },
                [_vm._v(_vm._s(_vm.$L("用户登录")))]
              ),
              _vm._v(" "),
              _vm.systemConfig.reg == "open"
                ? _c(
                    "div",
                    {
                      staticClass: "login-header-item",
                      class: { active: _vm.loginType == "reg" },
                      on: {
                        click: function($event) {
                          _vm.loginType = "reg"
                        }
                      }
                    },
                    [_vm._v(_vm._s(_vm.$L("注册账号")))]
                  )
                : _vm._e()
            ]
          ),
          _vm._v(" "),
          _c(
            "div",
            { attrs: { slot: "footer" }, slot: "footer" },
            [
              _c(
                "Button",
                {
                  attrs: { type: "default" },
                  on: {
                    click: function($event) {
                      _vm.loginShow = false
                    }
                  }
                },
                [_vm._v(_vm._s(_vm.$L("取消")))]
              ),
              _vm._v(" "),
              _c(
                "Button",
                {
                  attrs: { type: "primary", loading: _vm.loadIng > 0 },
                  on: { click: _vm.onLogin }
                },
                [
                  _vm._v(
                    _vm._s(_vm.$L(_vm.loginType == "reg" ? "注册" : "登录"))
                  )
                ]
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "welbg" }, [
      _c("div", { staticClass: "second" }, [_c("div", { staticClass: "bg" })])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "z-18" }, [
      _c("img", {
        attrs: { src: __webpack_require__(454) }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "z-18" }, [
      _c("img", {
        attrs: { src: __webpack_require__(455) }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "z-18" }, [
      _c("img", {
        attrs: { src: __webpack_require__(456) }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "z-18" }, [
      _c("img", {
        attrs: { src: __webpack_require__(457) }
      })
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-effe19ba", module.exports)
  }
}

/***/ }),

/***/ 452:
/***/ (function(module, exports) {

module.exports = "/images/statics/images/index/banner/1.jpg?76c04ea4";

/***/ }),

/***/ 453:
/***/ (function(module, exports) {

module.exports = "/images/statics/images/index/banner/2.jpg?7acabd56";

/***/ }),

/***/ 454:
/***/ (function(module, exports) {

module.exports = "/images/statics/images/index/todo.jpg?a9249b17";

/***/ }),

/***/ 455:
/***/ (function(module, exports) {

module.exports = "/images/statics/images/index/project.jpg?badbfb0f";

/***/ }),

/***/ 456:
/***/ (function(module, exports) {

module.exports = "/images/statics/images/index/wiki.jpg?e9154757";

/***/ }),

/***/ 457:
/***/ (function(module, exports) {

module.exports = "/images/statics/images/index/week.jpg?bcbeca9f";

/***/ })

});