webpackJsonp([12],{

/***/ 322:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(355)
}
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(357)
/* template */
var __vue_template__ = __webpack_require__(358)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-455d3017"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/main/components/docs/NestedDraggable.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-455d3017", Component.options)
  } else {
    hotAPI.reload("data-v-455d3017", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 355:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(356);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(1)("02b85fe6", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../node_modules/css-loader/index.js!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-455d3017\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/sass-loader/lib/loader.js!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./NestedDraggable.vue", function() {
     var newContent = require("!!../../../../../../node_modules/css-loader/index.js!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-455d3017\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/sass-loader/lib/loader.js!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./NestedDraggable.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 356:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)(false);
// imports


// module
exports.push([module.i, "\n.docs-group[data-v-455d3017] {\n  cursor: move;\n}\n.docs-group.readonly[data-v-455d3017] {\n    cursor: default;\n}\n.docs-group.hidden-children .docs-group[data-v-455d3017] {\n    display: none;\n}\n.docs-group.hidden-children .item .together[data-v-455d3017] {\n    display: block;\n}\n.docs-group .docs-group[data-v-455d3017] {\n    padding-left: 14px;\n    background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABwAAAAcCAYAAAByDd+UAAABS2lUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4KPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS42LWMxNDAgNzkuMTYwNDUxLCAyMDE3LzA1LzA2LTAxOjA4OjIxICAgICAgICAiPgogPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4KICA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIi8+CiA8L3JkZjpSREY+CjwveDp4bXBtZXRhPgo8P3hwYWNrZXQgZW5kPSJyIj8+LUNEtwAAAEtJREFUSIntzzEVwAAMQkFSKfi3FKzQqQ5oJm5h5P3ZXQMYkrgwtk+OPo8kSzo7bGFcC+NaGNfCuBbGtTCuhXEtzB+SHAAGAEm/7wv2LKvDNoBjfgAAAABJRU5ErkJggg==) no-repeat -2px -9px;\n    margin-left: 18px;\n    border-left: 1px dotted #ddd;\n}\n.docs-group .item[data-v-455d3017] {\n    padding: 4px 0 0 4px;\n    background-color: #ffffff;\n    border: solid 1px #ffffff;\n    line-height: 24px;\n    position: relative;\n}\n.docs-group .item .together[data-v-455d3017] {\n      display: none;\n      cursor: pointer;\n      position: absolute;\n      font-size: 12px;\n      color: #ffb519;\n      top: 50%;\n      left: -2px;\n      margin-top: 1px;\n      -webkit-transform: translate(0, -50%);\n              transform: translate(0, -50%);\n      z-index: 1;\n}\n.docs-group .item .dashed[data-v-455d3017] {\n      position: absolute;\n      margin: 0;\n      padding: 0;\n      top: 16px;\n      right: 0;\n      left: 20px;\n      height: 2px;\n      border-width: 0 0 1px 0;\n      border-bottom: dashed 1px #eee;\n}\n.docs-group .item .header[data-v-455d3017] {\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-orient: horizontal;\n      -webkit-box-direction: normal;\n          -ms-flex-direction: row;\n              flex-direction: row;\n      -webkit-box-align: start;\n          -ms-flex-align: start;\n              align-items: flex-start;\n      position: relative;\n      background: #fff;\n      padding: 0 8px;\n      cursor: pointer;\n}\n.docs-group .item .header .tip[data-v-455d3017] {\n        display: inline-block;\n        position: relative;\n}\n.docs-group .item .header .tip > img[data-v-455d3017] {\n          display: inline-block;\n          width: 14px;\n          height: 14px;\n          margin-top: 5px;\n          vertical-align: top;\n}\n.docs-group .item .header .title[data-v-455d3017] {\n        display: inline-block;\n        border-bottom: 1px solid transparent;\n        cursor: pointer;\n        padding: 0 3px 0 7px;\n        color: #555555;\n        word-break: break-all;\n}\n.docs-group .item .header .title.active[data-v-455d3017] {\n          color: #0396f2;\n}\n.docs-group .item .info[data-v-455d3017] {\n      position: absolute;\n      background: #fff;\n      padding-left: 12px;\n      color: #666;\n      right: 3px;\n      top: 5px;\n}\n.docs-group .item .info > i[data-v-455d3017] {\n        padding: 0 2px;\n        -webkit-transition: all 0.2s;\n        transition: all 0.2s;\n        cursor: pointer;\n}\n.docs-group .item .info > i[data-v-455d3017]:hover {\n          -webkit-transform: scale(1.2);\n                  transform: scale(1.2);\n}\n", ""]);

// exports


/***/ }),

/***/ 357:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vuedraggable__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vuedraggable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vuedraggable__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
    name: "NestedDraggable",
    props: {
        lists: {
            required: true,
            type: Array
        },
        isChildren: {
            type: Boolean,
            default: false
        },
        disabled: {
            type: Boolean,
            default: false
        },
        readonly: {
            type: Boolean,
            default: false
        },
        activeid: {
            default: ''
        }
    },
    data: function data() {
        return {
            listSortData: '',
            childrenHidden: false
        };
    },

    components: {
        draggable: __WEBPACK_IMPORTED_MODULE_0_vuedraggable___default.a
    },
    mounted: function mounted() {
        this.listSortData = this.getSort(this.lists);
    },

    methods: {
        getSort: function getSort(lists) {
            var _this = this;

            var parentid = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;

            var sortData = "";
            lists.forEach(function (item) {
                sortData += item.id + ":" + parentid + ";" + _this.getSort(item.children, item.id);
            });
            return sortData;
        },
        handleClick: function handleClick(act, detail) {
            if (act == 'open') {
                if (detail.type == 'folder') {
                    this.$set(detail, 'hiddenChildren', !detail.hiddenChildren);
                    return;
                }
            }
            if (act == 'sort') {
                if (this.isChildren) {
                    this.$emit("change", act, detail);
                } else {
                    var tempSortData = this.getSort(this.lists);
                    if (tempSortData != this.listSortData) {
                        this.listSortData = tempSortData;
                        this.$emit("change", act, tempSortData);
                    }
                }
                return;
            }
            this.$emit("change", act, detail);
        }
    }
});

/***/ }),

/***/ 358:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "draggable",
    {
      attrs: {
        tag: "div",
        list: _vm.lists,
        group: { name: "docs-nested" },
        animation: 150,
        disabled: _vm.disabled || _vm.readonly
      },
      on: {
        sort: function($event) {
          return _vm.handleClick("sort")
        }
      }
    },
    _vm._l(_vm.lists, function(detail) {
      return _c(
        "div",
        {
          key: detail.id,
          staticClass: "docs-group",
          class: {
            readonly: _vm.readonly,
            "hidden-children": detail.hiddenChildren === true
          }
        },
        [
          _c(
            "div",
            { staticClass: "item" },
            [
              _c("Icon", {
                staticClass: "together",
                attrs: { type: "md-add" },
                on: {
                  click: function($event) {
                    return _vm.handleClick("open", detail)
                  }
                }
              }),
              _vm._v(" "),
              _c("div", { staticClass: "dashed" }),
              _vm._v(" "),
              _c("div", { staticClass: "header" }, [
                _c("div", { staticClass: "tip" }, [
                  _c("img", { attrs: { src: detail.icon } })
                ]),
                _vm._v(" "),
                _c(
                  "div",
                  {
                    staticClass: "title",
                    class: { active: _vm.activeid == detail.id },
                    on: {
                      click: function($event) {
                        return _vm.handleClick("open", detail)
                      }
                    }
                  },
                  [_vm._v(_vm._s(detail.title))]
                )
              ]),
              _vm._v(" "),
              !_vm.readonly
                ? _c(
                    "div",
                    { staticClass: "info" },
                    [
                      _c("Icon", {
                        attrs: { type: "md-create" },
                        on: {
                          click: function($event) {
                            return _vm.handleClick("edit", detail)
                          }
                        }
                      }),
                      _vm._v(" "),
                      _c("Icon", {
                        attrs: { type: "md-add" },
                        on: {
                          click: function($event) {
                            return _vm.handleClick("add", detail)
                          }
                        }
                      }),
                      _vm._v(" "),
                      _c("Icon", {
                        attrs: { type: "md-trash" },
                        on: {
                          click: function($event) {
                            return _vm.handleClick("delete", detail)
                          }
                        }
                      })
                    ],
                    1
                  )
                : _vm._e()
            ],
            1
          ),
          _vm._v(" "),
          typeof detail.children === "object" && detail.children !== null
            ? _c("nested-draggable", {
                attrs: {
                  lists: detail.children,
                  isChildren: true,
                  disabled: _vm.disabled,
                  readonly: _vm.readonly,
                  activeid: _vm.activeid
                },
                on: { change: _vm.handleClick }
              })
            : _vm._e()
        ],
        1
      )
    }),
    0
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-455d3017", module.exports)
  }
}

/***/ })

});