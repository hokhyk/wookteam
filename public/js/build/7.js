webpackJsonp([7],{

/***/ 294:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(741)
  __webpack_require__(743)
}
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(745)
/* template */
var __vue_template__ = __webpack_require__(746)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-689d329c"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/main/pages/team.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-689d329c", Component.options)
  } else {
    hotAPI.reload("data-v-689d329c", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 305:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(306)
}
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(308)
/* template */
var __vue_template__ = __webpack_require__(309)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-35be3d57"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/main/components/WContent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-35be3d57", Component.options)
  } else {
    hotAPI.reload("data-v-35be3d57", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 306:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(307);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(1)("5ee96958", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-35be3d57\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./WContent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-35be3d57\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./WContent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 307:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)(false);
// imports


// module
exports.push([module.i, "\n.w-content[data-v-35be3d57] {\n  position: absolute;\n  top: 72px;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  overflow: auto;\n  background-repeat: no-repeat;\n  background-position: center;\n  background-color: #EEEEEE;\n  background-size: cover;\n}\n.w-content .w-container[data-v-35be3d57] {\n    min-height: 500px;\n}\n", ""]);

// exports


/***/ }),

/***/ 308:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: 'WContent',
    data: function data() {
        return {
            bgid: -1
        };
    },
    mounted: function mounted() {
        this.bgid = $A.runNum(this.usrInfo.bgid);
    },

    watch: {
        usrInfo: {
            handler: function handler(info) {
                this.bgid = $A.runNum(info.bgid);
            },

            deep: true
        }
    },
    methods: {
        getBgUrl: function getBgUrl(id, thumb) {
            if (id < 0) {
                return 'none';
            }
            id = Math.max(1, parseInt(id));
            return 'url(' + window.location.origin + '/images/bg/' + (thumb ? 'thumb/' : '') + id + '.jpg' + ')';
        }
    }
});

/***/ }),

/***/ 309:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {
      staticClass: "w-content",
      style: "background-image:" + _vm.getBgUrl(_vm.bgid)
    },
    [_vm._t("default")],
    2
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-35be3d57", module.exports)
  }
}

/***/ }),

/***/ 333:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(334)
}
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(336)
/* template */
var __vue_template__ = __webpack_require__(337)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/main/components/TagInput.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-63d616a5", Component.options)
  } else {
    hotAPI.reload("data-v-63d616a5", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 334:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(335);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(1)("72d872d6", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-63d616a5\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./TagInput.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-63d616a5\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./TagInput.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 335:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)(false);
// imports


// module
exports.push([module.i, "\n.tags-wrap {\n  display: inline-block;\n  width: 100%;\n  min-height: 32px;\n  padding: 2px 7px;\n  border: 1px solid #dddee1;\n  border-radius: 4px;\n  color: #495060;\n  background: #fff;\n  position: relative;\n  cursor: text;\n  vertical-align: middle;\n  line-height: normal;\n  -webkit-transition: border .2s ease-in-out, background .2s ease-in-out, -webkit-box-shadow .2s ease-in-out;\n  transition: border .2s ease-in-out, background .2s ease-in-out, -webkit-box-shadow .2s ease-in-out;\n}\n.tags-wrap .tags-item, .tags-wrap .tags-input {\n    position: relative;\n    float: left;\n    color: #495060;\n    background-color: #f1f8ff;\n    border-radius: 3px;\n    line-height: 22px;\n    margin: 2px 6px 2px 0;\n    padding: 0 20px 0 6px;\n}\n.tags-wrap .tags-item .tags-content, .tags-wrap .tags-input .tags-content {\n      line-height: 22px;\n}\n.tags-wrap .tags-item .tags-del, .tags-wrap .tags-input .tags-del {\n      width: 20px;\n      height: 22px;\n      text-align: center;\n      cursor: pointer;\n      position: absolute;\n      top: -1px;\n      right: 0;\n}\n.tags-wrap .tags-input {\n    max-width: 80%;\n    padding: 0;\n    background-color: inherit;\n    border: none;\n    color: inherit;\n    height: 22px;\n    line-height: 22px;\n    -webkit-appearance: none;\n    outline: none;\n    resize: none;\n    overflow: hidden;\n}\n.tags-wrap .tags-input::-webkit-input-placeholder {\n    color: #bbbbbb;\n}\n.tags-wrap .tags-input::-moz-placeholder {\n    color: #bbbbbb;\n}\n.tags-wrap .tags-input::-ms-input-placeholder {\n    color: #bbbbbb;\n}\n.tags-wrap .tags-input::placeholder {\n    color: #bbbbbb;\n}\n.tags-wrap .tags-placeholder {\n    position: absolute;\n    left: 0;\n    top: 0;\n    z-index: -1;\n    color: #ffffff00;\n}\n.tags-wrap::after {\n  content: \"\";\n  display: block;\n  height: 0;\n  clear: both;\n}\n", ""]);

// exports


/***/ }),

/***/ 336:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: 'TagInput',
    props: {
        value: {
            default: ''
        },
        cut: {
            default: ','
        },
        disabled: {
            type: Boolean,
            default: false
        },
        readonly: {
            type: Boolean,
            default: false
        },
        placeholder: {
            default: ''
        },
        max: {
            default: 0
        }
    },
    data: function data() {
        var disSource = [];
        this.value.split(",").forEach(function (item) {
            if (item) {
                disSource.push(item);
            }
        });
        return {
            minWidth: 80,

            tis: '',
            tisTimeout: null,

            showPlaceholder: true,

            content: '',

            disSource: disSource
        };
    },
    mounted: function mounted() {
        this.wayMinWidth();
    },

    watch: {
        placeholder: function placeholder() {
            this.wayMinWidth();
        },
        value: function value(val) {
            var disSource = [];
            if ($A.count(val) > 0) {
                val.split(",").forEach(function (item) {
                    if (item) {
                        disSource.push(item);
                    }
                });
            }
            this.disSource = disSource;
        },
        disSource: function disSource(val) {
            var _this = this;

            var temp = '';
            val.forEach(function (item) {
                if (temp != '') {
                    temp += _this.cut;
                }
                temp += item;
            });
            this.$emit('input', temp);
        }
    },
    methods: {
        wayMinWidth: function wayMinWidth() {
            var _this2 = this;

            this.showPlaceholder = true;
            this.$nextTick(function () {
                if (_this2.$refs.myPlaceholder) {
                    _this2.minWidth = Math.max(_this2.minWidth, _this2.$refs.myPlaceholder.offsetWidth);
                }
                setTimeout(function () {
                    try {
                        _this2.minWidth = Math.max(_this2.minWidth, _this2.$refs.myPlaceholder.offsetWidth);
                        _this2.showPlaceholder = false;
                    } catch (e) {}
                    if (!$A(_this2.$refs.myPlaceholder).is(":visible")) {
                        _this2.wayMinWidth();
                    }
                }, 500);
            });
        },
        pasteText: function pasteText(e) {
            e.preventDefault();
            var content = (e.clipboardData || window.clipboardData).getData('text');
            this.addTag(false, content);
        },
        clickWrap: function clickWrap() {
            this.$refs.myTextarea.focus();
        },
        downEnter: function downEnter(e) {
            e.preventDefault();
        },
        addTag: function addTag(e, content) {
            var _this3 = this;

            if (e.keyCode === 13 || e === false) {
                if (content.trim() != '' && this.disSource.indexOf(content.trim()) === -1) {
                    this.disSource.push(content.trim());
                }
                this.content = '';
            } else {
                if (this.max > 0 && this.disSource.length >= this.max) {
                    this.content = '';
                    this.tis = '最多只能添加' + this.max + '个';
                    clearInterval(this.tisTimeout);
                    this.tisTimeout = setTimeout(function () {
                        _this3.tis = '';
                    }, 2000);
                    return;
                }
                var temp = content.trim();
                var cutPos = temp.length - this.cut.length;
                if (temp != '' && temp.substring(cutPos) === this.cut) {
                    temp = temp.substring(0, cutPos);
                    if (temp.trim() != '' && this.disSource.indexOf(temp.trim()) === -1) {
                        this.disSource.push(temp.trim());
                    }
                    this.content = '';
                }
            }
        },
        delTag: function delTag(index) {
            if (index === false) {
                if (this.content !== '') {
                    return;
                }
                index = this.disSource.length - 1;
            }
            this.disSource.splice(index, 1);
        }
    }
});

/***/ }),

/***/ 337:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {
      staticClass: "tags-wrap",
      on: {
        paste: function($event) {
          return _vm.pasteText($event)
        },
        click: _vm.clickWrap
      }
    },
    [
      _vm._l(_vm.disSource, function(text, index) {
        return _c("div", { staticClass: "tags-item" }, [
          _c(
            "span",
            {
              staticClass: "tags-content",
              on: {
                click: function($event) {
                  $event.stopPropagation()
                }
              }
            },
            [_vm._v(_vm._s(text))]
          ),
          _c(
            "span",
            {
              staticClass: "tags-del",
              on: {
                click: function($event) {
                  $event.stopPropagation()
                  return _vm.delTag(index)
                }
              }
            },
            [_vm._v("×")]
          )
        ])
      }),
      _vm._v(" "),
      _c("textarea", {
        directives: [
          {
            name: "model",
            rawName: "v-model",
            value: _vm.content,
            expression: "content"
          }
        ],
        ref: "myTextarea",
        staticClass: "tags-input",
        style: { minWidth: _vm.minWidth + "px" },
        attrs: {
          placeholder: _vm.tis || _vm.placeholder,
          disabled: _vm.disabled,
          readonly: _vm.readonly
        },
        domProps: { value: _vm.content },
        on: {
          keydown: [
            function($event) {
              if (
                !$event.type.indexOf("key") &&
                _vm._k($event.keyCode, "enter", 13, $event.key, "Enter")
              ) {
                return null
              }
              return _vm.downEnter($event)
            },
            function($event) {
              if (
                !$event.type.indexOf("key") &&
                _vm._k($event.keyCode, "delete", [8, 46], $event.key, [
                  "Backspace",
                  "Delete",
                  "Del"
                ])
              ) {
                return null
              }
              return _vm.delTag(false)
            }
          ],
          keyup: function($event) {
            return _vm.addTag($event, _vm.content)
          },
          blur: function($event) {
            return _vm.addTag(false, _vm.content)
          },
          input: function($event) {
            if ($event.target.composing) {
              return
            }
            _vm.content = $event.target.value
          }
        }
      }),
      _vm._v(" "),
      _vm.showPlaceholder || _vm.tis !== ""
        ? _c(
            "span",
            { ref: "myPlaceholder", staticClass: "tags-placeholder" },
            [_vm._v(_vm._s(_vm.tis || _vm.placeholder))]
          )
        : _vm._e()
    ],
    2
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-63d616a5", module.exports)
  }
}

/***/ }),

/***/ 741:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(742);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(1)("02c58cc5", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-689d329c\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./team.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-689d329c\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./team.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 742:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)(false);
// imports


// module
exports.push([module.i, "\n.team-add-red-input {\n  display: inline-block;\n  margin-right: 4px;\n  line-height: 1;\n  font-family: SimSun,serif;\n  font-size: 14px;\n  color: #ed4014;\n}\n.team-add-form-item-changepass {\n  margin-top: -18px;\n  margin-bottom: 18px;\n  height: 0;\n  overflow: hidden;\n  -webkit-transition: all 0.2s;\n  transition: all 0.2s;\n}\n.team-add-form-item-changepass.show {\n    height: 32px;\n}\n", ""]);

// exports


/***/ }),

/***/ 743:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(744);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(1)("895aec1e", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-689d329c\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=1!./team.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-689d329c\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=1!./team.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 744:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)(false);
// imports


// module
exports.push([module.i, "\n.team .team-main[data-v-689d329c] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  width: 100%;\n  height: 100%;\n  padding: 15px;\n}\n.team .team-main .team-body[data-v-689d329c] {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-orient: vertical;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: column;\n            flex-direction: column;\n    width: 100%;\n    height: 100%;\n    min-height: 500px;\n    background: #fefefe;\n    border-radius: 3px;\n}\n.team .team-main .team-body .tableFill[data-v-689d329c] {\n      margin: 20px;\n      background-color: #ffffff;\n}\n", ""]);

// exports


/***/ }),

/***/ 745:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_WContent__ = __webpack_require__(305);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_WContent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__components_WContent__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_ImgUpload__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_ImgUpload___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__components_ImgUpload__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_TagInput__ = __webpack_require__(333);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_TagInput___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__components_TagInput__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
    components: { TagInput: __WEBPACK_IMPORTED_MODULE_2__components_TagInput___default.a, ImgUpload: __WEBPACK_IMPORTED_MODULE_1__components_ImgUpload___default.a, WContent: __WEBPACK_IMPORTED_MODULE_0__components_WContent___default.a },
    data: function data() {
        return {
            loadIng: 0,

            isAdmin: false,

            columns: [],

            lists: [],
            listPage: 1,
            listTotal: 0,
            noDataText: "",

            addShow: false,
            formData: {
                id: 0,
                userimg: '',
                profession: '',
                username: '',
                nickname: '',
                userpass: ''
            }
        };
    },
    mounted: function mounted() {
        this.getLists(true);
    },
    deactivated: function deactivated() {
        this.addShow = false;
    },

    watch: {
        usrName: function usrName() {
            this.usrLogin && this.getLists(true);
        }
    },
    methods: {
        initLanguage: function initLanguage() {
            var _this = this;

            this.isAdmin = $A.identity('admin');
            this.noDataText = this.$L("数据加载中.....");
            this.columns = [{
                "title": this.$L("头像"),
                "minWidth": 60,
                "maxWidth": 100,
                render: function render(h, params) {
                    return h('UserImg', {
                        props: {
                            info: params.row
                        },
                        style: {
                            width: "30px",
                            height: "30px",
                            fontSize: "16px",
                            lineHeight: "30px",
                            borderRadius: "15px",
                            verticalAlign: "middle"
                        }
                    });
                }
            }, {
                "title": this.$L("用户名"),
                "key": 'username',
                "minWidth": 80,
                "ellipsis": true,
                render: function render(h, params) {
                    var arr = [];
                    if (params.row.username == _this.usrName) {
                        arr.push(h('span', {
                            style: {
                                color: '#ff0000',
                                paddingRight: '4px'
                            }
                        }, '[自己]'));
                    }
                    if ($A.identityRaw('admin', params.row.identity)) {
                        arr.push(h('span', {
                            style: {
                                color: '#ff0000',
                                paddingRight: '4px'
                            }
                        }, '[管理员]'));
                    }
                    arr.push(h('span', params.row.username));
                    return h('span', arr);
                }
            }, {
                "title": this.$L("昵称"),
                "minWidth": 80,
                "ellipsis": true,
                render: function render(h, params) {
                    return h('span', params.row.nickname || '-');
                }
            }, {
                "title": this.$L("职位/职称"),
                "minWidth": 100,
                "ellipsis": true,
                render: function render(h, params) {
                    return h('span', params.row.profession || '-');
                }
            }, {
                "title": this.$L("加入时间"),
                "width": 160,
                render: function render(h, params) {
                    return h('span', $A.formatDate("Y-m-d H:i:s", params.row.regdate));
                }
            }, {
                "title": this.$L("操作"),
                "key": 'action',
                "width": this.isAdmin ? 160 : 80,
                "align": 'center',
                render: function render(h, params) {
                    var array = [];
                    array.push(h('Button', {
                        props: {
                            type: 'primary',
                            size: 'small'
                        },
                        style: {
                            fontSize: '12px'
                        },
                        on: {
                            click: function click() {
                                _this.$Modal.info({
                                    title: _this.$L('会员信息'),
                                    content: "<p>" + _this.$L('昵称') + ": " + (params.row.nickname || '-') + "</p><p>" + _this.$L('职位/职称') + ": " + (params.row.profession || '-') + "</p>"
                                });
                            }
                        }
                    }, _this.$L('查看')));
                    if (_this.isAdmin) {
                        array.push(h('Dropdown', {
                            props: {
                                trigger: 'click',
                                transfer: true
                            },
                            on: {
                                'on-click': function onClick(name) {
                                    _this.handleUser(name, params.row);
                                }
                            }
                        }, [h('Button', {
                            props: {
                                type: 'warning',
                                size: 'small'
                            },
                            style: {
                                fontSize: '12px',
                                marginLeft: '5px'
                            }
                        }, _this.$L('操作')), h('DropdownMenu', {
                            slot: 'list'
                        }, [h('DropdownItem', {
                            props: {
                                name: 'edit'
                            }
                        }, _this.$L('修改成员信息')), h('DropdownItem', {
                            props: {
                                name: $A.identityRaw('admin', params.row.identity) ? 'deladmin' : 'setadmin'
                            }
                        }, _this.$L($A.identityRaw('admin', params.row.identity) ? '取消管理员' : '设为管理员')), h('DropdownItem', {
                            props: {
                                name: 'delete'
                            }
                        }, _this.$L('删除'))])]));
                    }
                    return h('div', array);
                }
            }];
        },
        setPage: function setPage(page) {
            this.listPage = page;
            this.getLists();
        },
        setPageSize: function setPageSize(size) {
            if (Math.max($A.runNum(this.listPageSize), 10) != size) {
                this.listPageSize = size;
                this.getLists();
            }
        },
        getLists: function getLists(resetLoad) {
            var _this2 = this;

            if (resetLoad === true) {
                this.listPage = 1;
            }
            this.loadIng++;
            this.noDataText = this.$L("数据加载中.....");
            $A.apiAjax({
                url: 'users/team/lists',
                data: {
                    page: Math.max(this.listPage, 1),
                    pagesize: Math.max($A.runNum(this.listPageSize), 10)
                },
                complete: function complete() {
                    _this2.loadIng--;
                },
                error: function error() {
                    _this2.noDataText = _this2.$L("数据加载失败！");
                },
                success: function success(res) {
                    if (res.ret === 1) {
                        _this2.lists = res.data.lists;
                        _this2.listTotal = res.data.total;
                        _this2.noDataText = _this2.$L("没有相关的数据");
                    } else {
                        _this2.lists = [];
                        _this2.listTotal = 0;
                        _this2.noDataText = res.msg;
                    }
                }
            });
        },
        onAdd: function onAdd() {
            var _this3 = this;

            this.loadIng++;
            var id = $A.runNum(this.formData.id);
            $A.apiAjax({
                url: 'users/team/add',
                data: this.formData,
                complete: function complete() {
                    _this3.loadIng--;
                },
                success: function success(res) {
                    if (res.ret === 1) {
                        _this3.addShow = false;
                        _this3.$Message.success(res.msg);
                        _this3.$refs.add.resetFields();
                        //
                        _this3.getLists(id == 0);
                    } else {
                        _this3.$Modal.error({ title: _this3.$L('温馨提示'), content: res.msg });
                    }
                }
            });
        },
        handleUser: function handleUser(act, info) {
            var _this4 = this;

            switch (act) {
                case "add":
                    {
                        this.addShow = true;
                        this.formData = {
                            id: 0,
                            userimg: '',
                            profession: '',
                            username: '',
                            nickname: '',
                            userpass: '',
                            changepass: 1
                        };
                        break;
                    }
                case "edit":
                    {
                        this.addShow = true;
                        this.formData = Object.assign($A.cloneData(info));
                        break;
                    }
                case "delete":
                    {
                        this.$Modal.confirm({
                            title: this.$L('删除团队成员'),
                            content: this.$L('你确定要删除此团队成员吗？'),
                            loading: true,
                            onOk: function onOk() {
                                $A.apiAjax({
                                    url: 'users/team/delete?username=' + info.username,
                                    error: function error() {
                                        _this4.$Modal.remove();
                                        alert(_this4.$L('网络繁忙，请稍后再试！'));
                                    },
                                    success: function success(res) {
                                        _this4.$Modal.remove();
                                        _this4.getLists();
                                        setTimeout(function () {
                                            if (res.ret === 1) {
                                                _this4.$Message.success(res.msg);
                                            } else {
                                                _this4.$Modal.error({ title: _this4.$L('温馨提示'), content: res.msg });
                                            }
                                        }, 350);
                                    }
                                });
                            }
                        });
                        break;
                    }
                case "setadmin":
                case "deladmin":
                    {
                        this.$Modal.confirm({
                            title: this.$L('确定操作'),
                            content: this.$L(act == 'deladmin' ? '你确定取消管理员身份的操作吗？' : '你确定设置管理员的操作吗？'),
                            loading: true,
                            onOk: function onOk() {
                                $A.apiAjax({
                                    url: 'users/team/admin?act=' + (act == 'deladmin' ? 'del' : 'set') + '&username=' + info.username,
                                    error: function error() {
                                        _this4.$Modal.remove();
                                        alert(_this4.$L('网络繁忙，请稍后再试！'));
                                    },
                                    success: function success(res) {
                                        _this4.$Modal.remove();
                                        if (res.ret === 1) {
                                            _this4.lists.some(function (item) {
                                                if (item.username == info.username) {
                                                    _this4.$set(item, 'identity', res.data.identity);
                                                    return true;
                                                }
                                            });
                                            if (res.data.up === 1) {
                                                var data = {
                                                    type: 'text',
                                                    username: _this4.usrInfo.username,
                                                    userimg: _this4.usrInfo.userimg,
                                                    indate: Math.round(new Date().getTime() / 1000),
                                                    text: _this4.$L(act == 'deladmin' ? '您的管理员身份已被撤销。' : '恭喜您成为管理员。')
                                                };
                                                $A.WSOB.sendTo('user', info.username, data, 'special');
                                                $A.WSOB.sendTo('info', info.username, { 'type': 'update' });
                                            }
                                        }
                                        setTimeout(function () {
                                            if (res.ret === 1) {
                                                _this4.$Message.success(res.msg);
                                            } else {
                                                _this4.$Modal.error({ title: _this4.$L('温馨提示'), content: res.msg });
                                            }
                                        }, 350);
                                    }
                                });
                            }
                        });
                        break;
                    }
            }
        }
    }
});

/***/ }),

/***/ 746:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "w-main team" },
    [
      _c("v-title", [
        _vm._v(
          _vm._s(_vm.$L("团队")) + "-" + _vm._s(_vm.$L("轻量级的团队在线协作"))
        )
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "w-nav" }, [
        _c("div", { staticClass: "nav-row" }, [
          _c("div", { staticClass: "w-nav-left" }, [
            _c("div", { staticClass: "page-nav-left" }, [
              _c("span", [
                _c("i", { staticClass: "ft icon" }, [_vm._v("")]),
                _vm._v(" " + _vm._s(_vm.$L("团队成员")))
              ]),
              _vm._v(" "),
              _vm.loadIng > 0
                ? _c(
                    "div",
                    { staticClass: "page-nav-loading" },
                    [_c("w-loading")],
                    1
                  )
                : _c("div", { staticClass: "page-nav-refresh" }, [
                    _c(
                      "em",
                      {
                        on: {
                          click: function($event) {
                            return _vm.getLists(true)
                          }
                        }
                      },
                      [_vm._v(_vm._s(_vm.$L("刷新")))]
                    )
                  ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "w-nav-flex" }),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "m768-show w-nav-right" },
            [
              _c(
                "Dropdown",
                {
                  attrs: { trigger: "click", transfer: "" },
                  on: { "on-click": _vm.handleUser }
                },
                [
                  _c("Icon", { attrs: { type: "md-menu", size: "18" } }),
                  _vm._v(" "),
                  _c(
                    "DropdownMenu",
                    { attrs: { slot: "list" }, slot: "list" },
                    [
                      _c("DropdownItem", { attrs: { name: "add" } }, [
                        _vm._v(_vm._s(_vm.$L("添加团队成员")))
                      ])
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c("div", { staticClass: "m768-hide w-nav-right" }, [
            _c(
              "span",
              {
                staticClass: "ft hover",
                on: {
                  click: function($event) {
                    return _vm.handleUser("add")
                  }
                }
              },
              [
                _c("i", { staticClass: "ft icon" }, [_vm._v("")]),
                _vm._v(" " + _vm._s(_vm.$L("添加团队成员")))
              ]
            )
          ])
        ])
      ]),
      _vm._v(" "),
      _c("w-content", [
        _c("div", { staticClass: "team-main" }, [
          _c(
            "div",
            { staticClass: "team-body" },
            [
              _c("Table", {
                ref: "tableRef",
                staticClass: "tableFill",
                attrs: {
                  columns: _vm.columns,
                  data: _vm.lists,
                  loading: _vm.loadIng > 0,
                  "no-data-text": _vm.noDataText,
                  stripe: ""
                }
              }),
              _vm._v(" "),
              _c("Page", {
                staticClass: "pageBox",
                attrs: {
                  total: _vm.listTotal,
                  current: _vm.listPage,
                  disabled: _vm.loadIng > 0,
                  "page-size-opts": [10, 20, 30, 50, 100],
                  placement: "top",
                  "show-elevator": "",
                  "show-sizer": "",
                  "show-total": "",
                  simple: _vm.windowMax768
                },
                on: {
                  "on-change": _vm.setPage,
                  "on-page-size-change": _vm.setPageSize
                }
              })
            ],
            1
          )
        ])
      ]),
      _vm._v(" "),
      _c(
        "Modal",
        {
          attrs: {
            title: _vm.$L(
              _vm.formData.id > 0 ? "修改团队成员" : "添加团队成员"
            ),
            closable: false,
            "mask-closable": false,
            "class-name": "simple-modal"
          },
          model: {
            value: _vm.addShow,
            callback: function($$v) {
              _vm.addShow = $$v
            },
            expression: "addShow"
          }
        },
        [
          _c(
            "Form",
            {
              ref: "add",
              attrs: { model: _vm.formData, "label-width": 80 },
              nativeOn: {
                submit: function($event) {
                  $event.preventDefault()
                }
              }
            },
            [
              _c(
                "FormItem",
                { attrs: { prop: "userimg", label: _vm.$L("头像") } },
                [
                  _c("ImgUpload", {
                    model: {
                      value: _vm.formData.userimg,
                      callback: function($$v) {
                        _vm.$set(_vm.formData, "userimg", $$v)
                      },
                      expression: "formData.userimg"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "FormItem",
                { attrs: { prop: "nickname", label: _vm.$L("昵称") } },
                [
                  _c("Input", {
                    attrs: { type: "text" },
                    model: {
                      value: _vm.formData.nickname,
                      callback: function($$v) {
                        _vm.$set(_vm.formData, "nickname", $$v)
                      },
                      expression: "formData.nickname"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "FormItem",
                { attrs: { prop: "profession", label: _vm.$L("职位/职称") } },
                [
                  _c("Input", {
                    model: {
                      value: _vm.formData.profession,
                      callback: function($$v) {
                        _vm.$set(_vm.formData, "profession", $$v)
                      },
                      expression: "formData.profession"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "FormItem",
                { attrs: { prop: "username" } },
                [
                  _c("div", { attrs: { slot: "label" }, slot: "label" }, [
                    _vm.formData.id == 0
                      ? _c("em", { staticClass: "team-add-red-input" }, [
                          _vm._v("*")
                        ])
                      : _vm._e(),
                    _vm._v(_vm._s(_vm.$L("用户名")))
                  ]),
                  _vm._v(" "),
                  _vm.formData.id > 0
                    ? _c("Input", {
                        attrs: { disabled: "" },
                        model: {
                          value: _vm.formData.username,
                          callback: function($$v) {
                            _vm.$set(_vm.formData, "username", $$v)
                          },
                          expression: "formData.username"
                        }
                      })
                    : _c("TagInput", {
                        attrs: {
                          placeholder: _vm.$L(
                            "添加后不可修改，使用英文逗号添加多个。"
                          )
                        },
                        model: {
                          value: _vm.formData.username,
                          callback: function($$v) {
                            _vm.$set(_vm.formData, "username", $$v)
                          },
                          expression: "formData.username"
                        }
                      })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "FormItem",
                { attrs: { prop: "userpass" } },
                [
                  _c("div", { attrs: { slot: "label" }, slot: "label" }, [
                    _vm.formData.id == 0
                      ? _c("em", { staticClass: "team-add-red-input" }, [
                          _vm._v("*")
                        ])
                      : _vm._e(),
                    _vm._v(_vm._s(_vm.$L("登录密码")))
                  ]),
                  _vm._v(" "),
                  _c("Input", {
                    attrs: {
                      type: "password",
                      placeholder: _vm.$L(
                        _vm.formData.id > 0 ? "留空不修改" : "最少6位数"
                      )
                    },
                    model: {
                      value: _vm.formData.userpass,
                      callback: function($$v) {
                        _vm.$set(_vm.formData, "userpass", $$v)
                      },
                      expression: "formData.userpass"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _vm.formData.id == 0
                ? _c(
                    "FormItem",
                    {
                      staticClass: "team-add-form-item-changepass",
                      class: { show: _vm.formData.userpass },
                      attrs: { prop: "changepass" }
                    },
                    [
                      _c(
                        "Checkbox",
                        {
                          attrs: { "true-value": 1, "false-value": 0 },
                          model: {
                            value: _vm.formData.changepass,
                            callback: function($$v) {
                              _vm.$set(_vm.formData, "changepass", $$v)
                            },
                            expression: "formData.changepass"
                          }
                        },
                        [_vm._v(_vm._s(_vm.$L("首次登陆需修改密码")))]
                      )
                    ],
                    1
                  )
                : _vm._e()
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "div",
            { attrs: { slot: "footer" }, slot: "footer" },
            [
              _c(
                "Button",
                {
                  attrs: { type: "default" },
                  on: {
                    click: function($event) {
                      _vm.addShow = false
                    }
                  }
                },
                [_vm._v(_vm._s(_vm.$L("取消")))]
              ),
              _vm._v(" "),
              _c(
                "Button",
                {
                  attrs: { type: "primary", loading: _vm.loadIng > 0 },
                  on: { click: _vm.onAdd }
                },
                [_vm._v(_vm._s(_vm.$L(_vm.formData.id > 0 ? "修改" : "添加")))]
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-689d329c", module.exports)
  }
}

/***/ })

});