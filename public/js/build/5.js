webpackJsonp([5,12],{

/***/ 291:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(602)
}
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(604)
/* template */
var __vue_template__ = __webpack_require__(615)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-0172190c"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/main/pages/docs.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-0172190c", Component.options)
  } else {
    hotAPI.reload("data-v-0172190c", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 305:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(306)
}
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(308)
/* template */
var __vue_template__ = __webpack_require__(309)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-35be3d57"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/main/components/WContent.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-35be3d57", Component.options)
  } else {
    hotAPI.reload("data-v-35be3d57", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 306:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(307);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(1)("5ee96958", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-35be3d57\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./WContent.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-35be3d57\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./WContent.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 307:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)(false);
// imports


// module
exports.push([module.i, "\n.w-content[data-v-35be3d57] {\n  position: absolute;\n  top: 72px;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  overflow: auto;\n  background-repeat: no-repeat;\n  background-position: center;\n  background-color: #EEEEEE;\n  background-size: cover;\n}\n.w-content .w-container[data-v-35be3d57] {\n    min-height: 500px;\n}\n", ""]);

// exports


/***/ }),

/***/ 308:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    name: 'WContent',
    data: function data() {
        return {
            bgid: -1
        };
    },
    mounted: function mounted() {
        this.bgid = $A.runNum(this.usrInfo.bgid);
    },

    watch: {
        usrInfo: {
            handler: function handler(info) {
                this.bgid = $A.runNum(info.bgid);
            },

            deep: true
        }
    },
    methods: {
        getBgUrl: function getBgUrl(id, thumb) {
            if (id < 0) {
                return 'none';
            }
            id = Math.max(1, parseInt(id));
            return 'url(' + window.location.origin + '/images/bg/' + (thumb ? 'thumb/' : '') + id + '.jpg' + ')';
        }
    }
});

/***/ }),

/***/ 309:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {
      staticClass: "w-content",
      style: "background-image:" + _vm.getBgUrl(_vm.bgid)
    },
    [_vm._t("default")],
    2
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-35be3d57", module.exports)
  }
}

/***/ }),

/***/ 322:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(355)
}
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(357)
/* template */
var __vue_template__ = __webpack_require__(358)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-455d3017"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/main/components/docs/NestedDraggable.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-455d3017", Component.options)
  } else {
    hotAPI.reload("data-v-455d3017", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 355:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(356);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(1)("02b85fe6", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../node_modules/css-loader/index.js!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-455d3017\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/sass-loader/lib/loader.js!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./NestedDraggable.vue", function() {
     var newContent = require("!!../../../../../../node_modules/css-loader/index.js!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-455d3017\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/sass-loader/lib/loader.js!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./NestedDraggable.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 356:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)(false);
// imports


// module
exports.push([module.i, "\n.docs-group[data-v-455d3017] {\n  cursor: move;\n}\n.docs-group.readonly[data-v-455d3017] {\n    cursor: default;\n}\n.docs-group.hidden-children .docs-group[data-v-455d3017] {\n    display: none;\n}\n.docs-group.hidden-children .item .together[data-v-455d3017] {\n    display: block;\n}\n.docs-group .docs-group[data-v-455d3017] {\n    padding-left: 14px;\n    background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABwAAAAcCAYAAAByDd+UAAABS2lUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4KPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS42LWMxNDAgNzkuMTYwNDUxLCAyMDE3LzA1LzA2LTAxOjA4OjIxICAgICAgICAiPgogPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4KICA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIi8+CiA8L3JkZjpSREY+CjwveDp4bXBtZXRhPgo8P3hwYWNrZXQgZW5kPSJyIj8+LUNEtwAAAEtJREFUSIntzzEVwAAMQkFSKfi3FKzQqQ5oJm5h5P3ZXQMYkrgwtk+OPo8kSzo7bGFcC+NaGNfCuBbGtTCuhXEtzB+SHAAGAEm/7wv2LKvDNoBjfgAAAABJRU5ErkJggg==) no-repeat -2px -9px;\n    margin-left: 18px;\n    border-left: 1px dotted #ddd;\n}\n.docs-group .item[data-v-455d3017] {\n    padding: 4px 0 0 4px;\n    background-color: #ffffff;\n    border: solid 1px #ffffff;\n    line-height: 24px;\n    position: relative;\n}\n.docs-group .item .together[data-v-455d3017] {\n      display: none;\n      cursor: pointer;\n      position: absolute;\n      font-size: 12px;\n      color: #ffb519;\n      top: 50%;\n      left: -2px;\n      margin-top: 1px;\n      -webkit-transform: translate(0, -50%);\n              transform: translate(0, -50%);\n      z-index: 1;\n}\n.docs-group .item .dashed[data-v-455d3017] {\n      position: absolute;\n      margin: 0;\n      padding: 0;\n      top: 16px;\n      right: 0;\n      left: 20px;\n      height: 2px;\n      border-width: 0 0 1px 0;\n      border-bottom: dashed 1px #eee;\n}\n.docs-group .item .header[data-v-455d3017] {\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-orient: horizontal;\n      -webkit-box-direction: normal;\n          -ms-flex-direction: row;\n              flex-direction: row;\n      -webkit-box-align: start;\n          -ms-flex-align: start;\n              align-items: flex-start;\n      position: relative;\n      background: #fff;\n      padding: 0 8px;\n      cursor: pointer;\n}\n.docs-group .item .header .tip[data-v-455d3017] {\n        display: inline-block;\n        position: relative;\n}\n.docs-group .item .header .tip > img[data-v-455d3017] {\n          display: inline-block;\n          width: 14px;\n          height: 14px;\n          margin-top: 5px;\n          vertical-align: top;\n}\n.docs-group .item .header .title[data-v-455d3017] {\n        display: inline-block;\n        border-bottom: 1px solid transparent;\n        cursor: pointer;\n        padding: 0 3px 0 7px;\n        color: #555555;\n        word-break: break-all;\n}\n.docs-group .item .header .title.active[data-v-455d3017] {\n          color: #0396f2;\n}\n.docs-group .item .info[data-v-455d3017] {\n      position: absolute;\n      background: #fff;\n      padding-left: 12px;\n      color: #666;\n      right: 3px;\n      top: 5px;\n}\n.docs-group .item .info > i[data-v-455d3017] {\n        padding: 0 2px;\n        -webkit-transition: all 0.2s;\n        transition: all 0.2s;\n        cursor: pointer;\n}\n.docs-group .item .info > i[data-v-455d3017]:hover {\n          -webkit-transform: scale(1.2);\n                  transform: scale(1.2);\n}\n", ""]);

// exports


/***/ }),

/***/ 357:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vuedraggable__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vuedraggable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vuedraggable__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
    name: "NestedDraggable",
    props: {
        lists: {
            required: true,
            type: Array
        },
        isChildren: {
            type: Boolean,
            default: false
        },
        disabled: {
            type: Boolean,
            default: false
        },
        readonly: {
            type: Boolean,
            default: false
        },
        activeid: {
            default: ''
        }
    },
    data: function data() {
        return {
            listSortData: '',
            childrenHidden: false
        };
    },

    components: {
        draggable: __WEBPACK_IMPORTED_MODULE_0_vuedraggable___default.a
    },
    mounted: function mounted() {
        this.listSortData = this.getSort(this.lists);
    },

    methods: {
        getSort: function getSort(lists) {
            var _this = this;

            var parentid = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;

            var sortData = "";
            lists.forEach(function (item) {
                sortData += item.id + ":" + parentid + ";" + _this.getSort(item.children, item.id);
            });
            return sortData;
        },
        handleClick: function handleClick(act, detail) {
            if (act == 'open') {
                if (detail.type == 'folder') {
                    this.$set(detail, 'hiddenChildren', !detail.hiddenChildren);
                    return;
                }
            }
            if (act == 'sort') {
                if (this.isChildren) {
                    this.$emit("change", act, detail);
                } else {
                    var tempSortData = this.getSort(this.lists);
                    if (tempSortData != this.listSortData) {
                        this.listSortData = tempSortData;
                        this.$emit("change", act, tempSortData);
                    }
                }
                return;
            }
            this.$emit("change", act, detail);
        }
    }
});

/***/ }),

/***/ 358:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "draggable",
    {
      attrs: {
        tag: "div",
        list: _vm.lists,
        group: { name: "docs-nested" },
        animation: 150,
        disabled: _vm.disabled || _vm.readonly
      },
      on: {
        sort: function($event) {
          return _vm.handleClick("sort")
        }
      }
    },
    _vm._l(_vm.lists, function(detail) {
      return _c(
        "div",
        {
          key: detail.id,
          staticClass: "docs-group",
          class: {
            readonly: _vm.readonly,
            "hidden-children": detail.hiddenChildren === true
          }
        },
        [
          _c(
            "div",
            { staticClass: "item" },
            [
              _c("Icon", {
                staticClass: "together",
                attrs: { type: "md-add" },
                on: {
                  click: function($event) {
                    return _vm.handleClick("open", detail)
                  }
                }
              }),
              _vm._v(" "),
              _c("div", { staticClass: "dashed" }),
              _vm._v(" "),
              _c("div", { staticClass: "header" }, [
                _c("div", { staticClass: "tip" }, [
                  _c("img", { attrs: { src: detail.icon } })
                ]),
                _vm._v(" "),
                _c(
                  "div",
                  {
                    staticClass: "title",
                    class: { active: _vm.activeid == detail.id },
                    on: {
                      click: function($event) {
                        return _vm.handleClick("open", detail)
                      }
                    }
                  },
                  [_vm._v(_vm._s(detail.title))]
                )
              ]),
              _vm._v(" "),
              !_vm.readonly
                ? _c(
                    "div",
                    { staticClass: "info" },
                    [
                      _c("Icon", {
                        attrs: { type: "md-create" },
                        on: {
                          click: function($event) {
                            return _vm.handleClick("edit", detail)
                          }
                        }
                      }),
                      _vm._v(" "),
                      _c("Icon", {
                        attrs: { type: "md-add" },
                        on: {
                          click: function($event) {
                            return _vm.handleClick("add", detail)
                          }
                        }
                      }),
                      _vm._v(" "),
                      _c("Icon", {
                        attrs: { type: "md-trash" },
                        on: {
                          click: function($event) {
                            return _vm.handleClick("delete", detail)
                          }
                        }
                      })
                    ],
                    1
                  )
                : _vm._e()
            ],
            1
          ),
          _vm._v(" "),
          typeof detail.children === "object" && detail.children !== null
            ? _c("nested-draggable", {
                attrs: {
                  lists: detail.children,
                  isChildren: true,
                  disabled: _vm.disabled,
                  readonly: _vm.readonly,
                  activeid: _vm.activeid
                },
                on: { change: _vm.handleClick }
              })
            : _vm._e()
        ],
        1
      )
    }),
    0
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-455d3017", module.exports)
  }
}

/***/ }),

/***/ 602:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(603);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(1)("529cbb4f", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0172190c\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./docs.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-0172190c\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../node_modules/sass-loader/lib/loader.js!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./docs.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 603:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)(false);
// imports


// module
exports.push([module.i, "\n.docs .docs-main[data-v-0172190c] {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n  width: 100%;\n  min-width: 1024px;\n  height: 100%;\n  padding: 15px;\n}\n.docs .docs-main .docs-body[data-v-0172190c] {\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-orient: horizontal;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: row;\n            flex-direction: row;\n    width: 100%;\n    height: 100%;\n    min-height: 500px;\n}\n.docs .docs-main .docs-body .docs-menu[data-v-0172190c] {\n      display: -webkit-box;\n      display: -ms-flexbox;\n      display: flex;\n      -webkit-box-orient: vertical;\n      -webkit-box-direction: normal;\n          -ms-flex-direction: column;\n              flex-direction: column;\n      width: 230px;\n      border-radius: 3px 0 0 3px;\n      background: rgba(255, 255, 255, 0.8);\n}\n.docs .docs-main .docs-body .docs-menu h3[data-v-0172190c] {\n        font-size: 18px;\n        font-weight: normal;\n        padding: 10px 12px;\n        color: #333333;\n}\n.docs .docs-main .docs-body .docs-menu ul[data-v-0172190c] {\n        -webkit-box-flex: 1;\n            -ms-flex: 1;\n                flex: 1;\n        overflow: auto;\n}\n.docs .docs-main .docs-body .docs-menu ul li[data-v-0172190c] {\n          padding: 12px;\n          cursor: pointer;\n}\n.docs .docs-main .docs-body .docs-menu ul li.more[data-v-0172190c] {\n            text-align: center;\n            color: #555555;\n            margin-bottom: 6px;\n}\n.docs .docs-main .docs-body .docs-menu ul li.more[data-v-0172190c]:hover {\n              color: #333333;\n}\n.docs .docs-main .docs-body .docs-menu ul li.load[data-v-0172190c] {\n            text-align: center;\n            height: 42px;\n            margin-bottom: 9px;\n}\n.docs .docs-main .docs-body .docs-menu ul li.none[data-v-0172190c] {\n            background-color: transparent;\n            text-align: center;\n            color: #666666;\n            padding: 8px 18px;\n}\n.docs .docs-main .docs-body .docs-menu ul li.active[data-v-0172190c] {\n            background-color: #ffffff;\n}\n.docs .docs-main .docs-body .docs-menu ul li .docs-title[data-v-0172190c] {\n            color: #242424;\n            font-size: 13px;\n}\n.docs .docs-main .docs-body .docs-menu ul li .docs-time[data-v-0172190c] {\n            display: block;\n            color: #999;\n            font-size: 12px;\n            margin-top: 2px;\n            position: relative;\n}\n.docs .docs-main .docs-body .docs-container[data-v-0172190c] {\n      -webkit-box-flex: 1;\n          -ms-flex: 1;\n              flex: 1;\n      background-color: #ffffff;\n      border-radius: 0 3px 3px 0;\n}\n.docs .docs-main .docs-body .docs-container .docs-box[data-v-0172190c] {\n        width: 100%;\n        height: 100%;\n        display: -webkit-box;\n        display: -ms-flexbox;\n        display: flex;\n        -webkit-box-orient: vertical;\n        -webkit-box-direction: normal;\n            -ms-flex-direction: column;\n                flex-direction: column;\n}\n.docs .docs-main .docs-body .docs-container .docs-header[data-v-0172190c] {\n        display: -webkit-box;\n        display: -ms-flexbox;\n        display: flex;\n        -webkit-box-align: center;\n            -ms-flex-align: center;\n                align-items: center;\n        margin: 6px 24px 0;\n        padding: 12px 0;\n        border-bottom: 1px solid #eeeeee;\n}\n.docs .docs-main .docs-body .docs-container .docs-header .docs-h1[data-v-0172190c] {\n          -webkit-box-flex: 1;\n              -ms-flex: 1;\n                  flex: 1;\n          font-size: 16px;\n          white-space: nowrap;\n}\n.docs .docs-main .docs-body .docs-container .docs-header .docs-setting[data-v-0172190c] {\n          display: -webkit-box;\n          display: -ms-flexbox;\n          display: flex;\n          -webkit-box-align: center;\n              -ms-flex-align: center;\n                  align-items: center;\n}\n.docs .docs-main .docs-body .docs-container .docs-header .docs-setting > button[data-v-0172190c] {\n            margin: 0 6px;\n}\n.docs .docs-main .docs-body .docs-container .docs-header .docs-setting > button[data-v-0172190c]:last-child {\n              margin-right: 0;\n}\n.docs .docs-main .docs-body .docs-container .docs-section[data-v-0172190c] {\n        -webkit-box-flex: 1;\n            -ms-flex: 1;\n                flex: 1;\n        padding: 12px 26px;\n        overflow: auto;\n        -webkit-transform: translateZ(0);\n                transform: translateZ(0);\n}\n.docs .docs-main .docs-body .docs-container .docs-section .none[data-v-0172190c] {\n          background-color: transparent;\n          text-align: center;\n          color: #666666;\n          padding: 48px 24px;\n}\n", ""]);

// exports


/***/ }),

/***/ 604:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_WContent__ = __webpack_require__(305);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_WContent___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__components_WContent__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_docs_NestedDraggable__ = __webpack_require__(322);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_docs_NestedDraggable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__components_docs_NestedDraggable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_docs_setting__ = __webpack_require__(605);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_docs_setting___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__components_docs_setting__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_docs_users__ = __webpack_require__(610);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_docs_users___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__components_docs_users__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_iview_WDrawer__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_iview_WDrawer___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__components_iview_WDrawer__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//






/* harmony default export */ __webpack_exports__["default"] = ({
    components: { WDrawer: __WEBPACK_IMPORTED_MODULE_4__components_iview_WDrawer___default.a, BookUsers: __WEBPACK_IMPORTED_MODULE_3__components_docs_users___default.a, BookSetting: __WEBPACK_IMPORTED_MODULE_2__components_docs_setting___default.a, NestedDraggable: __WEBPACK_IMPORTED_MODULE_1__components_docs_NestedDraggable___default.a, WContent: __WEBPACK_IMPORTED_MODULE_0__components_WContent___default.a },
    data: function data() {
        return {
            loadIng: 0,

            bookLists: [],
            bookListPage: 1,
            bookListTotal: 0,
            bookLoading: false,
            bookHasMorePages: false,
            bookNoDataText: "",

            addBookId: 0,
            addBookShow: false,
            formBookAdd: {
                title: ''
            },
            ruleBookAdd: {},
            selectBookData: {},

            sectionLists: [],
            sectionNoDataText: "",

            addSectionId: 0,
            addSectionShow: false,
            formSectionAdd: {
                title: '',
                type: 'document'
            },
            ruleSectionAdd: {},
            sectionTypeLists: [],

            sortDisabled: false,

            settingDrawerShow: false,
            settingDrawerTab: 'setting'
        };
    },
    mounted: function mounted() {
        this.getBookLists(true);
    },
    deactivated: function deactivated() {
        this.addBookShow = false;
        this.addSectionShow = false;
    },


    computed: {},

    watch: {
        usrName: function usrName() {
            this.usrLogin && this.getBookLists(true);
        },
        addBookShow: function addBookShow(val) {
            var _this = this;

            if (val && this.addBookId > 0) {
                var tempLists = this.bookLists.filter(function (res) {
                    return res.id == _this.addBookId;
                });
                if (tempLists.length === 1) {
                    this.$set(this.formBookAdd, 'title', tempLists[0].title);
                } else {
                    this.$set(this.formBookAdd, 'title', '');
                }
            }
        },
        addSectionShow: function addSectionShow(val) {
            var _this2 = this;

            if (val && this.addSectionId > 0) {
                var tempLists = this.children2lists(this.sectionLists).filter(function (res) {
                    return res.id == _this2.addSectionId;
                });
                if (tempLists.length === 1) {
                    this.$set(this.formSectionAdd, 'title', tempLists[0].title);
                } else {
                    this.$set(this.formSectionAdd, 'title', '');
                }
            }
        }
    },

    methods: {
        initLanguage: function initLanguage() {
            this.bookNoDataText = this.$L("数据加载中.....");
            this.sectionNoDataText = this.$L("数据加载中.....");
            this.sectionTypeLists = [{ value: 'document', text: this.$L("文本") }, { value: 'mind', text: this.$L("脑图") }, { value: 'sheet', text: this.$L("表格") }, { value: 'flow', text: this.$L("流程图") }, { value: 'folder', text: this.$L("目录") }];
            this.ruleBookAdd = {
                title: [{ required: true, message: this.$L('请填写知识库名称！'), trigger: 'change' }, { type: 'string', min: 2, message: this.$L('知识库名称长度至少2位！'), trigger: 'change' }]
            };
            this.ruleSectionAdd = {
                title: [{ required: true, message: this.$L('请填写文档标题！'), trigger: 'change' }, { type: 'string', min: 2, message: this.$L('文档标题长度至少2位！'), trigger: 'change' }],
                type: [{ required: true }]
            };
        },
        children2lists: function children2lists(lists) {
            var _this3 = this;

            var array = [];
            lists.forEach(function (item) {
                array.push({
                    id: item.id,
                    title: item.title
                });
                array = array.concat(_this3.children2lists(item.children));
            });
            return array;
        },
        getBookLists: function getBookLists(resetLoad) {
            var _this4 = this;

            if (resetLoad === true) {
                this.bookListPage = 1;
            } else {
                if (this.bookHasMorePages === false) {
                    return;
                }
                this.bookListPage++;
            }
            this.bookLoading = true;
            this.bookNoDataText = this.$L("数据加载中.....");
            this.bookHasMorePages = false;
            $A.apiAjax({
                url: 'docs/book/lists',
                data: {
                    page: Math.max(this.bookListPage, 1),
                    pagesize: 20
                },
                complete: function complete() {
                    _this4.bookLoading = false;
                },
                error: function error() {
                    _this4.bookNoDataText = _this4.$L("数据加载失败！");
                },
                success: function success(res) {
                    if (res.ret === 1) {
                        res.data.lists.forEach(function (item) {
                            var find = _this4.bookLists.find(function (t) {
                                return t.id == item.id;
                            });
                            if (!find) {
                                _this4.bookLists.push(item);
                            }
                        });
                        _this4.bookListTotal = res.data.total;
                        _this4.bookNoDataText = _this4.$L("没有相关的数据");
                        _this4.bookHasMorePages = res.data.hasMorePages;
                        if (typeof _this4.selectBookData.id === "undefined") {
                            _this4.selectBookData = _this4.bookLists[0];
                            _this4.getSectionLists();
                        }
                    } else {
                        _this4.bookLists = [];
                        _this4.bookListTotal = 0;
                        _this4.bookNoDataText = res.msg;
                    }
                }
            });
        },
        onBookAdd: function onBookAdd() {
            var _this5 = this;

            this.$refs.bookAdd.validate(function (valid) {
                if (valid) {
                    _this5.loadIng++;
                    $A.apiAjax({
                        url: 'docs/book/add',
                        data: Object.assign(_this5.formBookAdd, { id: _this5.addBookId }),
                        complete: function complete() {
                            _this5.loadIng--;
                        },
                        success: function success(res) {
                            if (res.ret === 1) {
                                _this5.addBookShow = false;
                                _this5.$Message.success(res.msg);
                                _this5.$refs.bookAdd.resetFields();
                                //
                                if (_this5.addBookId > 0) {
                                    _this5.bookLists.some(function (item) {
                                        if (item.id == _this5.addBookId) {
                                            _this5.$set(item, 'title', res.data.title);
                                            return true;
                                        }
                                    });
                                } else {
                                    _this5.bookLists.unshift(res.data);
                                    _this5.selectBookData = _this5.bookLists[0];
                                    _this5.getSectionLists();
                                }
                            } else {
                                _this5.$Modal.error({ title: _this5.$L('温馨提示'), content: res.msg });
                            }
                        }
                    });
                }
            });
        },
        onBookDelete: function onBookDelete(bookId) {
            var _this6 = this;

            this.$Modal.confirm({
                title: this.$L('删除知识库'),
                content: this.$L('你确定要删除此知识库吗？'),
                loading: true,
                onOk: function onOk() {
                    $A.apiAjax({
                        url: 'docs/book/delete',
                        data: {
                            id: bookId
                        },
                        error: function error() {
                            _this6.$Modal.remove();
                            alert(_this6.$L('网络繁忙，请稍后再试！'));
                        },
                        success: function success(res) {
                            _this6.$Modal.remove();
                            _this6.bookLists.some(function (item, index) {
                                if (item.id == bookId) {
                                    _this6.bookLists.splice(index, 1);
                                    return true;
                                }
                            });
                            _this6.selectBookData = _this6.bookLists[0];
                            _this6.getSectionLists();
                            //
                            setTimeout(function () {
                                if (res.ret === 1) {
                                    _this6.$Message.success(res.msg);
                                } else {
                                    _this6.$Modal.error({ title: _this6.$L('温馨提示'), content: res.msg });
                                }
                            }, 350);
                        }
                    });
                }
            });
        },
        getSectionLists: function getSectionLists(isClear) {
            var _this7 = this;

            if (isClear === true) {
                this.sectionLists = [];
            }
            var bookid = this.selectBookData.id;
            this.loadIng++;
            this.sectionNoDataText = this.$L("数据加载中.....");
            $A.apiAjax({
                url: 'docs/section/lists',
                data: {
                    act: 'edit',
                    bookid: bookid
                },
                complete: function complete() {
                    _this7.loadIng--;
                },
                error: function error() {
                    if (bookid != _this7.selectBookData.id) {
                        return;
                    }
                    _this7.sectionNoDataText = _this7.$L("数据加载失败！");
                },
                success: function success(res) {
                    if (bookid != _this7.selectBookData.id) {
                        return;
                    }
                    if (res.ret === 1) {
                        _this7.sectionLists = res.data.tree;
                        _this7.sectionNoDataText = _this7.$L("没有相关的数据");
                    } else {
                        _this7.sectionLists = [];
                        _this7.sectionNoDataText = res.msg;
                    }
                }
            });
        },
        onSectionAdd: function onSectionAdd() {
            var _this8 = this;

            this.$refs.sectionAdd.validate(function (valid) {
                if (valid) {
                    _this8.loadIng++;
                    var bookid = _this8.selectBookData.id;
                    $A.apiAjax({
                        url: 'docs/section/add',
                        data: Object.assign(_this8.formSectionAdd, {
                            id: _this8.addSectionId,
                            bookid: bookid
                        }),
                        complete: function complete() {
                            _this8.loadIng--;
                        },
                        success: function success(res) {
                            if (bookid != _this8.selectBookData.id) {
                                return;
                            }
                            if (res.ret === 1) {
                                _this8.addSectionShow = false;
                                _this8.$Message.success(res.msg);
                                _this8.$refs.sectionAdd.resetFields();
                                //
                                _this8.getSectionLists();
                            } else {
                                _this8.$Modal.error({ title: _this8.$L('温馨提示'), content: res.msg });
                            }
                        }
                    });
                }
            });
        },
        onSectionDelete: function onSectionDelete(detail) {
            var _this9 = this;

            var sectionType = this.sectionTypeLists.find(function (item) {
                return item.value == detail.type;
            });
            this.$Modal.confirm({
                title: this.$L('删除文档'),
                content: this.$L('你确定要删除%【%】吗？', sectionType ? sectionType.text : '', detail.title),
                loading: true,
                onOk: function onOk() {
                    $A.apiAjax({
                        url: 'docs/section/delete',
                        data: {
                            id: detail.id
                        },
                        error: function error() {
                            _this9.$Modal.remove();
                            alert(_this9.$L('网络繁忙，请稍后再试！'));
                        },
                        success: function success(res) {
                            _this9.$Modal.remove();
                            _this9.getSectionLists();
                            //
                            setTimeout(function () {
                                if (res.ret === 1) {
                                    _this9.$Message.success(res.msg);
                                } else {
                                    _this9.$Modal.error({ title: _this9.$L('温馨提示'), content: res.msg });
                                }
                            }, 350);
                        }
                    });
                }
            });
        },
        handleSection: function handleSection(act, detail) {
            var _this10 = this;

            switch (act) {
                case 'open':
                    this.goForward({ name: 'docs-edit', params: { sid: detail.id, other: detail || {} } });
                    break;

                case 'edit':
                    this.addSectionId = detail.id;
                    this.addSectionShow = true;
                    break;

                case 'add':
                    this.addSectionId = detail.id * -1;
                    this.addSectionShow = true;
                    break;

                case 'delete':
                    if (this.sortDisabled) {
                        this.$Modal.warning({
                            title: this.$L('温馨提示'),
                            content: this.$L('正在进行其他操作，请稍后重试...')
                        });
                        return;
                    }
                    this.onSectionDelete(detail);
                    break;

                case 'sort':
                    this.sortDisabled = true;
                    this.loadIng++;
                    $A.apiAjax({
                        url: 'docs/section/sort',
                        data: {
                            bookid: this.selectBookData.id,
                            newsort: detail
                        },
                        complete: function complete() {
                            _this10.sortDisabled = false;
                            _this10.loadIng--;
                        },
                        error: function error() {
                            _this10.getSectionLists();
                            alert(_this10.$L('网络繁忙，请稍后再试！'));
                        },
                        success: function success(res) {
                            if (res.ret === 1) {
                                _this10.$Message.success(res.msg);
                            } else {
                                _this10.getSectionLists();
                                _this10.$Modal.error({ title: _this10.$L('温馨提示'), content: res.msg });
                            }
                        }
                    });
                    break;
            }
        },
        showShare: function showShare() {
            var _this11 = this;

            this.$Modal.confirm({
                render: function render(h) {
                    return h('div', [h('div', {
                        style: {
                            fontSize: '16px',
                            fontWeight: '500',
                            marginBottom: '20px'
                        }
                    }, _this11.$L('文档链接')), h('Input', {
                        props: {
                            value: $A.webUrl('docs/view/b' + _this11.selectBookData.id),
                            readonly: true
                        }
                    })]);
                }
            });
        },
        settingCallback: function settingCallback(data) {
            var _this12 = this;

            var tmpData = this.bookLists.find(function (_ref) {
                var id = _ref.id;
                return id == data.id;
            });
            if (tmpData) {
                $A.each(data, function (key, val) {
                    _this12.$set(tmpData, key, val);
                });
            }
        }
    }
});

/***/ }),

/***/ 605:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(606)
}
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(608)
/* template */
var __vue_template__ = __webpack_require__(609)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-b2d13f9e"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/main/components/docs/setting.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-b2d13f9e", Component.options)
  } else {
    hotAPI.reload("data-v-b2d13f9e", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 606:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(607);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(1)("94f30462", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../node_modules/css-loader/index.js!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-b2d13f9e\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/sass-loader/lib/loader.js!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./setting.vue", function() {
     var newContent = require("!!../../../../../../node_modules/css-loader/index.js!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-b2d13f9e\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/sass-loader/lib/loader.js!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./setting.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 607:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)(false);
// imports


// module
exports.push([module.i, "\n.book-setting[data-v-b2d13f9e] {\n  padding: 0 12px;\n}\n.book-setting .form-title[data-v-b2d13f9e] {\n    font-weight: bold;\n}\n.book-setting .form-link[data-v-b2d13f9e] {\n    text-decoration: underline;\n}\n.book-setting .form-placeholder[data-v-b2d13f9e] {\n    font-size: 12px;\n    color: #999999;\n}\n.book-setting .form-placeholder[data-v-b2d13f9e]:hover {\n    color: #000000;\n}\n", ""]);

// exports


/***/ }),

/***/ 608:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__DrawerTabsContainer__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__DrawerTabsContainer___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__DrawerTabsContainer__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
    name: 'BookSetting',
    components: { DrawerTabsContainer: __WEBPACK_IMPORTED_MODULE_0__DrawerTabsContainer___default.a },
    props: {
        id: {
            default: 0
        },
        canload: {
            type: Boolean,
            default: true
        }
    },
    data: function data() {
        return {
            loadYet: false,

            loadIng: 0,

            formSystem: {}
        };
    },
    mounted: function mounted() {
        if (this.canload) {
            this.loadYet = true;
            this.getSetting();
        }
    },


    watch: {
        id: function id() {
            if (this.loadYet) {
                this.getSetting();
            }
        },
        canload: function canload(val) {
            if (val && !this.loadYet) {
                this.loadYet = true;
                this.getSetting();
            }
        }
    },

    methods: {
        getSetting: function getSetting(save) {
            var _this = this;

            this.loadIng++;
            $A.apiAjax({
                url: 'docs/book/setting?type=' + (save ? 'save' : 'get'),
                data: Object.assign(this.formSystem, {
                    id: this.id
                }),
                complete: function complete() {
                    _this.loadIng--;
                },
                success: function success(res) {
                    if (res.ret === 1) {
                        var data = res.data;
                        data.role_edit = data.role_edit || 'reg';
                        data.role_look = data.role_look || 'edit';
                        data.role_view = data.role_view || 'all';
                        _this.formSystem = data;
                        _this.formSystem__reset = $A.cloneData(_this.formSystem);
                        if (save) {
                            _this.$emit('on-setting-callback', Object.assign(data, {
                                id: _this.id
                            }));
                            _this.$Message.success(_this.$L('修改成功'));
                        }
                    } else {
                        if (save) {
                            _this.$Modal.error({ title: _this.$L('温馨提示'), content: res.msg });
                        }
                    }
                }
            });
        },
        handleSubmit: function handleSubmit(name) {
            var _this2 = this;

            this.$refs[name].validate(function (valid) {
                if (valid) {
                    switch (name) {
                        case "formSystem":
                            {
                                _this2.getSetting(true);
                                break;
                            }
                    }
                }
            });
        },
        handleReset: function handleReset(name) {
            if (typeof this[name + '__reset'] !== "undefined") {
                this[name] = $A.cloneData(this[name + '__reset']);
                return;
            }
            this.$refs[name].resetFields();
        }
    }
});

/***/ }),

/***/ 609:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("drawer-tabs-container", [
    _c(
      "div",
      { staticClass: "book-setting" },
      [
        _c(
          "Form",
          {
            ref: "formSystem",
            attrs: { model: _vm.formSystem, "label-width": 80 },
            nativeOn: {
              submit: function($event) {
                $event.preventDefault()
              }
            }
          },
          [
            _c("FormItem", { attrs: { label: _vm.$L("文档链接") } }, [
              _c(
                "a",
                {
                  staticClass: "form-link",
                  attrs: {
                    target: "_blank",
                    href: _vm.$A.webUrl("docs/view/b" + this.id)
                  }
                },
                [_vm._v(_vm._s(_vm.$A.webUrl("docs/view/b" + this.id)))]
              )
            ]),
            _vm._v(" "),
            _c("FormItem", { attrs: { label: _vm.$L("管理权限") } }, [
              _c("div", [
                _c(
                  "div",
                  [
                    _c(
                      "RadioGroup",
                      {
                        model: {
                          value: _vm.formSystem.role_edit,
                          callback: function($$v) {
                            _vm.$set(_vm.formSystem, "role_edit", $$v)
                          },
                          expression: "formSystem.role_edit"
                        }
                      },
                      [
                        _c("Radio", { attrs: { label: "private" } }, [
                          _vm._v(_vm._s(_vm.$L("私有文库")))
                        ]),
                        _vm._v(" "),
                        _c("Radio", { attrs: { label: "member" } }, [
                          _vm._v(_vm._s(_vm.$L("成员开放")))
                        ]),
                        _vm._v(" "),
                        _c("Radio", { attrs: { label: "reg" } }, [
                          _vm._v(_vm._s(_vm.$L("注册会员")))
                        ])
                      ],
                      1
                    )
                  ],
                  1
                ),
                _vm._v(" "),
                _vm.formSystem.role_edit == "private"
                  ? _c("div", { staticClass: "form-placeholder" }, [
                      _vm._v(
                        "\n                        " +
                          _vm._s(_vm.$L("仅作者可以修改。")) +
                          "\n                    "
                      )
                    ])
                  : _vm.formSystem.role_edit == "member"
                  ? _c("div", { staticClass: "form-placeholder" }, [
                      _vm._v(
                        "\n                        " +
                          _vm._s(_vm.$L("仅作者和文档成员可以修改。")) +
                          "\n                    "
                      )
                    ])
                  : _vm.formSystem.role_edit == "reg"
                  ? _c("div", { staticClass: "form-placeholder" }, [
                      _vm._v(
                        "\n                        " +
                          _vm._s(_vm.$L("所有会员都可以修改。")) +
                          "\n                    "
                      )
                    ])
                  : _vm._e()
              ])
            ]),
            _vm._v(" "),
            _c("FormItem", { attrs: { label: _vm.$L("阅读权限") } }, [
              _c(
                "div",
                [
                  _c(
                    "RadioGroup",
                    {
                      model: {
                        value: _vm.formSystem.role_view,
                        callback: function($$v) {
                          _vm.$set(_vm.formSystem, "role_view", $$v)
                        },
                        expression: "formSystem.role_view"
                      }
                    },
                    [
                      _c("Radio", { attrs: { label: "private" } }, [
                        _vm._v(_vm._s(_vm.$L("私有文库")))
                      ]),
                      _vm._v(" "),
                      _c("Radio", { attrs: { label: "member" } }, [
                        _vm._v(_vm._s(_vm.$L("成员开放")))
                      ]),
                      _vm._v(" "),
                      _c("Radio", { attrs: { label: "reg" } }, [
                        _vm._v(_vm._s(_vm.$L("注册会员")))
                      ]),
                      _vm._v(" "),
                      _c("Radio", { attrs: { label: "all" } }, [
                        _vm._v(_vm._s(_vm.$L("完全开放")))
                      ])
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _vm.formSystem.role_view == "private"
                ? _c("div", { staticClass: "form-placeholder" }, [
                    _vm._v(
                      "\n                    " +
                        _vm._s(_vm.$L("仅作者可以阅读分享地址。")) +
                        "\n                "
                    )
                  ])
                : _vm.formSystem.role_view == "member"
                ? _c("div", { staticClass: "form-placeholder" }, [
                    _vm._v(
                      "\n                    " +
                        _vm._s(_vm.$L("仅作者和文档成员可以阅读分享地址。")) +
                        "\n                "
                    )
                  ])
                : _vm.formSystem.role_view == "reg"
                ? _c("div", { staticClass: "form-placeholder" }, [
                    _vm._v(
                      "\n                    " +
                        _vm._s(_vm.$L("所有会员都可以阅读分享地址。")) +
                        "\n                "
                    )
                  ])
                : _vm.formSystem.role_view == "all"
                ? _c("div", { staticClass: "form-placeholder" }, [
                    _vm._v(
                      "\n                    " +
                        _vm._s(_vm.$L("所有人（含游客）都可以阅读分享地址。")) +
                        "\n                "
                    )
                  ])
                : _vm._e()
            ]),
            _vm._v(" "),
            _c(
              "FormItem",
              [
                _c(
                  "Button",
                  {
                    attrs: { loading: _vm.loadIng > 0, type: "primary" },
                    on: {
                      click: function($event) {
                        return _vm.handleSubmit("formSystem")
                      }
                    }
                  },
                  [_vm._v(_vm._s(_vm.$L("提交")))]
                ),
                _vm._v(" "),
                _c(
                  "Button",
                  {
                    staticStyle: { "margin-left": "8px" },
                    attrs: { loading: _vm.loadIng > 0 },
                    on: {
                      click: function($event) {
                        return _vm.handleReset("formSystem")
                      }
                    }
                  },
                  [_vm._v(_vm._s(_vm.$L("重置")))]
                )
              ],
              1
            )
          ],
          1
        )
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-b2d13f9e", module.exports)
  }
}

/***/ }),

/***/ 610:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(611)
}
var normalizeComponent = __webpack_require__(2)
/* script */
var __vue_script__ = __webpack_require__(613)
/* template */
var __vue_template__ = __webpack_require__(614)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-df98fbae"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/main/components/docs/users.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-df98fbae", Component.options)
  } else {
    hotAPI.reload("data-v-df98fbae", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 611:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(612);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(1)("136d5f94", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../../node_modules/css-loader/index.js!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-df98fbae\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/sass-loader/lib/loader.js!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./users.vue", function() {
     var newContent = require("!!../../../../../../node_modules/css-loader/index.js!../../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-df98fbae\",\"scoped\":true,\"hasInlineConfig\":true}!../../../../../../node_modules/sass-loader/lib/loader.js!../../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./users.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 612:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(0)(false);
// imports


// module
exports.push([module.i, "\n.book-users[data-v-df98fbae] {\n  padding: 0 12px;\n}\n.book-users .tableFill[data-v-df98fbae] {\n    margin: 12px 0 20px;\n}\n", ""]);

// exports


/***/ }),

/***/ 613:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__DrawerTabsContainer__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__DrawerTabsContainer___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__DrawerTabsContainer__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
    name: 'BookUsers',
    components: { DrawerTabsContainer: __WEBPACK_IMPORTED_MODULE_0__DrawerTabsContainer___default.a },
    props: {
        id: {
            default: 0
        },
        canload: {
            type: Boolean,
            default: true
        }
    },
    data: function data() {
        return {
            loadYet: false,

            loadIng: 0,

            columns: [],

            lists: [],
            listPage: 1,
            listTotal: 0,
            noDataText: ""
        };
    },
    mounted: function mounted() {
        if (this.canload) {
            this.loadYet = true;
            this.getLists(true);
        }
    },


    watch: {
        id: function id() {
            if (this.loadYet) {
                this.getLists(true);
            }
        },
        canload: function canload(val) {
            if (val && !this.loadYet) {
                this.loadYet = true;
                this.getLists(true);
            }
        }
    },

    methods: {
        initLanguage: function initLanguage() {
            var _this = this;

            this.noDataText = this.$L("数据加载中.....");
            this.columns = [{
                "title": this.$L("头像"),
                "minWidth": 60,
                "maxWidth": 100,
                render: function render(h, params) {
                    return h('UserImg', {
                        props: {
                            info: params.row
                        },
                        style: {
                            width: "30px",
                            height: "30px",
                            fontSize: "16px",
                            lineHeight: "30px",
                            borderRadius: "15px",
                            verticalAlign: "middle"
                        }
                    });
                }
            }, {
                "title": this.$L("用户名"),
                "key": 'username',
                "minWidth": 80,
                "ellipsis": true
            }, {
                "title": this.$L("昵称"),
                "minWidth": 80,
                "ellipsis": true,
                render: function render(h, params) {
                    return h('span', params.row.nickname || '-');
                }
            }, {
                "title": this.$L("职位/职称"),
                "minWidth": 100,
                "ellipsis": true,
                render: function render(h, params) {
                    return h('span', params.row.profession || '-');
                }
            }, {
                "title": this.$L("加入时间"),
                "width": 160,
                render: function render(h, params) {
                    return h('span', $A.formatDate("Y-m-d H:i:s", params.row.indate));
                }
            }, {
                "title": this.$L("操作"),
                "key": 'action',
                "width": 80,
                "align": 'center',
                render: function render(h, params) {
                    return h('Button', {
                        props: {
                            type: 'primary',
                            size: 'small'
                        },
                        style: {
                            fontSize: '12px'
                        },
                        on: {
                            click: function click() {
                                _this.$Modal.confirm({
                                    title: _this.$L('移出成员'),
                                    content: _this.$L('你确定要将此成员移出项目吗？'),
                                    loading: true,
                                    onOk: function onOk() {
                                        $A.apiAjax({
                                            url: 'docs/users/join',
                                            data: {
                                                act: 'delete',
                                                id: params.row.bookid,
                                                username: params.row.username
                                            },
                                            error: function error() {
                                                _this.$Modal.remove();
                                                alert(_this.$L('网络繁忙，请稍后再试！'));
                                            },
                                            success: function success(res) {
                                                _this.$Modal.remove();
                                                _this.getLists();
                                                setTimeout(function () {
                                                    if (res.ret === 1) {
                                                        _this.$Message.success(res.msg);
                                                    } else {
                                                        _this.$Modal.error({ title: _this.$L('温馨提示'), content: res.msg });
                                                    }
                                                }, 350);
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    }, _this.$L('删除'));
                }
            }];
        },
        setPage: function setPage(page) {
            this.listPage = page;
            this.getLists();
        },
        setPageSize: function setPageSize(size) {
            if (Math.max($A.runNum(this.listPageSize), 10) != size) {
                this.listPageSize = size;
                this.getLists();
            }
        },
        getLists: function getLists(resetLoad) {
            var _this2 = this;

            if (resetLoad === true) {
                this.listPage = 1;
            }
            if (this.id == 0) {
                this.lists = [];
                this.listTotal = 0;
                this.noDataText = this.$L("没有相关的数据");
                return;
            }
            this.loadIng++;
            this.noDataText = this.$L("数据加载中.....");
            $A.apiAjax({
                url: 'docs/users/lists',
                data: {
                    page: Math.max(this.listPage, 1),
                    pagesize: Math.max($A.runNum(this.listPageSize), 10),
                    id: this.id
                },
                complete: function complete() {
                    _this2.loadIng--;
                },
                error: function error() {
                    _this2.noDataText = _this2.$L("数据加载失败！");
                },
                success: function success(res) {
                    if (res.ret === 1) {
                        _this2.lists = res.data.lists;
                        _this2.listTotal = res.data.total;
                        _this2.noDataText = _this2.$L("没有相关的数据");
                    } else {
                        _this2.lists = [];
                        _this2.listTotal = 0;
                        _this2.noDataText = res.msg;
                    }
                }
            });
        },
        addUser: function addUser() {
            var _this3 = this;

            this.userValue = "";
            this.$Modal.confirm({
                render: function render(h) {
                    return h('div', [h('div', {
                        style: {
                            fontSize: '16px',
                            fontWeight: '500',
                            marginBottom: '20px'
                        }
                    }, _this3.$L('添加成员')), h('UserInput', {
                        props: {
                            value: _this3.userValue,
                            multiple: true,
                            nousername: _this3.usrName,
                            nobookid: _this3.id,
                            placeholder: _this3.$L('请输入昵称/用户名搜索')
                        },
                        on: {
                            input: function input(val) {
                                _this3.userValue = val;
                            }
                        }
                    })]);
                },
                loading: true,
                onOk: function onOk() {
                    if (_this3.userValue) {
                        var username = _this3.userValue;
                        $A.apiAjax({
                            url: 'docs/users/join',
                            data: {
                                act: 'join',
                                id: _this3.id,
                                username: username
                            },
                            error: function error() {
                                _this3.$Modal.remove();
                                alert(_this3.$L('网络繁忙，请稍后再试！'));
                            },
                            success: function success(res) {
                                _this3.$Modal.remove();
                                _this3.getLists();
                                setTimeout(function () {
                                    if (res.ret === 1) {
                                        _this3.$Message.success(res.msg);
                                    } else {
                                        _this3.$Modal.error({ title: _this3.$L('温馨提示'), content: res.msg });
                                    }
                                }, 350);
                            }
                        });
                    } else {
                        _this3.$Modal.remove();
                    }
                }
            });
        }
    }
});

/***/ }),

/***/ 614:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("drawer-tabs-container", [
    _c(
      "div",
      { staticClass: "book-users" },
      [
        _c(
          "Button",
          {
            attrs: {
              loading: _vm.loadIng > 0,
              type: "primary",
              icon: "md-add"
            },
            on: { click: _vm.addUser }
          },
          [_vm._v(_vm._s(_vm.$L("添加成员")))]
        ),
        _vm._v(" "),
        _c("Table", {
          ref: "tableRef",
          staticClass: "tableFill",
          attrs: {
            columns: _vm.columns,
            data: _vm.lists,
            loading: _vm.loadIng > 0,
            "no-data-text": _vm.noDataText,
            stripe: ""
          }
        }),
        _vm._v(" "),
        _c("Page", {
          staticClass: "pageBox",
          attrs: {
            total: _vm.listTotal,
            current: _vm.listPage,
            disabled: _vm.loadIng > 0,
            "page-size-opts": [10, 20, 30, 50, 100],
            placement: "top",
            "show-elevator": "",
            "show-sizer": "",
            "show-total": "",
            transfer: "",
            simple: _vm.windowMax768
          },
          on: {
            "on-change": _vm.setPage,
            "on-page-size-change": _vm.setPageSize
          }
        })
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-df98fbae", module.exports)
  }
}

/***/ }),

/***/ 615:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "w-main docs" },
    [
      _c("v-title", [
        _vm._v(
          _vm._s(_vm.$L("知识库")) +
            "-" +
            _vm._s(_vm.$L("轻量级的团队在线协作"))
        )
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "w-nav" }, [
        _c("div", { staticClass: "nav-row" }, [
          _c("div", { staticClass: "w-nav-left" }, [
            _c("div", { staticClass: "page-nav-left" }, [
              _c(
                "span",
                {
                  staticClass: "hover",
                  on: {
                    click: function($event) {
                      ;[(_vm.addBookId = 0), (_vm.addBookShow = true)]
                    }
                  }
                },
                [
                  _c("i", { staticClass: "ft icon" }, [_vm._v("")]),
                  _vm._v(" " + _vm._s(_vm.$L("新建知识库")))
                ]
              ),
              _vm._v(" "),
              _vm.loadIng > 0
                ? _c(
                    "div",
                    { staticClass: "page-nav-loading" },
                    [_c("w-loading")],
                    1
                  )
                : _c("div", { staticClass: "page-nav-refresh" }, [
                    _c(
                      "em",
                      {
                        on: {
                          click: function($event) {
                            return _vm.getBookLists(true)
                          }
                        }
                      },
                      [_vm._v(_vm._s(_vm.$L("刷新")))]
                    )
                  ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "w-nav-flex" })
        ])
      ]),
      _vm._v(" "),
      _c("w-content", [
        _c("div", { staticClass: "docs-main" }, [
          _c("div", { staticClass: "docs-body" }, [
            _c("div", { staticClass: "docs-menu" }, [
              _c("h3", [_vm._v(_vm._s(_vm.$L("我的知识库")))]),
              _vm._v(" "),
              _c(
                "ul",
                [
                  _vm._l(_vm.bookLists, function(book) {
                    return _c(
                      "li",
                      {
                        class: { active: book.id == _vm.selectBookData.id },
                        on: {
                          click: function($event) {
                            ;[
                              (_vm.selectBookData = book),
                              _vm.getSectionLists(true)
                            ]
                          }
                        }
                      },
                      [
                        _c("div", { staticClass: "docs-title" }, [
                          _vm._v(_vm._s(book.title))
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "docs-time" }, [
                          _vm._v(
                            _vm._s(
                              _vm.$A.formatDate("Y-m-d H:i:s", book.indate)
                            )
                          )
                        ])
                      ]
                    )
                  }),
                  _vm._v(" "),
                  _vm.bookHasMorePages
                    ? _c(
                        "li",
                        {
                          staticClass: "more",
                          on: { click: _vm.getBookLists }
                        },
                        [_vm._v(_vm._s(_vm.$L("加载下一页...")))]
                      )
                    : _vm.bookLoading
                    ? _c("li", { staticClass: "load" }, [_c("WLoading")], 1)
                    : _vm.bookLists.length == 0
                    ? _c("li", { staticClass: "none" }, [
                        _vm._v(_vm._s(_vm.bookNoDataText))
                      ])
                    : _vm._e()
                ],
                2
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "docs-container" }, [
              _vm.selectBookData.id > 0
                ? _c("div", { staticClass: "docs-box" }, [
                    _c("div", { staticClass: "docs-header" }, [
                      _c("div", { staticClass: "docs-h1" }, [
                        _vm._v(_vm._s(_vm.selectBookData.title))
                      ]),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "docs-setting" },
                        [
                          _c(
                            "Button",
                            {
                              on: {
                                click: function($event) {
                                  ;[
                                    (_vm.addSectionId = 0),
                                    (_vm.addSectionShow = true)
                                  ]
                                }
                              }
                            },
                            [_vm._v(_vm._s(_vm.$L("新增章节")))]
                          ),
                          _vm._v(" "),
                          _c(
                            "Button",
                            {
                              on: {
                                click: function($event) {
                                  ;[
                                    (_vm.addBookId = _vm.selectBookData.id),
                                    (_vm.addBookShow = true)
                                  ]
                                }
                              }
                            },
                            [_vm._v(_vm._s(_vm.$L("修改标题")))]
                          ),
                          _vm._v(" "),
                          _c("Button", { on: { click: _vm.showShare } }, [
                            _vm._v(_vm._s(_vm.$L("分享")))
                          ]),
                          _vm._v(" "),
                          _c(
                            "Button",
                            {
                              on: {
                                click: function($event) {
                                  ;[
                                    (_vm.settingDrawerShow = true),
                                    (_vm.settingDrawerTab = "setting")
                                  ]
                                }
                              }
                            },
                            [_vm._v(_vm._s(_vm.$L("设置")))]
                          ),
                          _vm._v(" "),
                          _c(
                            "Button",
                            {
                              attrs: { type: "warning", ghost: "" },
                              on: {
                                click: function($event) {
                                  return _vm.onBookDelete(_vm.selectBookData.id)
                                }
                              }
                            },
                            [_vm._v(_vm._s(_vm.$L("删除")))]
                          )
                        ],
                        1
                      )
                    ]),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "docs-section" },
                      [
                        _c("nested-draggable", {
                          attrs: {
                            lists: _vm.sectionLists,
                            disabled: _vm.sortDisabled
                          },
                          on: { change: _vm.handleSection }
                        }),
                        _vm._v(" "),
                        _vm.sectionLists.length == 0
                          ? _c("div", { staticClass: "none" }, [
                              _vm._v(_vm._s(_vm.sectionNoDataText))
                            ])
                          : _vm._e()
                      ],
                      1
                    )
                  ])
                : _vm._e()
            ])
          ])
        ])
      ]),
      _vm._v(" "),
      _c(
        "Modal",
        {
          attrs: {
            title: _vm.$L(_vm.addBookId > 0 ? "修改标题" : "新建知识库"),
            closable: false,
            "mask-closable": false,
            "class-name": "simple-modal"
          },
          model: {
            value: _vm.addBookShow,
            callback: function($$v) {
              _vm.addBookShow = $$v
            },
            expression: "addBookShow"
          }
        },
        [
          _c(
            "Form",
            {
              ref: "bookAdd",
              attrs: {
                model: _vm.formBookAdd,
                rules: _vm.ruleBookAdd,
                "label-width": 110
              },
              nativeOn: {
                submit: function($event) {
                  $event.preventDefault()
                }
              }
            },
            [
              _c(
                "FormItem",
                {
                  staticStyle: { "margin-right": "28px" },
                  attrs: { prop: "title", label: _vm.$L("知识库名称") }
                },
                [
                  _c("Input", {
                    attrs: { type: "text", maxlength: 32 },
                    model: {
                      value: _vm.formBookAdd.title,
                      callback: function($$v) {
                        _vm.$set(_vm.formBookAdd, "title", $$v)
                      },
                      expression: "formBookAdd.title"
                    }
                  })
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "div",
            { attrs: { slot: "footer" }, slot: "footer" },
            [
              _c(
                "Button",
                {
                  attrs: { type: "default" },
                  on: {
                    click: function($event) {
                      _vm.addBookShow = false
                    }
                  }
                },
                [_vm._v(_vm._s(_vm.$L("取消")))]
              ),
              _vm._v(" "),
              _c(
                "Button",
                {
                  attrs: { type: "primary", loading: _vm.loadIng > 0 },
                  on: { click: _vm.onBookAdd }
                },
                [_vm._v(_vm._s(_vm.$L(_vm.addBookId > 0 ? "提交" : "添加")))]
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "Modal",
        {
          attrs: {
            title: _vm.$L(_vm.addSectionId > 0 ? "修改文档标题" : "新建文档"),
            closable: false,
            "mask-closable": false,
            "class-name": "simple-modal"
          },
          model: {
            value: _vm.addSectionShow,
            callback: function($$v) {
              _vm.addSectionShow = $$v
            },
            expression: "addSectionShow"
          }
        },
        [
          _c(
            "Form",
            {
              ref: "sectionAdd",
              attrs: {
                model: _vm.formSectionAdd,
                rules: _vm.ruleSectionAdd,
                "label-width": 110
              },
              nativeOn: {
                submit: function($event) {
                  $event.preventDefault()
                }
              }
            },
            [
              _c(
                "FormItem",
                {
                  staticStyle: { "margin-right": "28px" },
                  attrs: { prop: "title", label: _vm.$L("文档标题") }
                },
                [
                  _c("Input", {
                    attrs: { type: "text", maxlength: 32 },
                    model: {
                      value: _vm.formSectionAdd.title,
                      callback: function($$v) {
                        _vm.$set(_vm.formSectionAdd, "title", $$v)
                      },
                      expression: "formSectionAdd.title"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _vm.addSectionId <= 0
                ? _c(
                    "FormItem",
                    {
                      staticStyle: { "margin-right": "28px" },
                      attrs: { prop: "type", label: _vm.$L("文档类型") }
                    },
                    [
                      _c(
                        "ButtonGroup",
                        _vm._l(_vm.sectionTypeLists, function(it, ik) {
                          return _c(
                            "Button",
                            {
                              key: ik,
                              attrs: {
                                type:
                                  "" +
                                  (_vm.formSectionAdd.type == it.value
                                    ? "primary"
                                    : "default")
                              },
                              on: {
                                click: function($event) {
                                  _vm.formSectionAdd.type = it.value
                                }
                              }
                            },
                            [_vm._v(_vm._s(it.text))]
                          )
                        }),
                        1
                      )
                    ],
                    1
                  )
                : _vm._e()
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "div",
            { attrs: { slot: "footer" }, slot: "footer" },
            [
              _c(
                "Button",
                {
                  attrs: { type: "default" },
                  on: {
                    click: function($event) {
                      _vm.addSectionShow = false
                    }
                  }
                },
                [_vm._v(_vm._s(_vm.$L("取消")))]
              ),
              _vm._v(" "),
              _c(
                "Button",
                {
                  attrs: { type: "primary", loading: _vm.loadIng > 0 },
                  on: { click: _vm.onSectionAdd }
                },
                [_vm._v(_vm._s(_vm.$L(_vm.addSectionId > 0 ? "提交" : "添加")))]
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "WDrawer",
        {
          attrs: { maxWidth: "750" },
          model: {
            value: _vm.settingDrawerShow,
            callback: function($$v) {
              _vm.settingDrawerShow = $$v
            },
            expression: "settingDrawerShow"
          }
        },
        [
          _vm.settingDrawerShow
            ? _c(
                "Tabs",
                {
                  model: {
                    value: _vm.settingDrawerTab,
                    callback: function($$v) {
                      _vm.settingDrawerTab = $$v
                    },
                    expression: "settingDrawerTab"
                  }
                },
                [
                  _c(
                    "TabPane",
                    { attrs: { label: _vm.$L("文档设置"), name: "setting" } },
                    [
                      _c("book-setting", {
                        attrs: {
                          canload:
                            _vm.settingDrawerShow &&
                            _vm.settingDrawerTab == "setting",
                          id: _vm.selectBookData.id
                        },
                        on: { "on-setting-callback": _vm.settingCallback }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "TabPane",
                    { attrs: { label: _vm.$L("文档成员"), name: "member" } },
                    [
                      _c("book-users", {
                        attrs: {
                          canload:
                            _vm.settingDrawerShow &&
                            _vm.settingDrawerTab == "member",
                          id: _vm.selectBookData.id
                        }
                      })
                    ],
                    1
                  )
                ],
                1
              )
            : _vm._e()
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-0172190c", module.exports)
  }
}

/***/ })

});